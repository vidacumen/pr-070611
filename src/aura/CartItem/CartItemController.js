({
	openCartItemDetailModal: function(component, event, helper) {
		component.set("v.showCartItemDetailModal", "true");
	},

	deleteCartItem : function(component, event, helper) {
		var cartItemId = component.get("v.cartItem.Id");
		var cartId = component.get("v.cartItem.ccrz__Cart__c");
		var encryptedId = component.get("v.cartItem.ccrz__Cart__r.ccrz__EncryptedId__c");
		console.log('cartitem id: ' + cartItemId + 'cartId: ' + cartId);

		var action = component.get("c.deleteCartItemAndRegistration");
		action.setParams({
			cartItemId : cartItemId,
			cartId : cartId,
			encryptedId : encryptedId
		});
		
		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			if (response.getState() !== "SUCCESS") {
				var errors = response.getError();
				console.error(errors);
				helper.showErrorToast(component, errors);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);

		var event = component.getEvent("deleteCartItem");
		event.setParams({
			'selectedCartItem': component.get("v.cartItem"), "index": component.get("v.index")
		});
		event.fire();
	}, 

	openEditContact : function(component, event, helper){
		var openEdit = event.getParam("editContact");
		var closeDetails = event.getParam("productDetail");
		component.set("v.showCartItemDetailModal",closeDetails);
		component.set("v.showEditContactModal", openEdit);

	}
})