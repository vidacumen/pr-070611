({
	closeCartItemDetailModal: function(component, event, helper) {
		component.set("v.showCartItemDetailModal", "false");
	},
	
	openEditContactModal: function(component, event, helper) {
		var event = component.getEvent("openEditContactModal");
		event.setParams({
			'editContact': true, "productDetail": false
		});
		event.fire();
	}
})