({
	// createCaseForRegistration : function(component, event, helper) {
 //        var createRecordEvent = $A.get("e.force:createRecord");

 //        createRecordEvent.setParams({
 //            "entityApiName": "Case",
 //        	"defaultFieldValues": {
 //        		'Registration__c' : component.get("v.recordId"),
 //        		'Status' : 'New',
 //        		'Origin' : 'Community',
 //        		'Reason' : 'New Issue',
 //        		'Priority' : 'Medium',
 //        		'Category__c' : 'General Inquiry',
 //        		'Sub_Category__c' : null,
 //        		'System__c' : null,
 //        		'Program__c' : 'Other',
 //            	'Delivery_Type__c' : 'Other'
 //        	}
 //        });

 //        createRecordEvent.fire();
	// },

	// getCaseStatusValues : function(component, event, helper) {
	// 	var caseAction = component.get("c.queryForCaseStatusValues");

	// 	caseAction.setCallback(this, function(response){
	// 		if (response.getState() == "SUCCESS") {
	// 			component.set("v.caseStatusValues", response.getReturnValue());
	// 		}
	// 	});

	// 	$A.enqueueAction(caseAction);
	// },

	// getCasePriorityValues : function(component, event, helper) {
	// 	var caseAction = component.get("c.queryForCasePriorityValues");

	// 	caseAction.setCallback(this, function(response){
	// 		if (response.getState() == "SUCCESS") {
	// 			component.set("v.casePriorityValues", response.getReturnValue());
	// 		}
	// 	});

	// 	$A.enqueueAction(caseAction);
	// },

	createCase : function(component, event, helper) {
		var recordId = component.get("v.recordId");
		var caseSubject = component.find("caseSubject").get("v.value");
		var caseDescription = component.find("caseDescription").get("v.value");
		// var caseStatus = component.get("v.statusBoundValue");
		// var casePriority = component.get("v.priorityBoundValue");

		if (!caseSubject) {
			component.set("v.recordError", "Subject is required.");
			component.set("v.showErrorMessage", "true");
		} else if (!caseDescription) {
			component.set("v.recordError", "Description is required.");
			component.set("v.showErrorMessage", "true");
		} else {
			component.set("v.recordError", "");
			component.set("v.showErrorMessage", "false");
			var createCaseAction = component.get("c.createCaseForRegistration");

			createCaseAction.setParams({
				registrationId : recordId,
				subject : caseSubject,
				description : caseDescription
			});

			createCaseAction.setCallback(this, function(response){
				if (response.getState() == "SUCCESS") {
					console.log(response.getReturnValue());
					component.set("v.showCreateCaseModal", "false");

					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
					    title: "Success!",
					    message: "Case was created and we'll get back to you soon. Case Number: " + response.getReturnValue(),
					    type: "success",
					    mode: "sticky"
					});
					toastEvent.fire();
				} else {
					component.set("v.showError", "true");
					component.set("v.recordError", "Error creating Case");
					console.log(response.getReturnValue());
				}
			});

			$A.enqueueAction(createCaseAction);
		}
	}
})