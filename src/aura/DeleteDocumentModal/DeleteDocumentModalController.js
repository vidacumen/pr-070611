({
	handleDelete : function(component, event, helper) {
		helper.deleteDocument(component, event, helper);
	},

	handleClose : function(component, event, helper) {
		helper.closeDeleteModal(component, event, helper);
	}
})