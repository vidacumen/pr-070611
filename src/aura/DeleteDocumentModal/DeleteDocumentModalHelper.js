({
	deleteDocument : function(component, event, helper) {
		var queryDocumentListAction = component.get("c.deleteDocumentAndFilesById");
		var documentId = component.get("v.documentId");
		queryDocumentListAction.setParams({
			documentId : documentId
		});

		queryDocumentListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.showDeleteModal", "false");
				$A.get("e.force:refreshView").fire();
				//call refresh on parent comp
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error deleting documents: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryDocumentListAction);
	},

	closeDeleteModal : function(component, event, helper) {
		component.set("v.showDeleteModal", "false");
	}
})