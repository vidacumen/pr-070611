({
	handleClose : function(component, event, helper) {
		helper.closeDeleteModal(component, event, helper);
	},

	handleDelete : function(component, event, helper) {
		helper.deleteFile(component, event, helper);
	}
})