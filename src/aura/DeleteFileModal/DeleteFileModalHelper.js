({
	closeDeleteModal : function(component, event, helper) {
		component.set("v.showDeleteModal", "false");
	},

	deleteFile : function(component, event, helper) {
		var fileAction = component.get("c.deleteFileById");
		var fileId = component.get("v.fileId");
		fileAction.setParams({
			fileId : fileId
		});

		fileAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.showDeleteModal", "false");
				$A.get("e.force:refreshView").fire();
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading documents: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(fileAction);
	}
})