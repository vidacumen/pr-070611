({
	handleClose : function(component, event, helper) {
		helper.closeDocumentModal(component, event, helper);
	},

	handleSave : function(component, event, helper) {
		helper.createNewDocument(component, event, helper);
	}
})