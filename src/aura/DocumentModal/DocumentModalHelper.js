({
	closeDocumentModal : function(component, event, helper) {
		component.set("v.showDocumentModal", "false");
	},

	createNewDocument : function(component, event, helper) {
		var registrationId = component.get("v.registrationId");
		var documentName = component.find("docName").get("v.value");
		var documentType = component.get("v.typeBoundValue");

		if(!documentName) {
			component.set("v.errorMessage", "Document Name is required.");
			component.set("v.showErrorMessage", "true");
		} else if (!documentType) {
			component.set("v.errorMessage", "Document Type is required.");
			component.set("v.showErrorMessage", "true");
		} else {
			var queryDocumentListAction = component.get("c.createNewDocumentRecord");
			// var registrationId = component.get("v.registrationId");
			// var documentName = component.find("docName").get("v.value");
			// var documentType = component.get("v.typeBoundValue");
			queryDocumentListAction.setParams({
				registrationId : registrationId,
				documentName : documentName,
				documentType : documentType
			});

			queryDocumentListAction.setCallback(this, function(response){
				if (response.getState() == "SUCCESS") {
					component.set("v.showDocumentModal", "false");
					$A.get("e.force:refreshView").fire();
				} else {
					console.log(response.getReturnValue());
				}
			});

			$A.enqueueAction(queryDocumentListAction);
		}
	}
})