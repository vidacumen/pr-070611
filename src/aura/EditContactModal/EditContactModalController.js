({
	doInit: function(component, event, helper) {
		helper.getAccountInfo(component);
		helper.getFormMetadata(component);
	},

	closeEditContactModal: function(component, event, helper) {
		component.set("v.showEditContactModal", "false");
	}, 

	nextTab : function(component, event, helper) {
		helper.checkIfAddressIsBlank(component);
		helper.checkIfCountryIsBlank(component);
		var streetAddressValue = component.find("address").get("v.value");
		var countryValue = component.find("country").get("v.value");

		if(streetAddressValue && countryValue){
			var tab1 = component.find('contactInfo');
			var TabOneData = component.find('contactInfoLayout');

			var tab2 = component.find('t-and-c');
			var TabTwoData = component.find('tcLayout');
			var tab2Style = component.find('t-and-c-style');

			//show and Active terms and conditions tab
			$A.util.addClass(TabTwoData, 'slds-show');
			$A.util.removeClass(TabTwoData, 'slds-hide');
			var element = document.getElementById('tab-scoped-2__item');
			element.style.backgroundColor = "#ffffff";
			element.style.color = "#000000";

			// Hide and deactivate contact info
			$A.util.removeClass(TabOneData, 'slds-show');
			$A.util.addClass(TabOneData, 'slds-hide');
			var element = document.getElementById('tab-scoped-1__item');
			element.style.backgroundColor = "#3c5a76";
			element.style.color = "#ffffff";
			
		}
	},
	
	previousTab : function(component, event, helper) {
		//activate contact info tab
		var tab1 = component.find('contactInfo');
		var TabOneData = component.find('contactInfoLayout');

		var tab2 = component.find('t-and-c');
		var TabTwoData = component.find('tcLayout');

		$A.util.addClass(TabOneData, 'slds-show');
		$A.util.removeClass(TabOneData, 'slds-hide');
		var element = document.getElementById('tab-scoped-1__item');
		element.style.backgroundColor = "#ffffff";
		element.style.color = "#000000";

		// Hide and deactivate terms and conditions tab
		$A.util.removeClass(tab2, 'slds-active');
		$A.util.removeClass(TabTwoData, 'slds-show');
		$A.util.addClass(TabTwoData, 'slds-hide');
		var element = document.getElementById('tab-scoped-2__item');
		element.style.backgroundColor = "#3c5a76";
		element.style.color = "#ffffff";

	}, 

	checkIfAddressIsBlank : function(component, event, helper) {
		helper.checkIfAddressIsBlank(component);
	}, 

	checkIfCountryIsBlank : function(component, event, helper) {
		helper.checkIfCountryIsBlank(component);
	}, 


	updateRegistration : function(component, event, helper) {
		var personAcctId = component.get("v.personAccount.Id");
		var cartId = component.get("v.cartItem.ccrz__Cart__c");
		var itemId = component.get("v.cartItem.Id");
		var firstName = component.get("v.personAccount.FirstName");
		var middleName = component.get("v.personAccount.MiddleName");
		var lastName = component.get("v.personAccount.LastName");
		var suffix = component.get("v.personAccount.Suffix");
		var streetAddress = component.find("address").get("v.value");
		var city = component.find("city").get("v.value");
		var state = component.find("state").get("v.value");
		var zipcode = component.find("zipCode").get("v.value");
		var country = component.find("country").get("v.value");
		var email = component.find("email").get("v.value");
		var phone = component.find("phone").get("v.value");

		var action = component.get("c.updateRegistrationRecord");
		action.setParams({
			personAcctId : personAcctId,  
			cartId : cartId,
			itemId : itemId,
			firstName : firstName,
			middleName : middleName, 
			lastName : lastName,
			suffix : suffix, 
			streetAddress : streetAddress,
			city : city,
			state : state,
			zipcode : zipcode,
			country : country,
			email : email, 
			phone : phone
		});
		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			if (response.getState() !== "SUCCESS") {
				var errors = response.getError();
				console.error(errors);
				helper.showErrorToast(component, errors);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);

		component.set("v.showEditContactModal", "false");
	}

})