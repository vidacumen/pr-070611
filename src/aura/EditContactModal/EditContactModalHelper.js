({
	getAccountInfo: function(component) {
		// Create the action
		var action = component.get("c.getAccount");
		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var msg = response.getReturnValue();
				component.set("v.personAccount", response.getReturnValue());
				console.log("accountId: " + component.get("v.personAccount"));
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
				//helper.showErrorToast(component, errors);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);
	},

	getFormMetadata: function(component) {
		var registrationForm = component.get("v.cartItem.ccrz__Product__r.Registration_Form__c");

		var action = component.get("c.findFormByItemIdAttr");
		action.setParams({
			registrationForm : registrationForm
		});
		console.log('reg form: ' + registrationForm);
		// Add callback behavior for when response is received
		console.log('in edit contact modal init');
		action.setCallback(this, function(response) {
			console.log('state in edit contact modal: ' + response.getState());
			if (response.getState() == "SUCCESS") {
				component.set("v.form", response.getReturnValue());
				console.log('form: ' + component.get("v.form"));
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
				helper.showErrorToast(component, errors);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);
	},

	checkIfAddressIsBlank : function(component) {
		var streetAddressInput = component.find("address");
		var streetAddressValue = streetAddressInput.get("v.value");
		streetAddressInput.set("v.errors", null);

		if(!(streetAddressValue)){
			streetAddressInput.set("v.errors", [{message:"Street Address is required."}]);
		}
	}, 

	checkIfCountryIsBlank : function(component) {
		var countryInput = component.find("country");
		var countryValue = countryInput.get("v.value");
		countryInput.set("v.errors", null);

		if(!(countryValue)) {
			countryInput.set("v.errors", [{message:"Country is required."}]);
	 	}
	}, 

	showSuccessToast : function(component) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: "Success",
			"title": "Success!",
			"message": "Item was successfully deleted."
		});
		toastEvent.fire();
	},

	showErrorToast : function(component, errors) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: "Error",
			"title": "Error!",
			"message": errors
		});
		toastEvent.fire();
	}

})