({
	handleUpdate: function(component, event, helper) {
		var eventParams = event.getParams();

		if(eventParams.changeType === "LOADED") {
			helper.getRegistrationStatus(component, event, helper);
		} else if (eventParams.changeType === "CHANGED") {
			// alert('changed');
			// helper.refreshPage(component, event, helper);
		} else if(eventParams.changeType === "REMOVED") {
			// record is deleted
		} else if(eventParams.changeType === "ERROR") {
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
			    title: "Error",
			    message: "There was a problem getting the most up to date information for your registration. Please check back later or contact support if this error persists.",
			    type: "warning",
			    mode: "sticky"
			});
			toastEvent.fire();
		}
		// component.retrieveStatus();
	}
})