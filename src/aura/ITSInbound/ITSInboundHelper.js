({
	getRegistrationStatus : function(component, event, helper) {
		var registrationRecord = component.get("v.registrationRecordFields");
		
		if (registrationRecord.Delivery_Modality_Type__c == "ITS")  {
			if ((registrationRecord.Registration_Status__c == "Assessment Available") || (registrationRecord.Registration_Status__c == "Assessment In Progress")) {
				var action = component.get("c.queryStatus");
				action.setParams({
					registrationRecord : registrationRecord
				});

				action.setCallback(this, function(response) {
					console.log('testing ' + response.getReturnValue());
					if (response.getState() == "SUCCESS") {                
						if (response.getReturnValue() == "STATUS UPDATED") {
							$A.get('e.force:refreshView').fire();
						}
						if (response.getReturnValue() == "") {
							var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
							    title: "Error",
							    message: "There was a problem getting the most up to date information for your registration. Please check back later or contact support if this error persists.",
							    type: "warning",
							    mode: "sticky"
							});
							toastEvent.fire();
						}
					} else if (response.getState() == "ERROR") {
						var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
						    title: "Error",
						    message: "There was a problem getting the most up to date information for your registration. Please check back later or contact support if this error persists.",
						    type: "warning",
						    mode: "sticky"
						});
						toastEvent.fire();
					}

					component.set("v.showSpinner", "false");
				});

				$A.enqueueAction(action);
				component.set("v.showSpinner", "true");
			}
		}
	}
})