({
	handleClick: function(component, event, helper) {
        var modalStatus = component.get('v.showMedicalSchoolModal');
        if(modalStatus == 'true'){
            helper.closeMedicalSchoolModal(component, event, helper);
        } else {
            helper.openMedicalSchoolModal(component, event, helper);
        }
    },

    handleSearch: function(component, event, helper) {
    	helper.searchForMedicalSchools(component, event, helper);
    },

    onKeyUp: function(component, event, helpler) {
        //checks for "enter" key
        if (event.getParam('keyCode')===13) {
            helpler.searchForMedicalSchools(component, event, helpler);
        }
    },

    selectSchool: function(component, event, helper) {
    	helper.setMedicalSchool(component, event, helper);
    },

    init: function(component, event, helper) {
    	helper.getStatePicklistValues(component, event, helper);
    },

    handleCountryUpdate: function(component, event, helper) {
    	helper.getStatePicklistValues(component, event, helper);
    }
})