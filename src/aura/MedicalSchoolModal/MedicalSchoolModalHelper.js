({
	openMedicalSchoolModal: function(component, event, helper) {
        component.set("v.showMedicalSchoolModal", "true");
    },

    closeMedicalSchoolModal: function(component, event, helper) {
        component.set("v.selectedMedicalSchool", "");
        component.set("v.showMedicalSchoolModal", "false");
    },

    searchForMedicalSchools: function(component, event, helper) {
    	var action = component.get("c.returnMatchingMedicalSchools");
    	var medSchoolName = component.find("medicalschool").get("v.value");
    	var medSchoolCountry = component.find("country").get("v.value");
    	var medSchoolState = component.find("state").get("v.value");
    	action.setParams({
    		medSchoolName : medSchoolName,
    		medSchoolCountry : medSchoolCountry,
    		medSchoolState : medSchoolState
    	});

        action.setCallback(this, function(response){
            if (response.getState() == "SUCCESS") {
            	component.set("v.medSchoolSearchResults", response.getReturnValue());
            	if (response.getReturnValue().length > 0) {
            		component.set("v.noMedSchoolResultsAlert", "");
                } else {
            		component.set("v.noMedSchoolResultsAlert", "We couldn't find the Medical School you are looking for.");
                }
            }
        });

        $A.enqueueAction(action);
    },

    setMedicalSchool: function(component, event, helper) {
    	var medSchool = event.target.id;
    	component.set("v.selectedMedicalSchool", medSchool);
    	component.set("v.showMedicalSchoolModal", "false");
    },

    getStatePicklistValues: function(component, event, helper) {
    	var action = component.get("c.returnStatePicklistValues");
    	var medSchoolCountry = component.find("country").get("v.value");
    	action.setParams({
    		medSchoolCountry : medSchoolCountry
    	});

        action.setCallback(this, function(response){
            if (response.getState() == "SUCCESS") {
                component.set("v.statePicklistValues", response.getReturnValue());
            }
        });

        $A.enqueueAction(action);
    }
})