({
	init : function(component, event, helper) {
		helper.getData(component);
	},

	launchMergeBatch : function(component, event, helper) {
		var action = component.get("c.launchBatch");
		var data = component.get("v.mydata");
		console.log(JSON.stringify(data));
		action.setParams({
			"dupeList": data
		});
		action.setCallback(this, (function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var msg = response.getReturnValue();
				if (msg == "SUCCESS") {
					//helper.showSuccessToast(component);
					alert('Batch has been successfully started.')
				} else {
					//helper.showErrorToast(component, msg);
					alert(msg);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
				alert(errors);
				//helper.showErrorToast(component, errors);
			}
		}));
		$A.enqueueAction(action);
	},

	launchPriorityMergeBatch : function(component, event, helper) {
		var action = component.get("c.launchPriorityBatch");
		action.setParams({
			"dupeList": component.get("v.mydata")
		});
		action.setCallback(this, (function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var msg = response.getReturnValue();
				if (msg == "SUCCESS") {
					//helper.showSuccessToast(component);
					alert('Batch has been successfully started.')
				} else {
					//helper.showErrorToast(component, msg);
					alert(msg);
				}

			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
				helper.showErrorToast(component, errors);
			}
		}));
		$A.enqueueAction(action);
	}
})