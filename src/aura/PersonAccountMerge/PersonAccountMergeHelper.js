({
	getData : function(component) {
		var action = component.get("c.queryDuplicatePersonAccounts");
		console.log(JSON.stringify(action));
		action.setCallback(this, $A.getCallback(function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.mydata', response.getReturnValue());
				console.log(component.get("v.mydata[0].DSE__DS_Duplicate__r.Name"));
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
			}
		}));
		$A.enqueueAction(action);

	},

	showSuccessToast : function(component) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: "Success",
			"title": "Success!",
			"message": "Batch has been successfully started."
		});
		toastEvent.fire();
	},

	showErrorToast : function(component, errors) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: "Error",
			"title": "Error!",
			"message": errors
		});
		toastEvent.fire();
	}
})