({
	searchForProducts : function(component, event, helper) {
		var searchBarReference = component.find("searchText");
		var inputValue = searchBarReference.get("v.value");

		if (!$A.util.isEmpty(inputValue)){
			var navigateToURL = component.get("c.getCloudCrazeProductSearchURL");
			navigateToURL.setParams ({
				inputValue : inputValue
			});

			navigateToURL.setCallback(this, function(response){
				if (response.getState() == "SUCCESS") {
					console.log(response.getReturnValue());
					window.location = response.getReturnValue();
				} else if (response.getState() === "ERROR") {
					var errors = response.getError();
					console.log("Error navigating to URL: ");
					console.log(errors);
				}
			});

			$A.enqueueAction(navigateToURL);
		}
	}
})