({
	init : function (component, event, helper) {
		helper.getProductCategories(component, event, helper);
	},

	handleClick : function(component, event, helper) {
		helper.searchForProducts(component, event, helper);
	}
})