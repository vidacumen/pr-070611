({
	getProductCategories : function(component, event, helper) {
		var queryCCListAction = component.get("c.returnCCProductCategoriesForStorefront");

		queryCCListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				console.log(response.getReturnValue());
				component.set("v.ProductCategoryList", response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading product categories: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryCCListAction);
	},

	searchForProducts : function(component, event, helper) {
		var categoryId = event.target.id;

		var navigateToURL = component.get("c.getCloudCrazeProductSearchURL");
		navigateToURL.setParams ({
			categoryId : categoryId
		});

		navigateToURL.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				console.log(response.getReturnValue());
				window.location = response.getReturnValue();
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error navigating to URL: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(navigateToURL);
	}
})