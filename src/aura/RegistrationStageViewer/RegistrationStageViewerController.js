({
	init : function(component, event, helper) {
		helper.getRegistrationViewerContent(component, event, helper);
		helper.getCurrentStageNum(component, event, helper);
		helper.getModalityType(component, event, helper);
		helper.getButtonLabel(component, event, helper);
		helper.getDownloadLabel(component, event, helper);
	},

	launchExam: function(component, event, helper) {
		if ('WED' == component.get("v.modalityType")) {
			window.open('/ExamLauncher' + '?actionType=Launcher1&registrationId=' + component.get("v.registrationRecordId"), 'mywin', 'height='+screen.height+', width='+screen.width+', fullscreen=yes');
		} else if ('ITS' == component.get("v.modalityType")) {
			window.open('/ExamLauncher' + '?actionType=Launcher2&registrationId=' + component.get("v.registrationRecordId"), 'mywin', 'height='+screen.height+', width='+screen.width+', fullscreen=yes');
		}
	},

	downloadScoreReport: function(component, event, helper) {
		helper.getScoreReportLink(component, event, helper);
	},

	handleRecordUpdated: function(component, event, helper) {
		var eventParams = event.getParams();
		//alert('handleRecordUpdated');
		if(eventParams.changeType === "LOADED") {
			// record is loaded (render other component which needs record data value)
			console.log("Record is loaded successfully.");
		} else if(eventParams.changeType === "CHANGED") {
			// record is changed
		} else if(eventParams.changeType === "REMOVED") {
			// record is deleted
		} else if(eventParams.changeType === "ERROR") {
			// there’s an error while loading, saving, or deleting the record
		}
	}
})