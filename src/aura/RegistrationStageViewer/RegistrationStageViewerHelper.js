({
	getRegistrationViewerContent : function(component, event, helper) {
		var queryRegistrationListAction = component.get("c.returnRegistrationViewerContentForTheRegistrationRecord");
		var recordId = component.get("v.registrationRecordId");
		queryRegistrationListAction.setParams({
			registrationId:recordId
		});

		queryRegistrationListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.registrationViewerContent", response.getReturnValue());
				console.log(response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading registration viewer content: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryRegistrationListAction);
	},

	getCurrentStageNum : function(component, event, helper) {
		var queryRegistrationListAction = component.get("c.returnCurrentStageNum");
		var recordId = component.get("v.registrationRecordId");
		queryRegistrationListAction.setParams({
			registrationId : recordId
		});

		queryRegistrationListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.currentStageNum", response.getReturnValue());
				console.log(response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading registration stage number: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryRegistrationListAction);
	},

	getModalityType : function(component, event, helper) {
		var action = component.get("c.queryRegistrationModalityType");
		var recordId = component.get("v.registrationRecordId");
		action.setParams({
			registrationId : recordId
		});

		action.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.modalityType", response.getReturnValue());
				console.log("modalityType: " + response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error getting modality type: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(action);
	},

	getScoreReportLink : function(component, event, helper) {
		var action = component.get("c.queryDownloadLink");
		var recordId = component.get("v.registrationRecordId");
		action.setParams({
			registrationId : recordId
		});

		action.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				window.open(response.getReturnValue(), '_blank');
				console.log("modalityType: " + response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error getting download link: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(action);
	},

	getButtonLabel : function(component, event, helper) {
		var action = component.get("c.setButtonLabelByRegistrationStatus");
		var recordId = component.get("v.registrationRecordId");
		action.setParams({
			registrationId : recordId
		});

		action.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				if (response.getReturnValue() != "No Button") {
					component.set("v.buttonLabel", response.getReturnValue());
					component.set("v.showButton", "true");
					console.log("modalityType: " + response.getReturnValue());
				}
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error getting button label: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(action);
	},

	getDownloadLabel : function(component, event, helper) {
		var action = component.get("c.setButtonDownloadLabelByRegistrationStatus");
		var recordId = component.get("v.registrationRecordId");
		action.setParams({
			registrationId : recordId
		});

		action.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				if (response.getReturnValue() != "No Button") {
					component.set("v.downloadLabel", response.getReturnValue());
					component.set("v.showDownloadButton", "true");
					console.log("modalityType: " + response.getReturnValue());
				}
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error getting button label: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(action);
	}
})