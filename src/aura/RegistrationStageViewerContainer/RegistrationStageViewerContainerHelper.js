({
	getRegistrationStages : function(component, event, helper) {
		var queryRegistrationListAction = component.get("c.queryForRegistrationStagesByRegistrationId");
		var recordId = component.get("v.recordId");
		queryRegistrationListAction.setParams({
			registrationId:recordId
		});

		queryRegistrationListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.registrationStageList", response.getReturnValue());
				console.log(response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading registration stages: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryRegistrationListAction);
	}
})