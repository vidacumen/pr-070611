({
	toggleShowHide : function(component, event, helper) {
		helper.expandFilesForRelatedDocuments(component,event,'relatedFilesForDocument');
	},

	clickDelete : function(component, event, helper) {
		helper.openModalToDeleteDocument(component, event, helper);
	},

	init : function(component, event, helper) {
		helper.showDeleteButton(component, event, helper);
	}
})