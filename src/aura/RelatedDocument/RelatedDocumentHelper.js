({
	expandFilesForRelatedDocuments : function(component, event, helper) {
		var acc = component.find(helper);
			for(var cmp in acc) {
			$A.util.toggleClass(acc[cmp], 'slds-show');  
			$A.util.toggleClass(acc[cmp], 'slds-hide');  
		}
	},

	openModalToDeleteDocument : function(component, event, helper) {
		component.set("v.showDeleteModal", "true");
	},

	showDeleteButton : function(component, event, helper) {
		var showButton = component.get("v.document.Document_Type__c");

		if (showButton != 'Score Report') {
			component.set("v.showDeleteButton", "true");
		}
	}
})