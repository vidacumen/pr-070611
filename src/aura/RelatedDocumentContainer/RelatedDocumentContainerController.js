({
	init : function(component, event, helper) {
		helper.loadDocuments(component, event, helper);
		helper.loadTypes(component, event, helper);
		helper.getDocumentTypeValues(component, event, helper);
	},

	createNewDocument : function(component, event, helper) {
		helper.createNewDocument(component, event, helper);
	},

	handleForceRefreshView : function(component, event, helper) {
		helper.loadDocuments(component, event, helper);
		helper.loadTypes(component, event, helper);
	}
})