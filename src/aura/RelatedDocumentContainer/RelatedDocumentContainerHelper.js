({
	loadDocuments : function(component, event, helper) {
		var queryDocumentListAction = component.get("c.queryForDocumentsByRegistrationId");
		var recordId = component.get("v.recordId");
		queryDocumentListAction.setParams({
			registrationId : recordId
		});

		queryDocumentListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.documentList", response.getReturnValue());
				console.log(response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading documents: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryDocumentListAction);
	},

	loadTypes : function(component, event, helper) {
		var queryDocumentListAction = component.get("c.queryForDocumentTypesByRegistrationId");
		var recordId = component.get("v.recordId");
		queryDocumentListAction.setParams({
			registrationId : recordId
		});

		queryDocumentListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.documentTypes", response.getReturnValue());
				console.log(response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading documents: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryDocumentListAction);
	},

	createNewDocument : function(component, event, helper) {
		// var createRecordEvent = $A.get("e.force:createRecord");
		// var recordId = component.get("v.recordId");
  //   	createRecordEvent.setParams({
  //       	"entityApiName": "CustomDocument__c",
  //       	"defaultFieldValues": {
  //       		'Registration_ID__c' : recordId
  //       	}
  //   	});

  //   	createRecordEvent.fire();
  		component.set("v.showDocumentModal", "true");
	},

	getDocumentTypeValues : function(component, event, helper) {
		var queryDocumentListAction = component.get("c.queryForDocumentTypeValues");

		queryDocumentListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.documentTypeList", response.getReturnValue());
			}
		});

		$A.enqueueAction(queryDocumentListAction);
	}
})