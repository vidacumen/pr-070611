({
	clickDelete : function(component, event, helper) {
		helper.openModalToDeleteFile(component, event, helper);
	},

	clickDownload : function(component, event, helper) {
		helper.handleDownloadFile(component, event, helper);
	}
})