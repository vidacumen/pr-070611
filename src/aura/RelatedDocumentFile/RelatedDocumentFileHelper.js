({
	openModalToDeleteFile : function(component, event, helper) {
		component.set("v.showDeleteModal", "true");
	},

	handleDownloadFile : function(component, event, helper) {
		var Id = component.get("v.file.ContentDocumentId");
	    var actiondownload = component.get("c.downloadFile");

	    actiondownload.setParams({
	        fileId : Id
	    });

		actiondownload.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
	        	component.set("v.Baseurl", response.getReturnValue());
	        	var urlEvent = $A.get("e.force:navigateToURL");
			    urlEvent.setParams({
					"url": response.getReturnValue()
			    });
			    urlEvent.fire();
	        }
		});

		$A.enqueueAction(actiondownload);
	}
})