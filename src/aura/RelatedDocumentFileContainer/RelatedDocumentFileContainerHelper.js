({
	loadFiles : function(component, event, helper) {
		var queryFileListAction = component.get("c.queryForFilesByDocumentId");
		var documentId = component.get("v.documentId");
		queryFileListAction.setParams({
			DocumentId:documentId
		});

		queryFileListAction.setCallback(this, function(response){
			if (response.getState() == "SUCCESS") {
				component.set("v.fileList", response.getReturnValue());
				console.log(response.getReturnValue());
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				console.log("Error loading files: ");
				console.log(errors);
			}
		});

		$A.enqueueAction(queryFileListAction);
	}
})