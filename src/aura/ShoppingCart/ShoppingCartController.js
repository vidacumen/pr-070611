({
	// Load cart items from Salesforce
	doInit: function(component, event, helper) {
		// Create the action
		var action = component.get("c.getCartItems");
		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var msg = response.getReturnValue();
				component.set("v.cartItems", response.getReturnValue());
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
				helper.showErrorToast(component, errors);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);

		var action = component.get("c.getCart");
		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var msg = response.getReturnValue();
				component.set("v.cart", response.getReturnValue());
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.error(errors);
				helper.showErrorToast(component, errors);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);
	},

	removeCartItem : function(component, event, helper){
		var selCartItem = event.getParam("selectedCartItem");
		var cartItems = component.get("v.cartItems")
		var index = event.getParam("index");
		console.log('index: ' + index);
		if (index > -1) {
			cartItems.splice(index, 1);
		}
		component.set("v.cartItems",cartItems);
		$A.get("e.force:refreshView").fire();
	},

	goToCheckout: function(component, event, helper) {
		console.log('encryptedId: ' + component.get("v.cart.ccrz__EncryptedId__c"));
		window.open('/DefaultStore/ccrz__Cart?cartID=' + component.get("v.cart[0].ccrz__EncryptedId__c") + '&isCSRFlow=true&portalUser=&store=&cclcl=en_US', '_self');
	}
})