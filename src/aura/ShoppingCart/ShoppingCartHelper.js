({
	showSuccessToast : function(component) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: "Success",
			"title": "Success!",
			"message": "Item was successfully deleted."
		});
		toastEvent.fire();
	},

	showErrorToast : function(component, errors) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: "Error",
			"title": "Error!",
			"message": errors
		});
		toastEvent.fire();
	}
})