({
//    refreshRecord : function(component, event, helper) {       
//        $A.get('e.force:refreshView').fire();
//    },
    editRecord : function(component, event, helper) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.recordId")
        });
        editRecordEvent.fire();    
    },
    
    doInit : function(component, event, helper) {
        //This function will call the SuperController's getUser to find the user sObject. Then it checks if the current page's record (thisRecord) is the same as the user's Account and sets isMyAccount and headerTitle appropriately. - AB
        //alert('New Controller init called');
        var action = component.get("c.getUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.user", response.getReturnValue());
                //alert('User: ' + component.get("v.user"));
                var pageAcct = component.get("v.thisRecord");
                var userAcct = component.get("v.user.Account_ID_Text__c");
                //alert('Setting isMyAcct');
                //alert(pageAcct + '-' + userAcct);
                if (pageAcct == userAcct) {
                    component.set("v.isMyAcct", "true");
                    component.set("v.headerTitle", "Biographic Profile");
                } else {
                    component.set("v.isMyAcct", "false");
                    component.set("v.headerTitle", "Account Bio");   
                };
                                
            } else {
                console.log('There was an error');
            }
        });
        $A.enqueueAction(action);
        
    }
    
    
})