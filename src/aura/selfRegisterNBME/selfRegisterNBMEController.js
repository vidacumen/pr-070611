({
    initialize: function(component, event, helper) {
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap2}).fire();        
        component.set('v.extraFields', helper.getExtraFields(component, event, helper));
        component.set('v.countryPicklistValues', helper.getCountryPicklistValues(component, event, helper));
    },
    
    handleSelfRegister: function (component, event, helpler) {
        helpler.handleSelfRegister(component, event, helpler);
    },
    
    setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if(startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
    
    setExpId: function (component, event, helper) {
        var expId = event.getParam('expid');
        if (expId) {
            component.set("v.expid", expId);
        }
        helper.setBrandingCookie(component, event, helper);
    },

    handleClick: function(component, event, helper) {
        var modalStatus = component.get('v.showMedicalSchoolModal');
        if(modalStatus == 'true'){
            helper.closeMedicalSchoolModal(component, event, helper);
        } else {
            helper.openMedicalSchoolModal(component, event, helper);
        }
    },

    handleClear: function(component, event, helper) {
        helper.clearMedicalSchoolField(component, event, helper);
    },

    handleRedirect: function(component, event, helper) {
        helper.redirectUserToLoginPage(component, event, helper);
    },

    showSpinnerComp : function (component, event, helper) {
        component.set("v.showSpinner", "true");
    },

    hideSpinnerComp : function (component, event, helper) {
        component.set("v.showSpinner", "false");
    }   
})