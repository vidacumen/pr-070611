({
    qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    },
    
    qsToEventMap2: {
        'expid'  : 'e.c:setExpId'
    },
        
    handleSelfRegister: function (component, event, helpler) {
        var accountId = component.get("v.accountId");
        var regConfirmUrl = component.get("v.regConfirmUrl");
        var firstname = component.find("firstname").get("v.value");
        var middlename = component.find("middlename").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var email = component.find("email").get("v.value");
        var usmleid = component.find("usmleid").get("v.value");
        var aamcid = component.find("aamcid").get("v.value");
        var nbmeid = component.find("nbmeid").get("v.value");
        var birthdate = component.find("birthdate").get("v.value");
        var phone = component.find("phone").get("v.value");
        var medicalschool = component.find("medicalschool").get("v.value");
        var gradyear = component.find("gradyear").get("v.value");
        var includePassword = component.get("v.includePasswordField");
        var password = component.find("password").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");
        var action = component.get("c.selfRegister");
        var extraFields = JSON.stringify(component.get("v.extraFields"));   // somehow apex controllers refuse to deal with list of maps
        var startUrl = component.get("v.startUrl");

        var birthdatefield = component.find("birthdate").get("v.validity").valid;
        var phonefield = component.find("phone").get("v.validity").valid;
        var gradyearfield = component.find("gradyear").get("v.validity").valid;
        var emailfield = component.find("email").get("v.validity").valid;

        if(birthdatefield && phonefield && gradyearfield && emailfield) {
            component.set("v.showSpinner", "true");
            // continue processing
            startUrl = decodeURIComponent(startUrl);
        
            action.setParams({firstname:firstname,middlename:middlename,lastname:lastname,email:email,usmleid:usmleid,aamcid:aamcid,nbmeid:nbmeid,birthdate:birthdate,phone:phone,
                    medicalschool:medicalschool,gradyear:gradyear,password:password,confirmPassword:confirmPassword,accountId:accountId,regConfirmUrl:regConfirmUrl,extraFields:extraFields,
                    startUrl:startUrl,includePassword:includePassword});
            action.setCallback(this, function(a){
                component.set("v.showSpinner", "false");
                var rtnValue = a.getReturnValue();
                if (rtnValue.isSuccess == false) {
                    component.set("v.errorMessage",rtnValue.message);
                    component.set("v.showError",true);
                } else {
                    component.set("v.successMessage",rtnValue.message);
                    component.set("v.showNotificationModal",true);
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.errorMessage","Please address any errors before continuing.");
            component.set("v.showError",true);
        }
    },
    
    getExtraFields : function (component, event, helpler) {
        var action = component.get("c.getExtraFields");
        action.setParam("extraFieldsFieldSet", component.get("v.extraFieldsFieldSet"));
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.extraFields',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    setBrandingCookie: function (component, event, helpler) {        
        var expId = component.get("v.expid");
        if (expId) {
            var action = component.get("c.setExperienceId");
            action.setParams({expId:expId});
            action.setCallback(this, function(a){ });
            $A.enqueueAction(action);
        }        
    },

    openMedicalSchoolModal: function(component, event, helper) {
        component.set("v.showMedicalSchoolModal", "true");
    },

    closeMedicalSchoolModal: function(component, event, helper) {
        component.set("v.showMedicalSchoolModal", "false");
    },

    getCountryPicklistValues: function(component, event, helper) {
        var action = component.get("c.getCountryPicklistValues");

        action.setCallback(this, function(response){
            if (response.getState() == "SUCCESS") {
                component.set("v.countryPicklistValues", response.getReturnValue());
            }
        });

        $A.enqueueAction(action);
    },

    clearMedicalSchoolField: function(component, event, helper) {
        component.set("v.selectedMedicalSchool", "");
    },

    redirectUserToLoginPage: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url" : "/"
        })
        urlEvent.fire();
    }   
})