({
	handleClick : function(component, event, helper) {
		helper.searchForProducts(component, event, helper);
	},

	handleEnterKey : function(component, event, helper) {
		if (event.getParams().keyCode == 13) {
        	helper.searchForProducts(component, event, helper);
      	}
	}
})