public without sharing class AccountManagement {
	public Boolean isSuccess;

	public Boolean updateAccount(Account a) {
		try {
			update a;
			isSuccess = true;
		} catch (Exception ex) {
			isSuccess = false;
		}

		return isSuccess;
	}
}