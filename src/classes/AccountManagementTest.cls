/**
 * Created by aszampias on 8/17/18.
 */

@isTest
public with sharing class AccountManagementTest {
	@isTest static void updateAccount_AccountManagement_returnTrue() {
		Account aNew = new Account(FirstName='Amanda', LastName='Szampias');
		insert aNew;
		AccountManagement a = new AccountManagement();
		Test.startTest();
			System.assertEquals(true, a.updateAccount(aNew));
		Test.stopTest();
	}

	@isTest static void updateAccount_AccountManagement_returnFalse() {
		Account aNew = new Account(FirstName='Amanda', LastName='Szampias');
		//insert aNew; this will be false since we didn't insert anything.
		AccountManagement a = new AccountManagement();
		Test.startTest();
			System.assertEquals(false, a.updateAccount(aNew));
		Test.stopTest();
	}
}