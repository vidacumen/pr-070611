/**
 * Created by btenis on 1/23/2018
 */

public with sharing class AccountTriggerHandler {

	public void handleBeforeInsert(List<Account> accounts) {
		String newGuid = '';

		for (Account a : accounts) {
			if (a.IsPersonAccount) {
				newGuid = generateGUIDForPersonAccount();
				if(!String.isBlank(newGuid)){
					a.Global_ID__pc = newGuid;
				}
			}
		}
	}

	private String generateGUIDForPersonAccount() {
		final String kHexChars = '0123456789abcdef';
		String returnGuid = '';
        Integer nextByte = 0;

        for (Integer i=0; i<16; i++) {
            if (i==4 || i==6 || i==8 || i==10) {
                returnGuid += '-';
            }

            nextByte = (Math.round(Crypto.getRandomInteger() * 255)-128) & 255;

            if (i==6) {
                nextByte = nextByte & 15;
                nextByte = nextByte | (4 << 4);
            }

            if (i==8) {
                nextByte = nextByte & 63;
                nextByte = nextByte | 128;
            }

            returnGuid += getCharAtIndex(kHexChars, nextByte >> 4);
            returnGuid += getCharAtIndex(kHexChars, nextByte & 15);
        }

        return returnGuid;
	}

	private String getCharAtIndex(String str, Integer index) {

        if (str == null || index == str.length()){
        	return null;
        } else if (str.length() <= 0){
        	return str;    
        } else {
        	return str.substring(index, index+1);
        }
    }
}