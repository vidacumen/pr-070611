/**
 * Created by btenis on 1/23/2018
 */

@isTest
private class AccountTriggerHandlerTest {

	@isTest
	static void handleBeforeInsert_PassInBusinessAccountRecords_NoGuidIsCreated() {
		RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];

		Account testBusinessAccount = new Account(Name = 'Test Business Account', 
									RecordType = businessAccountRecordType);

		Test.startTest();
		insert testBusinessAccount;
		Test.stopTest();

		Account acct = [
			SELECT Id, Global_ID__pc
			FROM Account
			WHERE Name = 'Test Business Account'
		];

		System.debug('Business Account GUID: ' + acct.Global_ID__pc);
		system.assertEquals(acct.Global_ID__pc, null);
	}

	@isTest
	static void handleBeforeInsert_PassInPersonAccountRecords_GuidIsPopulatedForPersonAccountAndContact() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Test', 
									LastName = 'PersonAccount', 
									RecordType = personAccountRecordType);

		Test.startTest();
		insert testPersonAccount;
		Test.stopTest();

		Account acct = [
			SELECT Id, Global_ID__pc
			FROM Account
			WHERE FirstName = 'Test' AND LastName = 'PersonAccount'
		];

		Contact c = [
			SELECT Id, Global_ID__c
			FROM Contact
			WHERE Name = 'Test PersonAccount'
		];

		System.debug('Person Account GUID: ' + acct.Global_ID__pc);
		System.debug('Contact GUID: ' + c.Global_ID__c);
		system.assertNotEquals(acct.Global_ID__pc, null);
		system.assertNotEquals(c.Global_ID__c, null);
	}
}