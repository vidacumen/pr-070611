global with sharing class ApplicationModalController {
    public Account personAccount {get;set;}
    //public Id registrationId {get;set;}
    public Form_Agreement__mdt formAgreement {get;set;}
    public String sanitizedFeeText {get;set;}
    public String sanitizedPrivacyText {get;set;}
    public String sku {get;set;}
    public String privacyPolicyLink {get;set;}
    //public String FirstName {get; set;}

    global ApplicationModalController() {
        //personAccount = [SELECT FirstName, LastName FROM Account WHERE IsPersonAccount = true LIMIT 1];
        String uId = UserInfo.getUserId();
        User u = [SELECT ContactId 
                    FROM User 
                    WHERE Id =: uId];

        personAccount = [SELECT FirstName, 
                                MiddleName, 
                                LastName, 
                                Suffix, 
                                BillingStreet, 
                                BillingCity, 
                                BillingState, 
                                BillingPostalCode, 
                                BillingCountry, 
                                PersonEmail, 
                                Phone 
                        FROM Account 
                        WHERE PersonContactId = :u.ContactId LIMIT 1];
        
        personAccount.BillingStreet = personAccount.BillingStreet.replace('\r\n', '\\r\\n');
        personAccount.BillingStreet = personAccount.BillingStreet.replace('\n', '\\n');
        personAccount.BillingStreet = personAccount.BillingStreet.replace('\r', '\\r');

        Form_Agreement__mdt f = [SELECT Privacy_Policy_Link__c
                                    FROM Form_Agreement__mdt
                                    WHERE Form__c = 'All Privacy Policy'];

        privacyPolicyLink = f.Privacy_Policy_Link__c;
        //FirstName = personAccount.FirstName;
    }

    @RemoteAction
    global static Form_Agreement__mdt findFormBySku(String skuId) {
        ccrz__E_Product__c product = [SELECT Registration_Form__c
                                        FROM ccrz__E_Product__c
                                        WHERE ccrz__SKU__c =: skuId];

        Form_Agreement__mdt formAgreement = [SELECT Label,
                                                    Form__c,
                                                    Fee_Agreement_Text__c,
                                                    Privacy_Agreement_Text__c
                                            FROM Form_Agreement__mdt
                                            WHERE Form__c =: product.Registration_Form__c
                                            LIMIT 1];

        //sanitizedFeeText = String.escapeSingleQuotes(formAgreement.Fee_Agreement_Text__c);
        //sanitizedPrivacyText = String.escapeSingleQuotes(formAgreement.Privacy_Agreement_Text__c);

        //FormWrapper fw = new FormWrapper(formAgreement);
        //return fw;
        return formAgreement;
    }

    //global class FormWrapper {
    //  public Form_Agreement__mdt formAgreement {get;set;}
    //  //public String sanitizedFeeText {get;set;}
    //  //public String sanitizedPrivacyText {get;set;}

    //  global FormWrapper(Form_Agreement__mdt fa) {
    //      formAgreement = fa;
    //      //sanitizedFeeText = String.escapeSingleQuotes(formAgreement.Fee_Agreement_Text__c);
    //      //sanitizedPrivacyText = String.escapeSingleQuotes(formAgreement.Privacy_Agreement_Text__c);
    //  }
    //}

    @RemoteAction
    global static ccrz__E_Product__c findRequiredCheckboxesBySku(String skuId) {
        return [SELECT Require_EULA_Agreement__c,
                        Require_Fee_Agreement__c,
                        Require_Privacy_Agreement__c,
                        Product_Terms__c
                FROM ccrz__E_Product__c
                WHERE ccrz__SKU__c =: skuId];
    }

    @RemoteAction
    global static Boolean autopopulateAppModalCheckboxes(List<String> productIds, String currentProduct) {
        Boolean result = false;

        List<ccrz__E_Product__c> products = [SELECT Id,
                                                Registration_Form__c
                                            FROM ccrz__E_Product__c
                                            WHERE Id IN :productIds];

        ccrz__E_Product__c currentProd = [SELECT Id,
                                            Registration_Form__c
                                        FROM ccrz__E_Product__c
                                        WHERE Id =: currentProduct];

        Form_Agreement__mdt formAgreement = [SELECT Copy_Registrations_in_Cart__c
                                            FROM Form_Agreement__mdt
                                            WHERE Form__c =: currentProd.Registration_Form__c];

        if (formAgreement.Copy_Registrations_in_Cart__c) {
            for (ccrz__E_Product__c prod : products) {
                if (prod.Registration_Form__c == currentProd.Registration_Form__c) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    //@RemoteAction
    //global static RegistrationResponse submit(Id accountId,  
    //                          String productName, 
    //                          String productId, 
    //                          String sku,
    //                          List<String> attrSFIds,
    //                          List<String> attrIds,
    //                          List<String> attrDisplayNames,
    //                          String firstName,
    //                          String middleName,
    //                          String lastName,
    //                          String suffix,
    //                          String streetAddress,
    //                          String city,
    //                          String state,
    //                          String zipcode,
    //                          String country,
    //                          String email,
    //                          String phone) {

    //  String result = '';

    //  Map<String, String> formData = checkForFormAttribute(attrSFIds, attrIds, attrDisplayNames);
    //  Map<String, String> pacingData = checkForPacingAttribute(attrSFIds, attrIds, attrDisplayNames);

    //  String formSfid = (formData.size() > 0 ? formData.get('sfid') : '');
    //  String formAttrId = (formData.size() > 0 ? formData.get('attrId') : '');
    //  String formDisplayName = (formData.size() > 0 ? formData.get('displayName') : '');

    //  String pacingSfid = (pacingData.size() > 0 ? pacingData.get('sfid') : '');
    //  String pacingAttrId = (pacingData.size() > 0 ? pacingData.get('attrId') : '');
    //  String pacingDisplayName = (pacingData.size() > 0 ? pacingData.get('displayName') : '');

    //  RecordType regRecordType = [SELECT Id 
    //                              FROM RecordType 
    //                              WHERE SobjectType = 'Registration__c' 
    //                              AND Name = 'NSAS Registration' 
    //                              LIMIT 1];

    //  Account myPersonAccount = [SELECT PersonBirthdate,
    //                                      Gender__c,
    //                                      Medical_School_Country__c,
    //                                      Medical_School__c,
    //                                      Medical_School_Grad_Month__c,
    //                                      Med_School_Grad_Year__c,
    //                                      Degree__c,
    //                                      Citizenship_Country__c,
    //                                      Is_English_your_native_language__c
    //                              FROM Account 
    //                              WHERE IsPersonAccount = true 
    //                              AND Id = :accountId 
    //                              LIMIT 1];

    //  ccrz__E_Product__c product = [SELECT Registration_Form__c
    //                                  FROM ccrz__E_Product__c
    //                                  WHERE ccrz__SKU__c =: sku];

    //  ccrz__E_CartItem__c cartItem = [SELECT Id,
    //                                  Name,
    //                                  ccrz__PrimaryAttr__c,
    //                                  ccrz__SecondaryAttr__c,
    //                                  ccrz__Quantity__c,
    //                                  ccrz__Cart__c 
    //                                  FROM ccrz__E_CartItem__c 
    //                                  WHERE ccrz__Cart__r.ccrz__ActiveCart__c = true
    //                                  AND  ccrz__Cart__r.ccrz__Account__c =: accountId
    //                                  AND ccrz__Product__c =: productId
    //                                  AND (ccrz__PrimaryAttr__c =: formSfid OR ccrz__PrimaryAttr__c =: pacingSfid)
    //                                  AND (ccrz__SecondaryAttr__c =: pacingSfid OR ccrz__SecondaryAttr__c =: formSfid)
    //                                  ORDER BY Name DESC
    //                                  LIMIT 1];

    //  String timingFactor = '';

    //  if (String.isNotEmpty(pacingSfid)) {
    //      ccrz__E_Attribute__c productPacingAttributes = [SELECT Timing_Factor__c 
    //                                                      FROM ccrz__E_Attribute__c 
    //                                                      WHERE Id =: pacingSfid];

    //      timingFactor = productPacingAttributes.Timing_Factor__c;
    //  }

    //  try {
    //      Registration__c newRegistration = new Registration__c(RecordTypeId = regRecordType.Id,
    //                                                              CreatedById = UserInfo.getUserId(),
    //                                                              LastModifiedById = UserInfo.getUserId(),
    //                                                              OwnerId = UserInfo.getUserId(),
    //                                                              Examinee__c = accountId,
    //                                                              Cart__c = cartItem.ccrz__Cart__c,
    //                                                              Cart_Item_ID__c = cartItem.Id,
    //                                                              Registration_Status__c = 'Application Saved',
    //                                                              Registration_Form__c = product.Registration_Form__c,
    //                                                              Timing_Factor__c = timingFactor,
    //                                                              Exam__c = productName,
    //                                                              Assessment_Product__c = productId,
    //                                                              Form_Name__c = formDisplayName,
    //                                                              Form__c = formSfid,
    //                                                              Pacing__c = pacingSfid,
    //                                                              First_Name__c = firstName,
    //                                                              Middle_Name__c = middleName,
    //                                                              Last_Name__c = lastName,
    //                                                              Suffix__c = suffix,
    //                                                              Street_Address__c = streetAddress,
    //                                                              City__c = city,
    //                                                              State_Province__c = state,
    //                                                              Zip_Postal_Code__c = zipcode,
    //                                                              Country2__c = country,
    //                                                              Birthdate__c = myPersonAccount.PersonBirthdate,
    //                                                              Gender__c = myPersonAccount.Gender__c,
    //                                                              Email__c = email,
    //                                                              Phone__c = phone,
    //                                                              Medical_School_Country__c = myPersonAccount.Medical_School_Country__c,
    //                                                              Medical_School__c = myPersonAccount.Medical_School__c,
    //                                                              Medical_School_Grad_Month__c = myPersonAccount.Medical_School_Grad_Month__c,
    //                                                              Medical_School_Grad_Year__c = myPersonAccount.Med_School_Grad_Year__c,
    //                                                              Medical__c = myPersonAccount.Degree__c,
    //                                                              Current_Citzenship__c = myPersonAccount.Citizenship_Country__c,
    //                                                              Is_English_your_native_language__c = myPersonAccount.Is_English_your_native_language__c);

    //      insert newRegistration;
    //      RegistrationResponse rr = new RegistrationResponse(null, 'SUCCESS');
    //      return rr;
    //  } catch (Exception ex) {
    //      //result = ex.getMessage();
    //      RegistrationResponse rr = new RegistrationResponse(null, ex.getMessage());

    //      return rr;
    //  }
    //}

    @RemoteAction
    global static RegistrationResponse submit(Id accountId,  
                                String productName, 
                                String productId, 
                                String sku,
                                List<String> attrSFIds,
                                List<String> attrIds,
                                List<String> attrDisplayNames,
                                String firstName,
                                String middleName,
                                String lastName,
                                String suffix,
                                String streetAddress,
                                String city,
                                String state,
                                String zipcode,
                                String country,
                                String email,
                                String phone) {

        String result = '';

        //Map<String, String> formData = checkForFormAttribute(attrSFIds, attrIds, attrDisplayNames);
        //Map<String, String> pacingData = checkForPacingAttribute(attrSFIds, attrIds, attrDisplayNames);

        //String formSfid = (formData.size() > 0 ? formData.get('sfid') : '');
        //String formAttrId = (formData.size() > 0 ? formData.get('attrId') : '');
        //String formDisplayName = (formData.size() > 0 ? formData.get('displayName') : '');

        //String pacingSfid = (pacingData.size() > 0 ? pacingData.get('sfid') : '');
        //String pacingAttrId = (pacingData.size() > 0 ? pacingData.get('attrId') : '');
        //String pacingDisplayName = (pacingData.size() > 0 ? pacingData.get('displayName') : '');

        RecordType regRecordType = [SELECT Id 
                                    FROM RecordType 
                                    WHERE SobjectType = 'Registration__c' 
                                    AND Name = 'NSAS Registration' 
                                    LIMIT 1];

        Account myPersonAccount = [SELECT PersonBirthdate,
                                            Gender__c,
                                            Medical_School_Country__c,
                                            Medical_School__c,
                                            Medical_School_Grad_Month__c,
                                            Med_School_Grad_Year__c,
                                            Degree__c,
                                            Citizenship_Country__c,
                                            Is_English_your_native_language__c,
                                            BillingStreet,
                                            BillingCity,
                                            BillingState,
                                            BillingPostalCode,
                                            BillingCountry,
                                            PersonEmail,
                                            Phone
                                    FROM Account 
                                    WHERE IsPersonAccount = true 
                                    AND Id = :accountId 
                                    LIMIT 1];

        ccrz__E_Product__c product = [SELECT Registration_Form__c,
                                            Driver_Assessment_ID__c
                                        FROM ccrz__E_Product__c
                                        WHERE ccrz__SKU__c =: sku];

        ccrz__E_CartItem__c cartItem = [SELECT Id,
                                        Name,
                                        ccrz__PrimaryAttr__c,
                                        ccrz__PrimaryAttr__r.Driver_Form_ID__c,
                                        ccrz__SecondaryAttr__c,
                                        ccrz__SecondaryAttr__r.Timing_Factor__c,
                                        ccrz__Quantity__c,
                                        ccrz__Cart__c 
                                        FROM ccrz__E_CartItem__c 
                                        WHERE ccrz__Cart__r.ccrz__ActiveCart__c = true
                                        AND  ccrz__Cart__r.ccrz__Account__c =: accountId
                                        AND ccrz__Product__c =: productId
                                        AND (ccrz__PrimaryAttr__c IN: attrSFIds AND ccrz__SecondaryAttr__c IN: attrSFIds)
                                        ORDER BY Name DESC
                                        LIMIT 1];

        try {
            Registration__c newRegistration = new Registration__c(RecordTypeId = regRecordType.Id,
                                                                    CreatedById = UserInfo.getUserId(),
                                                                    LastModifiedById = UserInfo.getUserId(),
                                                                    OwnerId = UserInfo.getUserId(),
                                                                    Examinee__c = accountId,
                                                                    Cart__c = cartItem.ccrz__Cart__c,
                                                                    Cart_Item_ID__c = cartItem.Id,
                                                                    Registration_Status__c = 'Application Saved',
                                                                    Registration_Form__c = product.Registration_Form__c,
                                                                    //Timing_Factor__c = cartItem.ccrz__SecondaryAttr__r.Timing_Factor__c,
                                                                    //Exam__c = product.Driver_Assessment_ID__c,
                                                                    Assessment_Product__c = productId,
                                                                    //Form_Name__c = cartItem.ccrz__PrimaryAttr__r.Driver_Form_ID__c,
                                                                    Form__c = cartItem.ccrz__PrimaryAttr__c,
                                                                    Pacing__c = cartItem.ccrz__SecondaryAttr__c,
                                                                    First_Name__c = firstName,
                                                                    Middle_Name__c = middleName,
                                                                    Last_Name__c = lastName,
                                                                    Suffix__c = suffix,
                                                                    Street_Address__c = streetAddress,
                                                                    City__c = city,
                                                                    State_Province__c = state,
                                                                    Zip_Postal_Code__c = zipcode,
                                                                    Country2__c = country,
                                                                    Birthdate__c = myPersonAccount.PersonBirthdate,
                                                                    Gender__c = myPersonAccount.Gender__c,
                                                                    Email__c = email,
                                                                    Phone__c = phone,
                                                                    Medical_School_Country__c = myPersonAccount.Medical_School_Country__c,
                                                                    Medical_School__c = myPersonAccount.Medical_School__c,
                                                                    Medical_School_Grad_Month__c = myPersonAccount.Medical_School_Grad_Month__c,
                                                                    Medical_School_Grad_Year__c = myPersonAccount.Med_School_Grad_Year__c,
                                                                    Medical__c = myPersonAccount.Degree__c,
                                                                    Current_Citzenship__c = myPersonAccount.Citizenship_Country__c,
                                                                    Is_English_your_native_language__c = myPersonAccount.Is_English_your_native_language__c);

            insert newRegistration;
            
            //String escapedBillingStreet = myPersonAccount.BillingStreet.replace('\r\n', ' ');
            //escapedBillingStreet = escapedBillingStreet.replace('\n', ' ');
            //escapedBillingStreet = escapedBillingStreet.replace('\r', ' ');

            //if (!escapedBillingStreet.equals(streetAddress)) {
            //    myPersonAccount.BillingStreet = streetAddress;
            //}
            myPersonAccount.BillingStreet = streetAddress;
            myPersonAccount.BillingCity = city;
            myPersonAccount.BillingState = state;
            myPersonAccount.BillingPostalCode = zipcode;
            myPersonAccount.BillingCountry = country;
            myPersonAccount.PersonEmail = email;
            myPersonAccount.Phone = phone;

            update myPersonAccount;

            RegistrationResponse rr = new RegistrationResponse(null, 'SUCCESS');
            return rr;
        } catch (Exception ex) {
            //result = ex.getMessage();
            RegistrationResponse rr = new RegistrationResponse(null, ex.getMessage());

            return rr;
        }
    }

    global static Map<String, String> checkForFormAttribute(List<String> attrSFIds, List<String> attrIds, List<String> attrDisplayNames) {
        Map<String, String> result = new Map<String, String>();

        if (attrIds.size() > 0) {
            ccrz__E_Attribute__c attribute = [SELECT Id,
                                                    Name
                                                FROM ccrz__E_Attribute__c
                                                WHERE Id IN: attrIds
                                                AND Name LIKE '%Form%'
                                                LIMIT 1];

            for (String attrId : attrIds) {
                if (attrId == attribute.Id) {
                    Integer index = attrIds.indexOf(attrId);
                    result.put('sfid', attrSFIds[index]);
                    result.put('attrId', attrIds[index]);
                    result.put('displayName', attrDisplayNames[index]);
                }
            }
        }

        return result;
    }

    global static Map<String, String> checkForPacingAttribute(List<String> attrSFIds, List<String> attrIds, List<String> attrDisplayNames) {
        Map<String, String> result = new Map<String, String>();

        if (attrIds.size() > 0) {
            ccrz__E_Attribute__c attribute = [SELECT Id,
                                                    Name
                                                FROM ccrz__E_Attribute__c
                                                WHERE Id IN: attrIds
                                                AND Name LIKE '%Pacing%'
                                                LIMIT 1];

            for (String attrId : attrIds) {
                if (attrId == attribute.Id) {
                    Integer index = attrIds.indexOf(attrId);
                    result.put('sfid', attrSFIds[index]);
                    result.put('attrId', attrIds[index]);
                    result.put('displayName', attrDisplayNames[index]);
                }
            }
        }

        return result;
    }

    //@RemoteAction
    //global static String updateCartFields(Id regId,
    //                                      String cartId,
    //                                      String productId,
    //                                      String formId,
    //                                      String pacingId) {

    //  ccrz__E_CartItem__c cartItem = [SELECT Id,
    //                                  ccrz__PrimaryAttr__c,
    //                                  ccrz__SecondaryAttr__c,
    //                                  ccrz__Quantity__c 
    //                                  FROM ccrz__E_CartItem__c 
    //                                  WHERE ccrz__Cart__c =: cartId 
    //                                  AND ccrz__Product__c =: productId
    //                                  AND ccrz__PrimaryAttr__c =: formId
    //                                  AND ccrz__SecondaryAttr__c =: pacingId];

    //  ccrz__E_Attribute__c productPacingAttributes = [SELECT Timing_Factor__c 
    //                                                  FROM ccrz__E_Attribute__c 
    //                                                  WHERE Id =: cartItem.ccrz__SecondaryAttr__c];

    //  Registration__c newRegistration = [SELECT Cart_Item_ID__c,
    //                                              Form__c,
    //                                              Pacing__c,
    //                                              Timing_Factor__c
    //                                      FROM Registration__c
    //                                      WHERE Id =: regId];

    //  newRegistration.Cart_Item_ID__c = cartItem.Id;
    //  newRegistration.Form__c = cartItem.ccrz__PrimaryAttr__c;
    //  newRegistration.Pacing__c = cartItem.ccrz__SecondaryAttr__c;
    //  newRegistration.Timing_Factor__c = productPacingAttributes.Timing_Factor__c;

    //}

    global class RegistrationResponse {
        public Id cartItem {get;set;}
        public String resultResponse {get;set;}

        global RegistrationResponse(Id item, String response) {
            cartItem = item;
            resultResponse = response;
        }
    }
}