@isTest
public with sharing class ApplicationModalControllerTest2 {

	@testSetup
	static void setup() {
		ccrz__E_Product__c product = new ccrz__E_Product__c(Name = 'Test Product',
															ccrz__SKU__c = '123456',
															Registration_Form__c = 'NSAS',
															Require_EULA_Agreement__c = true,
															Require_Fee_Agreement__c = true,
															Require_Privacy_Agreement__c = true,
															Product_Terms__c = 'Here are product terms');

		insert product;

		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
								PersonEmail = 'testing@email.com',
								PersonBirthdate = Date.newInstance(1990, 1, 1),
								RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Lightning_Community_Setup__c lcs = new Lightning_Community_Setup__c(Name = 'Community Name', Value__c = '');
		insert lcs;

		User u = new User(LastName = 'Tester',
						Email = 'tester123@email.com',
						Birthdate__c = Date.newInstance(1989, 1, 1),
						Username = 'tester123@email.com',
						Alias = 'Tester',
						TimeZoneSidKey = 'America/New_York',
						LocaleSidKey = 'en_US',
						EmailEncodingKey = 'UTF-8',
						LanguageLocaleKey = 'en_US',
						CommunityNickname = 'Tester',
						ProfileId = p.Id,
						ContactId = userAccount.PersonContactId);

		insert u;

		ccrz__E_Cart__c cart = new ccrz__E_Cart__c(ccrz__Account__c = a.Id,
													ccrz__ActiveCart__c = true);

		insert cart;

		ccrz__E_Attribute__c parentattr1 = new ccrz__E_Attribute__c(Name = 'Forms');
		ccrz__E_Attribute__c parentattr2 = new ccrz__E_Attribute__c(Name = 'Pacing');

		insert parentattr1;
		insert parentattr2;

		ccrz__E_Attribute__c attr1 = new ccrz__E_Attribute__c(Name = 'Form 1',
																Driver_Form_ID__c = 'ABC123',
																ccrz__ParentAttribute__c = parentattr1.Id);
		ccrz__E_Attribute__c attr2 = new ccrz__E_Attribute__c(Name = 'Pacing: Standard',
																Timing_Factor__c = '1',
																ccrz__ParentAttribute__c = parentattr2.Id);

		insert attr1;
		insert attr2;

		ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c(ccrz__PrimaryAttr__c = attr1.Id,
																ccrz__SecondaryAttr__c = attr2.Id,
																ccrz__Cart__c = cart.Id,
																ccrz__Price__c = 10.00,
																ccrz__Product__c = product.Id);

		insert cartItem;
	}


	//@isTest
	//static void ApplicationModalController_callConstructor_pageVariablesSet() {
	//	User u = [SELECT Id,
	//					AccountId
	//				FROM User 
	//				WHERE Email = 'tester123@email.com'];

	//	System.runAs(u) {
	//		Test.startTest();
	//			ApplicationModalController amc = new ApplicationModalController();
	//		Test.stopTest();

	//		System.assertEquals(amc.personAccount.Id, u.AccountId);
	//	}
	//}

	@isTest
	static void findFormBySku_passInSku_formAgreementObjectReturned() {
		Test.startTest();
			Form_Agreement__mdt fa = ApplicationModalController.findFormBySku('123456');
		Test.stopTest();

		System.assertNotEquals(fa, null);
	}

	@isTest
	static void findRequiredCheckboxesBySku_passInSku_returnProductObject() {
		ccrz__E_Product__c product = [SELECT Require_EULA_Agreement__c,
											Require_Fee_Agreement__c,
											Require_Privacy_Agreement__c,
											Product_Terms__c,
											ccrz__SKU__c
										FROM ccrz__E_Product__c
										WHERE ccrz__SKU__c = '123456'];

		Test.startTest();
			ccrz__E_Product__c p = ApplicationModalController.findRequiredCheckboxesBySku(product.ccrz__SKU__c);
		Test.stopTest();

		System.assertEquals(product.Require_EULA_Agreement__c, p.Require_EULA_Agreement__c);
		System.assertEquals(product.Require_Fee_Agreement__c, p.Require_Fee_Agreement__c);
		System.assertEquals(product.Require_Privacy_Agreement__c, p.Require_Privacy_Agreement__c);
		System.assertEquals(product.Product_Terms__c, p.Product_Terms__c);
	}

	@isTest
	static void autopopulateAppModalCheckboxes_passInProductListAndCurrentProduct_returnMdtBoolean() {
		List<ccrz__E_Product__c> prodList = new List<ccrz__E_Product__c>();
		prodList.add(new ccrz__E_Product__c(Name = 'Product 1', ccrz__SKU__c = '111111', Registration_Form__c = 'NSAS'));
		insert prodList;

		List<String> prodListIds = new List<String>();
		for (ccrz__E_Product__c p : prodList) {
			prodListIds.add((String) p.Id);
		}

		ccrz__E_Product__c product = new ccrz__E_Product__c(Name = 'Product 2',
															ccrz__SKU__c = '222222',
															Registration_Form__c = 'NSAS');

		insert product;

		String productId = product.Id;

		Form_Agreement__mdt formAgreement = [SELECT Copy_Registrations_in_Cart__c
											FROM Form_Agreement__mdt
											WHERE Form__c =: product.Registration_Form__c];

		Test.startTest();
			Boolean b = ApplicationModalController.autopopulateAppModalCheckboxes(prodListIds, productId);
		Test.stopTest();

		System.assertEquals(b, formAgreement.Copy_Registrations_in_Cart__c);
	}

	@isTest
	static void submit_passInValidParams_successResponseReturned() {
		User u = [SELECT Id,
						AccountId
					FROM User
					WHERE Email = 'tester123@email.com'];

		ccrz__E_Product__c p = [SELECT Id,
									Name,
									ccrz__SKU__c
								FROM ccrz__E_Product__c
								WHERE ccrz__SKU__c = '123456'];

		ccrz__E_CartItem__c ci = [SELECT Id,
									ccrz__PrimaryAttr__c,
									ccrz__SecondaryAttr__c
								FROM ccrz__E_CartItem__c];

		List<String> attrIds = new List<String>();
		attrIds.add(ci.ccrz__PrimaryAttr__c);
		attrIds.add(ci.ccrz__SecondaryAttr__c);

		Test.startTest();
			ApplicationModalController.RegistrationResponse rr = ApplicationModalController.submit(u.AccountId, 
																		p.Name, 
																		p.Id, 
																		p.ccrz__SKU__c, 
																		attrIds, 
																		null, 
																		null, 
																		'John', 
																		'Lee', 
																		'Smith', 
																		'Jr', 
																		'1234 State St', 
																		'Cleveland', 
																		'OH', 
																		'44444', 
																		'United States of America', 
																		'myemail123@yahoo.com', 
																		'555-777-8888');
		Test.stopTest();

		System.assertEquals(rr.resultResponse, 'SUCCESS');
	}

	@isTest
	static void checkForFormAttribute_passValidParentAndChildAttributes_nonemptyMapReturned() {
		ccrz__E_Attribute__c attr = [SELECT Id
									FROM ccrz__E_Attribute__c
									WHERE Name = 'Form 1'];
																
		List<String> attrIdsList = new List<String>();
		attrIdsList.add(attr.Id);

		List<String> attrSFIdsList = new List<String>();
		attrSFIdsList.add('1234');
		List<String> attrDisplayNameList = new List<String>();
		attrDisplayNameList.add('Product Name');

		Test.startTest();
			Map<String, String> resultMap = ApplicationModalController.checkForFormAttribute(attrSFIdsList, attrIdsList, attrDisplayNameList);
		Test.stopTest();

		System.assertNotEquals(resultMap.size(), 0);
	}

	@isTest
	static void checkForPacingAttribute_passValidParentAndChildAttributes_nonemptyMapReturned() {
		ccrz__E_Attribute__c attr = [SELECT Id
									FROM ccrz__E_Attribute__c
									WHERE Name = 'Pacing: Standard'];
																
		List<String> attrIdsList = new List<String>();
		attrIdsList.add(attr.Id);

		List<String> attrSFIdsList = new List<String>();
		attrSFIdsList.add('1234');
		List<String> attrDisplayNameList = new List<String>();
		attrDisplayNameList.add('Product Name');

		Test.startTest();
			Map<String, String> resultMap = ApplicationModalController.checkForPacingAttribute(attrSFIdsList, attrIdsList, attrDisplayNameList);
		Test.stopTest();

		System.assertNotEquals(resultMap.size(), 0);
	}
}