public with sharing class AuthenticatedUserErrorController { 
    
        public PageReference redirectToPage(){
        
        PageReference newPage = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/s/error');
 
        return newPage;
    }
    
}