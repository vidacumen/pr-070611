public with sharing class CCResponse {
	@AuraEnabled
	public String name {get;private set;}
	@AuraEnabled
	public Id parentCategory {get;private set;}
	@AuraEnabled
	public Id sfid {get;private set;}
	@AuraEnabled
	public String categoryID {get;private set;}
	@AuraEnabled
	public Id ownerId {get;private set;}

	@AuraEnabled
	public static List<CCResponse> parse(String json) {
		return (List<CCResponse>) System.JSON.deserialize(json, List<CCResponse>.class);
	}
}