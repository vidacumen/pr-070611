public class CalculateApexSharing {

	/**
	 * [calculateCaseSharing description]
	 * @param  caseId [description]
	 * @param  userId [description]
	 * @return        [description]
	 */
	public static CaseShare calculateCaseSharing(Id caseId, Id userId) {
		CaseShare share = new CaseShare();
		share.CaseId = caseId;
		share.UserOrGroupId = userId;
		share.CaseAccessLevel = 'edit';
		share.RowCause = 'manual';

		System.debug('NEW SHARE REORD: ' + share);
		return share;
	}

	/**
	 * [calculateCaseSharingDelete description]
	 * @param  caseId [description]
	 * @param  userId [description]
	 * @return        [description]
	 */
	public static Map<Id, CaseShare> calculateCaseSharingDelete(Set<Id> caseIds, Map<Id, User> contactIdToUserMap) {
		Set<Id> userIds = new Set<Id>();
		for (User u : contactIdToUserMap.values()) {
			userIds.add(u.Id);
		}

		List<CaseShare> csList = [SELECT Id, CaseId, UserOrGroupId, RowCause FROM CaseShare WHERE CaseId IN :caseIds AND UserOrGroupId IN :userIds AND RowCause = 'Manual'];
		Map<Id, CaseShare> shareMap = new Map<Id, CaseShare>();
		for (CaseShare cs : csList) {
			if (userIds.contains(cs.UserOrGroupId)) {
				shareMap.put(cs.CaseId, cs);
			}
		}

		return shareMap;
	}

	/**
	 * [calculateDocumentSharing description]
	 * @param  docId  [description]
	 * @param  userId [description]
	 * @return        [description]
	 */
	public static CustomDocument__Share calculateDocumentSharing(Id docId, Id userId) {
		CustomDocument__Share share = new CustomDocument__Share();
		share.ParentId = docId;
		share.UserOrGroupId = userId;
		share.AccessLevel = 'edit';
		share.RowCause = Schema.CustomDocument__Share.RowCause.Examinee_Access__c;

		return share;
	}

	/**
	 * [calculateDocumentSharingDelete description]
	 * @param  docId  [description]
	 * @param  userId [description]
	 * @return        [description]
	 */
	public static Map<Id, CustomDocument__Share> calculateDocumentSharingDelete(Set<Id> docIds, Map<Id, User> contactIdToUserMap) {
		Set<Id> userIds = new Set<Id>();
		for (User u : contactIdToUserMap.values()) {
			userIds.add(u.Id);
		}

		List<CustomDocument__Share> csList = [SELECT Id, ParentId, UserOrGroupId, RowCause FROM CustomDocument__Share WHERE ParentId IN :docIds AND UserOrGroupId IN :userIds];
		Map<Id, CustomDocument__Share> shareMap = new Map<Id, CustomDocument__Share>();
		for (CustomDocument__Share cs : csList) {
			if (userIds.contains(cs.UserOrGroupId)) {
				shareMap.put(cs.ParentId, cs);
			}
		}

		return shareMap;
	}



















}