global with sharing class CartAttributeItemsController {
    public Account personAccountAttr {get;set;}
    public Form_Agreement__mdt formAgreementAttr {get;set;}
    public String privacyPolicyLinkAttr {get;set;}

    global CartAttributeItemsController() {
        String uId = UserInfo.getUserId();
        User u = [SELECT ContactId 
                    FROM User 
                    WHERE Id =: uId];

        personAccountAttr = [SELECT FirstName, 
                                MiddleName, 
                                LastName, 
                                Suffix, 
                                BillingStreet, 
                                BillingCity, 
                                BillingState, 
                                BillingPostalCode, 
                                BillingCountry, 
                                PersonEmail, 
                                Phone 
                        FROM Account 
                        WHERE PersonContactId = :u.ContactId LIMIT 1];
        
        personAccountAttr.BillingStreet = personAccountAttr.BillingStreet.replace('\r\n', '\\r\\n');
        personAccountAttr.BillingStreet = personAccountAttr.BillingStreet.replace('\n', '\\n');
        personAccountAttr.BillingStreet = personAccountAttr.BillingStreet.replace('\r', '\\r');

        Form_Agreement__mdt f = [SELECT Privacy_Policy_Link__c
                                    FROM Form_Agreement__mdt
                                    WHERE Form__c = 'All Privacy Policy'];

        privacyPolicyLinkAttr = f.Privacy_Policy_Link__c;
    }

    @RemoteAction
    global static Form_Agreement__mdt findFormByItemIdAttr(String itemId) {
        ccrz__E_CartItem__c cartItem = [SELECT ccrz__Product__c
                                        FROM ccrz__E_CartItem__c
                                        WHERE Id =: itemId
                                        AND ccrz__Product__c != null];

        ccrz__E_Product__c product = [SELECT Registration_Form__c
                                        FROM ccrz__E_Product__c
                                        WHERE Id =: cartItem.ccrz__Product__c];

        //ccrz__E_Product__c product = [SELECT Registration_Form__c
        //                              FROM ccrz__E_Product__c
        //                              WHERE ccrz__SKU__c =: skuId];

        Form_Agreement__mdt formAgreementAttr = [SELECT Label,
                                Form__c,
                                Fee_Agreement_Text__c,
                                Privacy_Agreement_Text__c
                        FROM Form_Agreement__mdt
                        WHERE Form__c =: product.Registration_Form__c
                        LIMIT 1];

        return formAgreementAttr;
    }

    @RemoteAction
    global static ccrz__E_Product__c findRequiredCheckboxesByItemIdAttr(String itemId) {
        ccrz__E_CartItem__c cartItem = [SELECT ccrz__Product__c
                                        FROM ccrz__E_CartItem__c
                                        WHERE Id =: itemId
                                        AND ccrz__Product__c != null];

        ccrz__E_Product__c product = [SELECT Require_EULA_Agreement__c,
                                            Require_Fee_Agreement__c,
                                            Require_Privacy_Agreement__c,
                                            Product_Terms__c
                                        FROM ccrz__E_Product__c
                                        WHERE Id =: cartItem.ccrz__Product__c];

        return product;
    }

    @RemoteAction
    global static String updateRegistrationRecord(Id personAcctId,
                                                    String cartId,
                                                    String itemId,
                                                    String firstName,
                                                    String middleName,
                                                    String lastName,
                                                    String suffix,
                                                    String streetAddress,
                                                    String city,
                                                    String state,
                                                    String zipcode,
                                                    String country,
                                                    String email,
                                                    String phone) {

        String result = '';

        try {
            List<Registration__c> reg = [SELECT First_Name__c,
                                                Middle_Name__c,
                                                Last_Name__c,
                                                Suffix__c,
                                                Street_Address__c,
                                                City__c,
                                                State_Province__c,
                                                Zip_Postal_Code__c,
                                                Country2__c,
                                                Email__c,
                                                Phone__c,
                                                Cart__c,
                                                Cart_Item_ID__c
                                        FROM Registration__c
                                        WHERE Cart__c =: cartId
                                        AND Cart_Item_ID__c =: itemId
                                        AND Examinee__c =: personAcctId];

            for(Registration__c r : reg) {
                r.First_Name__c = firstName;
                r.Middle_Name__c = middleName;
                r.Last_Name__c = lastName;
                r.Suffix__c = suffix;
                r.Street_Address__c = streetAddress;
                r.City__c = city;
                r.State_Province__c = state;
                r.Zip_Postal_Code__c = zipcode;
                r.Country2__c = country;
                r.Email__c = email;
                r.Phone__c = phone;
            }

            update reg;

            result = 'SUCCESS';
        } catch (Exception ex) {
            result = ex.getMessage();
        }

        return result;
    }

    @RemoteAction
    global static String updateAccountRecord(Id personAcctId,
                                            String streetAddress,
                                            String city,
                                            String state,
                                            String zipcode,
                                            String country,
                                            String email,
                                            String phone) {

        String result = '';

        try {
            Account acc = [SELECT BillingStreet, 
                                    BillingCity, 
                                    BillingState, 
                                    BillingPostalCode, 
                                    BillingCountry, 
                                    PersonEmail, 
                                    Phone 
                            FROM Account 
                            WHERE Id = :personAcctId LIMIT 1];

            acc.BillingStreet = streetAddress;
            acc.BillingCity = city;
            acc.BillingState = state;
            acc.BillingPostalCode = zipcode;
            acc.BillingCountry = country;
            acc.PersonEmail = email;
            acc.Phone = phone;

            update acc;

            result = 'SUCCESS';
        } catch (Exception ex) {
            result = ex.getMessage();
        }

        return result;
    }

    @RemoteAction
    global static String deleteRegistrationRecords(Id personAcctId,
                                                    String cartId,
                                                    String itemId) {
        String result = '';

        try {
            List<Registration__c> reg = [SELECT Id
                                        FROM Registration__c
                                        WHERE Cart__c =: cartId
                                        AND Cart_Item_ID__c =: itemId
                                        AND Examinee__c =: personAcctId];

            delete reg;

            result = 'SUCCESS';
        } catch (Exception ex) {
            result = ex.getMessage();
        }

        return result;
    }

    @RemoteAction
    global static String deleteCartRegistrationRecords(Id personAcctId,
                                                        String cartId,
                                                        String productName) {
        String result = '';

        try {
            List<Registration__c> reg = [SELECT Id
                                        FROM Registration__c
                                        WHERE Cart__c =: cartId
                                        AND Assessment_Product__r.Name =: productName
                                        AND Examinee__c =: personAcctId];

            delete reg;

            result = 'SUCCESS';
        } catch (Exception ex) {
            result = ex.getMessage();
        }

        return result;
    }
}