@isTest
public class CartAttributeItemsControllerTest {
	@testSetup
	static void setup() {
		ccrz__E_Product__c product1 = new ccrz__E_Product__c(Name = 'Test Product',
															ccrz__SKU__c = '123456',
															Registration_Form__c = 'NSAS',
															Require_EULA_Agreement__c = true,
															Require_Fee_Agreement__c = true,
															Require_Privacy_Agreement__c = true,
                                                            //Case_Product_Categorization__c = 'CCMSA',
                                                            //Case_Program_Categorization__c = 'NBME',
															Product_Terms__c = 'Here are product terms');

		ccrz__E_Product__c product2 = new ccrz__E_Product__c(Name = 'Test Product 2',
															ccrz__SKU__c = '654321',
															Registration_Form__c = 'NSAS',
															Require_EULA_Agreement__c = true,
															Require_Fee_Agreement__c = true,
															Require_Privacy_Agreement__c = true,
															//Case_Product_Categorization__c = 'CCMSA',
															//Case_Program_Categorization__c = 'NBME',
															Product_Terms__c = 'Here are product terms again');

		insert product1;
		insert product2;

		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
								PersonEmail = 'testing@email.com',
								PersonBirthdate = Date.newInstance(1990, 1, 1),
								RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Lightning_Community_Setup__c lcs = new Lightning_Community_Setup__c(Name = 'Community Name', Value__c = 'examinees');
		insert lcs;

		User u = new User(LastName = 'Tester',
						Email = 'tester123@email.com',
						Birthdate__c = Date.newInstance(1989, 1, 1),
						Username = 'tester123@email.com',
						Alias = 'Tester',
						TimeZoneSidKey = 'America/New_York',
						LocaleSidKey = 'en_US',
						EmailEncodingKey = 'UTF-8',
						LanguageLocaleKey = 'en_US',
						CommunityNickname = 'Tester',
						ProfileId = p.Id,
						ContactId = userAccount.PersonContactId);

		insert u;

		ccrz__E_Cart__c cart = new ccrz__E_Cart__c(ccrz__Account__c = a.Id,
													ccrz__ActiveCart__c = true);

		insert cart;

		ccrz__E_Attribute__c parentattr1 = new ccrz__E_Attribute__c(Name = 'Forms');
		ccrz__E_Attribute__c parentattr2 = new ccrz__E_Attribute__c(Name = 'Pacing');

		insert parentattr1;
		insert parentattr2;

		ccrz__E_Attribute__c attr1 = new ccrz__E_Attribute__c(Name = 'Form 1',
																Driver_Form_ID__c = 'ABC123',
																ccrz__ParentAttribute__c = parentattr1.Id);
		ccrz__E_Attribute__c attr2 = new ccrz__E_Attribute__c(Name = 'Pacing: Standard',
																Timing_Factor__c = '1',
																ccrz__ParentAttribute__c = parentattr2.Id);

		insert attr1;
		insert attr2;

		ccrz__E_CartItem__c cartItem1 = new ccrz__E_CartItem__c(ccrz__PrimaryAttr__c = attr1.Id,
																ccrz__SecondaryAttr__c = attr2.Id,
																ccrz__Cart__c = cart.Id,
																ccrz__Price__c = 10.00,
																ccrz__Product__c = product1.Id);

		ccrz__E_CartItem__c cartItem2 = new ccrz__E_CartItem__c(ccrz__PrimaryAttr__c = attr1.Id,
																ccrz__SecondaryAttr__c = attr2.Id,
																ccrz__Cart__c = cart.Id,
																ccrz__Price__c = 15.00,
																ccrz__Product__c = product2.Id);

		insert cartItem1;
		insert cartItem2;

		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		List<Registration__c> regList = new List<Registration__c>();
		regList.add(new Registration__c(RecordTypeId = testRecordType.Id,
													Assessment_Product__c = product1.Id,
													Examinee__c = a.Id,
													Cart__c = cart.Id,
													Cart_Item_ID__c = cartItem1.Id));
		regList.add(new Registration__c(RecordTypeId = testRecordType.Id,
													Assessment_Product__c = product2.Id,
													Examinee__c = a.Id,
													Cart__c = cart.Id,
													Cart_Item_ID__c = cartItem2.Id));

		insert regList;
	}

	//@isTest
	//static void CartAttributeItemsController_callConstructor_pageVariablesSet() {
	//	User u = [SELECT Id,
	//					AccountId
	//				FROM User 
	//				WHERE Email = 'tester123@email.com'];

	//	Form_Agreement__mdt f = [SELECT Privacy_Policy_Link__c
	//							FROM Form_Agreement__mdt
	//							WHERE Form__c = 'All Privacy Policy'];

	//	System.runAs(u) {
	//		Test.startTest();
	//			CartAttributeItemsController caic = new CartAttributeItemsController();
	//		Test.stopTest();

	//		System.assertEquals(caic.personAccountAttr.Id, u.AccountId);
	//		System.assertEquals(caic.privacyPolicyLinkAttr, f.Privacy_Policy_Link__c);
	//		System.assertEquals(null, caic.formAgreementAttr);
	//	}
	//}

	@isTest
	static void CartAttributeItemsController_callConstructor_pageVariablesSet_Without_BillingStreet() {
		User u = [SELECT Id,
				AccountId
		FROM User
		WHERE Email = 'tester123@email.com'];
		Account a = [SELECT BillingStreet FROM Account WHERE Id =: u.AccountId];
		a.BillingStreet = '5055 Helen Drive';
		update a;
		Form_Agreement__mdt f = [SELECT Privacy_Policy_Link__c
		FROM Form_Agreement__mdt
		WHERE Form__c = 'All Privacy Policy'];

		System.runAs(u) {
			Test.startTest();
			CartAttributeItemsController caic = new CartAttributeItemsController();
			Test.stopTest();

			System.assertEquals(caic.personAccountAttr.Id, u.AccountId);
			System.assertEquals(caic.privacyPolicyLinkAttr, f.Privacy_Policy_Link__c);
		}
	}

	@isTest
	static void findFormByItemIdAttr_passInItemId_formAgreementMdtReturned() {
		ccrz__E_CartItem__c ci = [SELECT Id
									FROM ccrz__E_CartItem__c
									LIMIT 1];

		Test.startTest();
			Form_Agreement__mdt fa = CartAttributeItemsController.findFormByItemIdAttr(ci.Id);
		Test.stopTest();

		System.assertNotEquals(fa.Id, null);
	}

	@isTest
	static void findRequiredCheckboxesByItemIdAttr_passInItemId_productObjectReturned() {
		ccrz__E_CartItem__c ci = [SELECT Id
									FROM ccrz__E_CartItem__c
									LIMIT 1];

		Test.startTest();
			ccrz__E_Product__c p = CartAttributeItemsController.findRequiredCheckboxesByItemIdAttr(ci.Id);
		Test.stopTest();

		System.assertNotEquals(p.Id, null);
	}

	@isTest
	static void updateRegistrationRecord_passInValidParams_successStringReturned() {
		User u = [SELECT Id,
						AccountId
					FROM User
					WHERE Email = 'tester123@email.com'];

		ccrz__E_Cart__c cart = [SELECT Id
								FROM ccrz__E_Cart__c];

		ccrz__E_CartItem__c ci = [SELECT Id,
									ccrz__PrimaryAttr__c,
									ccrz__SecondaryAttr__c
								FROM ccrz__E_CartItem__c
								LIMIT 1];

		Test.startTest();
			String result = CartAttributeItemsController.updateRegistrationRecord(u.AccountId, 
																				cart.Id,
																				ci.Id,
																				'John', 
																				'Lee', 
																				'Smith', 
																				'Jr', 
																				'1234 State St', 
																				'Cleveland', 
																				'OH', 
																				'44444', 
																				'United States of America', 
																				'myemail123@yahoo.com', 
																				'555-777-8888');
		Test.stopTest();

		System.assertEquals(result, 'SUCCESS');
	}

	@isTest
	static void updateAccountRecord_passInValidParams_successStringReturned() {
		User u = [SELECT Id,
						AccountId
					FROM User
					WHERE Email = 'tester123@email.com'];

		Test.startTest();
			String result = CartAttributeItemsController.updateAccountRecord(u.AccountId, 
																			'1234 State St', 
																			'Cleveland', 
																			'OH', 
																			'44444', 
																			'United States of America', 
																			'myemail123@yahoo.com', 
																			'555-777-8888');
		Test.stopTest();

		System.assertEquals(result, 'SUCCESS');
	}

	@isTest
	static void deleteRegistrationRecords_passInValidParams_successStringReturned() {
		User u = [SELECT Id,
						AccountId
					FROM User
					WHERE Email = 'tester123@email.com'];

		ccrz__E_Cart__c cart = [SELECT Id
								FROM ccrz__E_Cart__c];

		ccrz__E_CartItem__c ci = [SELECT Id
									FROM ccrz__E_CartItem__c
									LIMIT 1];

		Test.startTest();
			String result = CartAttributeItemsController.deleteRegistrationRecords(u.AccountId, 
																					cart.Id,
																					ci.Id);
		Test.stopTest();

		System.assertEquals(result, 'SUCCESS');
	}

	@isTest
	static void deleteCartRegistrationRecords_passInValidParams_successStringReturned() {
		User u = [SELECT Id,
						AccountId
					FROM User
					WHERE Email = 'tester123@email.com'];

		ccrz__E_Cart__c cart = [SELECT Id
								FROM ccrz__E_Cart__c];

		ccrz__E_Product__c p = [SELECT Name
									FROM ccrz__E_Product__c
									LIMIT 1];

		Test.startTest();
			String result = CartAttributeItemsController.deleteCartRegistrationRecords(u.AccountId, 
																						cart.Id,
																						p.Name);
		Test.stopTest();

		System.assertEquals(result, 'SUCCESS');
	}
}