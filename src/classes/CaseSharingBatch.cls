global class CaseSharingBatch implements Database.Batchable<sObject> {

	global String query;

	global CaseSharingBatch() {
		query = 'SELECT Id, AccountId, Account.PersonContactId, Account.IsCustomerPortal, Account.RecordTypeId, Grant_Examinee_Access__c, Examinee_Access_Status__c'+
				' FROM Case'+
				' WHERE Account.IsCustomerPortal = true'+
				' AND ((Grant_Examinee_Access__c = true'+
				' AND Examinee_Access_Status__c != \'Granted\')'+
				' OR (Grant_Examinee_Access__c = false'+
				' AND Examinee_Access_Status__c != \'None\'))';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Case> scope) {
		CustomSharingHelper.updateCaseSharing(scope);
	}

	global void finish(Database.BatchableContext BC) {
		
	}

}