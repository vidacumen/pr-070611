public class CaseSharingHelper {

	public static Boolean skipTrigger = false;

	public static final Id EXAMINEE_REC_TYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
	public static final Map<String, String> PICKLIST_OPTIONS = new Map<String, String> {
		'none' => 'None',
		'granted' => 'Granted',
		'manual' => 'Pending User',
		'pending' => 'Pending Access',
		'remove' => 'Pending Removal',
		'batch' => 'Pending Batch',
		'error' => 'Error'
	};

	/**
	 * [afterInsertUpdateExamineeSharing description]
	 * @param triggerNew Collection of cases from trigger context
	 */
	public static void afterInsertUpdateExamineeSharing(List<Case> triggerNew) {
		Set<Id> caseIds = new Set<Id>();
		Map<Id, Boolean> caseProcessMap = new Map<Id, Boolean>();
		for (Case c : triggerNew) {
			if (c.AccountId != null && c.Grant_Examinee_Access__c) {
				caseIds.add(c.Id);
				caseProcessMap.put(c.Id, true);
			} 
		}

		if (caseIds.size() > 0) {
			processCaseSharingRequestFuture(caseIds, caseProcessMap);
		}
	}

	/**
	 * [afterUpdateUpdateExamineeSharing description]
	 * @param triggerNew    Collection of cases from trigger context
	 * @param triggerOldMap Collection of cases from trigger context
	 */
	public static void afterUpdateUpdateExamineeSharing(List<Case> triggerNew, Map<Id, Case> triggerOldMap) {
		Set<Id> caseIds = new Set<Id>();
		Map<Id, Boolean> caseProcessMap = new Map<Id, Boolean>();
		for (Case c : triggerNew) {
			if ((c.OwnerId != triggerOldMap.get(c.Id).OwnerId && c.Grant_Examinee_Access__c) || (c.Grant_Examinee_Access__c && !triggerOldMap.get(c.Id).Grant_Examinee_Access__c)) {
				caseIds.add(c.Id);
				caseProcessMap.put(c.Id, true);
			} else if (!c.Grant_Examinee_Access__c && triggerOldMap.get(c.Id).Grant_Examinee_Access__c) {
				caseIds.add(c.Id);
				caseProcessMap.put(c.Id, false);
			}
		}

		if (caseIds.size() > 0) {
			processCaseSharingRequestFuture(caseIds, caseProcessMap);
		}
	}

	@future
	public static void processCaseSharingRequestFuture(Set<Id> caseIds, Map<Id, Boolean> caseProcessMap) {
		//Step 1: Query Cases.
		List<Case> cases = queryCasesById(caseIds);

		processCaseSharingRequest(cases, caseProcessMap);
	}

	public static void processCaseSharingRequest(List<Case> cases, Map<Id, Boolean> caseProcessMap) {
		//Step 2: Extract Set of Person Contact Ids
		Set<Id> contactIds = extractPersonContactIds(cases);

		//Step 3: Query Users with those person contact Ids
		List<User> examinees = queryUsersByPersonContactId(contactIds);

		//Step 4: Map users to Contact I
		Map<Id, User> contactIdToUserMap = mapUsersToContactId(examinees);

		Set<Id> deleteIds = new Set<Id>();
		for (Case c : cases) {
			if (!caseProcessMap.get(c.Id)) {
				deleteIds.add(c.Id);
			}
		}

		Map<Id, CaseShare> shareMap = CalculateApexSharing.calculateCaseSharingDelete(deleteIds, contactIdToUserMap);

		//Step 5: Loop over cases, extract user from map
		List<CaseShare> newCaseShareRecords = new List<CaseShare>();
		Map<Id, CaseShare> oldCaseShareRecords = new Map<Id, CaseShare>();
		Map<Id, Case> caseUpdates = new Map<Id, Case>();
		for (Case c : cases) {
			User u = contactIdToUserMap.get(c.Account.PersonContactId);
			System.debug('USER FOR SHARING: ' + u + ' CASEID: ' + c.Id);
			if (caseProcessMap.get(c.Id)) {
				newCaseShareRecords.add(CalculateApexSharing.calculateCaseSharing(c.Id, u.Id));
				caseUpdates.put(c.Id, new Case(Id=c.Id, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('granted')));
			} else {
				if (shareMap.containsKey(c.Id)) {
					CaseShare oldCs = shareMap.get(c.Id);
					oldCaseShareRecords.put(oldCs.Id, oldCs);
					caseUpdates.put(c.Id, new Case(Id=c.Id, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('none')));
				}
			}
		}

		//Step 6: Delete old sharing records
		if (oldCaseShareRecords.keySet().size() > 0) {
			List<Database.DeleteResult> drList = Database.Delete(oldCaseShareRecords.values(), false);
			for (Database.DeleteResult dr : drList) {
				if (!dr.isSuccess()) {
					CaseShare cs = oldCaseShareRecords.get(dr.getId());
					caseUpdates.put(cs.CaseId, new Case(Id=cs.CaseId, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('error')));
				}
			}
		}

		//Step 7: Insert list of CaseShare records (use allornothing = false and get save result to update cases properly)
		if (newCaseShareRecords.size() > 0) {
			System.debug('newCaseShareRecords: ' + JSON.serializePretty(newCaseShareRecords));
			List<Database.SaveResult> srList = Database.Insert(newCaseShareRecords, false);
			for (CaseShare cs : newCaseShareRecords) {
				if (cs.Id == null) {
					caseUpdates.put(cs.CaseId, new Case(Id=cs.CaseId, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('error')));
				}
			}
		}

		//Step 8: Use Save Results to update case Examinee_Access_Status__c picklist.
		CaseSharingHelper.skipTrigger = true;
		update caseUpdates.values();
	}

	public static List<Case> queryCasesById(Set<Id> caseIds) {
		return [
					SELECT Id, CaseNumber, AccountId, Account.PersonContactId, Account.RecordTypeId, Grant_Examinee_Access__c, Examinee_Access_Status__c 
					FROM Case 
					WHERE Id IN :caseIds 
					AND Account.RecordTypeId = :EXAMINEE_REC_TYPE
				];
	}

	public static Set<Id> extractPersonContactIds(List<Case> cases) {
		Set<Id> contactIds = new Set<Id>();
		for (Case c : cases) {
			contactIds.add(c.Account.PersonContactId);
		}

		return contactIds;
	}

	public static List<User> queryUsersByPersonContactId(Set<Id> pContactIds) {
		return [
					SELECT Id, ContactId 
					FROM User 
					WHERE ContactId IN :pContactIds
				];
	}

	public static Map<Id, User> mapUsersToContactId(List<User> users) {
		Map<Id, User> userMap = new Map<Id, User>();
		for (User u : users) {
			userMap.put(u.ContactId, u);
		}

		return userMap;
	}

	public static void onBeforeInsert(List<Case> triggerNew) {
		checkExamineeAccessCheckbox(triggerNew);
	}

	public static void checkExamineeAccessCheckbox(List<Case> triggerNew) {
		Set<Id> accountIds = new Set<Id>();
		for (Case c : triggerNew) {
			accountIds.add(c.AccountId);
		}

		Map<Id, Account> accountMap = new Map<Id, Account>([
																SELECT Id, IsPersonAccount 
																FROM Account 
																WHERE Id IN :accountIds 
																AND IsPersonAccount = true 
																AND IsCustomerPortal = true
															]);

		for (Case c : triggerNew) {
			Account acct = accountMap.get(c.AccountId);
			if (acct != null) {
				c.Grant_Examinee_Access__c = true;
				c.Examinee_Access_Status__c = 'Pending Access';
			}
		}

	}


}