@isTest
private class CaseSharingTest {

	@testSetup
	static void setup() {
		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Id examineeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
		Account newPersonAcct = new Account(FirstName = 'Test'
												, LastName = 'Testing'
												, PersonEmail = 'test123@noemail.com'
												, BillingCountry = 'United States'
												, BillingState = 'NY'
												, BillingStreet = '101 Main Street'
												, BillingCity = 'New York'
												, BillingPostalCode = '10027'
												, RecordTypeId = examineeRecordTypeId
											);
		Insert newPersonAcct;

		Account newPersonAcct2 = new Account(FirstName = 'Test2'
												, LastName = 'Testing2'
												, PersonEmail = 'test123@noemail2.com'
												, BillingCountry = 'United States'
												, BillingState = 'NY'
												, BillingStreet = '101 Main Street'
												, BillingCity = 'New York'
												, BillingPostalCode = '10027'
												, RecordTypeId = examineeRecordTypeId
											);
		Insert newPersonAcct2;

		Account acct = [SELECT Id, PersonContactId FROM Account WHERE Id = :newPersonAcct.Id];
		Account acct2 = [SELECT Id, PersonContactId FROM Account WHERE Id = :newPersonAcct2.Id];

		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User user = new User(Alias = 'test123', Email='test123@noemail.com',
				EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
				LocalesIdKey='en_US', ProfileId = p.Id, Country='United States',IsActive =true,
				ContactId = acct.PersonContactId,
				TimezonesIdKey='America/Los_Angeles', Username='tester@noemail.com');
		insert user;

		User user2 = new User(Alias = 'test1232', Email='test123@noemail2.com',
				EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
				LocalesIdKey='en_US', ProfileId = p.Id, Country='United States',IsActive =true,
				ContactId = acct2.PersonContactId,
				TimezonesIdKey='America/Los_Angeles', Username='tester@noemail2.com');
		insert user2;

		Account a = [SELECT PersonContactId FROM Account WHERE PersonEmail = 'test123@noemail.com'];
		Case c = new Case();
		c.Subject = 'test';
		c.Description = 'test';
		c.Delivery_Type__c = 'CBT (Computer Based Testing)';
		c.Test_Delivery_Location__c = 'Not Applicable';
		c.Program__c = 'AAOMS';
		c.Product__c = 'Comprehensive Basic Science';
		c.Reason = 'Ongoing / Repeat Issue';
		c.ContactId = acct2.PersonContactId;
		c.AccountId = acct2.Id;
		c.Grant_Examinee_Access__c = true;

		insert c;

	}

	@isTest
	static void test_method_one() {
		User u = [SELECT Id FROM User WHERE Username = 'tester@noemail.com'];
		Account a = [SELECT PersonContactId FROM Account WHERE PersonEmail = 'test123@noemail.com'];
		Case c = new Case();
		System.runAs(u) {
			c.Subject = 'test';
			c.Description = 'test';
			c.Delivery_Type__c = 'CBT (Computer Based Testing)';
			c.Test_Delivery_Location__c = 'Not Applicable';
			c.Program__c = 'AAOMS';
			c.Product__c = 'Comprehensive Basic Science';
			c.Reason = 'Ongoing / Repeat Issue';
			c.ContactId = a.PersonContactId;
			c.AccountId = a.Id;
			c.Grant_Examinee_Access__c = true;

			insert c;
		}

		Case c2 = new Case(Id = c.Id, Grant_Examinee_Access__c = false);
		update c2;

	}

	@isTest
	static void test_method_two() {
		User u1 = [SELECT Id, IsActive FROM User WHERE Username = 'tester@noemail2.com'];
		u1.IsActive = false;
		update u1;
		test.startTest();
		User u = [SELECT Id, IsActive FROM User WHERE Username = 'tester@noemail2.com'];
		u.IsActive = true;
		update u;
		test.stopTest();
	}


}