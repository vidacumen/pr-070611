global class CaseTeamMemberAdded implements Database.Batchable<sObject>, Database.Stateful {

    public static Datetime CurrentDate = Datetime.now();

    global CaseTeamMemberAdded() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator([
            SELECT Id, MemberId, ParentId, TeamRoleId, TeamTemplateMemberId
            FROM CaseTeamMember
            WHERE CreatedDate >= :(CurrentDate.addMinutes(-15))
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<Messaging.SingleEmailMessage> SendEmailsList;
    
        EmailTemplate CaseTeamMemberTemplate;

        CaseTeamMemberTemplate = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'Added_to_a_Case_Team'];

        List<CaseTeamMember> caseTeamMembers = (List<CaseTeamMember>)scope;
        SendEmailsList = new List<Messaging.SingleEmailMessage>();

        Messaging.SingleEmailMessage newMessage = new Messaging.SingleEmailMessage();

        for (CaseTeamMember c : caseTeamMembers) {
        
            newMessage.setTemplateId(CaseTeamMemberTemplate.Id);
            newMessage.setTargetObjectId(c.MemberId);
            newMessage.setSubject(CaseTeamMemberTemplate.Subject);

            if (String.valueOf(c.MemberId).left(3) == '003') {
                newMessage.setWhatId(c.ParentId);
            } else {
                Case caseReference = [SELECT Id, CaseNumber, Subject FROM Case WHERE Id =: c.ParentId];
                String salesforceLink = System.URL.getSalesforceBaseURL().toExternalForm() + '/' + caseReference.Id;
                String newBody = CaseTeamMemberTemplate.Body;
                newBody = newBody.replace('{!Case.Link}', salesforceLink);
                newBody = newBody.replace('{!Case.CaseNumber}', caseReference.CaseNumber);
                newBody = newBody.replace('{!Case.Subject}', caseReference.Subject);
                newMessage.setSaveAsActivity(false);
                newMessage.setPlainTextBody(newBody);
            }

            SendEmailsList.add(newMessage);
        }
            
        Messaging.sendEmail(SendEmailsList, true);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}