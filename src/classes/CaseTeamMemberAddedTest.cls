@isTest
public with sharing class CaseTeamMemberAddedTest {
	public CaseTeamMemberAddedTest() {
		
	}

	@isTest
	static void testEmails() {

		Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Person';
        newContact.Email = 'btenis@acumensolutions.com';
        insert newContact; 
        Contact testContact = [select Name, Email from Contact where Name = 'Test Person']; 
        System.assertEquals(testContact.Name, 'Test Person');

		Case newCase = new Case();
        newCase.Subject = 'Unittest';
        newCase.Status = 'New';
        insert newCase; 
        Case testCase = [select Subject, Status from Case where Subject = 'Unittest']; 
        System.assertEquals(testCase.Subject, 'Unittest');

        CaseTeamRole newRole = [select Id, Name from CaseTeamRole limit 1];

        CaseTeamMember newMember = new CaseTeamMember();
        newMember.ParentId = newCase.Id;
        newMember.MemberId = newContact.Id;
        newMember.TeamRoleId = newRole.Id;
        insert newMember;
        CaseTeamMember testMember = [select ParentId, MemberId, TeamRoleId from CaseTeamMember where ParentId = :newCase.Id]; 
        System.assertEquals(testMember.MemberId, newContact.Id);

        Test.startTest();
        	CaseTeamMemberAdded testBatchEmail = new CaseTeamMemberAdded();
        	Database.executeBatch(testBatchEmail);
        Test.stopTest();
	}
}