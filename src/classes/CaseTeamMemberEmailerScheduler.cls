global class CaseTeamMemberEmailerScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		CaseTeamMemberAdded b = new CaseTeamMemberAdded();
		Id myBatchJobId = database.executeBatch(b, 200);
	}

	public static void setupSchedule() {
		String sch1 = '0 0 * * * ?';
		String sch2 = '0 15 * * * ?';
		String sch3 = '0 30 * * * ?';
		String sch4 = '0 45 * * * ?';
		String jobID1 = system.schedule('CaseTeamMemberEmailerBatch1', sch1, new CaseTeamMemberEmailerScheduler());
		String jobID2 = system.schedule('CaseTeamMemberEmailerBatch2', sch2, new CaseTeamMemberEmailerScheduler());
		String jobID3 = system.schedule('CaseTeamMemberEmailerBatch3', sch3, new CaseTeamMemberEmailerScheduler());
		String jobID4 = system.schedule('CaseTeamMemberEmailerBatch4', sch4, new CaseTeamMemberEmailerScheduler());
	}
}