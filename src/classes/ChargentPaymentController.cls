global with sharing class ChargentPaymentController {
    //****USED BY PAGE TO CREATE CHARGENT ORDER****
	@RemoteAction
    global static ccrz.cc_RemoteActionResult createChargentOrder(ccrz.cc_RemoteActionContext ctx, Map<String,Object> paymentData) {

        //****Flow step - chargent order method called from page, User Bio and Billing info passed to method****
        ccrz.cc_RemoteActionResult res     = ccrz.cc_CallContext.init(ctx);
        res.success = false;

		String cartId = ccrz.cc_CallContext.currCartId;
		Decimal chargeAmount;
		Id cartSfId;
		
		if (String.isNotBlank(cartId)) {
			
	        // Call cart for SMALL data size
	        Map<String, Object> cartRequest = new Map<String, Object>{
	            ccrz.ccAPICart.CART_ENCID => cartId,
	            ccrz.ccAPI.API_VERSION => 1,
	            ccrz.ccAPI.SIZING => new Map<String, Object>{
	                ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
	                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L, // Changed to Large
	                    ccrz.ccAPI.SZ_ASSC => true
	                }
	            }
	        };
	        Map<String,Object> cartResult = ccrz.ccApiCart.fetch(cartRequest);
	        List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) cartResult.get(ccrz.ccAPICart.CART_OBJLIST);
	        
	        chargeAmount = (Decimal) ((Map<String, Object>) ((List<Object>)cartResult.get(ccrz.ccAPICart.CART_OBJLIST))[0]).get('totalAmount');
	        cartSfId = (Id) ((Map<String, Object>) ((List<Object>)cartResult.get(ccrz.ccAPICart.CART_OBJLIST))[0]).get('sfid');
		}
		// else throw an exception
        // End of get Charge Amount 

        try {
            //****Flow Step - Call to create Chargent Order****
            //****Flow Step - Order ID returned from controller****
            Id orderId = ChargentPaymentExt.upsertOrder(cartSfId, paymentData);
            Map<String,Object> orderResult = new Map<String,Object>();
            orderResult.put('orderId',String.valueOf(orderId));
            res.data=orderResult;
            res.success=true;

        } catch (Exception ex) {
            ccrz.ccLog.log(LoggingLevel.ERROR,'mcm - ChargentPaymentController.createChargentOrder',ex);
        } finally{
            ccrz.ccLog.close(res);
        }

        return res;

    }
  
	//****USED BY PAGE TO CHARGE PAYMENT****
	@RemoteAction
    global static ccrz.cc_RemoteActionResult chargePayment(ccrz.cc_RemoteActionContext ctx, String orderId, Map<String,Object> paymentData) {

        //****Flow step - chargent payment method called from page, Order ID and Payment Data passed to method****
        ccrz.cc_RemoteActionResult res     = ccrz.cc_CallContext.init(ctx);
        res.success = false;

        Map<String,Object> orderResult = new Map<String,Object>();

		// Get the Charge Amount
		String cartId = ccrz.cc_CallContext.currCartId;
		Decimal chargeAmount;
		
		if (String.isNotBlank(cartId)) {
			
	        // Call cart for SMALL data size
	        Map<String, Object> cartRequest = new Map<String, Object>{
	            ccrz.ccAPICart.CART_ENCID => cartId,
	            ccrz.ccAPI.API_VERSION => 1,
	            ccrz.ccAPI.SIZING => new Map<String, Object>{
	                ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
	                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L, // Changed to Large
	                    ccrz.ccAPI.SZ_ASSC => true
	                }
	            }
	        };
	        Map<String,Object> cartResult = ccrz.ccApiCart.fetch(cartRequest);
	        List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) cartResult.get(ccrz.ccAPICart.CART_OBJLIST);
	        
	        chargeAmount = (Decimal) ((Map<String, Object>) ((List<Object>)cartResult.get(ccrz.ccAPICart.CART_OBJLIST))[0]).get('totalAmount');
		}
		// else throw an exception
        // End of get Charge Amount
                
        try {
            //****Flow step - controller invokes Chargent Charge API****
            ChargentOrders.TChargentOperations.TChargentResult result = ChargentPaymentExt.chargeOrder3(orderId, paymentData, String.valueOf(chargeAmount)); // Get the chargeAmount from the cart!
            
            ChargentOrders__Transaction__c tx = [Select Id, ChargentOrders__Response_Status__c, ChargentOrders__Response_Message__c from ChargentOrders__Transaction__c where Id = :result.TransactID];
            if ('Error' == tx.ChargentOrders__Response_Status__c) {
            	res.success=false;
                orderResult.put('orderId',tx.ChargentOrders__Response_Message__c);
                res.data=orderResult;
	    		throw new MyException(tx.ChargentOrders__Response_Message__c);
            }
            
            orderResult.put('orderId','payment charged');
            res.data=orderResult;
            res.success=true;

        } catch (Exception ex) {
            ccrz.ccLog.log(LoggingLevel.ERROR,'mcm - ChargentPaymentController.chargePayment',ex);
        } finally{
            ccrz.ccLog.close(res);
        }

        return res;
    }

	public class MyException extends Exception {}
}