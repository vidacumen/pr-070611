public class ChargentPaymentExt {
	
	public static Id orderId;

    //****USED BY CREATE CHARGENT PAYMENT IN CONTROLLER****
    public static ChargentOrders.TChargentOperations.TChargentResult chargeOrder3(String orderId, Map<String,Object> paymentData, String chargeAmount) {
        ChargentOrders.TChargentOperations.TChargentResult result = ChargentOrders.TChargentOperations.ChargeOrder_ClickJSON(
        '{'
        +'"ObjectId":"'+orderId+'"'
        +',"CardMonth":"'+(String) paymentData.get('expirationMonth')+'"'
        +',"CardYear":"'+(String) paymentData.get('expirationYear')+'"'
        +',"CardType":"'+(String) paymentData.get('paymentType')+'"'
        +',"CardNumber":"'+(String) paymentData.get('accountNumber')+'"'
        +',"ChargeAmount":"'+chargeAmount+'"'
        +'}'
        );

        //****Flow step - controller passes Chargent Charge API results to page****
        return result;
        
    }

    //****USED BY CREATE CHARGENT ORDER IN CONTROLLER****
    public static String upsertOrder(Id cartSfId, Map<String,Object> paymentData) {
		ChargentOrders__ChargentOrder__c order;
		
		List<ChargentOrders__ChargentOrder__c> orderList = [Select Id, Name, CloudCraze_Cart__c from ChargentOrders__ChargentOrder__c where CloudCraze_Cart__c = :cartSfId ORDER BY CreatedDate DESC];
		
		if (orderList.size() > 1) {
		   throw new MyException('There should only be one Chargent order on the cart.');
		} else if (orderList.size() == 1) {
		   order = orderList.get(0);
		}
		else {
	        order = new ChargentOrders__ChargentOrder__c();        
	        order.ChargentOrders__Payment_Method__c = 'Credit Card';
			order.CloudCraze_Cart__c = cartSfId; // Link to the CloudCraze cart 
		}
		
        order.ChargentOrders__Billing_First_Name__c = (String) paymentData.get('billingFirstName');
        order.ChargentOrders__Billing_Last_Name__c = (String) paymentData.get('billingLastName');
        order.ChargentOrders__Billing_Address__c = (String) paymentData.get('billingAddress');
        order.ChargentOrders__Billing_City__c = (String) paymentData.get('billingCity');
        order.ChargentOrders__Billing_State__c = (String) paymentData.get('billingState');
        order.ChargentOrders__Billing_Zip_Postal__c = (String) paymentData.get('billingZip');
        order.ChargentOrders__Billing_Country__c = (String) paymentData.get('billingCountry');
        
        //order.ChargentOrders__Subtotal__c = 8.99; // ** NEED TO ALSO POPULATE THE SUBTOTAL
		
		upsert order;

		ChargentOrders__ChargentOrder__c orderAfterUpsert = [SELECT Id,
																	Name
															FROM ChargentOrders__ChargentOrder__c
															WHERE Id =: order.Id];

        order.ChargentOrders__Order_Information__c = orderAfterUpsert.Name;
        order.ChargentOrders__Invoice_Number__c = String.valueOf(cartSfId).substring(0, 15);
        
		update order;

		System.debug('order: ' + order);

        return order.id;
    }

	public class MyException extends Exception {}

}