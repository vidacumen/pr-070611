global with sharing class Chargent_hk_Order extends ccrz.cc_hk_Order {

    global override Map<String,Object> place(Map<String,Object> inputData) {
    	
    	Map<String, Object> retData = super.place(inputData);
    	
        final String placeStep = (String) inputData.get(ccrz.cc_hk_Order.PARAM_PLACE_STEP);
        if (ccrz.cc_hk_Order.STEP_END.equals(placeStep)) {
            ccrz__E_Order__c theOrder = (ccrz__E_Order__c) inputData.get(ccrz.cc_hk_Order.PARAM_ORDER);
            if (null != theOrder) {
	        	Id cartId = (Id) inputData.get(ccrz.cc_hk_Order.PARAM_CART_ID);
	        	ccrz__E_Cart__c cart = (ccrz__E_Cart__c) inputData.get(ccrz.cc_hk_Order.PARAM_CART);
	        	Decimal cartTotal = (Decimal) cart.get('ccrz__TotalAmount__c');
	        	
	        	// Link the Chargent and CloudCraze orders
	        	//** Should not limit 1?
	        	if (cartTotal > 0) {
	        		ChargentOrders__ChargentOrder__c order = [Select Id, Name, CloudCraze_Cart__c from ChargentOrders__ChargentOrder__c where CloudCraze_Cart__c = :cartId ORDER BY CreatedDate DESC limit 1];
		        	order.CloudCraze_Order__c = theOrder.Id;

		        	update order;
		        }
	        	
	        	theOrder = (ccrz__E_Order__c) inputData.get(ccrz.cc_hk_Order.PARAM_ORDER_W_STATUS);
	        	
	        	List<ccrz__E_OrderItem__c> theOrderItems = (List<ccrz__E_OrderItem__c>) inputData.get(ccrz.cc_hk_Order.PARAM_ORDER_ITEMS);

	        	//Set<Id> theOrderItemIds = (new Map<Id,SObject>(theOrderItems)).keySet();

	        	List<ccrz__E_CartItem__c> theCartItems = (List<ccrz__E_CartItem__c>) inputData.get(ccrz.cc_hk_Order.PARAM_CART_ITEMS);

	        	//List<ccrz__E_OrderItem__c> theOrderMinorItems = (List<ccrz__E_OrderItem__c>) inputData.get(ccrz.cc_hk_Order.PARAM_ORDER_CHILD_ITEMS);

	        	try {
	        		updateRegistrations(cartId, theCartItems, theOrderItems);
        		} catch (Exception ex) {
        			ccrz.ccLog.log(LoggingLevel.INFO,'Error updating Registrations - ', ex.getMessage() + ' ' + ex.getLineNumber());
        		}

	        	
	        	//throw new MyException('Simulated Order Creation Failure!'); 
	        	
            }
        } else if (ccrz.cc_hk_Order.STEP_UPDATE_ORDER_STATUS_PRE.equals(placeStep)) {
            ccrz__E_Order__c theOrder = (ccrz__E_Order__c) inputData.get(ccrz.cc_hk_Order.PARAM_ORDER_W_STATUS);
            if (null != theOrder) {
	        	theOrder.ccrz__OrderStatus__c = 'Order Paid';
	        	//retData.put(ccrz.cc_hk_Order.PARAM_ORDER_W_STATUS, theOrder);
            }
        }
        return retData;
    }

    public class MyException extends Exception {}

    public void updateRegistrations(Id cartId, List<ccrz__E_CartItem__c> cartItems, List<ccrz__E_OrderItem__c> orderItems) {
    	Set<Id> orderItemIds = (new Map<Id,SObject>(orderItems)).keySet();
    	Set<Id> cartItemIds = (new Map<Id,SObject>(cartItems)).keySet();

    	List<Registration__c> registrations = [SELECT Id,
				    								Assessment_Order__c,
				    								Order_Item_ID__c,
				    								Cart__c,
				    								Cart_Item_ID__c,
				    								Payment_Date__c,
				    								Form__c,
				    								Pacing__c
					    						FROM Registration__c
					    						WHERE Cart__c =: cartId];

		System.debug('regs: ' + registrations);

		List<ccrz__E_OrderItem__c> oi = [SELECT Id,
												ccrz__Order__c,
												ccrz__ParentOrderItem__c,
												ccrz__ParentOrderItem__r.ccrz__OrderItemId__c,
												ccrz__PrimaryAttr__c,
												ccrz__SecondaryAttr__c
										FROM ccrz__E_OrderItem__c
										WHERE ccrz__ParentOrderItem__c IN :orderItemIds
										AND ccrz__ParentOrderItem__r.ccrz__OrderItemId__c IN :cartItemIds];

		System.debug('order items: ' + oi);

		List<ccrz__E_CartItem__c> ci = [SELECT Id,
												ccrz__ParentCartItem__c,
												ccrz__PrimaryAttr__c,
												ccrz__SecondaryAttr__c
										FROM ccrz__E_CartItem__c
										WHERE 	ccrz__ParentCartItem__c IN :cartItemIds];

		System.debug('cart items: ' + ci);

		List<Registration__c> registrationsToUpate = new List<Registration__c>();

		for (Registration__c reg : registrations) {
			for (ccrz__E_CartItem__c citem : ci) {
				for (ccrz__E_OrderItem__c oitem : oi) {
					if ((reg.Cart_Item_ID__c == citem.Id) 
						&& (oitem.ccrz__ParentOrderItem__r.ccrz__OrderItemId__c == citem.ccrz__ParentCartItem__c) 
						&& (oitem.ccrz__PrimaryAttr__c == reg.Form__c)
						&& (oitem.ccrz__SecondaryAttr__c == reg.Pacing__c)) {
						
						if (!registrationsToUpate.contains(reg)) {
							reg.Assessment_Order__c = oitem.ccrz__Order__c;
							reg.Order_Item_ID__c = oitem.Id;
							reg.Payment_Date__c = DateTime.now();

							registrationsToUpate.add(reg);
						}
					}
				}
			}
		}

		update registrationsToUpate;
    }
    
}