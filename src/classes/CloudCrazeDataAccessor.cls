public with sharing class CloudCrazeDataAccessor implements CloudCrazeDAO {
	public static List<CCResponse> getCCCategories(String communityStorefront) {
		Map<String,Object> readRes = ccrz.ccApiPublicCache.fetch(new Map<String,Object>{
			ccrz.ccApi.API_VERSION => 1
			,ccrz.ccApiPublicCache.CACHE_NAME => ccrz.ccCategoryCacheBuilder.CACHE_NAME
			,ccrz.ccApiPublicCache.STORE_NAME => communityStorefront
			,ccrz.ccApiPublicCache.LOCALE => 'en_US'
		});

		System.debug(communityStorefront);
		System.debug(readRes);

		List<CCResponse> r = CCResponse.parse((String)readRes.get(ccrz.ccApiPublicCache.CONTENT));

		return r;
	}
}