public with sharing class CloudCrazeSettings {
	@AuraEnabled
	public String getCommunityURLPathName() {
		CloudCraze_Redirect_Setting__mdt ccSetting = [SELECT Community_Path__c
														FROM CloudCraze_Redirect_Setting__mdt
														LIMIT 1
		];
		return ccSetting.Community_Path__c;
	}

	@AuraEnabled
	public String getCommunityStorefront() {
		CloudCraze_Redirect_Setting__mdt ccSetting = [SELECT Community_Storefront__c
														FROM CloudCraze_Redirect_Setting__mdt
														LIMIT 1
		];
		return ccSetting.Community_Storefront__c;
	}
}