@isTest
public with sharing class CloudCrazeSettingsTest {
	
	@isTest
	static void getCommunityURLPathName_CallMethod_CommunityPathReturned() {
		Test.startTest();
		CloudCrazeSettings ccrz = new CloudCrazeSettings();
		String result = ccrz.getCommunityURLPathName();
		Test.stopTest();

		System.assertNotEquals(result, null);
	}

	@isTest
	static void getCommunityStorefront_CallMethod_CommunityStorefrontReturned() {
		Test.startTest();
		CloudCrazeSettings ccrz = new CloudCrazeSettings();
		String result = ccrz.getCommunityStorefront();
		Test.stopTest();

		System.assertNotEquals(result, null);
	}
}