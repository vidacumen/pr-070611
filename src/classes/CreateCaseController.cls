public without sharing class CreateCaseController {

	@AuraEnabled
	public static String createCaseForRegistration(Id registrationId, 
													String subject, 
													String description) {
		String result = '';
		Case c = new Case();

		c.Registration__c = registrationId;
		c.Status = 'New';
		//c.Status = status;
		c.Origin = 'Community';
		c.Reason = 'New Issue';
		c.Priority = 'Medium';
		//c.Priority = priority;
		c.Category__c = 'General Inquiry';
		c.Sub_Category__c = null;
		c.System__c = null;
		c.Program__c = 'Other';
		c.Delivery_Type__c = 'Other';
		c.Subject = subject;
		c.Description = description;

		try {
			insert c;

			Case newCase = [SELECT CaseNumber
							FROM Case
							WHERE Id =: c.Id];

			result = newCase.CaseNumber;
		} catch (Exception ex) {
			result = 'Error: ' + ex.getMessage() + ' ' + ex.getLineNumber();
		}
		
		return result;
	}

	//@AuraEnabled
	//public static List<PicklistOptions> queryForCaseStatusValues() {
	//	List<PicklistOptions> lstPickvals = new List<PicklistOptions>();
	//	Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
	//	List<Schema.PicklistEntry> caseStatusValues = fieldResult.getPicklistValues();

	//	for(Schema.PicklistEntry ple : caseStatusValues) {
	//		PicklistOptions option = new PicklistOptions();
	//		option.label = ple.getLabel();
	//		option.value = ple.getValue();
	//		lstPickvals.add(option);
	//	}

	//	return lstPickvals;
	//}

	//@AuraEnabled
	//public static List<PicklistOptions> queryForCasePriorityValues() {
	//	List<PicklistOptions> lstPickvals = new List<PicklistOptions>();
	//	Schema.DescribeFieldResult fieldResult = Case.Priority.getDescribe();
	//	List<Schema.PicklistEntry> casePriorityValues = fieldResult.getPicklistValues();

	//	for(Schema.PicklistEntry ple : casePriorityValues) {
	//		PicklistOptions option = new PicklistOptions();
	//		option.label = ple.getLabel();
	//		option.value = ple.getValue();
	//		lstPickvals.add(option);
	//	}

	//	return lstPickvals;
	//}
}