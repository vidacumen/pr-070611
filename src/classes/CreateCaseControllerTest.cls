/**
 * Created by aszampias on 8/17/18.
 */
@isTest
public with sharing class CreateCaseControllerTest {

	@isTest static void CreateCaseController_createCaseForRegistration_RunSuccessful(){
		Registration__c r = new Registration__c();
		String result = CreateCaseController.createCaseForRegistration(r.Id, 'Three Cs', 'We have three cs');
		Test.startTest();
			System.assertEquals(true, result.isNumeric());
		Test.stopTest();
	}
}