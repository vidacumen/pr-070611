public with sharing class CreateTestData {

	public static String createFakeId(String sObjectName, Integer i) {
		String idValue = String.valueOf(i);
		String keyPrefix = Schema.getGlobalDescribe().get(sObjectName).getDescribe().getKeyPrefix();
		return keyPrefix + idValue.leftPad(12, '0');
	}

}