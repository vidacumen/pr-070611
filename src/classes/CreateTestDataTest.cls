/**
 * Created by aszampias on 8/17/18.
 */
@isTest
public with sharing class CreateTestDataTest {
	@isTest static void CreateTestData_CreateFakeId_ReturnId(){
		Id idCustomDoc = CreateTestData.createFakeId('CustomDocument__c', 5);
		Test.startTest();
		String keyPrefix = Schema.getGlobalDescribe().get('CustomDocument__c').getDescribe().getKeyPrefix();
		System.assertEquals(keyPrefix + '000000000005', idCustomDoc);
		Test.stopTest();
	}

}