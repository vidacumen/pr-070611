global class CustomCouponValidationCtrl {
	global CustomCouponValidationCtrl() {}

	@RemoteAction
	global static String validateCoupon(String couponCode, String cartId) {
		//Put your logic here, to check validity of coupon before applying coupon.
		String isValid = 'false';

		ccrz__E_Coupon__c coupon = queryCouponByCode(couponCode);

		List<ccrz__E_CartItem__c> cartItems = queryCartItemsByCartId(cartId);

		if (coupon != null) {
			for (ccrz__E_CartItem__c cartItem : cartItems) {
				if (cartItem.ccrz__PrimaryAttr__r.Id == coupon.Target_Primary_Attribute__r.Id || cartItem.ccrz__Product__r.ccrz__PrimaryAttr__c == coupon.Target_Primary_Attribute__c) {
					isValid = 'true';
					break;
				}
			}
		} else {
			isValid = 'invalid';
		}

		return isValid;
	}

	public static ccrz__E_Coupon__c queryCouponByCode(String couponCode) {
		try {
			return [
						SELECT Id, ccrz__CouponCode__c, Target_Primary_Attribute__r.Id 
						FROM ccrz__E_Coupon__c 
						WHERE ccrz__CouponCode__c = :couponCode 
						AND Target_Primary_Attribute__c != null
					];
		} catch (Exception e) {
			return null;
		}
	}

	public static List<ccrz__E_CartItem__c> queryCartItemsByCartId(String cartId) {
		return [
					SELECT Id, ccrz__Cart__c, ccrz__PrimaryAttr__r.Id, ccrz__Product__c, ccrz__Product__r.ccrz__PrimaryAttr__c, 
						ccrz__Cart__r.ccrz__CartId__c, ccrz__Cart__r.ccrz__EncryptedId__c 
					FROM ccrz__E_CartItem__c 
					WHERE ccrz__Cart__r.ccrz__EncryptedId__c = :cartId
				];
	}
}