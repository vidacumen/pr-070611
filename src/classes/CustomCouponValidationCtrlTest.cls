/**
 * Created by aszampias on 8/19/18.
 */
@isTest
public with sharing class CustomCouponValidationCtrlTest {
	@isTest static void CustomCouponValidation_queryCouponByCode_ReturnCouponCode(){
		ccrz__E_Attribute__c attr = new ccrz__E_Attribute__c();
		insert attr;
		ccrz__E_Coupon__c coupon1 = new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'ZZ77336RQ1',
														  ccrz__CouponName__c = 'Panda',
														  ccrz__MaxUse__c =  5,
														  ccrz__TotalUsed__c = 0,
														  Target_Primary_Attribute__c = attr.Id);
		insert coupon1;
		ccrz__E_Coupon__c coupon = CustomCouponValidationCtrl.queryCouponByCode('ZZ77336RQ1');
		Test.startTest();
			System.assertNotEquals(null, coupon);
			System.assertEquals('ZZ77336RQ1', coupon.ccrz__CouponCode__c);
		Test.stopTest();
	}
	@isTest static void CustomCouponValidate_queryCartItemsByCartId_ReturnCartItems(){
		ccrz__E_Cart__c BigCart = new ccrz__E_Cart__c(ccrz__EncryptedId__c = 'string');
		insert BigCart;
		ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c(ccrz__Price__c = 15.00, ccrz__Cart__c = BigCart.id);
		insert cartItem;

		Test.startTest();
			List<ccrz__E_CartItem__c> cartItems = CustomCouponValidationCtrl.queryCartItemsByCartId(BigCart.ccrz__EncryptedId__c);
			System.debug(cartItems + ' Amanda Over here');
			System.assertNotEquals(null, cartItems);
		Test.stopTest();
	}

	@isTest static void validateCoupon_ReturnInvalid(){
		ccrz__E_Cart__c BigCart = new ccrz__E_Cart__c(ccrz__EncryptedId__c = 'string');
		insert BigCart;
		ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c(ccrz__Price__c = 15.00, ccrz__Cart__c = BigCart.id);
		insert cartItem;

		ccrz__E_Attribute__c attr = new ccrz__E_Attribute__c();
		insert attr;
		ccrz__E_Coupon__c coupon1 = new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'WrongCode',
				ccrz__CouponName__c = 'Panda',
				ccrz__MaxUse__c =  5,
				ccrz__TotalUsed__c = 0,
				Target_Primary_Attribute__c = attr.Id);
		insert coupon1;
		String validCode = CustomCouponValidationCtrl.validateCoupon('ZZ77336RQ1', cartItem.id);
		Test.startTest();
			System.assertEquals('invalid', validCode);
		Test.stopTest();
	}

	@isTest static void validateCoupon_ReturnFalse(){
		ccrz__E_Cart__c BigCart = new ccrz__E_Cart__c(ccrz__EncryptedId__c = 'string');
		insert BigCart;
		ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c(ccrz__Price__c = 15.00, ccrz__Cart__c = BigCart.id);
		insert cartItem;

		ccrz__E_Attribute__c attr = new ccrz__E_Attribute__c();
		insert attr;
		ccrz__E_Coupon__c coupon1 = new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'ZZ77336RQ1',
														  ccrz__CouponName__c = 'Panda',
														  ccrz__MaxUse__c =  5,
														  ccrz__TotalUsed__c = 0,
														  Target_Primary_Attribute__c = attr.Id);
		insert coupon1;
		String validCode = CustomCouponValidationCtrl.validateCoupon('ZZ77336RQ1', cartItem.id);
		Test.startTest();
			System.assertEquals('false', validCode);
		Test.stopTest();
	}
}