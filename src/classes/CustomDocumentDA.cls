public with sharing class CustomDocumentDA implements CustomDocumentDAI {
	public List<CustomDocument__c> queryForDocumentsByRegistrationId(Id registrationId) {
		return [SELECT Name,
						CreatedDate,
						Document_Type__c
				FROM CustomDocument__c
				WHERE Registration_ID__c =: registrationId
				ORDER BY Document_Type__c, Name];
	}

	public List<AggregateResult> queryForDocumentTypesByRegistrationId(Id registrationId) {
		return [SELECT Document_Type__c
				FROM CustomDocument__c
				WHERE Registration_ID__c =: registrationId
				GROUP BY Document_Type__c];
	}
}