public interface CustomDocumentDAI {
	List<CustomDocument__c> queryForDocumentsByRegistrationId(Id registrationId);
	List<AggregateResult> queryForDocumentTypesByRegistrationId(Id registrationId);
}