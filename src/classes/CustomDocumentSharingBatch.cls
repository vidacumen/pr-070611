global class CustomDocumentSharingBatch implements Database.Batchable<sObject> {

	global String query;

	global CustomDocumentSharingBatch() {
		query = 'SELECT Id, Examinee__c, Examinee__r.PersonContactId, Examinee__r.IsCustomerPortal, Examinee__r.RecordTypeId, Grant_Examinee_Access__c, Examinee_Access_Status__c'+
				' FROM CustomDocument__c'+
				' WHERE Examinee__r.IsCustomerPortal = true'+
				' AND ((Grant_Examinee_Access__c = true'+
				' AND Examinee_Access_Status__c != \'Granted\')'+
				' OR (Grant_Examinee_Access__c = false'+
				' AND Examinee_Access_Status__c != \'None\'))';

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<CustomDocument__c> scope) {
		CustomSharingHelper.updateCustomDocumentSharing(scope);
	}

	global void finish(Database.BatchableContext BC) {
		
	}

}