public class CustomDocumentSharingHelper {

	public static final Id EXAMINEE_REC_TYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
	public static final Map<String, String> PICKLIST_OPTIONS = new Map<String, String> {
		'none' => 'None',
		'granted' => 'Granted',
		'manual' => 'Pending User',
		'pending' => 'Pending Access',
		'remove' => 'Pending Removal',
		'batch' => 'Pending Batch',
		'error' => 'Error'
	};

	/**
	 * [afterInsertUpdateExamineeSharing description]
	 * @param triggerNew [description]
	 */
	public static void afterInsertUpdateExamineeSharing(List<CustomDocument__c> triggerNew) {
		Set<Id> documentIds = new Set<Id>();
		Map<Id, Boolean> documentProcessMap = new Map<Id, Boolean>();
		for (CustomDocument__c cd : triggerNew) {
			if (cd.Examinee__c != null && cd.Grant_Examinee_Access__c) {
				documentIds.add(cd.Id);
				documentProcessMap.put(cd.Id, true);
			}
		}

		if (documentIds.size() > 0) {
			processDocumentSharingRequestFuture(documentIds, documentProcessMap);
		}
	}

	/**
	 * [afterUpdateUpdateExamineeSharing description]
	 * @param triggerNew    [description]
	 * @param triggerOldMap [description]
	 */
	public static void afterUpdateUpdateExamineeSharing(List<CustomDocument__c> triggerNew, Map<Id, CustomDocument__c> triggerOldMap) {
		Set<Id> documentIds = new Set<Id>();
		Map<Id, Boolean> documentProcessMap = new Map<Id, Boolean>();
		for (CustomDocument__c cd : triggerNew) {
			if ((cd.Grant_Examinee_Access__c && !triggerOldMap.get(cd.Id).Grant_Examinee_Access__c) || (cd.Grant_Examinee_Access__c && cd.Examinee_Access_Status__c != PICKLIST_OPTIONS.get('granted'))) {
				documentIds.add(cd.Id);
				documentProcessMap.put(cd.Id, true);
			} else if (!cd.Grant_Examinee_Access__c && triggerOldMap.get(cd.Id).Grant_Examinee_Access__c) {
				documentIds.add(cd.Id);
				documentProcessMap.put(cd.Id, false);
			}
		}

		if (documentIds.size() > 0) {
			processDocumentSharingRequestFuture(documentIds, documentProcessMap);
		}
	}

	@future
	public static void processDocumentSharingRequestFuture(Set<Id> documentIds, Map<Id, Boolean> documentProcessMap) {
		//Step 1: Query Cases.
		List<CustomDocument__c> documents = queryDocumentsById(documentIds);

		processDocumentSharingRequest(documents, documentProcessMap);
	}

	/**
	 * [processDocumentSharingRequest description]
	 * @param documents          [description]
	 * @param documentProcessMap [description]
	 */
	public static void processDocumentSharingRequest(List<CustomDocument__c> documents, Map<Id, Boolean> documentProcessMap) {
		//Step 2: Extract Set of Person Contact Ids
		Set<Id> contactIds = extractPersonContactIds(documents);

		//Step 3: Query Users with those person contact Ids
		List<User> examinees = queryUsersByPersonContactId(contactIds);

		//Step 4: Map users to Contact I
		Map<Id, User> contactIdToUserMap = mapUsersToContactId(examinees);

		Set<Id> deleteIds = new Set<Id>();
		for (CustomDocument__c c : documents) {
			if (!documentProcessMap.get(c.Id)) {
				deleteIds.add(c.Id);
			}
		}

		Map<Id, CustomDocument__Share> shareMap = CalculateApexSharing.calculateDocumentSharingDelete(deleteIds, contactIdToUserMap);

		//Step 5: Loop over cases, extract user from map
		List<CustomDocument__Share> newDocumentShareRecords = new List<CustomDocument__Share>();
		Map<Id, CustomDocument__Share> oldDocumentShareRecords = new Map<Id, CustomDocument__Share>();
		Map<Id, CustomDocument__c> documentUpdates = new Map<Id, CustomDocument__c>();
		for (CustomDocument__c c : documents) {
			User u = contactIdToUserMap.get(c.Examinee__r.PersonContactId);
			if (documentProcessMap.get(c.Id)) {
				newDocumentShareRecords.add(CalculateApexSharing.calculateDocumentSharing(c.Id, u.Id));
				documentUpdates.put(c.Id, new CustomDocument__c(Id=c.Id, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('granted')));
			} else {
				if (shareMap.containsKey(c.Id)) {
					CustomDocument__Share oldCs = shareMap.get(c.Id);
					oldDocumentShareRecords.put(oldCs.Id, oldCs);
					documentUpdates.put(c.Id, new CustomDocument__c(Id=c.Id, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('none')));
				}
				//CustomDocument__Share oldCs = CalculateApexSharing.calculateDocumentSharingDelete(c.Id, u.Id);
				//System.debug('OLD CS: ' + oldCs);
				//if (oldCs != null) {
				//	oldDocumentShareRecords.put(oldCs.Id, oldCs);
				//	documentUpdates.put(c.Id, new CustomDocument__c(Id=c.Id, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('none')));
				//}
			}
		}

		//Step 6: Delete old sharing records
		if (oldDocumentShareRecords.keySet().size() > 0) {
			List<Database.DeleteResult> drList = Database.Delete(oldDocumentShareRecords.values(), false);
			for (Database.DeleteResult dr : drList) {
				if (!dr.isSuccess()) {
					CustomDocument__Share cs = oldDocumentShareRecords.get(dr.getId());
					documentUpdates.put(cs.ParentId, new CustomDocument__c(Id=cs.ParentId, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('error')));
				}
			}
		}

		//Step 7: Insert list of CaseShare records (use allornothing = false and get save result to update cases properly)
		if (newDocumentShareRecords.size() > 0) {
			List<Database.SaveResult> srList = Database.Insert(newDocumentShareRecords, false);
			for (CustomDocument__Share cs : newDocumentShareRecords) {
				if (cs.Id == null) {
					documentUpdates.put(cs.ParentId, new CustomDocument__c(Id=cs.ParentId, Examinee_Access_Status__c=PICKLIST_OPTIONS.get('error')));
				}
			}
		}

		//Step 8: Use Save Results to update case Examinee_Access_Status__c picklist.
		CustomDocumentTriggerFacade.skipTrigger = true;
		update documentUpdates.values();
	}

	public static List<CustomDocument__c> queryDocumentsById(Set<Id> documentIds) {
		return [
					SELECT Id, Examinee__c, Examinee__r.PersonContactId, Examinee__r.RecordTypeId, Grant_Examinee_Access__c, Examinee_Access_Status__c 
					FROM CustomDocument__c 
					WHERE Id IN :documentIds 
					AND Examinee__r.RecordTypeId = :EXAMINEE_REC_TYPE
				];
	}

	public static Set<Id> extractPersonContactIds(List<CustomDocument__c> documents) {
		Set<Id> contactIds = new Set<Id>();
		for (CustomDocument__c c : documents) {
			contactIds.add(c.Examinee__r.PersonContactId);
		}

		return contactIds;
	}

	public static List<User> queryUsersByPersonContactId(Set<Id> pContactIds) {
		return [
					SELECT Id, ContactId 
					FROM User 
					WHERE ContactId IN :pContactIds
				];
	}

	public static Map<Id, User> mapUsersToContactId(List<User> users) {
		Map<Id, User> userMap = new Map<Id, User>();
		for (User u : users) {
			userMap.put(u.ContactId, u);
		}

		return userMap;
	}


}