@isTest
private class CustomDocumentSharingTest {

	@testSetup
	static void setup() {
		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Id examineeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
		Account newPersonAcct = new Account(FirstName = 'Test'
												, LastName = 'Testing'
												, PersonEmail = 'test123@noemail.com'
												, BillingCountry = 'United States'
												, BillingState = 'NY'
												, BillingStreet = '101 Main Street'
												, BillingCity = 'New York'
												, BillingPostalCode = '10027'
												, RecordTypeId = examineeRecordTypeId
											);
		Insert newPersonAcct;

		Account acct = [SELECT Id, PersonContactId FROM Account WHERE Id = :newPersonAcct.Id];

		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User user = new User(Alias = 'test123', Email='test123@noemail.com',
				EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
				LocalesIdKey='en_US', ProfileId = p.Id, Country='United States',IsActive =true,
				ContactId = acct.PersonContactId,
				TimezonesIdKey='America/Los_Angeles', Username='tester@noemail.com');
		insert user;

	}

	@isTest
	static void test_method_one() {
		User u = [SELECT Id FROM User WHERE Username = 'tester@noemail.com'];
		Account a = [SELECT PersonContactId FROM Account WHERE PersonEmail = 'test123@noemail.com'];
		CustomDocument__c cd = new CustomDocument__c();
		//System.runAs(u) {
			cd.Grant_Examinee_Access__c = true;
			cd.Name = 'Test';
			cd.Examinee__c = a.Id;

			insert cd;
		//}

		CustomDocument__c cd2 = new CustomDocument__c(Id = cd.Id, Grant_Examinee_Access__c = false);
		update cd2;

	}
}