public class CustomDocumentTriggerFacade {

	public static Boolean skipTrigger = false;

	public static void onAfterInsert(List<CustomDocument__c> triggerNew) {
		CustomDocumentSharingHelper.afterInsertUpdateExamineeSharing(triggerNew);
	}

	public static void onAfterUpdate(List<CustomDocument__c> triggerNew, Map<Id, CustomDocument__c> triggerOldMap) {
		CustomDocumentSharingHelper.afterUpdateUpdateExamineeSharing(triggerNew, triggerOldMap);
		//CustomSharingHelper.updateCustomDocumentSharing(triggerNew);
	}

}