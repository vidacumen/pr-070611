public class CustomSharingHelper {

	public static final Map<String, String> PICKLIST_OPTIONS = new Map<String, String> {
		'none' => 'None',
		'granted' => 'Granted',
		'manual' => 'Pending User',
		'pending' => 'Pending Access',
		'remove' => 'Pending Removal',
		'batch' => 'Pending Batch',
		'error' => 'Error'
	};

	/**
	 * [updateCustomDocumentSharing description]
	 * @param documents [description]
	 */
	public static void updateCustomDocumentSharing(List<CustomDocument__c> documents) {
		//Step 1: Build Processes Map
		Map<Id, Boolean> documentProcessMap = buildProcessMap(documents);

		CustomDocumentSharingHelper.processDocumentSharingRequest(documents, documentProcessMap);
	}

	/**
	 * [updateCaseSharing description]
	 * @param cases [description]
	 */
	public static void updateCaseSharing(List<Case> cases) {
		//Step 1: Build Processes Map
		Map<Id, Boolean> caseProcessMap = buildProcessMap(cases);

		CaseSharingHelper.processCaseSharingRequest(cases, caseProcessMap);
	}

	/**
	 * [buildProcessMap description]
	 * @param  objects [description]
	 * @return         [description]
	 */
	public static Map<Id, Boolean> buildProcessMap(List<SObject> objects) {
		Map<Id, Boolean> processMap = new Map<Id, Boolean>();
		for (SObject so : objects) {
			if (Boolean.valueOf(so.get('Grant_Examinee_Access__c'))) {
				processMap.put(String.valueOf(so.get('Id')), true);
			} else {
				processMap.put(String.valueOf(so.get('Id')), false);
			}
		}

		return processMap;
	}

}