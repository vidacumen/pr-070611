/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_AccountTriggerHandler implements DSE.DS_Classes.ICloudMDMTriggerInterface {
    global static DSE.API_AccountTriggerHandler.TRIGGER_FAILURE_STATUS AccountTriggerState;
    global API_AccountTriggerHandler() {

    }
    global void Trigger_DeleteAfter_Handler() {

    }
    global void Trigger_DeleteBefore_Handler() {

    }
    global void Trigger_InsertAfter_Handler() {

    }
    global void Trigger_InsertBefore_Handler() {

    }
    global void Trigger_UpdateAfter_Handler() {

    }
    global void Trigger_UpdateBefore_Handler() {

    }
    global void disableCloudMDMTrigger() {

    }
    global void enableCloudMDMTrigger() {

    }
    global void executeTrigger() {

    }
    global Boolean getCloudMDMTriggerStatus() {
        return null;
    }
    global void initTrigger() {

    }
    global static void setForceSkipAccountTrigger(Boolean state) {

    }
    global static void setForceSkipBeanTrigger(Boolean state) {

    }
global class DS_CloudMDMPartialTriggerFailureException extends Exception {
}
global class DS_CloudMDMSkipTriggerException extends Exception {
}
global class DS_CloudMDMTriggerAPIException extends Exception {
}
global enum TRIGGER_FAILURE_STATUS {TRIGGER_AFTER_ROLLBACK, TRIGGER_DEFAULT, TRIGGER_FAILED, TRIGGER_ROLLBACK_INPROGRESS}
}
