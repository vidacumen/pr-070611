/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_Compare {
    global API_Compare() {

    }
    global static Decimal getScore(Id sobjectId1, Id sobjectId2, String settingName) {
        return null;
    }
    global static Decimal getScore(Id sobjectId1, Id sobjectId2, Boolean isPartial, Decimal threshold, String settingName) {
        return null;
    }
    global static Boolean isDuplicate(Id sobjectId1, Id sobjectId2, String settingName) {
        return null;
    }
    global static Boolean isMatch(Id sobjectId1, Id sobjectId2, String settingName) {
        return null;
    }
global class DS_CloudMDMCompareException extends Exception {
}
}
