/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_CustomScoutClass {
    global API_CustomScoutClass(String aObjName) {

    }
    global void triggerSynchronize(List<SObject> newObjects, List<SObject> oldObjects, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUnDelete, Boolean isBefore, Boolean isAfter) {

    }
}
