/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_DataManager {
    global API_DataManager() {

    }
    global static List<DSE__DS_Bean__c> getBeans(List<Account> accounts) {
        return null;
    }
    global static List<DSE__DS_Bean__c> getBeans(List<Contact> contacts) {
        return null;
    }
    global static List<DSE__DS_Bean__c> getBeans(List<DSE__DS_Master_Bean__c> masterBeans) {
        return null;
    }
    global static List<DSE__DS_Bean__c> getBeans(List<Lead> leads) {
        return null;
    }
    global static List<DSE__DS_Master_Bean__c> getMasterBeans(List<Account> accounts) {
        return null;
    }
    global static List<DSE__DS_Master_Bean__c> getMasterBeans(List<Contact> contacts) {
        return null;
    }
    global static List<DSE__DS_Master_Bean__c> getMasterBeans(List<Lead> leads) {
        return null;
    }
}
