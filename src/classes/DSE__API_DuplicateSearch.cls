/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_DuplicateSearch {
    global API_DuplicateSearch() {

    }
    global static List<DSE__DS_Bean__c> basicSearch(SObject obj, String searchMode, Set<String> searchSources, String sortOrder) {
        return null;
    }
    global static List<DSE__DS_Bean__c> fuzzySearch(SObject obj, Set<String> searchSources, String sortOrder) {
        return null;
    }
}
