/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_HierarchyHandler {
    global API_HierarchyHandler() {

    }
    global static void LockCloudMDMHierarchy(Id rootNodeId) {

    }
    global static void LockCloudMDMHierarchy(Account account) {

    }
    global static void LockCloudMDMHierarchy(Set<Id> beanIds) {

    }
    global static void UnLockCloudMDMHierarchy(Id rootNodeId) {

    }
    global static void UnLockCloudMDMHierarchy(Account account) {

    }
    global static void UnLockCloudMDMHierarchy(Set<Id> beanIds) {

    }
global class DS_CloudMDM_HierarchyAPIException extends Exception {
}
}
