/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_JobManager {
    global API_JobManager() {

    }
    global static Boolean abortJob(String jobName) {
        return null;
    }
    global static DSE.DS_BeanConversionBatchClass instantiateBeanConversionBatchClass(Set<Id> jobIdSet, List<DSE__DS_Bean__c> selectedBeans, List<DSE__DS_Bean__c> selectedCompBeans) {
        return null;
    }
    global static DSE.DS_ConsolidationBatchClass instantiateConsolidationBatchClass(Set<Id> jobIdSet, List<DSE__DS_Bean__c> selectedBeans, List<DSE__DS_Master_Bean__c> selectedMBeans) {
        return null;
    }
    global static DSE.DS_DuplicateBatchClass instantiateDuplicateBatchClass(Set<Id> jobIdSet, List<DSE__DS_Master_Bean__c> selectedMBeans, List<DSE__DS_Master_Bean__c> selectedCompMBeans) {
        return null;
    }
    global static DSE.DS_DuplicateBeanBatchClass instantiateDuplicateBeanBatchClass(Set<Id> jobIdSet, List<DSE__DS_Bean__c> selectedBeans, List<DSE__DS_Bean__c> selectedCompBeans) {
        return null;
    }
    global static DSE.DS_MatchingBatchClass instantiateMatchingBatchClass(Set<Id> jobIdSet, List<DSE__DS_Bean__c> selectedBeans, List<DSE__DS_Master_Bean__c> selectedMBeans) {
        return null;
    }
    global static DSE.DS_MergeAccountBatchClass instantiateMergeAccountBatchClass(List<DSE__DS_Duplicates__c> selectedDupes) {
        return null;
    }
    global static DSE.DS_MergeContactBatchClass instantiateMergeContactBatchClass(List<DSE__DS_DuplicateBean__c> selectedDupes) {
        return null;
    }
    global static DSE.DS_MergeLeadBatchClass instantiateMergeLeadBatchClass(List<DSE__DS_DuplicateBean__c> selectedDupes) {
        return null;
    }
    global static DSE.DS_MigrationBatchClass instantiateMigrationBatchClass(Set<Id> jobIdSet, String objName) {
        return null;
    }
    global static String lastError() {
        return null;
    }
    global static Boolean runConcurrentJob(String jobName, Integer concurrentBatchJobs) {
        return null;
    }
    global static Boolean runJob(String jobName) {
        return null;
    }
}
