/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_LicenseManager {
    global API_LicenseManager() {

    }
    global static String activateLicense(String key) {
        return null;
    }
    global static Boolean isFeatureEnabled(String featureName) {
        return null;
    }
    global static void setFeature(String featureName, Boolean enabled) {

    }
    global static void setFeature(String featureName, Boolean enabled, String password) {

    }
    global static void setFeature(String featureName, Boolean enabled, String value, Date expirationDate) {

    }
    global static void setFeature(String featureName, Boolean enabled, String value, Date expirationDate, String password) {

    }
}
