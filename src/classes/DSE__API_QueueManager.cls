/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_QueueManager {
    global API_QueueManager(String jobName, String jobClassName) {

    }
    global static void UpdateStatus(Id batchQueueId, String phase, Integer itemProcessed) {

    }
    global static void UpdateStatus(Id batchQueueId, String phase, Integer itemProcessed, Boolean isItemProcessedDelta) {

    }
    global static void UpdateStatus(Id batchQueueId, String phase, DSE.API_QueueManager.BATCH_STATUS batchStatus, Integer totalRecords, Integer itemProcessed) {

    }
    global static void UpdateStatus(Id batchQueueId, String phase, DSE.API_QueueManager.BATCH_STATUS batchStatus, Integer totalRecords, Integer itemProcessed, Boolean isItemProcessedDelta) {

    }
    global static List<String> compressParams(List<String> pList, Integer unitSize) {
        return null;
    }
    global static List<String> expandParams(List<String> pList, Integer unitSize) {
        return null;
    }
    global static Id finish(Id batchQueueId) {
        return null;
    }
    global static void finish(Id batchQueueId, DSE.API_QueueManager.BATCH_STATUS batchStatus) {

    }
    global static List<String> getExtendedParameters(Id batchQueueId, String parameterName) {
        return null;
    }
    global static List<String> getQuickParameters(Id batchQueueId) {
        return null;
    }
    global static String getStatus(Id batchQueueId) {
        return null;
    }
    global void pushExtendedParameters(String parameterName, List<String> parameterValues) {

    }
    global static void refreshQueueManager() {

    }
    global static Id registerJobLoop(Id batchQueueId) {
        return null;
    }
    global static Id registerJobLoop(Id batchQueueId, List<String> quickParameters) {
        return null;
    }
    global static Id registerJobLoop(Id batchQueueId, List<String> quickParameters, Boolean retainExtendedParams, Boolean skipLoopLogic) {
        return null;
    }
    global void setBatchSize(Integer batchSize) {

    }
    global static void setExtendedParameters(Id batchQueueId, String parameterName, List<String> parameterValues) {

    }
    global static void setExtendedParameters(Id batchQueueId, String parameterName, List<String> parameterValues, Boolean retainOld) {

    }
    global static void setQuickParameters(Id batchQueueId, String phase, List<String> quickParameters) {

    }
    global void setQuickParameters(List<String> quickParameters) {

    }
    global static Id start(Database.BatchableContext BC) {
        return null;
    }
    global Id submitJob() {
        return null;
    }
    global Id submitJob(Boolean allowConcurrentExecution) {
        return null;
    }
    global Id submitJob(Integer batchSize) {
        return null;
    }
    global Id submitJob(Boolean allowConcurrentExecution, Id dependsOnQueueEntry) {
        return null;
    }
    global static void submitJob(Id batchQueueId) {

    }
    global static void updateExecutionProgress(Id batchQueueId, Integer totalRecords, Integer itemProcessed) {

    }
global enum BATCH_STATUS {ABORTED, FAILURE, SUCCESS}
global class DS_QueueManagerException extends Exception {
}
}
