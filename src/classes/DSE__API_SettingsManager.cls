/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_SettingsManager {
    global API_SettingsManager() {

    }
    global static void ResetExecutionManager() {

    }
    global static void enableDefaultJobPriorityForRunAutomatic(Boolean value) {

    }
    global static void enableExtractCountryFromPicklist(Boolean value) {

    }
    global static void enableHierarchyAccountEnrichment(Boolean value) {

    }
    global static void enableLeadPersonAccountComparison(Boolean value) {

    }
    global static void enableQueueMonitorAutomaticRefresh(Boolean value) {

    }
    global static DSE__DS_SF_Enrichment_Settings__c getAccountEnrichmentSettings() {
        return null;
    }
    global static DSE__DS_SF_Enrichment_Settings__c getAccountEnrichmentSettings(String recType) {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getAccountSyncSettings() {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getAccountSyncSettings(String recType) {
        return null;
    }
    global static DSE__DS_Settings__c getCloudMDMSettings() {
        return null;
    }
    global static Object getConfig(String configName, List<Object> params) {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getContactSyncSettings() {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getContactSyncSettings(String recType) {
        return null;
    }
    global static DSE__DS_Settings__c getDataScoutSettings() {
        return null;
    }
    global static String getInternalSettingValue(String settingName) {
        return null;
    }
    global static Map<String,String> getInternalSettings() {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getLeadSyncSettings() {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getLeadSyncSettings(String recType) {
        return null;
    }
    global static DSE__DS_Matching__c getMatchingSettings(String objectClass, String recordTypeName) {
        return null;
    }
    global static DSE__DS_Performance__c getPerformanceSettings(String performanceSettingName) {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getPersonAccountSyncSettings() {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getPersonAccountSyncSettings(String recType) {
        return null;
    }
    global static DSE__DS_Sources__c getSourceSettings(String sourceName) {
        return null;
    }
    global static DSE__DS_Sources__c getSourceSettings(String sourceName, String recTypeName) {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getSyncSettings(String objectName) {
        return null;
    }
    global static DSE__DS_SF_Synchronization_Settings__c getSyncSettings(String objectName, String recType) {
        return null;
    }
    global static Boolean isPersonAccountsEnabled() {
        return null;
    }
    global static Boolean isSetupCompleted() {
        return null;
    }
    global static Boolean isSkipTests() {
        return null;
    }
    global static Boolean isTestMode() {
        return null;
    }
    global static void resetDefaultRecordTypesValues() {

    }
    global static Boolean resetInternalSetting(String settingName) {
        return null;
    }
    global static void setMasterDataResponseSize(Integer value) {

    }
    global static void updateInternalSetting(String internalSettingName, Long value) {

    }
    global static void updateInternalSetting(String internalSettingName, String value) {

    }
}
