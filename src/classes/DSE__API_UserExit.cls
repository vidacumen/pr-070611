/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class API_UserExit {
    global API_UserExit() {

    }
    global static String getUserExitMergeAccountHandler() {
        return null;
    }
    global static String getUserExitMergeContactHandler() {
        return null;
    }
    global static String getUserExitMergeLeadHandler() {
        return null;
    }
    global static void setUserExitMergeAccountHandler(String className, Boolean isPartial) {

    }
    global static void setUserExitMergeContactHandler(String className, Boolean isPartial) {

    }
    global static void setUserExitMergeLeadHandler(String className, Boolean isPartial) {

    }
}
