/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DS_Classes {
    global static String DS_ADPT_OP_FINALIZE;
    global static String DS_ADPT_OP_INITIALIZE;
    global static String DS_ADPT_OP_MARSHAL;
    global static String DS_ADPT_OP_PROCESS;
    global static String DS_ADPT_OP_UNMARSHAL;
    global DS_Classes() {

    }
global class AdapterContext {
    global Map<String,String> additionalProperties {
        get;
    }
    global String currentRuleName {
        get;
    }
    global String currentSObjectApiName {
        get;
    }
    global String operationName {
        get;
    }
}
global class AdapterPayload {
    global Map<String,Object> keyValuePair {
        get;
        set;
    }
    global String message {
        get;
        set;
    }
    global List<Map<String,Object>> outputKeyValuePairs {
        get;
        set;
    }
    global Integer status {
        get;
        set;
    }
}
global class BeanFamily {
    global List<DSE.DS_Classes.BeanFamily> children {
        get;
        set;
    }
    global DSE__DS_Bean__c node {
        get;
        set;
    }
    global Boolean nodeClone;
    global DSE__DS_Bean__c parent {
        get;
        set;
    }
    global BeanFamily(DSE__DS_Bean__c aNode, DSE__DS_Bean__c aParent) {

    }
    global BeanFamily(DSE__DS_Bean__c aNode, DSE__DS_Bean__c aParent, DSE.DS_Classes.BeanFamily aChild) {

    }
    global Boolean isClonedNode() {
        return null;
    }
}
global class HierarchyInfo {
}
global interface ICloudMDMAdapter {
    Object processor(Object param0, DSE.DS_Classes.AdapterContext param1);
    Boolean testConnection();
}
global interface ICloudMDMUserExit {
    Object userExitHandler(Map<String,Object> param0, List<SObject> param1, Object param2, Map<Id,String> param3);
}
global class ICloudMDMUserExitException extends Exception {
}
}
