/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DS_ConsolidationSchedulerClass implements System.Schedulable {
    global DS_ConsolidationSchedulerClass() {

    }
    global void execute(System.SchedulableContext sc) {

    }
}
