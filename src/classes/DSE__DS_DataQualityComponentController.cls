/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DS_DataQualityComponentController {
    @RemoteAction
    global static DSE.DS_Classes.AdapterPayload callDataQualityService(DSE.DS_DataQualityComponentController.ServiceDetails sDetails, String dqProcessMode) {
        return null;
    }
    @RemoteAction
    global static DSE__DS_Event_Queue__c clearTransactionMessage(String ObjectName, String ruleNumber, String ruleInstance, String recordId) {
        return null;
    }
    @RemoteAction
    global static DSE.DS_DataQualityComponentController.ServiceDetails createTransactionRequest(Map<String,String> fieldValues, String ObjectName, String ruleNumber, String ruleInstance, String transactionId) {
        return null;
    }
    @RemoteAction
    global static DSE.DS_DataQualityComponentController.RecordStatus getTransactionMessage(String recordId, String ObjectName, String ruleNumber, String ruleInstance, String transactionId, String fieldsArray) {
        return null;
    }
    @RemoteAction
    global static void modifyTransactionMessage(String transactionId) {

    }
global class RecordStatus {
    global RecordStatus() {

    }
}
global class ServiceDetails {
    global ServiceDetails() {

    }
}
}
