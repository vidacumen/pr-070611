/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DS_Enumeration {
    global DS_Enumeration() {

    }
global enum COUNT_TYPE {ABSOLUTE, INCREMENTAL}
global enum DML {OP_DELETE, OP_INSERT, OP_MERGE, OP_UPDATE, OP_UPSERT}
global enum DMLOPTION {ALL_OR_NONE, ALLOW_PARTIAL}
global enum FAILTYPE {NON_TOLERABLE, TOLERABLE}
global enum MESSAGE_TYPE {ERROR, INFO, WARN}
global enum NEXTLOOP_OLD_PARAMS {APPEND, REPLACE}
}
