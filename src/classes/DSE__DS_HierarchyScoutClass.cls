/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DS_HierarchyScoutClass {
    global DS_HierarchyScoutClass() {

    }
    global Map<String,DSE.DS_Classes.BeanFamily> buildBeanHierarchies(Map<Id,DSE__DS_Bean__c> beanMap) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> cloneBeanHierarchies(Map<String,DSE.DS_Classes.BeanFamily> origHierarchies) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> collapseHierarchies(Map<String,DSE.DS_Classes.BeanFamily> familyMap) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> expandHierarchies(Map<String,DSE.DS_Classes.BeanFamily> familyMap) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> getBeanHierarchies(List<DSE__DS_Bean__c> beans) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> getBeanHierarchies(List<DSE__DS_Bean__c> beans, Boolean sortHierarchies, Boolean expand) {
        return null;
    }
    global DSE.DS_Classes.BeanFamily getBeanHierarchy(DSE__DS_Bean__c bean) {
        return null;
    }
    global Map<Id,DSE__DS_Bean__c> getBeanRelatives(List<DSE__DS_Bean__c> beans) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> getHierarchiesV2(Id objectId, String hierarchyType) {
        return null;
    }
    global Map<String,DSE.DS_Classes.BeanFamily> getHierarchies(Id objectId, String hierarchyType) {
        return null;
    }
    global DSE.DS_Classes.BeanFamily getHierarchy(Id objectId, String hierarchyType) {
        return null;
    }
    global String getHierarchyType(DSE__DS_Bean__c bean) {
        return null;
    }
    global Set<String> getHierarchyTypes(Id objectId) {
        return null;
    }
    global void setBeanHierarchies(Map<String,DSE.DS_Classes.BeanFamily> hierarchies) {

    }
}
