public with sharing class DeleteDocumentModalAuraService {
	@AuraEnabled
	public static void deleteDocumentAndFilesById(Id documentId) {
		CustomDocument__c doc = [SELECT Id
								FROM CustomDocument__c
								WHERE Id =: documentId];

		Map<Id, ContentDocumentLink> cdLink = new Map<Id, ContentDocumentLink>([SELECT ContentDocumentId
											FROM ContentDocumentLink
											WHERE LinkedEntityId =: doc.Id]);

		Set<Id> idsForFiles = new Set<Id>();

		for (Id cdLinkId : cdLink.keySet()) {
			Id link = cdLink.get(cdLinkId).ContentDocumentId;
			idsForFiles.add(link);
		}

		Set<Id> files = new Map<Id, ContentDocument>([SELECT Id
													FROM ContentDocument
													WHERE Id IN :idsForFiles]).keySet();

		Savepoint sp = Database.setSavepoint();
		if (files.size() > 0) {
			try {
				deleteContentDocument(files);
				deleteDocument(doc);
			} catch (Exception ex) {
				Database.rollback(sp);
				System.debug(ex.getMessage());
			}
		} else {
			try {
				deleteDocument(doc);
			} catch (Exception ex) {
				Database.rollback(sp);
				System.debug(ex.getMessage());
			}
		}
	}

	public static void deleteContentDocument(Set<Id> files) {
		List<ContentDocument> filesToDelete = [SELECT Id
												FROM ContentDocument
												WHERE Id IN :files];
		
		delete filesToDelete;
	}

	public static void deleteDocument(CustomDocument__c document) {
		delete document;
	}
}