/**
 * Created by aszampias on 8/17/18.
 */
@isTest
public with sharing class DeleteDocumentModalAuraServiceTest {
	@testSetup
	static void setup() {
		ContentVersion cv = new ContentVersion(Title='Software',
				PathOnClient = 'Penguins.jpg',
				VersionData = Blob.valueOf('TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG4='));
		insert cv;

		CustomDocument__c customDoc = new CustomDocument__c();
		insert customDoc;

		ContentDocument contDoc = [SELECT Id FROM ContentDocument];

		ContentDocumentLink cdlLink = new ContentDocumentLink(LinkedEntityId=customDoc.id,
															  ContentDocumentId=contDoc.id,
															  ShareType='I');
		insert cdlLink;
	}

	@isTest 
	static void deleteDocumentAndFilesById_passInDocWithFile_deleteDocumentAndFiles(){
		CustomDocument__c customDoc = [SELECT Id from CustomDocument__c];

		Test.startTest();
			DeleteDocumentModalAuraService.deleteDocumentAndFilesById(customDoc.id);
		Test.stopTest();

		List<ContentVersion> listContentVersions = [SELECT Id FROM ContentVersion];
		List<ContentDocument> listContentDocuments = [SELECT Id FROM ContentDocument];
		List<CustomDocument__c> listCustomDocuments = [SELECT Id FROM CustomDocument__c];

		System.assertEquals(0, listContentVersions.size());
		System.assertEquals(0, listContentDocuments.size());
		System.assertEquals(0, listCustomDocuments.size());
	}

	@isTest 
	static void deleteDocumentAndFilesById_passInDocWithoutFile_deleteDocument(){
		CustomDocument__c customDoc = new CustomDocument__c();
		insert customDoc;

		Test.startTest();
			DeleteDocumentModalAuraService.deleteDocumentAndFilesById(customDoc.id);
		Test.stopTest();

		List<CustomDocument__c> listCustomDocuments = [SELECT Id FROM CustomDocument__c WHERE Id =: customDoc.Id];

		System.assertEquals(0, listCustomDocuments.size());
	}

	@isTest 
	static void deleteContentDocument_passInFileIds_deleteFiles(){
		Set<Id> fileIds = new Set<Id>();

		ContentDocument cd = [SELECT Id FROM ContentDocument];

		fileIds.add(cd.Id);

		Test.startTest();
			DeleteDocumentModalAuraService.deleteContentDocument(fileIds);
		Test.stopTest();

		List<ContentDocument> listContentDocuments = [SELECT Id FROM ContentDocument];

		System.assertEquals(0, listContentDocuments.size());
	}

	@isTest 
	static void deleteDocument_passInContentDocument_deleteDocument(){
		CustomDocument__c customDoc = new CustomDocument__c();
		insert customDoc;

		Test.startTest();
			DeleteDocumentModalAuraService.deleteDocument(customDoc);
		Test.stopTest();

		List<CustomDocument__c> listCustomDocuments = [SELECT Id FROM CustomDocument__c WHERE Id =: customDoc.Id];

		System.assertEquals(0, listCustomDocuments.size());
	}
}