public with sharing class DeleteFileModalAuraService {
	@AuraEnabled
	public static void deleteFileById(Id fileId) {
		ContentDocument file = [SELECT Id
								FROM ContentDocument
								WHERE Id =: fileId];
		
		delete file;
	}
}