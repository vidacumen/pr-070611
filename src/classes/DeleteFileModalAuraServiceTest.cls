/**
 * Created by aszampias on 8/17/18.
 */
@isTest
public with sharing class DeleteFileModalAuraServiceTest {

	@isTest
	static void deleteFileModal(){
		ContentVersion contentVersion_1 = new ContentVersion(
			Title = 'Penguins',
			PathOnClient = 'Penguins.jpg',
			VersionData = Blob.valueOf('Test Content'),
			IsMajorVersion = true
		);
		insert contentVersion_1;

		ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
		List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
		DeleteFileModalAuraService.deleteFileById(documents[0].Id);
		List<ContentDocument> cd = [SELECT Id FROM ContentDocument];
		Test.startTest();
			System.assert(cd.isEmpty(), true);
		Test.stopTest();
	}

}