public with sharing class DocumentAuraService {
	public static CustomDocumentDAI customDocAccessor = new CustomDocumentDA();

	@AuraEnabled
	public static List<CustomDocument__c> queryForDocumentsByRegistrationId(Id registrationId) {
		List<CustomDocument__c> documents = customDocAccessor.queryForDocumentsByRegistrationId(registrationId);

		return documents;
	}

	@AuraEnabled
	public static List<AggregateResult> queryForDocumentTypesByRegistrationId(Id registrationId) {
		List<AggregateResult> documents = customDocAccessor.queryForDocumentTypesByRegistrationId(registrationId);

		return documents;
	}

	@AuraEnabled
	public static List<PicklistOptions> queryForDocumentTypeValues() {
		List<PicklistOptions> lstPickvals = new List<PicklistOptions>();
		Schema.DescribeFieldResult fieldResult = CustomDocument__c.Document_Type__c.getDescribe();
		List<Schema.PicklistEntry> documentTypeValues = fieldResult.getPicklistValues();

		for(Schema.PicklistEntry ple : documentTypeValues) {
			PicklistOptions option = new PicklistOptions();
			option.label = ple.getLabel();
			option.value = ple.getValue();
			lstPickvals.add(option);
		}

		return lstPickvals;
	}
}