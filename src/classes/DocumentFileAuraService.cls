public with sharing class DocumentFileAuraService {

	@AuraEnabled
	public static List<ContentDocumentLink> queryForFilesByDocumentId(Id DocumentId) {
		return[SELECT ContentDocumentId, ContentDocument.Title
				FROM ContentDocumentLink
				WHERE LinkedEntityId = :DocumentId];
	}

	@AuraEnabled
	public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String contentVersionId) {
		if (contentVersionId == '') {
			contentVersionId = saveTheFile(parentId, fileName, base64Data, contentType);
		} else {
			appendToFile(contentVersionId, base64Data);
		}
 
		return Id.valueOf(contentVersionId);
	}
 
	public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
		base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

		ContentVersion cv = new ContentVersion();
		cv.ContentLocation = 'S';
		cv.VersionData = EncodingUtil.base64Decode(base64Data);
		cv.Title = fileName;
		cv.PathOnClient = fileName;

		insert cv;

		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
		cdl.LinkedEntityId = parentId;
		cdl.ShareType = 'V';
		insert cdl;

		return cv.Id;

	}
 
	private static void appendToFile(Id contentVersionId, String base64Data) {
		base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
		ContentVersion cdl = [
			SELECT Id, ContentDocumentId, VersionData
			FROM ContentVersion
			WHERE Id =: contentVersionId
		];
 
		String existingVersionData = EncodingUtil.base64Encode(cdl.VersionData);
 
		cdl.VersionData = EncodingUtil.base64Decode(existingVersionData + base64Data);
 
		update cdl;
	}
}