/**
 * Created by aszampias on 8/21/18.
 */
@isTest
public with sharing class DocumentFileAuraServiceTest {
	@testSetup
	static void setup() {
		ContentVersion cv = new ContentVersion(Title='Cats',
											   PathOnClient = 'cat.jpg',
											   VersionData=Blob.valueOf('TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG45='));
		insert cv;

		ContentDocument contDoc = [SELECT Id FROM ContentDocument LIMIT 1];
		CustomDocument__c customDoc = new CustomDocument__c();
		insert customDoc;

		ContentDocumentLink cdlLink = new ContentDocumentLink(LinkedEntityId=customDoc.id,
															  ContentDocumentId=contDoc.id,
															  ShareType='V');
		insert cdlLink;
	}

	@isTest
	static void DocumentFileAuraService_shouldQueryForFilesByDocumentId_ReturnListContentDocumentLink() {
		CustomDocument__c customDoc = [SELECT Id FROM CustomDocument__c];

		Test.startTest();
			List<ContentDocumentLink> cdlAnswer = DocumentFileAuraService.queryForFilesByDocumentId(customDoc.id);
		Test.stopTest();

		System.assertNotEquals(0, cdlAnswer.size());
		System.assertEquals('Cats', cdlAnswer[0].ContentDocument.Title);
	}

	@isTest
	static void DocumentFileAuraService_shouldSaveChunk_OptionSaveFile() {
		ContentDocument cd = [SELECT Id FROM ContentDocument LIMIT 1];
		CustomDocument__c customDoc = [SELECT Id FROM CustomDocument__c LIMIT 1];
		ContentDocumentLink cdl = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: customDoc.id];

		DocumentFileAuraService.saveChunk(cdl.LinkedEntityId, 'Dogs', 'TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG451=', 'image/jpeg', '');
		
		Test.startTest();
			DocumentFileAuraService.saveChunk(cdl.LinkedEntityId, 'Dogs', 'TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG451=', 'image/jpeg', '');
		Test.stopTest();

		List<ContentVersion> listCV = [SELECT Title FROM ContentVersion WHERE Title='Dogs'];

		System.assertNotEquals(0, listCV.size());
		System.assertEquals('Dogs', listCV[0].Title);
	}

	@isTest
	static void DocumentFileAuraService_shouldSaveChunk_OptionAppendFile() {
		ContentDocument cd = [SELECT Id FROM ContentDocument LIMIT 1];
		ContentVersion cv = [SELECT Id FROM ContentVersion];
		ContentDocumentLink cdl = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId =: cd.id LIMIT 1];

		Test.startTest();
			DocumentFileAuraService.saveChunk(cv.id , 'Cats', 'TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG451=', 'image/jpeg', cv.id);
		Test.stopTest();

		List<ContentVersion> listCV = [SELECT Title FROM ContentVersion WHERE Title='Cats'];
		
		System.assertNotEquals(0, listCV.size());
		System.assertEquals('Cats', listCV[0].Title);
	}
}