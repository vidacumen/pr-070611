public with sharing class DocumentModalAuraService {
	@AuraEnabled
	public static void createNewDocumentRecord(Id registrationId, String documentName, String documentType) {
		CustomDocument__c doc = new CustomDocument__c(Name = documentName,
														Document_Type__c = documentType,
														Registration_ID__c = registrationId);

		insert doc;
	}
}