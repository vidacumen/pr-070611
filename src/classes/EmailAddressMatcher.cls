public class EmailAddressMatcher implements MatcherInterface {
	public Set<Id> findMatches(String firstname,
								String middlename, 
								String lastname, 
								String email, 
								String usmleid, 
								String aamcid,
								String nbmeid,
								Date birthdate,
								String phone,
								String medicalschool,
								String gradyear) {
		String newPhone = '';
		Set<Id> matches = new Map<Id, Account>().keySet();

		if (String.isNotEmpty(phone)) {
			phone = phone.replaceAll('[^0-9]', '');
			for (Integer i = 0; i < phone.length(); i++) {
				newPhone += '%' + phone.substring(i,i+1) + '%';
			}
			matches = new Map<Id, Account>	([SELECT Id
												FROM Account
												WHERE IsPersonAccount = true 
												AND PersonEmail =: email 
												AND (LastName =: lastname OR FirstName =: firstname) 
												AND (PersonBirthdate =: birthdate OR Phone LIKE :newPhone OR USMLE_ID__c =: usmleid OR AAMC_ID__c =: aamcid)
											]).keySet();
		} else {
			matches = new Map<Id, Account>	([SELECT Id
												FROM Account
												WHERE IsPersonAccount = true 
												AND PersonEmail =: email 
												AND (LastName =: lastname OR FirstName =: firstname) 
												AND (PersonBirthdate =: birthdate OR Phone =: newPhone OR USMLE_ID__c =: usmleid OR AAMC_ID__c =: aamcid)
											]).keySet();
		}

		return matches;
	}

	public RegistrationResponse handleMatch(User user,
											Set<Id> accountId,
											String firstname,
											String middlename,
											String lastname, 
											String email, 
											String usmleid, 
											String aamcid,
											String nbmeid,
											Date birthdate,
											String phone,
											String medicalschool,
											String gradyear) {

		RegistrationResponse result = new RegistrationResponse();

		if (user != null) {
			//user.isactive = true;
			//update user;
			//System.resetPassword(user.Id,true);
			Savepoint sp = Database.setSavepoint();
			UserManagement userMgmt = new UserManagement();
			Boolean successful1 = userMgmt.updateUser(user);
			Boolean successful2 = userMgmt.resetUserPassword(user);

			if (successful1 && successful2) {
				result.message = 'A matching user has been found. Your password has been reset, please check your e-mail for the link to reset your password.';
				result.isSuccess = true;
			} else {
				Database.rollback(sp);
				result.message = 'Error - cannot update user and reset password.';
				result.isSuccess = true;
			}
		} else {
			result.message = 'Matching error - No user found.';
			result.isSuccess = false;
		}

		return result;
	}

	public RegistrationResponse handleNoMatch(User user,
												Set<Id> accountId,
												String firstname,
												String middlename, 
												String lastname, 
												String email, 
												String usmleid, 
												String aamcid,
												String nbmeid,
												Date birthdate,
												String phone,
												String medicalschool,
												String gradyear) {

		RegistrationResponse result = new RegistrationResponse();

		Savepoint sp = null;
		try {
			sp = Database.setSavepoint();
			if (accountId.size() == 1) {
				Account a = [SELECT Id,
									OwnerId,
									PersonContactId
							FROM Account
							WHERE Id IN :accountId];

				User accOwnerUser = [SELECT Id,
										UserRoleId
									FROM User
									WHERE Id =: a.OwnerId];

				if(accOwnerUser.UserRoleId != null) {
					//String strRandomInt = String.valueOf(Crypto.getRandomInteger());
					String alias = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
					alias += String.valueOf(Crypto.getRandomInteger()).substring(1,7);

					//String nickname = ((email != null && email.length() > 40) ? email.substring(0,40) : email );
					String nickname = alias;

					Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

					User u = new User(FirstName = firstname,
										MiddleName = middlename,
										LastName = lastname,
										Email = email,
										Username = email,
										Alias = alias,
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = nickname,
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

					//Id userId = Site.createExternalUser(u, a.Id, null, true);
					UserManagement userMgmt = new UserManagement();
					Boolean succesful = userMgmt.createUser(u, a);

					if (succesful) {
						result.message = 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.';
						result.isSuccess = true;
					} else {
						Database.rollback(sp);
						result.message = 'Error - cannot create new user.';
						result.isSuccess = false;
					}
				}
			}
		} catch (Exception ex) {
			Database.rollback(sp);
			//result.message = ex.getMessage();
			System.debug(ex.getMessage());
			result.message = 'Error - cannot create new user.';
			result.isSuccess = false;
		}

		return result;
	}

	public void updateEmail(Set<Id> accountId, String email) {
		
	}
}