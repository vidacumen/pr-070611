@isTest
public with sharing class EmailAddressMatcherTest {

	@isTest
	static void findMatches_PassInValuesForSingleSuccessfulMatch_OneMatchIdReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(LastName = 'Testing',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Test.startTest();
			EmailAddressMatcher matchTest = new EmailAddressMatcher();
			Set<Id> result = matchTest.findMatches(null,null,testPersonAccount.LastName,testPersonAccount.PersonEmail,null,null,null,testPersonAccount.PersonBirthdate,null,null,null);
		Test.stopTest();

		System.assertEquals(result.size(), 1);
	}

	@isTest
	static void findMatches_PassInValuesForMultipleSuccessfulMatches_MultipleMatchIdsReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount1 = new Account(LastName = 'Testing',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount1;

		Account testPersonAccount2 = new Account(FirstName = 'Tester',
												LastName = 'Smith',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount2;

		Test.startTest();
			EmailAddressMatcher matchTest = new EmailAddressMatcher();
			Set<Id> result = matchTest.findMatches(testPersonAccount2.FirstName,null,testPersonAccount1.LastName,testPersonAccount1.PersonEmail,null,null,null,testPersonAccount1.PersonBirthdate,null,null,null);
		Test.stopTest();

		System.assertEquals(result.size() > 1, true);
	}

	@isTest
	static void findMatches_PassInValuesForNoSuccessfulMatches_NoMatchIdsReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(LastName = 'Testing',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Test.startTest();
			EmailAddressMatcher matchTest = new EmailAddressMatcher();
			Set<Id> result = matchTest.findMatches('Test',null,'Test','test@yahoo.com',null,null,null,Date.newInstance(1989, 1, 1),null,null,null);
		Test.stopTest();

		System.assertEquals(result.size(), 0);
	}
	
	@isTest
	static void handleMatch_PassInUser_EmailSentToUserAndSuccessObjectReturned() {
		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;

			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			Test.startTest();
				EmailAddressMatcher matchTest = new EmailAddressMatcher();
				RegistrationResponse result = matchTest.handleMatch(testUser,null,null,null,null,null,null,null,null,null,null,null,null);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'A matching user has been found. Your password has been reset, please check your e-mail for the link to reset your password.');
		}
	}

	@isTest
	static void handleMatch_NoUserPassedIn_NoEmailSentAndFailObjectReturned() {
		Test.startTest();
			EmailAddressMatcher matchTest = new EmailAddressMatcher();
			RegistrationResponse result = matchTest.handleMatch(null,null,null,null,null,null,null,null,null,null,null,null,null);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, 'Matching error - No user found.');
	}

	@isTest
	static void handleNoMatch_AccountIdPassedIn_NewPortalUserCreatedForPersonAccount() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Test',
												LastName = 'Testing',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Set<Id> acc = new Map<Id, Account> ([SELECT Id
											FROM Account
											WHERE Id =: testPersonAccount.Id]).keySet();
		
		Test.startTest();
			EmailAddressMatcher matchTest = new EmailAddressMatcher();
			RegistrationResponse result = matchTest.handleNoMatch(null,acc,testPersonAccount.FirstName,null,testPersonAccount.LastName,testPersonAccount.PersonEmail,null,null,null,null,null,null,null);
		Test.stopTest();

		System.assertEquals(result.isSuccess, true);
		System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
	}
}