public class ExamLauncherCtrl {

	public Id registrationId {get;set;}
	public String actionType {get;set;}
	public string redirectUrl {get;set;}
	public Boolean displayPopup {get;set;}
	public String pageMessage {get;set;}

	public ExamLauncherCtrl() {
		init();
		//redirectUrl = launchExam();
	}

	public void closePopup() {
		displayPopup = false;
	}

	public void showPopup() {
		displayPopup = true;
	}

	public PageReference processRedirect() {
		PageReference newPage = null;
		init();
		redirectUrl = launchExam();
		if (redirectUrl.startsWith('ERROR')) {
			pageMessage = 'There was a problem launching your exam. Please check back later or contact support if this error persists.';
			showPopup();
			return null;
		}
		newPage = new PageReference(redirectUrl);
		return newPage;
	}

	public void init() {
		registrationId = ApexPages.currentPage().getParameters().get('registrationId');
		actionType = ApexPages.currentPage().getParameters().get('actionType');
		System.debug('actionType' + actionType);
		System.debug('registrationId' + registrationId);
	}

	public String launchExam() {
		redirectUrl = '';
		if (actionType == 'Launcher1') {
			//call WebFredExamLauncherExt Class - SHOULD RETURN A URL!!!
			redirectUrl = WebFredExamLauncherExt.processWebFredExamRequest(registrationId);
		} else if (actionType == 'Launcher2') {
			//call ITSExamLauncherExt CLass - SHOULD RETURN A URL
			redirectUrl = ITSExamLauncherExt.getUrl(registrationId);
		} else if (actionType == 'Support') {
			redirectUrl = WebFredSupportToolExt.processWebFredSupportToolRequest(registrationId);
		}
		System.debug('----------------------------------' + redirectUrl);
		return redirectUrl;
	}
}