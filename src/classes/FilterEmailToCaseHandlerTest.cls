/**
 * Created by mworkman on 9/7/2017.
 */

@IsTest
private class FilterEmailToCaseHandlerTest {

    @TestSetup
    static void setup() {
        OOOEmailFilterKeywords__c keywords = new OOOEmailFilterKeywords__c();
        keywords.Name = 'OutOfOfficePhrase';
        keywords.Keywords__c = 'Out of Office';
        insert keywords;

    }

    @IsTest
    static void handleOOOReplies_createOOOReplyCase_caseClosedAndFlagged() {
        Account acc = new Account(Name = 'TestAccount');
        insert acc;

        Contact con = new Contact(AccountId = acc.Id, LastName = 'Testerson');
        insert con;

        Case oooCase = new Case(Status = 'New', Subject = 'Out of Office', ArchivedasOOOReply__c = false,
                AccountId = acc.Id, ContactId = con.Id);

        Test.startTest();
        insert oooCase;
        Test.stopTest();

        oooCase = [
                SELECT Status, ArchivedasOOOReply__c
                FROM Case
                LIMIT 1
        ];

        System.debug(oooCase.ArchivedasOOOReply__c);
        System.debug(oooCase.Status);

        System.assert(oooCase.ArchivedasOOOReply__c);
        System.assertEquals('Closed - Resolved', oooCase.Status);
    }
}