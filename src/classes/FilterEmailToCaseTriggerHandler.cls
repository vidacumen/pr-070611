/**
 * Created by mworkman on 9/5/2017.
 */

public with sharing class FilterEmailToCaseTriggerHandler {
	private List<OOOEmailFilterKeywords__c> keywords = OOOEmailFilterKeywords__c.getAll().values();

	public void handleOOOReplies(List<Case> insertedCases) {

		for (Case inserted : insertedCases) {
			for (OOOEmailFilterKeywords__c keyword : keywords) {
				if (keyword.Keywords__c != null) {
					if (!String.isBlank(inserted.Subject)) {
						if (inserted.Subject.containsIgnoreCase(keyword.Keywords__c)) {
							inserted.ArchivedasOOOReply__c = true;
							inserted.Status = 'Closed - Resolved';
							inserted.Case_Resolution_Comments__c = 'Closed due to being an OOO response.';
						}
					}
				}
			}
		}
	}
}