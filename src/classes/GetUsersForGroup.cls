public class GetUsersForGroup {
	public static List<User> GetUsersForGroupByGroupId(String groupId) {
		//Get group member Ids
		List<GroupMember> paGroupMembers = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: groupId];
		Set<Id> paGroupMemberIds = new Set<Id>();
		for (GroupMember gm : paGroupMembers) {
			paGroupMemberIds.add(gm.UserOrGroupId);
		}

		//Get role Ids for group member Ids
		List<Group> groupRoles = [SELECT Id, RelatedId FROM Group WHERE Id IN: paGroupMemberIds];
		Set<Id> groupRoleIds = new Set<Id>();
		for (Group g : groupRoles) {
			groupRoleIds.add(g.RelatedId);
		}

		//Get role Ids and parent role Ids
		List<UserRole> userRoles = [SELECT Id, ParentRoleId FROM UserRole WHERE Id IN: groupRoleIds];
		Set<Id> userRoleIds = new Set<Id>();
		Set<Id> userParentRoleIds = new Set<Id>();
		for (UserRole ur : userRoles) {
			userRoleIds.add(ur.Id);
			if (ur.ParentRoleId != null) {
				userParentRoleIds.add(ur.ParentRoleId);
			}
		}

		//Continue to requery to get roles in hierarchy missed from above query, until all possible role ids have been added to the list
		for (Id i : userParentRoleIds) {
			while (!userRoleIds.contains(i)) {
				userRoles = [SELECT Id, ParentRoleId FROM UserRole WHERE Id IN: userRoleIds OR Id IN: userParentRoleIds];
				for (UserRole ur : userRoles) {
					userRoleIds.add(ur.Id);
					if (ur.ParentRoleId != null) {
						userParentRoleIds.add(ur.ParentRoleId);
					}
				}
			}
		}

		//Get list of users
		List<User> users = [SELECT Id, Name, Person_Accounts_Owned__c 
							FROM User 
							WHERE (UserRoleId IN: userRoleIds 
							OR UserRoleId IN: userParentRoleIds) 
							AND IsActive=true 
							ORDER BY Person_Accounts_Owned__c DESC];

		return users;
	}
}