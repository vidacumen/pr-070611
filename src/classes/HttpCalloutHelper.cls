public class HttpCalloutHelper {

	public static HttpRequest buildHttpRequestWithoutHeader(String body, String endpoint, String method) {
		HttpRequest request = new HttpRequest();

		request.setEndpoint(endpoint);
		request.setBody(body);
		request.setMethod(method);

		return request;
	}

	public static HttpRequest buildHttpRequestWithHeader(String body, Map<String, String> headerMap, String endpoint, String method) {
		HttpRequest request = new HttpRequest();

		request.setEndpoint(endpoint);

		for (String s : headerMap.keySet()) {
			request.setHeader(s, headerMap.get(s));
		}

		request.setBody(body);
		request.setMethod(method);

		return request;
	}

}