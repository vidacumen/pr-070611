/**
 * Created by aszampias on 8/17/18.
 */

@isTest
public with sharing class HttpCalloutHelperTest {

		static String body = 'Example body sentence';
		static String method = 'POST';
		static String endpoint = 'http://www.google.com';

	@isTest static void HttpCalloutHelper_WithoutHeader_ReturnHttpRequest(){
		HttpRequest hRequest = HttpCalloutHelper.buildHttpRequestWithoutHeader(body, endpoint, method);
		Test.startTest();
			System.assertEquals(method, hRequest.getMethod());
		Test.stopTest();
	}

	@isTest static void HttpCalloutHelper_WithHeader_ReturnHttpRequest(){
		Map<String, String> headerMap = new Map<String, String>();
		headerMap.put('Accept-Encoding', 'gzip, deflate');
		headerMap.put('Accept-Language', 'en-US,en;q=0.5');
		HttpRequest hRequest = HttpCalloutHelper.buildHttpRequestWithHeader(body, headerMap, endpoint, method);
		Test.startTest();
			System.assertEquals(headerMap.get('Accept-Language'), hRequest.getHeader('Accept-Language'));
		Test.stopTest();
	}
}