public with sharing class ITSExamLauncherExt {
	@TestVisible private double mdblFactor1 = 1.0;
	@TestVisible private double mdblFactor2 = 1.0;
	@TestVisible private double mdblFactor3 = 1.0;
	@TestVisible private double mdblFactor4 = 1.0;
	@TestVisible private double mdblFactor5 = 1.0;
	@TestVisible private String mstrValue1 = null;
	@TestVisible private String mstrValue2 = null;
	@TestVisible private String mstrValue3 = null;
	@TestVisible private double mdblNumber1 = 1.0;
	@TestVisible private double mdblNumber2 = 1.0;
	@TestVisible private String mstrEncryptionKey = null;
	@TestVisible private Integer mlngErrCode = 0; // CONVERSION
	@TestVisible private String mstrErrDescription = null;

	public void setmdblFactor1(double d) {
		mdblFactor1 = d;
	}

	public void setmdblFactor2(double d) {
		mdblFactor2 = d;
	}

	public void setmdblFactor3(double d) {
		mdblFactor3 = d;
	}

	public void setmdblFactor4(double d) {
		mdblFactor4 = d;
	}

	public void setmdblFactor5(double d) {
		mdblFactor5 = d;
	}

	public void setmstrValue1(String s) {
		mstrValue1 = s;
	}

	public void setmstrValue2(String s) {
		mstrValue2 = s;
	}

	public void setmstrValue3(String s) {
		mstrValue3 = s;
	}

	public void setmdblNumber1(double d) {
		mdblNumber1 = d;
	}

	public void setmdblNumber2(double d) {
		mdblNumber2 = d;
	}

	public void setmstrEncryptionKey(String s) {
		mstrEncryptionKey = s;
	}

	public void setmlngErrCode(Integer i) {// CONVERSION
		mlngErrCode = i;
	}

	public void setmstrErrDescription(String s) {
		mstrErrDescription = s;
	}

	public Integer getmlngErrCode() {// CONVERSION
		return mlngErrCode;
	}

	public String getmstrErrDescription() {
		return mstrErrDescription;
	}

	/******************************************************************************/
	/** getCustomDateString **/
	/** Returns the current date and time in the following format: **/
	/** JJJHHmm where: **/
	/** JJJ - 3 digit julian day, HH - hour of the day, mm - minutes **/
	/**
	 * This method will return 0710744 for a date of March 12, 2003 at 7:44 am
	 **/
	/******************************************************************************/
	/*String getCustomDateString() {
		Calendar calendar = new GregorianCalendar();
		Date date = calendar.getTime();
		DateFormat currentYearFormat = new SimpleDateFormat("yyyy");
		String formattedYear = currentYearFormat.format(date);
		DateFormat julianDateFormat = new SimpleDateFormat("D");
		String julianDateStr = julianDateFormat.format(date);
		int julianDate = Integer.parseInt(julianDateStr, 10);
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumIntegerDigits(3);
		String formattedJulianDate = nf.format(julianDate);
		DateFormat currentTimeFormat = new SimpleDateFormat("HHmm");
		String formattedCurrentTime = currentTimeFormat.format(date);
		return formattedYear + formattedJulianDate + formattedCurrentTime;
	}*/

	/*
	* Converted Method
	* QUESTION: Should these dates/times be local or GMT?
	*/
	public static String getCustomDateString2() {
		String returnValue = null;
		
		Datetime dt = datetime.now();

		String year = String.valueOf(dt.year());
		String day = String.valueOf(dt.dayOfYear());
		day = day.leftPad(3).replace(' ', '0');
		String hour = String.valueOf(dt.hour());
		hour = hour.leftPad(2).replace(' ', '0'); // Not sure if this is needed, check the java behavior
		String minute = String.valueOf(dt.minute());
		minute = minute.leftPad(2).replace(' ', '0'); // Not sure if this is needed, check the java behavior
		
		returnValue = year + day + hour + minute;
		
		return returnValue;
	}

	public String GenerateCode() {
		String GenerateCode = '';
		try {
			String strDate = '';
			double dblResult = 0.0;
			String strCode = '';
			strDate = getCustomDateString2();// CONVERSION getCustomDateString()
			dblResult = (SumAscString(mstrValue1) * mdblFactor1) + (SumAscString(mstrValue2) * mdblFactor2)
					+ (SumAscString(mstrValue3) * mdblFactor3);
			dblResult = dblResult + (mdblNumber1 * mdblFactor4) + ((mdblNumber2 + 52) * mdblFactor5);
			Double dbl = dblResult.round();// CONVERSION Math.round()
			strCode = strDate + dbl.longValue();
			GenerateCode = EncryptWithALP(XOREncrypt(strCode, mstrEncryptionKey));
		} catch (Exception e) {
			setmstrErrDescription('Error:GesnerateCode ' + e);// CONVERSION .toString()
		}
		return GenerateCode;
	}

	public ITSExamLauncherExt(double F1, double F2, double F3, double F4, double F5, String str1, String str2, String str3,
			double num1, double num2, String key) {
		try {
			setmdblFactor1(F1);
			setmdblFactor2(F2);
			setmdblFactor3(F3);
			setmdblFactor4(F4);
			setmdblFactor5(F5);
			setmstrValue1(str1);
			setmstrValue2(str2);
			setmstrValue3(str3);
			setmdblNumber1(num1);
			setmdblNumber2(num2);
			setmstrEncryptionKey(key);
			setmlngErrCode(0);
			setmstrErrDescription('');// CONVERSION
		} catch (Exception e) {
			setmstrErrDescription('Error:Security' + e);// CONVERSION .toString()
		}
	}

	@TestVisible
	private double SumAscString(String value) {
		double SumAscString = 0.0;
		try {
			Integer lngPos = 0;// CONVERSION
			Integer lngLength = 0;// CONVERSION
			double dblResult = 0.0;
			lngLength = value.length();
			for (lngPos = 0; lngPos < lngLength; lngPos++) {
				dblResult = dblResult + value.charAt(lngPos);
			}
			SumAscString = dblResult;
		} catch (Exception e) {
			setmstrErrDescription('Error:SumAscString ' + e); // CONVERSION .toString()
		}
		return SumAscString;
	}

	@TestVisible
	private String XOREncrypt(String strValueA, String strValueB) {
		String XOREncrypt = '';
		try {
			Integer count = 0;// CONVERSION
			Integer index = 0;// CONVERSION
			String strCurrentCharC = '';
			String strOutput = '';
			String strOutput2 = '';
			index = 1;
			for (count = 0; count < strValueA.length(); count++) {
				strCurrentCharC = String.valueOf(strValueA.charAt(count) ^ strValueB.charAt(index - 1));
				String myChar = String.fromCharArray( new List<Integer> { Integer.valueOf(strCurrentCharC) });// CONVERSION parsInt(), String.valueOf((char)(Integer.parseInt(strCurrentCharC)))
				strOutput = strOutput + String.valueOf(myChar);// CREATED newly added line to help line above
				if (index >= strValueB.length()) {
					index = 1;
				} else {
					index = index + 1;
				}
			}
			XOREncrypt = strOutput;
		} catch (Exception e) {
			setmstrErrDescription('Error:XOREncrypt ' + e);// CONVERSION .toString()
		}
		return XOREncrypt;
	}

	public String EncryptWithALP(String strData) {
		String EncryptWithALP = '';
		String strALPKey = '';
		String strALPKeyMask = '';
		Integer lngIterator = 0;// CONVERSION
		boolean blnOscillator = false;
		String strOutput = '';
		Integer lngHex = 0;// CONVERSION
		Integer lngALPKeyLength = 0;// CONVERSION
		String strDataBuf = strData;// CONVERSION StringBuffer
		String strTemp = '';
		// Function encrypts with hex number
		try {
			lngALPKeyLength = 8;
			if (strData.length() == 0) {
				return EncryptWithALP;
			}
			for (lngIterator = 0; lngIterator < lngALPKeyLength; lngIterator++) {
				strALPKey = strALPKey
						+ toHexString((Integer) Math.floor((16 * Math.random()))).trim().toUpperCase();// CONVERSION int, randomizer.nextDouble()
				strALPKeyMask = strALPKeyMask
						+ String.valueOf((Integer) Math.floor((2 * Math.random()))).trim();// CONVERSION int, randomizer.nextDouble()
			}
			lngIterator = -1;
			while (strDataBuf.length() != 0) {
				blnOscillator = !(blnOscillator);
				if (lngIterator >= lngALPKeyLength - 1) {
					lngIterator = -1;
				}
				lngIterator = lngIterator + 1;
				lngHex = (blnOscillator ? strDataBuf.charAt(0) + strALPKey.charAt(lngIterator)
						: strDataBuf.charAt(0) - strALPKey.charAt(lngIterator));
				if (lngHex > 255) {
					lngHex = lngHex - 256;
				} else if (lngHex < 0) {
					lngHex = lngHex + 256;
				}
				strTemp = '00' + toHexString(lngHex).toUpperCase();
				strOutput = strOutput + strTemp.substring(strTemp.length() - 2);
				strDataBuf = strDataBuf.substring(1);// CONVERSION deleteCharAt(0)
			}
			for (lngIterator = 0; lngIterator < lngALPKeyLength; lngIterator++) {
				if (strALPKeyMask.charAt(lngIterator) == 1) {
					strOutput = strALPKey.charAt(lngIterator) + strOutput;
				} else {
					strOutput = strOutput + strALPKey.substring(lngIterator, lngIterator + 1);// CONVERSION strALPKey.charAt(lngIterator)
				}
			}
			strTemp = '00' + toHexString((Integer) BinaryToDouble(strALPKeyMask)).toUpperCase();// CONVERSION int
			EncryptWithALP = 'I' + strTemp.substring(strTemp.length() - 2) + strOutput;
			return EncryptWithALP;
		} catch (Exception e) {
			setmstrErrDescription('Error:EncryptWithALP ' + e);// CONVERSION .toString()
		}
		return EncryptWithALP;
	}

	private static String toHexString(Integer i) {
    	String [] digits = new String[] {
           '0' , '1' , '2' , '3' , '4' , '5' ,
           '6' , '7' , '8' , '9' , 'a' , 'b' ,
           'c' , 'd' , 'e' , 'f' , 'g' , 'h' ,
           'i' , 'j' , 'k' , 'l' , 'm' , 'n' ,
           'o' , 'p' , 'q' , 'r' , 's' , 't' ,
           'u' , 'v' , 'w' , 'x' , 'y' , 'z'
           };
    	Integer shift = 4;

        String[] buf = new String[32];
        Integer charPos = 32;
        Integer radix = 1 << shift;
        Integer mask = radix - 1;
        do {
           buf[--charPos] = digits[i & mask];
           i >>>= shift;
        } while (i != 0);

        String result = '';
        for (Integer pos = charPos; pos < 32; pos++) {
        	result += buf[pos];
        }
        return result;
    }

	private double BinaryToDouble(String strData) {
		double BinaryToDouble = 0.0;
		double dblOutput = 0.0;
		Integer lngIterator = 0;// CONVERSION
		String strBuf = strData;// CONVERSION StringBuffer
		try {
			while (strBuf.length() != 0) {
				dblOutput = dblOutput
						+ (strBuf.charAt(strBuf.length() - 1) == 1 ? (Integer) Math.rint(Math.pow(2, lngIterator)) : 0);// CONVERSION int
				lngIterator = lngIterator + 1;
				strBuf = strBuf.substring(0, strBuf.length() - 1);//CONVERSION deleteCharAt(strBuf.length()-1)
			}
			BinaryToDouble = dblOutput;
			return BinaryToDouble;
		} catch (Exception e) {
			setmstrErrDescription('Error:BinaryToDouble ' + e);// CONVERSION .toString()
		}
		return BinaryToDouble;
	}

	public static String getUrl(Id registrationId) {
		String domain = Web_Service_Setup__c.getValues('ITSExamLaunchDomain').Value__c;
		String encreptKey = Web_Service_Setup__c.getValues('ITSExamLaunchEncreptKey').Value__c;
		String workShop = Web_Service_Setup__c.getValues('ITSExamLaunchWorkShop').Value__c;
		String version = Web_Service_Setup__c.getValues('ITSExamLaunchVersion').Value__c;
		String url = '';
		String str1 = '';
		String str2 = '';
		String str3 = '';
		String fcode = '';

		Registration__c registrationRecord = queryForUrlParamsByRegistrationId(registrationId);
		String regNameOrWBTId = (String.isBlank(registrationRecord.WBT_Register_ID__c) ? registrationRecord.Name : registrationRecord.WBT_Register_ID__c);

		if (registrationRecord.Assessment_Product__c != null) {
			ccrz__E_Attribute__c primaryAttributeVals = queryForPrimaryAttributeVals(registrationRecord.Form__c);
			ccrz__E_Attribute__c secondaryAttributeVals = queryForSecondaryAttributeVals(registrationRecord.Pacing__c);
			
			if (registrationRecord.Registration_Status__c == 'Assessment Available') {
				str1 = 'StartTest';
				str2 = registrationRecord.Assessment_Product__r.Program_Name__c + registrationRecord.NSAS_Candidate_ID__c + regNameOrWBTId + '0';
				str3 = registrationRecord.Driver_Assessment_ID__c + primaryAttributeVals.Driver_Form_Id__c;

				ITSExamLauncherExt itsCode = new ITSExamLauncherExt((double) 1.0, 
																	(double) 2.0, 
																	(double) 3.0, 
																	(double) 4.0, 
																	(double) 5.0, 
																	str1, 
																	str2, 
																	str3, 
																	(double) 0.0, 
																	//(double) 1.0, // use secondaryAttributeVals.Timing_Factor__c?
																	Double.valueOf(secondaryAttributeVals.Timing_Factor__c),
																	encreptKey);
				
				fcode = itsCode.GenerateCode();

				url = 
					domain + '/api/' + version + '/starttest.aspx' +
					'?program=' + registrationRecord.Assessment_Product__r.Program_Name__c + '&examinee=' + registrationRecord.NSAS_Candidate_ID__c + '&registration=' + regNameOrWBTId +
					'&code='+fcode+'&'+
					'xml=<Test><Examinee><LastName><![CDATA[' + registrationRecord.Last_Name__c + ']]></LastName><FirstName><![CDATA[' + registrationRecord.First_Name__c + ']]></FirstName>' + 
					'<Email>' + registrationRecord.Email__c + '</Email></Examinee><TestName>' + registrationRecord.Driver_Assessment_ID__c + '</TestName>' + 
					'<FormName>' + primaryAttributeVals.Driver_Form_Id__c + '</FormName><TimeFactor>' + secondaryAttributeVals.Timing_Factor__c + '</TimeFactor><Sequential>0</Sequential>' + 
					'<Language>' + primaryAttributeVals.Driver_Language__c + '</Language><Demo>0</Demo><Secure>0</Secure></Test>';
			} else if (registrationRecord.Registration_Status__c == 'Assessment In Progress') {
				str1 = 'RestartTest';
				str2 = registrationRecord.Assessment_Product__r.Program_Name__c + registrationRecord.NSAS_Candidate_ID__c;
				str3 = regNameOrWBTId;

				ITSExamLauncherExt itsCode = new ITSExamLauncherExt((double) 1.0, 
																	(double) 2.0, 
																	(double) 3.0, 
																	(double) 4.0, 
																	(double) 5.0, 
																	str1, 
																	str2, 
																	str3, 
																	(double) 0.0, 
																	(double) 1.0,
																	encreptKey);
				
				fcode = itsCode.GenerateCode();

				url = 
					domain + '/api/' + version + '/RestartTest.aspx' +
					'?program=' + registrationRecord.Assessment_Product__r.Program_Name__c + '&examinee=' + registrationRecord.NSAS_Candidate_ID__c + '&registration=' + regNameOrWBTId +
					'&secure=0&code='+fcode;
			} else if (registrationRecord.Registration_Status__c == 'Expired after Launch' || 
						registrationRecord.Registration_Status__c == 'Assessment Complete' ||
						(registrationRecord.Registration_Status__c == 'Score Report Available' &&
							registrationRecord.Exam_Expiry_Date__c > Date.today())) {
				str1 = 'ViewScoreReport';
				str2 = registrationRecord.Assessment_Product__r.Program_Name__c + registrationRecord.NSAS_Candidate_ID__c;
				str3 = regNameOrWBTId;

				ITSExamLauncherExt itsCode = new ITSExamLauncherExt((double) 1.0, 
																	(double) 2.0, 
																	(double) 3.0, 
																	(double) 4.0, 
																	(double) 5.0, 
																	str1, 
																	str2, 
																	str3, 
																	(double) 1.0, 
																	(double) 1.0,
																	encreptKey);
				
				fcode = itsCode.GenerateCode();

				url = 
					domain + '/api/' + version + '/ViewScoreReport.aspx' +
					'?program=' + registrationRecord.Assessment_Product__r.Program_Name__c + '&examinee=' + registrationRecord.NSAS_Candidate_ID__c + '&registration=' + regNameOrWBTId +
					'&code='+fcode;
			}
		} else {
			url = 'ERROR - No Assessment Product found for the Registration record.';
		}

		//// Since this is not a callout type of integration, TBD whether to log a message for it or not
		//Integration_Log__c log = new Integration_Log__c(
		//	Integration_Target__c = domain + '/api/' + version, 
		//	Integration_Type__c = 'ITSExamLauncherExt',
		//	Integration_Process__c = 'getUrl', 
		//	Message_Details__c = 'registrationId: ' + registrationId + ', registration: ' + registrationRecord.Name + ' ('+str1+')', 
		//	Message_Status__c = 'Success',
		//	Message_Request_Body__c = 'N/A',
		//	Message_Response_Body__c = url,
		//	Message_HTTP_Status_Code__c = 'N/A'
		//);
		
		//IntegrationLogHelper.insertLog(log);

		return url;
	}

	public static Registration__c queryForUrlParamsByRegistrationId(Id registrationId) {
		return [SELECT Id,
					Registration_Status__c,
					Exam_Expiry_Date__c,
					Name, 
					First_Name__c, 
					Last_Name__c,
					Email__c,
					NSAS_Candidate_ID__c,
					WBT_Register_ID__c, 
					Assessment_Product__c, 
					Assessment_Product__r.Program_Name__c, 
					Driver_Assessment_ID__c, 
					Form__c, 
					Pacing__c 
				FROM Registration__c 
				WHERE Id =: registrationId];
	}

	public static ccrz__E_Attribute__c queryForPrimaryAttributeVals(Id primaryAttributeId) {
		return [SELECT Id, 
					Driver_Form_Id__c, 
					Driver_Language__c 
				FROM ccrz__E_Attribute__c 
				WHERE Id =: primaryAttributeId];
	}

	public static ccrz__E_Attribute__c queryForSecondaryAttributeVals(Id secondaryAttributeId) {
		return [SELECT Id, 
					Timing_Factor__c 
				FROM ccrz__E_Attribute__c 
				WHERE Id =: secondaryAttributeId];
	}

	//public static String getUrl() {
	//	String str1 = 'StartTest';

	//	//String str2 = 'NSAS02'+'10004'+'4491193'+'0';
	//	String str2 = 'NSAS02'+'10004'+'R-000001'+'0';
	//	String str3 = 'FSAC-CLN-1601'+'FSAC-EN-002';
	//	ITSTest itsCode_test = new ITSTest((double) 1.0, (double) 2.0, (double) 3.0, (double) 4.0, (double) 5.0, str1, str2, str3, (double) 0.0, (double) 1.0,
	//			'p1r3i5m7e11s');
		
	//	String fcode = itsCode_test.GenerateCode();
		
	//	String url = 
	//			'https://staging.starttest.com/api/11.1.0.1/starttest.aspx?'+
	//			'program=NSAS02&examinee=10004&registration=R-000001&'+
	//			'code='+fcode+'&'+
	//			'xml=<Test><Examinee><LastName><![CDATA[Roselli]]></LastName><FirstName><![CDATA[Theresa]]></FirstName><Email>troselli@nbme.org</Email><HomePhone>215-590-9614</HomePhone><WorkPhone>215-590-9614</WorkPhone><City><![CDATA[Philadelphia]]></City><State>PA</State><Zip>19104</Zip><Country>United States including PR\\, VI\\, Guam</Country></Examinee><TestName>FSAC-CLN-1601</TestName><FormName>FSAC-EN-002</FormName><TimeFactor>1</TimeFactor><Sequential>0</Sequential><Language>ENG</Language><Demo>0</Demo><Secure>0</Secure></Test>';

	//	return url;
	//}
}