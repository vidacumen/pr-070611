@isTest
public with sharing class ITSExamLauncherExtTest {
	@testSetup
	static void setup() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
								PersonEmail = 'testing@email.com',
								PersonBirthdate = Date.newInstance(1990, 1, 1),
								RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Lightning_Community_Setup__c lcs = new Lightning_Community_Setup__c(Name = 'Community Name', Value__c = '');
		insert lcs;

		User u = new User(LastName = 'Tester',
						Email = 'tester123@email.com',
						Birthdate__c = Date.newInstance(1989, 1, 1),
						Username = 'tester123@email.com',
						Alias = 'Tester',
						TimeZoneSidKey = 'America/New_York',
						LocaleSidKey = 'en_US',
						EmailEncodingKey = 'UTF-8',
						LanguageLocaleKey = 'en_US',
						CommunityNickname = 'Tester',
						ProfileId = p.Id,
						ContactId = userAccount.PersonContactId);

		insert u;

		ccrz__E_Attribute__c parentattr1 = new ccrz__E_Attribute__c(Name = 'Forms');
		ccrz__E_Attribute__c parentattr2 = new ccrz__E_Attribute__c(Name = 'Pacing');

		insert parentattr1;
		insert parentattr2;

		ccrz__E_Attribute__c attr1 = new ccrz__E_Attribute__c(Name = 'Form 1',
																Driver_Form_ID__c = 'ABC123',
																Driver_Language__c = 'ENG',
																ccrz__ParentAttribute__c = parentattr1.Id);
		ccrz__E_Attribute__c attr2 = new ccrz__E_Attribute__c(Name = 'Pacing: Standard',
																Timing_Factor__c = '1',
																ccrz__ParentAttribute__c = parentattr2.Id);

		insert attr1;
		insert attr2;

		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		ccrz__E_Product__c product = new ccrz__E_Product__c(Name = 'Test Product',
														ccrz__SKU__c = '123456',
														Registration_Form__c = 'NSAS',
														Program_Name__c = 'NSAS101');
		insert product;

		List<Registration__c> regList = new List<Registration__c>();
		regList.add(new Registration__c(RecordTypeId = testRecordType.Id));
		regList.add(new Registration__c(RecordTypeId = testRecordType.Id,
													Assessment_Product__c = product.Id,
													Examinee__c = a.Id,
													Registration_Status__c = 'Assessment Available',
													First_Name__c = 'Test',
													Last_Name__c = 'Tester',
													Email__c = 'test123@yahoo.com',
													Form__c = attr1.Id,
													Pacing__c = attr2.Id));
		regList.add(new Registration__c(RecordTypeId = testRecordType.Id,
													Assessment_Product__c = product.Id,
													Examinee__c = a.Id,
													Registration_Status__c = 'Assessment In Progress',
													First_Name__c = 'Test',
													Last_Name__c = 'Tester',
													Email__c = 'test123@yahoo.com',
													Form__c = attr1.Id,
													Pacing__c = attr2.Id));
		regList.add(new Registration__c(RecordTypeId = testRecordType.Id,
													Assessment_Product__c = product.Id,
													Examinee__c = a.Id,
													Registration_Status__c = 'Assessment Complete',
													First_Name__c = 'Test',
													Last_Name__c = 'Tester',
													Email__c = 'test123@yahoo.com',
													Form__c = attr1.Id,
													Pacing__c = attr2.Id));

		insert regList;

		List<Web_Service_Setup__c> wssList = new List<Web_Service_Setup__c>();
		wssList.add(new Web_Service_Setup__c(Name = 'ITSExamLaunchDomain', Value__c = 'https://google.com'));
		wssList.add(new Web_Service_Setup__c(Name = 'ITSExamLaunchEncreptKey', Value__c = 'fakekey123456789123456789'));
		wssList.add(new Web_Service_Setup__c(Name = 'ITSExamLaunchWorkShop', Value__c = 'staging.test.com'));
		wssList.add(new Web_Service_Setup__c(Name = 'ITSExamLaunchVersion', Value__c = '11.1.0.1'));

		insert wssList;
	}

	@isTest
	static void constructor_passInParams_classVariablesSet() {
		Test.startTest();
			ITSExamLauncherExt its = new ITSExamLauncherExt((double) 1.0, 
															(double) 2.0, 
															(double) 3.0, 
															(double) 4.0, 
															(double) 5.0, 
															'str1', 
															'str2', 
															'str3', 
															(double) 0.0, 
															(double) 1.0,
															'encreptKey');
		Test.stopTest();

		System.assertEquals(its.mdblFactor1, 1.0);
		System.assertEquals(its.mdblFactor2, 2.0);
		System.assertEquals(its.mdblFactor3, 3.0);
		System.assertEquals(its.mdblFactor4, 4.0);
		System.assertEquals(its.mdblFactor5, 5.0);
		System.assertEquals(its.mstrValue1, 'str1');
		System.assertEquals(its.mstrValue2, 'str2');
		System.assertEquals(its.mstrValue3, 'str3');
		System.assertEquals(its.mdblNumber1, 0.0);
		System.assertEquals(its.mdblNumber2, 1.0);
		System.assertEquals(its.mstrEncryptionKey, 'encreptKey');
	}

	@isTest
	static void getCustomDateString2_callMethod_dateStringReturned() {
		Test.startTest();
			String result = ITSExamLauncherExt.getCustomDateString2();
		Test.stopTest();

		System.assertNotEquals(result, null);
	}

	@isTest
	static void SumAscString_callMethod_doubleReturned() {
		String s = '12';
		Double d = s.charAt(0) + s.charAt(1);

		Test.startTest();
			ITSExamLauncherExt its = new ITSExamLauncherExt((double) 1.0, 
															(double) 2.0, 
															(double) 3.0, 
															(double) 4.0, 
															(double) 5.0, 
															'str1', 
															'str2', 
															'str3', 
															(double) 0.0, 
															(double) 1.0,
															'encreptKey');
			Double result = its.SumAscString(s);

			System.debug('result: ' + result);
		Test.stopTest();

		System.assertEquals(result, d);
	}

	@isTest
	static void getUrl_registrationIdPassedInWithoutProduct_noProductFoundStringReturned() {
		Registration__c r = [SELECT Id
							FROM Registration__c
							WHERE Assessment_Product__c = null];
		Test.startTest();
			String result = ITSExamLauncherExt.getUrl(r.Id);
		Test.stopTest();

		System.assertEquals(result, 'ERROR - No Assessment Product found for the Registration record.');
	}

	@isTest
	static void getUrl_registrationIdPassedInAssessmentAvailable_assessmentAvailableUrlStringReturned() {
		Registration__c r = [SELECT Id
							FROM Registration__c
							WHERE Registration_Status__c = 'Assessment Available'];
		Test.startTest();
			String result = ITSExamLauncherExt.getUrl(r.Id);
		Test.stopTest();

		System.assertEquals(result.contains('/starttest.aspx'), true);
	}

	@isTest
	static void getUrl_registrationIdPassedInAssessmentInProgress_assessmentInProgressUrlStringReturned() {
		Registration__c r = [SELECT Id
							FROM Registration__c
							WHERE Registration_Status__c = 'Assessment In Progress'];
		Test.startTest();
			String result = ITSExamLauncherExt.getUrl(r.Id);
		Test.stopTest();

		System.assertEquals(result.contains('/RestartTest.aspx'), true);
	}

	@isTest
	static void getUrl_registrationIdPassedInAssessmentComplete_assessmentCompleteUrlStringReturned() {
		Registration__c r = [SELECT Id
							FROM Registration__c
							WHERE Registration_Status__c = 'Assessment Complete'];
		Test.startTest();
			String result = ITSExamLauncherExt.getUrl(r.Id);
		Test.stopTest();

		System.assertEquals(result.contains('/ViewScoreReport.aspx'), true);
	}
}