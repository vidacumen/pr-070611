public with sharing class ITSInboundAuraService {

	@AuraEnabled
	public static String queryStatus(Registration__c registrationRecord) {
		String returnValue = '';
		
		String regNameOrWBTId = (String.isBlank(registrationRecord.WBT_Register_ID__c) ? registrationRecord.Name : registrationRecord.WBT_Register_ID__c);
		String programRegistrationID = regNameOrWBTId;
		//String programId = registrationRecord.Assessment_Product__r.Program_Name__c;
		String programId = registrationRecord.Assessment_Product__r.Driver_Program_ID__c;
		System.debug('reg record: ' + registrationRecord);
		System.debug('reg program Id: ' + programId);

		if (String.isNotEmpty(programId)) {
			Map<String,String> authDetailsMap = ITSInboundExt.retrieveCode();
			if (authDetailsMap.get('Code') != null) {
				String code = authDetailsMap.get('Code');
				String userId = authDetailsMap.get('UserID');    	    	
				String resultID = ITSInboundExt.queryByProgramRegistrationIdResults(code, userId, programRegistrationID, programId);
				if (String.isNotEmpty(resultID)) {
					if (resultID != 'Assessment not started. No error.') {
						Map<String, String> status = ITSInboundExt.query(code, userId, programId, resultID);
						if (!status.containsKey('Error')) {
							returnValue = updateRegistrationRecord(status, registrationRecord);
						}
					} else {
						returnValue = 'Assessment not started. No error.';
					}
				}
			}
		}

		return returnValue;
	}

	@AuraEnabled
	public static String updateRegistrationRecord(Map<String, String> ITSResults, Registration__c regRecord) {
		String result = '';
		Savepoint sp = null;

		try {
			sp = Database.setSavepoint();
			String initialStatus = regRecord.Registration_Status__c;

			if (String.isBlank(ITSResults.get('CompleteDate'))) {
				regRecord.Registration_Status__c = 'Assessment in Progress';
			} else {
				regRecord.Registration_Status__c = 'Assessment Complete';
			}

			if (String.isNotBlank(ITSResults.get('StartDate'))) {
				String examStartDate = ITSResults.get('StartDate').replace('T', ' ');
				regRecord.Exam_Start_Date__c = Datetime.valueOf(examStartDate);
			}
			if (String.isNotBlank(ITSResults.get('RestartDate'))) {
				String examRestartDate = ITSResults.get('RestartDate').replace('T', ' ');
				regRecord.Exam_Restart_Date__c = Datetime.valueOf(examRestartDate);
			}
			if (String.isNotBlank(ITSResults.get('CompleteDate'))) {
				String examCompleteDate = ITSResults.get('CompleteDate').replace('T', ' ');
				regRecord.Exam_Completion_Date__c = Datetime.valueOf(examCompleteDate);
			}
			if (String.isNotBlank(ITSResults.get('MinutesRemaining'))) {
				regRecord.Minutes_Remaining__c = Decimal.valueOf(ITSResults.get('MinutesRemaining'));
			}
			if (String.isNotBlank(ITSResults.get('CurrentQuestion'))) {
				regRecord.Current_Question__c = ITSResults.get('CurrentQuestion');
			}
			if ((regRecord.Exam_Complete_Date_Original__c == null) && (String.isNotBlank(ITSResults.get('CompleteDate'))))  {
				String examCompleteDate = ITSResults.get('CompleteDate').replace('T', ' ');
				regRecord.Exam_Complete_Date_Original__c = Datetime.valueOf(examCompleteDate);
			}
			if ((regRecord.Exam_Start_Date_Original__c == null) && (String.isNotBlank(ITSResults.get('StartDate'))))  {
				String examStartDate = ITSResults.get('StartDate').replace('T', ' ');
				regRecord.Exam_Start_Date_Original__c = Datetime.valueOf(examStartDate);
			}

			update regRecord;

			if (initialStatus == regRecord.Registration_Status__c) {
				result = 'NO STATUS UPDATE';
			} else {
				result = 'STATUS UPDATED';
			}

		} catch (Exception ex) {
			Database.rollback(sp);
			System.debug('error: ' + ex.getMessage() + ' ' + ex.getLineNumber());
		}

		return result;
	}

	public static void myTestMethod() {
		Integer x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
		x = 0;
	}
}