@isTest
public with sharing class ITSInboundAuraServiceTest {
	@testSetup
	static void setup() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
								PersonEmail = 'testing@email.com',
								PersonBirthdate = Date.newInstance(1990, 1, 1),
								RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Lightning_Community_Setup__c lcs = new Lightning_Community_Setup__c(Name = 'Community Name', Value__c = '');
		insert lcs;

		User u = new User(LastName = 'Tester',
						Email = 'tester123@email.com',
						Birthdate__c = Date.newInstance(1989, 1, 1),
						Username = 'tester123@email.com',
						Alias = 'Tester',
						TimeZoneSidKey = 'America/New_York',
						LocaleSidKey = 'en_US',
						EmailEncodingKey = 'UTF-8',
						LanguageLocaleKey = 'en_US',
						CommunityNickname = 'Tester',
						ProfileId = p.Id,
						ContactId = userAccount.PersonContactId);

		insert u;

		ccrz__E_Product__c product = new ccrz__E_Product__c(Name = 'Test Product',
															ccrz__SKU__c = '123456',
															Registration_Form__c = 'NSAS',
															Interactive_Score_Report_Expiry_Days__c = 3);

		insert product;

		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		Registration__c reg = new Registration__c(RecordTypeId = testRecordType.Id,
													Examinee__c = a.Id,
													Assessment_Product__c = product.Id);

		insert reg;
	}

	@isTest
	static void updateRegistrationRecord_passInParamsForUpdate_updateStringReturned() {
		Registration__c r = [SELECT Id,
								Registration_Status__c,
								Exam_Start_Date__c,
								Exam_Restart_Date__c,
								Exam_Completion_Date__c,
								Minutes_Remaining__c,
								Current_Question__c,
								Exam_Complete_Date_Original__c,
								Exam_Start_Date_Original__c
							FROM Registration__c
							LIMIT 1];

		Map<String, String> testMap = new Map<String, String>();
		testMap.put('CompleteDate', '2018-09-09 12:00:00');
		testMap.put('StartDate', '2018-09-09 12:00:00');
		testMap.put('RestartDate', '2018-09-09 12:00:00');
		testMap.put('CompleteDate', '2018-09-09 12:00:00');
		testMap.put('MinutesRemaining', '10');
		testMap.put('CurrentQuestion', '5');

		Test.startTest();
			String result = ITSInboundAuraService.updateRegistrationRecord(testMap, r);
		Test.stopTest();

		System.assertEquals(result, 'STATUS UPDATED');
	}

	@isTest
	static void myTestMethod_callMethod_success() {
		Test.startTest();
			ITSInboundAuraService.myTestMethod();
		Test.stopTest();
	}
}