public with sharing class ITSInboundExt {
	public static List<Integration_Log__c> logsToInsert = new List<Integration_Log__c>();

	public static Map<String,String> retrieveCode() {
		Map<String,String> authDetailsMap = new Map<String,String>();

		Http http = new Http();
		HttpRequest request = new HttpRequest();

		String endpoint = Web_Service_Setup__c.getValues('ITSExamInboundEndpoint').Value__c;
		endpoint += '/User/Login';
		request.setEndpoint(endpoint);
		String username = Web_Service_Setup__c.getValues('ITSExamInboundUsername').Value__c;
		String password = Web_Service_Setup__c.getValues('ITSExamInboundPassword').Value__c;
		String encodedBody = 
		'username='+EncodingUtil.urlEncode(username,'UTF-8')+
		'&password='+EncodingUtil.urlEncode(password,'UTF-8');

		request.setBody(encodedBody);

		request.setMethod('POST');

		HttpResponse response = http.send(request);

		// Parse the JSON response
		if (response.getStatusCode() != 200) {
			System.debug('The status code returned was not expected: ' +
				response.getStatusCode() + ' ' + response.getStatus());

			authDetailsMap.put('Error', 'Error');

			//Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endpoint, 
			//	Integration_Type__c = 'ITSInboundExt', 
			//	Integration_Process__c = 'retrieveCode',
			//	Message_Details__c = endpoint, 
			//	Message_Status__c = 'Error',
			//	Message_Request_Body__c = request.getBody(),
			//	Message_Response_Error_Code__c = String.valueOf(response.getStatusCode()),
			//	Message_Response_Error_Description__c = response.getStatus()
			//);

			//IntegrationLogHelper.insertLog(log);
		} else {
			Dom.Document doc = response.getBodyDocument();

			//Retrieve the root element for this document.
			Dom.XMLNode login = doc.getRootElement();

			authDetailsMap.put('Code', login.getChildElement('Code', null).getText());
			authDetailsMap.put('UserID', login.getChildElement('UserID', null).getText());
			authDetailsMap.put('Status', login.getChildElement('Status', null).getText());

			//Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endpoint, 
			//	Integration_Type__c = 'ITSInboundExt',
			//	Integration_Process__c = 'retrieveCode', 
			//	Message_Details__c = 'Code: ' + login.getChildElement('Code', null).getText() + ', UserID: ' + login.getChildElement('UserID', null).getText() + ', Status: ' + login.getChildElement('Status', null).getText(), 
			//	Message_Status__c = 'Success',
			//	Message_Request_Body__c = request.getBody(),
			//	Message_Response_Body__c = String.valueOf(doc),
			//	Message_HTTP_Status_Code__c = String.valueOf(response.getStatusCode())
			//);

			//logsToInsert.add(log);
		}

		return authDetailsMap;    

	}
 
	public static String queryByProgramRegistrationIdResults(String code, String userId, String programRegistrationID, String programId) {
		String returnValue = null;

		Http http = new Http();
		HttpRequest request = new HttpRequest();

		String endpoint = Web_Service_Setup__c.getValues('ITSExamInboundEndpoint').Value__c;
		endpoint += '/Result/QueryByProgramRegistrationId';
		request.setEndpoint(endpoint);

		String body = 
		'UserID='+userId+
		'&Code='+code+
		'&ProgramID='+programId+
		'&ProgramRegistrationIDs='+programRegistrationID;

		request.setBody(body);

		request.setMethod('POST');

		HttpResponse response = http.send(request);

		// Parse the JSON response
		if (response.getStatusCode() != 200) {
			System.debug('The status code returned was not expected: ' +
				response.getStatusCode() + ' ' + response.getStatus());

		 //   Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endpoint, 
			//	Integration_Type__c = 'ITSInboundExt',
			//	Integration_Process__c = 'queryByProgramRegistrationIdResults', 
			//	Message_Details__c = 'userId: ' + userId + ', programRegistrationID: ' + programRegistrationID + ', programId: ' + programId + ', + code: ' + code, 
			//	Message_Status__c = 'Error',
			//	Message_Request_Body__c = request.getBody(),
			//	Message_Response_Error_Code__c = String.valueOf(response.getStatusCode()),
			//	Message_Response_Error_Description__c = response.getStatus()
			//);

		 //   logsToInsert.add(log);
		 //   IntegrationLogHelper.insertLogList(logsToInsert);
		} else {
			Dom.Document doc = response.getBodyDocument();

			//Retrieve the root element for this document.
			Dom.XMLNode resultIds = doc.getRootElement();

			//May need to account for multiple results
			Dom.XMLNode[] allChildNodes = resultIds.getChildElements();
			for (Dom.XMLNode node : allChildNodes) {
				if (node.getName() == 'ResultID') {
					returnValue = resultIds.getChildElement('ResultID', null).getText();
				}
			}

		 //   Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endpoint, 
			//	Integration_Type__c = 'ITSInboundExt',
			//	Integration_Process__c = 'queryByProgramRegistrationIdResults', 
			//	Message_Details__c = 'userId: ' + userId + ', programRegistrationID: ' + programRegistrationID + ', programId: ' + programId + ', + code: ' + code, 
			//	Message_Status__c = 'Success',
			//	Message_Request_Body__c = request.getBody(),
			//	Message_Response_Body__c = String.valueOf(doc),
			//	Message_HTTP_Status_Code__c = String.valueOf(response.getStatusCode())
			//);

		 //   logsToInsert.add(log);
			
			if (String.isEmpty(returnValue)) {
				returnValue = 'Assessment not started. No error.';
			}
		}

		return returnValue;

	}

	public static Map<String, String> query(String code, String userId, String programId, String resultID) {
		Map<String, String> allResults = new Map<String, String>();
		Http http = new Http();
		HttpRequest request = new HttpRequest();

		String endpoint = Web_Service_Setup__c.getValues('ITSExamInboundEndpoint').Value__c;
		endpoint += '/Result/Query';
		request.setEndpoint(endpoint);

		//String programID = '30';
		//String includeFlag = '260';
		String includeFlag = Web_Service_Setup__c.getValues('ITSExamInboundIncludeFlag').Value__c;

		String body = 
		'UserID='+userId+
		'&Code='+code+
		'&ProgramID='+programId+
		'&ResultID='+resultID+
		'&IncludeFlag='+includeFlag;

		request.setBody(body);

		request.setMethod('POST');

		HttpResponse response = http.send(request);
		
		if (response.getStatusCode() != 200) {
			System.debug('The status code returned was not expected: ' +
				response.getStatusCode() + ' ' + response.getStatus());
			allResults.put('Error', 'ITS Error - Please contact NBME');

		 //   Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endpoint, 
			//	Integration_Type__c = 'ITSInboundExt', 
			//	Integration_Process__c = 'query',
			//	Message_Details__c = 'userId: ' + userId + ', programId: ' + programId + ', code: ' + code + ', resultID: ' + resultID + ', includeFlag: ' + includeFlag, 
			//	Message_Status__c = 'Error',
			//	Message_Request_Body__c = request.getBody(),
			//	Message_Response_Error_Code__c = String.valueOf(response.getStatusCode()),
			//	Message_Response_Error_Description__c = response.getStatus()
			//);

		 //   logsToInsert.add(log);
		 //   IntegrationLogHelper.insertLogList(logsToInsert);
		} else {
			Dom.Document doc = response.getBodyDocument();

			//May need to account for multiple results
			Dom.XMLNode[] allStartsChildNodes = doc.getRootElement().getChildElement('Starts', null).getChildren();
			Dom.XMLNode[] allEventChildNodes = doc.getRootElement().getChildElement('Event', null).getChildren();
			String numberOfStarts = doc.getRootElement().getChildElement('Event', null).getChildElement('NumStarts', null).getText();
			String startDate = '';
			String restartDate = '';
			String completeDate = '';
			String minutesRemaining = '';
			String currentQuestion = '';

			for (Dom.XMLNode node : allStartsChildNodes) {
				if (node.getName() == 'Start') {
					if (node.getChildElement('StartNumber', null).getText() == numberOfStarts) {
						restartDate = node.getChildElement('DateTime', null).getText();
					}
				}
			}

			for (Dom.XMLNode node : allEventChildNodes) {
				if (node.getName() == 'StartTime') {
					startDate = node.getText();
				} else if (node.getName() == 'MinutesRemaining') {
					minutesRemaining = node.getText();
				} else if (node.getName() == 'CurrentLocation') {
					currentQuestion = node.getText();
				} else if (node.getName() == 'CompleteDate') {
					completeDate = node.getText();
				}
			}

			allResults.put('StartDate', startDate);
			allResults.put('RestartDate', restartDate);
			allResults.put('CompleteDate', completeDate);
			allResults.put('MinutesRemaining', minutesRemaining);
			allResults.put('CurrentQuestion', currentQuestion);

			System.debug('allResults: ' + allResults);

		 //   Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endpoint, 
			//	Integration_Type__c = 'ITSInboundExt', 
			//	Integration_Process__c = 'query',
			//	Message_Details__c = 'userId: ' + userId + ', programId: ' + programId + ', code: ' + code + ', resultID: ' + resultID + ', includeFlag: ' + includeFlag, 
			//	Message_Status__c = 'Success',
			//	Message_Request_Body__c = request.getBody(),
			//	Message_Response_Body__c = String.valueOf(doc),
			//	Message_HTTP_Status_Code__c = String.valueOf(response.getStatusCode())
			//);

		 //   logsToInsert.add(log);
		 //   IntegrationLogHelper.insertLogList(logsToInsert);
		}

		return allResults;
	}
}