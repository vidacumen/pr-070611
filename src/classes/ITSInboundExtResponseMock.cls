@isTest
public class ITSInboundExtResponseMock implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
		res.setBody('<Login><Code>fakeCodeHere</Code><Status>Success</Status><UserID>fakeUserIdHere</UserID></Login>');
		res.setStatusCode(200);
		res.setStatus('Success');
		return res;
	}
}