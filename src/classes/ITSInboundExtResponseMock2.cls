@isTest
public class ITSInboundExtResponseMock2 implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
		res.setBody('<ResultIDs><ResultID>12345</ResultID></ResultIDs>');
		res.setStatusCode(200);
		res.setStatus('Success');
		return res;
	}
}