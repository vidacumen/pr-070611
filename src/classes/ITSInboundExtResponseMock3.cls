@isTest
public class ITSInboundExtResponseMock3 implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
		res.setBody('<Results><Starts><Start><StartNumber>1</StartNumber><DateTime>2018-09-09</DateTime></Start></Starts><Event><NumStarts>1</NumStarts><StartTime>2018-09-09</StartTime><MinutesRemaining>5</MinutesRemaining><CurrentLocation>2</CurrentLocation><CompleteDate>2018-09-09</CompleteDate></Event></Results>');
		res.setStatusCode(200);
		res.setStatus('Success');
		return res;
	}
}