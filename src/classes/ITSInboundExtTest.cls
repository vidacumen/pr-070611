@isTest
public with sharing class ITSInboundExtTest {
	@testSetup
	static void setup() {
		Web_Service_Setup__c wsSetup1 = new Web_Service_Setup__c(Name = 'ITSExamInboundEndpoint', Value__c = 'https://staging.programworkshop.com/web%20services/2.1.2');
		Web_Service_Setup__c wsSetup2 = new Web_Service_Setup__c(Name = 'ITSExamInboundUsername', Value__c = 'blah@nbme.org');
		Web_Service_Setup__c wsSetup3 = new Web_Service_Setup__c(Name = 'ITSExamInboundPassword', Value__c = 'jkh2452');
		Web_Service_Setup__c wsSetup4 = new Web_Service_Setup__c(Name = 'ITSExamInboundIncludeFlag', Value__c = '260');

		List<Web_Service_Setup__c> wsSetupList = new List<Web_Service_Setup__c>{wsSetup1,
																				wsSetup2,
																				wsSetup3,
																				wsSetup4};

		insert wsSetupList;
	}

	@isTest
	static void retrieveCode_callMethod_successMapReturned() {
		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new ITSInboundExtResponseMock());
			Map<String,String> res = ITSInboundExt.retrieveCode();
		Test.stopTest();

		System.assertEquals(res.get('Code'), 'fakeCodeHere');
		System.assertEquals(res.get('Status'), 'Success');
		System.assertEquals(res.get('UserID'), 'fakeUserIdHere');
	}

	@isTest
	static void queryByProgramRegistrationIdResults_callMethod_successMapReturned() {
		String code = 'fakeCodeHere';
		String userId = 'fakeUserIdHere';
		String programRegistrationID = 'fakeProgRegId';
		String programId = 'fakeProgId';

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new ITSInboundExtResponseMock2());
			String res = ITSInboundExt.queryByProgramRegistrationIdResults(code, userId, programRegistrationID, programId);
		Test.stopTest();

		System.assertEquals(res, '12345');
	}

	@isTest
	static void query_callMethod_successMapReturned() {
		String code = 'fakeCodeHere';
		String userId = 'fakeUserIdHere';
		String programId = 'fakeProgId';
		String resultID = 'fakeResultId';

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new ITSInboundExtResponseMock3());
			Map<String,String> res = ITSInboundExt.query(code, userId, programId, resultID);
		Test.stopTest();

		System.assertEquals(res.get('StartDate'), '2018-09-09');
		System.assertEquals(res.get('RestartDate'), '2018-09-09');
		System.assertEquals(res.get('CompleteDate'), '2018-09-09');
		System.assertEquals(res.get('MinutesRemaining'), '5');
		System.assertEquals(res.get('CurrentQuestion'), '2');
	}
}