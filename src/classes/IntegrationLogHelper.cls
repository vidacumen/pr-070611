public class IntegrationLogHelper {
	public List<Integration_Log__c> logsToInsert = new List<Integration_Log__c>();

	public static void insertLogList(List<Integration_Log__c> logs) {
		insert logs;
	}

	public static void insertLog(Integration_Log__c log) {
		insert log;
	}

	public static void deleteAllLogs() {
		List<Integration_Log__c> logList = [SELECT Id
											FROM Integration_Log__c];

		delete logList;
	}
}