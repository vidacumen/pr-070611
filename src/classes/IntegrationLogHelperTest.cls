/**
 * Created by aszampias on 8/19/18.
 */
@isTest
public with sharing class IntegrationLogHelperTest {

	@isTest static void IntegrationLogHelper_insertLogList_Success(){
		Integration_Log__c log1 = new Integration_Log__c(Integration_Type__c='ITSInboundExt', Message_Status__c='Success');
		Integration_Log__c log2 = new Integration_Log__c(Integration_Type__c='WebFredExamLauncherExt', Message_Status__c='Success');
		List<Integration_Log__c> logs = new List<Integration_Log__c>();
		logs.add(log1);
		logs.add(log2);
		IntegrationLogHelper.insertLogList(logs);
		List<Integration_Log__c> listOfLogsReturned = [SELECT Id FROM Integration_Log__c];
		Test.startTest();
			System.assertEquals(2, listOfLogsReturned.size());
		Test.stopTest();
	}
	@isTest static void IntegrationLogHelper_insertSingleLog_Success(){
		Integration_Log__c log1 = new Integration_Log__c(Integration_Type__c='WebFredExamLauncherExt', Message_Status__c='Success');
		IntegrationLogHelper.insertLog(log1);
		List<Integration_Log__c> listOfLogsReturned = [SELECT Id FROM Integration_Log__c];
		Test.startTest();
			System.assertEquals(1, listOfLogsReturned.size());
		Test.stopTest();
	}

	@isTest static void IntegrationLogHelper_deleteLogs_Success(){
		Integration_Log__c log1 = new Integration_Log__c(Integration_Type__c='WebFredExamLauncherExt', Message_Status__c='Success');
		IntegrationLogHelper.insertLog(log1);
		IntegrationLogHelper.deleteAllLogs();
		List<Integration_Log__c> listOfLogsReturned = [SELECT Id FROM Integration_Log__c];
		Test.startTest();
			System.assertEquals(0, listOfLogsReturned.size());
		Test.stopTest();
	}
}