public class LastNameMatcher implements MatcherInterface {
	public Set<Id> findMatches(String firstname,
								String middlename, 
								String lastname, 
								String email, 
								String usmleid, 
								String aamcid,
								String nbmeid,
								Date birthdate,
								String phone,
								String medicalschool,
								String gradyear) {

		String newPhone = '';
		Set<Id> matches = new Map<Id, Account>().keySet();

		if (String.isNotEmpty(phone)) {
			phone = phone.replaceAll('[^0-9]', '');
			for (Integer i = 0; i < phone.length(); i++) {
				newPhone += '%' + phone.substring(i,i+1) + '%';
			}
			matches = new Map<Id, Account>	([SELECT Id
												FROM Account
												WHERE IsPersonAccount = true 
												AND LastName =: lastname 
												AND PersonBirthdate =: birthdate 
												AND (Phone LIKE :newPhone OR USMLE_ID__c =: usmleid OR AAMC_ID__c =: aamcid)
											]).keySet();
		} else {
			matches = new Map<Id, Account>	([SELECT Id
												FROM Account
												WHERE IsPersonAccount = true 
												AND LastName =: lastname 
												AND PersonBirthdate =: birthdate 
												AND (Phone =: newPhone OR USMLE_ID__c =: usmleid OR AAMC_ID__c =: aamcid)
											]).keySet();
		}

		return matches;
	}

	public RegistrationResponse handleMatch(User user,
											Set<Id> accountId,
											String firstname,
											String middlename, 
											String lastname, 
											String email, 
											String usmleid, 
											String aamcid,
											String nbmeid,
											Date birthdate,
											String phone,
											String medicalschool,
											String gradyear) {

		RegistrationResponse result = new RegistrationResponse();

		if (user != null) {
			User u = [SELECT Email FROM User WHERE Id =: user.Id];
			String maskedEmail;
			String[] splitEmail = u.Email.split('@');

			if(splitEmail.size() == 2){
				maskedEmail = splitEmail[0].left(2);
				String asterisk = '*';
				maskedEmail += asterisk.repeat(splitEmail[0].length() - 2);

				String[] splitEmail2 = splitEmail[1].split('\\.', 2);
				maskedEmail += '@' + splitEmail2[0].left(1);
				maskedEmail += asterisk.repeat(splitEmail2[0].length() - 1);

				maskedEmail += '.' + splitEmail2[1];
			}

			result.message = 'The following email address was found for the user: ' + maskedEmail;
			result.isSuccess = true;
		} else {
			result.message = 'Matching error - No user found.';
			result.isSuccess = false;
		}

		return result;
	}

	public RegistrationResponse handleNoMatch(User user,
												Set<Id> accountId,
												String firstname,
												String middlename, 
												String lastname, 
												String email, 
												String usmleid, 
												String aamcid,
												String nbmeid,
												Date birthdate,
												String phone,
												String medicalschool,
												String gradyear) {
		
		RegistrationResponse result = new RegistrationResponse();

		Savepoint sp = null;
        try {
        	sp = Database.setSavepoint();
			if (accountId.size() == 1) {
				Account a = [SELECT Id,
									OwnerId,
									PersonContactId,
									PersonEmail
							FROM Account
							WHERE Id IN :accountId];

				User accOwnerUser = [SELECT Id,
										UserRoleId
									FROM User
									WHERE Id =: a.OwnerId];

				if(accOwnerUser.UserRoleId != null) {
					String alias = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
		            alias += String.valueOf(Crypto.getRandomInteger()).substring(1,7);

		            String nickname = ((email != null && email.length() > 40) ? email.substring(0,40) : email );

					Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

					User u = new User(FirstName = firstname,
										MiddleName = middlename,
										LastName = lastname,
										Email = email,
										Username = email,
										Alias = alias,
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = nickname,
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

					//Id userId = Site.createExternalUser(u, a.Id, null, true);
					UserManagement userMgmt = new UserManagement();
					Boolean succesful = userMgmt.createUser(u, a);

					if (succesful) {
						try {
							updateEmail(accountId,email);
							result.message = 'A new user has been created and the email address on your account has been updated.';
							result.isSuccess = true;
						} catch (Exception ex) {
							Database.rollback(sp);
							result.message = 'Error - cannot create new user and update email address.';
							result.isSuccess = false;
						}
					} else {
						Database.rollback(sp);
						result.message = 'Error - cannot create new user and update email address.';
						result.isSuccess = false;
					}
				}
			}
		} catch (Exception ex) {
            Database.rollback(sp);
            result.message = 'Error - cannot create new user and update email address.';
            result.isSuccess = false;
        }

		return result;
	}

	@future
	public static void updateEmail(Set<Id> accountId, String email) {
		Account a = [SELECT Id,
							PersonEmail
						FROM Account
						WHERE Id IN :accountId];

		a.PersonEmail = email;
		//update a;
		AccountManagement acctMgmt = new AccountManagement();
		Boolean successful = acctMgmt.updateAccount(a);
	}
}