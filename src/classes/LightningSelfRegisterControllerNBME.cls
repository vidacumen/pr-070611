global without sharing class LightningSelfRegisterControllerNBME {

	public LightningSelfRegisterControllerNBME() {}

	@TestVisible 
	private static boolean isValidPassword(String password, String confirmPassword) {
		return password == confirmPassword;
	}

	@TestVisible 
	private static boolean siteAsContainerEnabled(String communityUrl) {
		Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');

		return authConfig.isCommunityUsingSiteAsContainer();
	}

	@TestVisible 
	private static void validatePassword(User u, String password, String confirmPassword) {
		if (!Test.isRunningTest()) {
			Site.validatePassword(u, password, confirmPassword);
		}

		return;
	}

	@AuraEnabled
	public static RegistrationResponse selfRegister(String firstname , 
													String middlename, 
													String lastname, 
													String email, 
													String usmleid, 
													String aamcid,
													String nbmeid,
													String birthdate,
													String phone,
													String medicalschool,
													String gradyear, 
													String password, 
													String confirmPassword, 
													String accountId, 
													String regConfirmUrl, 
													String extraFields, 
													String startUrl, 
													Boolean includePassword) {

		firstname = String.escapeSingleQuotes(firstname);
		middlename = String.escapeSingleQuotes(middlename);
		lastname = String.escapeSingleQuotes(lastname);
		email = String.escapeSingleQuotes(email);
		usmleid = String.escapeSingleQuotes(usmleid);
		aamcid = String.escapeSingleQuotes(aamcid);
		nbmeid = String.escapeSingleQuotes(nbmeid);
		phone = String.escapeSingleQuotes(phone);
		medicalschool = String.escapeSingleQuotes(medicalschool);
		gradyear = String.escapeSingleQuotes(gradyear);

		Savepoint sp = Database.setSavepoint();
		RegistrationResponse result = new RegistrationResponse();

		try {
			if (lastname == null || String.isEmpty(lastname)) {
				result.message = Label.Site.lastname_is_required;
				result.isSuccess = false;
				return result;
			}

			if (email == null || String.isEmpty(email)) {
				result.message = Label.Site.email_is_required;
				result.isSuccess = false;
				return result;
			}

			if (birthdate == null || String.isEmpty(birthdate)) {
				result.message = 'Birthdate is required.';
				result.isSuccess = false;
				return result;
			}

			String birthdayMonth = birthdate.substring(0, 2);
			String birthdayDay = birthdate.substring(3, 5);
			String birthdayYear = birthdate.substring(6, 10);
			String formattedBirthdate = birthdayYear + '-' + birthdayMonth + '-' + birthdayDay;
			Date newbirthdate;

			try {
				newbirthdate = Date.valueOf(formattedBirthdate);
			} catch (Exception ex) {
				result.message = 'Please enter a valid date for birthdate.';
				result.isSuccess = false;
				return result;
			}

			Integer totalDays = newbirthdate.daysBetween(Date.today());

			if ((Integer)(Math.floor(totalDays/365.2425)) < 13) {
				result.message = 'Examinee must be at least 13 years of age.';
				result.isSuccess = false;
				return result;
			}

			String newPhone = phone;

			if (!String.isBlank(phone)) {
				phone = phone.replaceAll('\\D','');
				if (phone.length() == 10) {
					String piece1 = phone.substring(0, 3);
					String piece2 = phone.substring(3, 6);
					String piece3 = phone.substring(6, 10);

					newPhone = '(' + piece1 + ') ' + piece2 + '-' + piece3;
				}
			}

			EmailAddressMatcher matchByEmail = new EmailAddressMatcher();
			Set<Id> accountsForMatch1 = matchByEmail.findMatches(firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
			if (accountsForMatch1.size() == 1) {
				User usersForMatch1 = searchForAssociatedUser(accountsForMatch1);
				if (usersForMatch1 != null) {
					//display message with masked version of email
					result = matchByEmail.handleMatch(usersForMatch1,accountsForMatch1,firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
					return result;
				} else {
					//create new user and update email on Person Account
					result = matchByEmail.handleNoMatch(usersForMatch1,accountsForMatch1,firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
					//matchByEmail.updateEmail(accountsForMatch1,email);
					return result;
				}
			}

			List<MatcherInterface> matchOrder = new List<MatcherInterface> {new LastNameMatcher(), 
																			new NoEmailOrLastNameMatcher(), 
																			new MedSchool1Matcher(), 
																			new MedSchool2Matcher()};

			for (MatcherInterface match : matchOrder) {
				Set<Id> accountsForMatch = match.findMatches(firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
				if (accountsForMatch.size() == 1) {
					User usersForMatch = searchForAssociatedUser(accountsForMatch);
					if (usersForMatch != null) {
						//display message with masked version of email
						result = match.handleMatch(usersForMatch,accountsForMatch,firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
						return result;
					} else {
						//create new user and update email on Person Account
						//uncomment line below to do original no match logic
						result = createPersonAccountAndUser(firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
						//result = match.handleNoMatch(usersForMatch,accountsForMatch,firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,country,medicalschool,gradyear);
						//match.updateEmail(accountsForMatch,email);
						return result;
					}
				}
			}

			//no matches, create new Person Account AND user
			result = createPersonAccountAndUser(firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,newbirthdate,newPhone,medicalschool,gradyear);
			return result;

		} catch (Exception ex) {
			Database.rollback(sp);
			result.message = 'Error - Please contact NBME.';
			result.isSuccess = false;
			return result;
		}
	}
	
	@AuraEnabled
	global static String setExperienceId(String expId) {
		// Return null if there is no error, else it will return the error message 
		try {
			if (expId != null) {
				Site.setExperienceId(expId);
			}
			return null; 
		} catch (Exception ex) {
			return ex.getMessage();
		}
	}

	@AuraEnabled
	public static List<DSE__DS_International__c> getCountryPicklistValues() {
		List<DSE__DS_International__c> countries = [SELECT DSE__DS_Billing_Country__c
													FROM DSE__DS_International__c
													ORDER BY DSE__DS_Billing_Country__c
		];
		return countries;
	}

	public static User searchForAssociatedUser(Set<Id> accountId) {
		Account a = [SELECT PersonContactId
					FROM Account
					WHERE Id =: accountId];

		List<User> u = [SELECT Name, Id
						FROM User
						WHERE ContactId =: a.PersonContactId
						LIMIT 1];

		if(u.size() > 0) {
			return u[0];
		}

		return null;
	}

	public static RegistrationResponse createPersonAccountAndUser(String firstname,
																	String middlename,
																	String lastname,
																	String email,
																	String usmleid,
																	String aamcid,
																	String nbmeid,
																	Date birthdate,
																	String phone,
																	String medicalschool,
																	String gradyear) {
		Savepoint sp = Database.setSavepoint();
		RegistrationResponse result = new RegistrationResponse();

		try {
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee' LIMIT 1];

			ccrz__E_AccountGroup__c accountGroup = [SELECT Id FROM ccrz__E_AccountGroup__c WHERE Name = 'Examinee Account Group' LIMIT 1];

			List<Account_Owner_Table__c> accountOwners = Account_Owner_Table__c.getall().values();

			//List<User_Portal_Ownership_Tacker__c> userList = User_Portal_Ownership_Tacker__c.getall().values();

			Account personAccount = new Account(FirstName = firstname,
												MiddleName = middlename,
												LastName = lastname,
												PersonEmail = email,
												Phone = phone,
												USMLE_Id__c = usmleid,
												AAMC_Id__c = aamcid,
												PersonBirthdate = birthdate,
												Med_School_Grad_Year__c = gradyear,
												RecordTypeId = personAccountRecordType.Id,
												ccrz__E_AccountGroup__c = accountGroup.Id,
												OwnerId = accountOwners[0].Owner_Id__c);

			if (String.isNotEmpty(medicalschool)) {
				Account medSchool = [SELECT Id FROM Account WHERE Name =: medicalschool LIMIT 1];
				personAccount.Medical_School__c = medSchool.Id;
			}

			insert personAccount;

			if (personAccount != null) {
				Account a = [SELECT Id,
									OwnerId,
									PersonContactId,
									PersonEmail
							FROM Account
							WHERE Id =: personAccount.Id];

				User accOwnerUser = [SELECT Id,
										UserRoleId
									FROM User
									WHERE Id =: a.OwnerId];

				if (accOwnerUser.UserRoleId != null) {
					String alias = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
					alias += String.valueOf(Crypto.getRandomInteger()).substring(1,7);

					//String nickname = ((email != null && email.length() > 40) ? email.substring(0,40) : email );
					String nickname = alias;

					Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

					User u = new User(FirstName = firstname,
										MiddleName = middlename,
										LastName = lastname,
										Email = email,
										Username = email,
										Alias = alias,
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = nickname,
										ProfileId = p.Id,
										ContactId = a.PersonContactId,
										IsActive = true);

					//String userId = Site.createPortalUser(u, null, null);
					Id userId = Site.createExternalUser(u, personAccount.Id, null, true);

					result.message = 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.';
					result.isSuccess = true;
				} else {
					Database.rollback(sp);
					result.message = 'Error - Please contact NBME.';
					result.isSuccess = false;
				}
			}
		} catch (Exception ex) {
			Database.rollback(sp);
			//result.message = JSON.serializePretty(ex.getCause()) + ' - ' + ex.getMessage() + ' - ' + ex.getTypeName() + ' - ' + ex.getStackTraceString();
			System.debug(ex.getMessage());
			//if (ex.getTypeName() == 'Site.ExternalUserCreateException') {
			//	result.message = 'Error - SOME CUSTOM MESSAGE DUE TO EMAIL ONLY MATCH';
			//} else {
				result.message = 'Error - Please contact NBME.';
			//}

			result.isSuccess = false;
		}

		return result;
	}

	@AuraEnabled
	public static List<Map<String,Object>> getExtraFields(String extraFieldsFieldSet) { 
		List<Map<String,Object>> extraFields = new List<Map<String,Object>>();
		Schema.FieldSet fieldSet = Schema.SObjectType.User.fieldSets.getMap().get(extraFieldsFieldSet);
		if (!Test.isRunningTest()) {
		if (fieldSet != null) {
			for (Schema.FieldSetMember f : fieldSet.getFields()) {
				Map<String, Object> fieldDetail = new Map<String, Object>();
				fieldDetail.put('dbRequired', f.getDBRequired());
				fieldDetail.put('fieldPath', f.getFieldPath());
				fieldDetail.put('label', f.getLabel());
				fieldDetail.put('required', f.getRequired());
				fieldDetail.put('type', f.getType());
				fieldDetail.put('value', '');   // client will populate
				extraFields.add(fieldDetail);
			}}}
		return extraFields;
	}
}