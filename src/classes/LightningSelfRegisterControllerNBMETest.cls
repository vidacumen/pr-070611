@isTest
public with sharing class LightningSelfRegisterControllerNBMETest {
	
	@isTest
	static void isValidPassword_PassInValidPassword_ReturnTrue() {
		String password = 'abcd1234!';
		String confirmPassword = password;

		Test.startTest();
			Boolean result = LightningSelfRegisterControllerNBME.isValidPassword(password, confirmPassword);
		Test.stopTest();

		System.assertEquals(result, true);
	}

	@isTest
	static void isValidPassword_PassInInvalidPassword_ReturnFalse() {
		String password = 'abcd1234!';
		String confirmPassword = 'helloworld1$';

		Test.startTest();
			Boolean result = LightningSelfRegisterControllerNBME.isValidPassword(password, confirmPassword);
		Test.stopTest();

		System.assertEquals(result, false);
	}

	@isTest
	static void getCountryPicklistValues_CallMethod_CountryPicklistValuesReturned() {
		Test.startTest();
			List<DSE__DS_International__c> result = LightningSelfRegisterControllerNBME.getCountryPicklistValues();
		Test.stopTest();

		System.assertNotEquals(result, null);
	}

	@isTest
	static void searchForAssociatedUser_PassInAccountIdForAccountWithUser_UserReturned() {
    	UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;

			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;
		

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			Set<Id> acc = new Map<Id, Account> ([SELECT Id
												FROM Account
												WHERE Id =: testPersonAccount.Id]).keySet();

			Test.startTest();
				User result = LightningSelfRegisterControllerNBME.searchForAssociatedUser(acc);
			Test.stopTest();

			System.assertNotEquals(result, null);
		}
	}

	@isTest
	static void searchForAssociatedUser_PassInAccountIdForAccountWithoutUser_NoUserReturned() {
		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;
		}

		Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];

		Set<Id> acc = new Map<Id, Account> ([SELECT Id
											FROM Account
											WHERE Id =: a.Id]).keySet();

		Test.startTest();
			User result = LightningSelfRegisterControllerNBME.searchForAssociatedUser(acc);
		Test.stopTest();

		System.assertEquals(result, null);
	}

	@isTest
	static void createPersonAccountAndUser_OnlyRequiredFieldsPopulated_SuccessObjectReturned() {
		String lastname = 'Smith';
		String email = 'jsmith@yahoo.com';
		Date birthdate = Date.newInstance(1989, 1, 1);
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		insert userWithRole;
		System.runAs(userWithRole) {
			Account_Owner_Table__c testOwner = new Account_Owner_Table__c(Name = 'testOwner',
																			Owner_Id__c = userWithRole.Id);

			insert testOwner;

			ccrz__E_AccountGroup__c testAcctGroup = new ccrz__E_AccountGroup__c(Name = 'Examinee Account Group');
			insert testAcctGroup;

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.createPersonAccountAndUser(null,null,lastname,email,null,null,null,birthdate,null,null,null);
			Test.stopTest();

			//System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
		}
	}

	@isTest
	static void createPersonAccountAndUser_AllFieldsPopulated_SuccessObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		insert userWithRole;

		System.runAs(userWithRole) {
			RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];
		
			Account medSchool = new Account(Name = 'Indiana University School of Medicine',
											Type = 'Medical School',
											Institution_ID__c = 'NBME~123456',
											RecordType = businessAccountRecordType);
			insert medSchool;

			Account_Owner_Table__c testOwner = new Account_Owner_Table__c(Name = 'testOwner',
																			Owner_Id__c = userWithRole.Id);

			insert testOwner;

			ccrz__E_AccountGroup__c testAcctGroup = new ccrz__E_AccountGroup__c(Name = 'Examinee Account Group');
			insert testAcctGroup;

			String firstname = 'John';
			String middlename = 'Lee';
			String lastname = 'Smith';
			String email = 'jsmith@yahoo.com';
			String usmleid = '01369180';
			String aamcid = '11121162';
			String nbmeid = '376418';
			Date birthdate = Date.newInstance(1989, 1, 1);
			String phone = '111-222-3333';
			String medicalschool = medSchool.Name;
			String gradyear = '2005';

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.createPersonAccountAndUser(firstname,middlename,lastname,email,usmleid,aamcid,nbmeid,birthdate,phone,medicalschool,gradyear);
			Test.stopTest();

			//System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
		}
	}

	@isTest
	static void selfRegister_LastNameNull_LastNameRequiredObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();
		String email = 'jsmith@yahoo.com';
		String birthdate = '01-01-1989';

		Test.startTest();
			result = LightningSelfRegisterControllerNBME.selfRegister('','','',email,'','','',birthdate,'','','','','','','./CheckPasswordResetEmail','','',false);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, Label.Site.lastname_is_required);
	}

	@isTest
	static void selfRegister_EmailNull_EmailRequiredObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();
		String lastname = 'Smith';
		String birthdate = '01-01-1989';

		Test.startTest();
			result = LightningSelfRegisterControllerNBME.selfRegister('','',lastname,'','','','',birthdate,'','','','','','','./CheckPasswordResetEmail','','',false);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, Label.Site.email_is_required);
	}

	@isTest
	static void selfRegister_BirthdateNull_BirthdateRequiredObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();
		String lastname = 'Smith';
		String email = 'jsmith@yahoo.com';

		Test.startTest();
			result = LightningSelfRegisterControllerNBME.selfRegister('','',lastname,email,'','','','','','','','','','','./CheckPasswordResetEmail','','',false);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, 'Birthdate is required.');
	}

	@isTest
	static void selfRegister_BirthdateNot13OrOlder_BirthdateInvalidObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();
		String lastname = 'Smith';
		String email = 'jsmith@yahoo.com';
		String birthdate = '01-01-2010';

		Test.startTest();
			result = LightningSelfRegisterControllerNBME.selfRegister('','',lastname,email,'','','',birthdate,'','','','','','','./CheckPasswordResetEmail','','',false);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, 'Examinee must be at least 13 years of age.');
	}

	@isTest
	static void selfRegister_BirthdateNotValid_BirthdateInvalidObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();
		String lastname = 'Smith';
		String email = 'jsmith@yahoo.com';
		String birthdate = '41-01-2010';

		Test.startTest();
			result = LightningSelfRegisterControllerNBME.selfRegister('','',lastname,email,'','','',birthdate,'','','','','','','./CheckPasswordResetEmail','','',false);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, 'Please enter a valid date for birthdate.');
	}

	@isTest
	static void selfRegister_PersonAccountMatchForEmailAddressMatcherAndUserMatch_HandleMatchObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;

			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			String birthdate = '01-01-1990';

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.selfRegister('','',testPersonAccount.LastName,testPersonAccount.PersonEmail,'','','',birthdate,'','','','','','','./CheckPasswordResetEmail','','',false);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'A matching user has been found. Your password has been reset, please check your e-mail for the link to reset your password.');
		}
	}

	@isTest
	static void selfRegister_PersonAccountMatchForEmailAddressMatcherAndNoUserMatch_HandleNoMatchObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;
		}

		String birthdate = '01-01-1990';

		Test.startTest();
			result = LightningSelfRegisterControllerNBME.selfRegister('','',testPersonAccount.LastName,testPersonAccount.PersonEmail,'','','',birthdate,'','','','','','','./CheckPasswordResetEmail','','',false);
		Test.stopTest();

		System.assertEquals(result.isSuccess, true);
		System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
	}

	@isTest
	static void selfRegister_PersonAccountMatchForLastNameMatcherAndUserMatch_HandleMatchObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;

			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													Phone = '111-222-3333',
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Phone = '111-222-3333',
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			String birthdate = '01-01-1990';

			String maskedEmail;
			String[] splitEmail = testUser.Email.split('@');

			if(splitEmail.size() == 2){
				maskedEmail = splitEmail[0].left(2);
				String asterisk = '*';
				maskedEmail += asterisk.repeat(splitEmail[0].length() - 2);

				String[] splitEmail2 = splitEmail[1].split('\\.', 2);
				maskedEmail += '@' + splitEmail2[0].left(1);
				maskedEmail += asterisk.repeat(splitEmail2[0].length() - 1);

				maskedEmail += '.' + splitEmail2[1];
			}

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.selfRegister('','',testPersonAccount.LastName,'wrongemail@yahoo.com','','','',birthdate,testPersonAccount.Phone,'','','','','','./CheckPasswordResetEmail','','',false);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'The following email address was found for the user: ' + maskedEmail);
		}
	}

	//@isTest
	//static void selfRegister_PersonAccountMatchForLastNameMatcherAndNoUserMatch_HandleNoMatchObjectReturned() {
	//	RegistrationResponse result = new RegistrationResponse();

	//	UserRole r = new UserRole(name = 'Test Role');
	//	insert r;

	//	User userWithRole = new User(LastName = 'Tester',
	//								Email = 'tester123@email.com',
	//								Birthdate__c = Date.newInstance(1989, 1, 1),
	//								Username = 'tester123@email.com',
	//								Alias = 'Tester',
	//								TimeZoneSidKey = 'America/New_York',
	//								LocaleSidKey = 'en_US',
	//								EmailEncodingKey = 'UTF-8',
	//								LanguageLocaleKey = 'en_US',
	//								CommunityNickname = 'Tester',
	//								ProfileId = UserInfo.getProfileId(),
	//								UserRoleId = r.Id);

	//	Account testPersonAccount;
	//	System.runAs(userWithRole) {
	//		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

	//		testPersonAccount = new Account(LastName = 'Testing',
	//												PersonEmail = 'testing@email.com',
	//												PersonBirthdate = Date.newInstance(1990, 1, 1),
	//												Phone = '111-222-3333',
	//												RecordType = personAccountRecordType);

	//		insert testPersonAccount;
	//	}

	//	String birthdate = '01-01-1990';

	//	Test.startTest();
	//		result = LightningSelfRegisterControllerNBME.selfRegister('','',testPersonAccount.LastName,'wrongemail@yahoo.com','','','',birthdate,testPersonAccount.Phone,'','','','','','','./CheckPasswordResetEmail','','',false);
	//	Test.stopTest();

	//	System.assertEquals(result.isSuccess, true);
	//	System.assertEquals(result.message, 'A new user has been created and the email address on your account has been updated.');
	//}

	@isTest
	static void selfRegister_PersonAccountMatchForLastNameMatcherAndNoUserMatch_HandleNoMatchObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			Account_Owner_Table__c testOwner = new Account_Owner_Table__c(Name = 'testOwner',
																			Owner_Id__c = userWithRole.Id);

			insert testOwner;

			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			ccrz__E_AccountGroup__c testAcctGroup = new ccrz__E_AccountGroup__c(Name = 'Examinee Account Group');
			insert testAcctGroup;

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													Phone = '111-222-3333',
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			String birthdate = '01-01-1990';

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.selfRegister('','',testPersonAccount.LastName,'wrongemail@yahoo.com','','','',birthdate,testPersonAccount.Phone,'','','','','','./CheckPasswordResetEmail','','',false);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
		}
	}

	@isTest
	static void selfRegister_PersonAccountMatchForNoEmailOrLastNameMatcherAndUserMatch_HandleMatchObjectReturned() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;
			
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			ccrz__E_AccountGroup__c testAcctGroup = new ccrz__E_AccountGroup__c(Name = 'Examinee Account Group');
			insert testAcctGroup;

			testPersonAccount = new Account(LastName = 'Testing',
													FirstName = 'Tester',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													Phone = '111-222-3333',
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										FirstName = 'Tester',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Phone = '111-222-3333',
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			String birthdate = '01-01-1990';

			String maskedEmail;
			String[] splitEmail = testUser.Email.split('@');

			if(splitEmail.size() == 2){
				maskedEmail = splitEmail[0].left(2);
				String asterisk = '*';
				maskedEmail += asterisk.repeat(splitEmail[0].length() - 2);

				String[] splitEmail2 = splitEmail[1].split('\\.', 2);
				maskedEmail += '@' + splitEmail2[0].left(1);
				maskedEmail += asterisk.repeat(splitEmail2[0].length() - 1);

				maskedEmail += '.' + splitEmail2[1];
			}

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.selfRegister(testPersonAccount.FirstName,'','wronglastname','wrongemail@yahoo.com','','','',birthdate,testPersonAccount.Phone,'','','','','','./CheckPasswordResetEmail','','',false);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'The following email address was found for the user: ' + maskedEmail);
		}
	}

	//@isTest
	//static void selfRegister_PersonAccountMatchForNoEmailOrLastNameMatcherAndNoUserMatch_HandleNoMatchObjectReturned() {
	//	RegistrationResponse result	= new RegistrationResponse();

	//	UserRole r = new UserRole(name = 'Test Role');
	//	insert r;

	//	User userWithRole = new User(LastName = 'Tester',
	//								Email = 'tester123@email.com',
	//								Birthdate__c = Date.newInstance(1989, 1, 1),
	//								Username = 'tester123@email.com',
	//								Alias = 'Tester',
	//								TimeZoneSidKey = 'America/New_York',
	//								LocaleSidKey = 'en_US',
	//								EmailEncodingKey = 'UTF-8',
	//								LanguageLocaleKey = 'en_US',
	//								CommunityNickname = 'Tester',
	//								ProfileId = UserInfo.getProfileId(),
	//								UserRoleId = r.Id);

	//	Account testPersonAccount;
	//	System.runAs(userWithRole) {
	//		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

	//		testPersonAccount = new Account(LastName = 'Testing',
	//												FirstName = 'Tester',
	//												PersonEmail = 'testing@email.com',
	//												PersonBirthdate = Date.newInstance(1990, 1, 1),
	//												Phone = '111-222-3333',
	//												RecordType = personAccountRecordType);

	//		insert testPersonAccount;
	//	}

	//	String birthdate = '01-01-1990';

	//	Test.startTest();
	//		result = LightningSelfRegisterControllerNBME.selfRegister(testPersonAccount.FirstName,'','wronglastname','wrongemail@yahoo.com','','','',birthdate,testPersonAccount.Phone,'','','','','','','./CheckPasswordResetEmail','','',false);
	//	Test.stopTest();

	//	System.assertEquals(result.isSuccess, true);
	//	System.assertEquals(result.message, 'A new user has been created and the email address on your account has been updated.');
	//}

	@isTest
	static void selfRegister_PersonAccountMatchForNoEmailOrLastNameMatcherAndNoUserMatch_HandleNoMatchObjectReturned() {
		RegistrationResponse result	= new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			Account_Owner_Table__c testOwner = new Account_Owner_Table__c(Name = 'testOwner',
																			Owner_Id__c = userWithRole.Id);

			insert testOwner;
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			ccrz__E_AccountGroup__c testAcctGroup = new ccrz__E_AccountGroup__c(Name = 'Examinee Account Group');
			insert testAcctGroup;

			testPersonAccount = new Account(LastName = 'Testing',
													FirstName = 'Tester',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													Phone = '111-222-3333',
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			String birthdate = '01-01-1990';

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.selfRegister(testPersonAccount.FirstName,'','wronglastname','wrongemail@yahoo.com','','','',birthdate,testPersonAccount.Phone,'','','','','','./CheckPasswordResetEmail','','',false);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
		}
	}

	@isTest
	static void selfRegister_NoPersonAccountMatch_CreateNewPersonAccountAndNewUser() {
		RegistrationResponse result = new RegistrationResponse();

		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		insert userWithRole;
		System.runAs(userWithRole) {

			Account_Owner_Table__c testOwner = new Account_Owner_Table__c(Name = 'testOwner',
																			Owner_Id__c = userWithRole.Id);

			insert testOwner;

			Account testPersonAccount;
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			ccrz__E_AccountGroup__c testAcctGroup = new ccrz__E_AccountGroup__c(Name = 'Examinee Account Group');
			insert testAcctGroup;

			testPersonAccount = new Account(LastName = 'Testing',
													FirstName = 'Tester',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													Phone = '111-222-3333',
													OwnerId = testOwner.Owner_Id__c,
													RecordType = personAccountRecordType);

			insert testPersonAccount;
		

			String birthdate = String.valueOf(testPersonAccount.PersonBirthdate);

			Test.startTest();
				result = LightningSelfRegisterControllerNBME.selfRegister('wrongfirstname','','wronglastname','wrongemail@yahoo.com','','','','02-02-1985','333-222-1111','','','','','','./CheckPasswordResetEmail','','',false);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'Your account was created successfully. Please check your e-mail for the link to create your password and log in.');
		}
	}
}