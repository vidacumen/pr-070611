public class MasterBeanCheckboxExt {

	public static Id examineeAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();

	public static void updateIsCustomerPortalCheckbox(List<DSE__DS_Master_Bean__c> triggerNew) {
		Set<Id> accountIds = extractAccountIds(triggerNew);
		Map<Id, Account> accountMap = queryAccounts(accountIds);

		for (DSE__DS_Master_Bean__c mb : triggerNew) {
			if (accountMap.containsKey(mb.DSE__DS_Account__c) && accountMap.get(mb.DSE__DS_Account__c).IsCustomerPortal != null) {
				mb.IsCustomerPortal__c = accountMap.get(mb.DSE__DS_Account__c).IsCustomerPortal;
			}
		}

	}

	public static Map<Id, Account> queryAccounts(Set<Id> accountIds) {
		return new Map<Id, Account>([SELECT Id, IsCustomerPortal FROM Account WHERE Id IN :accountIds AND RecordTypeId = :examineeAcctRecTypeId]);
	}

	public static Set<Id> extractAccountIds(List<DSE__DS_Master_Bean__c> triggerNew) {
		Set<Id> acctIds = new Set<Id>();
		for (DSE__DS_Master_Bean__c mb : triggerNew) {
			acctIds.add(mb.DSE__DS_Account__c);
		}

		return acctIds;
	}

}