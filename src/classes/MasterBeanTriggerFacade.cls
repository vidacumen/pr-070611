public class MasterBeanTriggerFacade {

	public static void onBeforeInsert(List<DSE__DS_Master_Bean__c> triggerNew) {
		MasterBeanCheckboxExt.updateIsCustomerPortalCheckbox(triggerNew);
	}

	public static void onBeforeUpdate(List<DSE__DS_Master_Bean__c> triggerNew) {
		MasterBeanCheckboxExt.updateIsCustomerPortalCheckbox(triggerNew);
	}

}