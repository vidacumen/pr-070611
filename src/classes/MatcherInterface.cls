public interface MatcherInterface {
	Set<Id> findMatches(String firstname,
						String middlename, 
						String lastname, 
						String email, 
						String usmleid, 
						String aamcid,
						String nbmeid,
						Date birthdate,
						String phone,
						String medicalschool,
						String gradyear);

	RegistrationResponse handleMatch(User user,
									Set<Id> accountId,
									String firstname,
									String middlename, 
									String lastname, 
									String email, 
									String usmleid, 
									String aamcid,
									String nbmeid,
									Date birthdate,
									String phone,
									String medicalschool,
									String gradyear);

	RegistrationResponse handleNoMatch(User user,
										Set<Id> accountId,
										String firstname,
										String middlename, 
										String lastname, 
										String email, 
										String usmleid, 
										String aamcid,
										String nbmeid,
										Date birthdate,
										String phone,
										String medicalschool,
										String gradyear);

	void updateEmail(Set<Id> accountId, String email);
}