@isTest
public with sharing class MedSchool1MatcherTest {

	@isTest
	static void findMatches_PassInValuesForSingleSuccessfulMatch_OneMatchIdReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Testing',
												LastName = 'Test',
												USMLE_Id__c = '123456',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Test.startTest();
			MedSchool1Matcher matchTest = new MedSchool1Matcher();
			Set<Id> result = matchTest.findMatches(testPersonAccount.FirstName,null,testPersonAccount.LastName,null,testPersonAccount.USMLE_Id__c,null,null,testPersonAccount.PersonBirthdate,null,null,null);
		Test.stopTest();

		System.assertEquals(result.size(), 1);
	}

	@isTest
	static void findMatches_PassInValuesForMultipleSuccessfulMatches_MultipleMatchIdsReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];
		RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];
		
		Account medSchool = new Account(Name = 'Cleveland Clinic',
										Type = 'Medical School',
										Institution_ID__c = 'NBME~123456',
										RecordType = businessAccountRecordType);
		insert medSchool;

		Account testPersonAccount1 = new Account(FirstName = 'Testing',
												LastName = 'Test',
												USMLE_Id__c = '123456',
												Medical_School__c = medSchool.Id,
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount1;

		Account testPersonAccount2 = new Account(FirstName = 'Testing',
												LastName = 'Tester',
												USMLE_Id__c = '123456',
												Medical_School__c = medSchool.Id,
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount2;

		Test.startTest();
			MedSchool1Matcher matchTest = new MedSchool1Matcher();
			Set<Id> result = matchTest.findMatches(testPersonAccount1.FirstName,null,testPersonAccount2.LastName,null,testPersonAccount1.USMLE_Id__c,null,null,testPersonAccount1.PersonBirthdate,null,medSchool.Name,null);
		Test.stopTest();

		System.assertEquals(result.size() > 1, true);
	}

	@isTest
	static void findMatches_PassInValuesForNoSuccessfulMatches_NoMatchIdsReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Testing',
												LastName = 'Test',
												USMLE_Id__c = '123456',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Test.startTest();
			MedSchool1Matcher matchTest = new MedSchool1Matcher();
			Set<Id> result = matchTest.findMatches('Test',null,null,null,'999999',null,null,Date.newInstance(1989, 1, 1),null,null,null);
		Test.stopTest();

		System.assertEquals(result.size(), 0);
	}

	@isTest
	static void handleMatch_PassInUser_MaskedUserEmailObjectReturned() {
		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;
			
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			String maskedEmail;
			String[] splitEmail = testUser.Email.split('@');

			if(splitEmail.size() == 2){
				maskedEmail = splitEmail[0].left(2);
				String asterisk = '*';
				maskedEmail += asterisk.repeat(splitEmail[0].length() - 2);

				String[] splitEmail2 = splitEmail[1].split('\\.', 2);
				maskedEmail += '@' + splitEmail2[0].left(1);
				maskedEmail += asterisk.repeat(splitEmail2[0].length() - 1);

				maskedEmail += '.' + splitEmail2[1];
			}

			Test.startTest();
				MedSchool1Matcher matchTest = new MedSchool1Matcher();
				RegistrationResponse result = matchTest.handleMatch(testUser,null,null,null,null,null,null,null,null,null,null,null,null);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'The following email address was found for the user: ' + maskedEmail);
		}
	}

	@isTest
	static void handleMatch_NoUserPassedIn_FailedObjectReturned() {
		Test.startTest();
			MedSchool1Matcher matchTest = new MedSchool1Matcher();
			RegistrationResponse result = matchTest.handleMatch(null,null,null,null,null,null,null,null,null,null,null,null,null);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, 'Matching error - No user found.');
	}

	@isTest
	static void handleNoMatch_AccountIdPassedIn_NewPortalUserCreatedForPersonAccountAndPersonAccountEmailUpdated() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Test',
												LastName = 'Testing',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Set<Id> acc = new Map<Id, Account> ([SELECT Id
											FROM Account
											WHERE Id =: testPersonAccount.Id]).keySet();
		
		Test.startTest();
			MedSchool1Matcher matchTest = new MedSchool1Matcher();
			RegistrationResponse result = matchTest.handleNoMatch(null,acc,testPersonAccount.FirstName,null,testPersonAccount.LastName,testPersonAccount.PersonEmail,null,null,null,null,null,null,null);
		Test.stopTest();

		System.assertEquals(result.isSuccess, true);
		System.assertEquals(result.message, 'A new user has been created and the email address on your account has been updated.');
	}
}