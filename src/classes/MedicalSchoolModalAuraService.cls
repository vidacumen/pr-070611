public without sharing class MedicalSchoolModalAuraService {
	@AuraEnabled
	public static List<Account> returnMatchingMedicalSchools(String medSchoolName, String medSchoolCountry, String medSchoolState) {
		String abbrForQuery = '';
		String searchString = 'SELECT Name, BillingState, BillingCountry FROM Account WHERE IsPersonAccount = False AND Examinee_Profile_Visibility_Formula__c = True';
		String completeWhereClause = '';
		String searchByName = (String.isBlank(medSchoolName) ? '' : '%'+String.escapeSingleQuotes(medSchoolName)+'%');

		if (String.isNotBlank(medSchoolName)) {
			completeWhereClause += ' AND Name LIKE :searchByName';
		}
		if (String.isNotBlank(medSchoolCountry)) {
			completeWhereClause += ' AND BillingCountry =: medSchoolCountry';
		}
		if (String.isNotBlank(medSchoolState)) {
			States_and_Territories__mdt stateAbbr = [SELECT Abbreviation__c 
													FROM States_and_Territories__mdt
													WHERE MasterLabel =: medSchoolState];
			abbrForQuery = stateAbbr.Abbreviation__c;

			completeWhereClause += ' AND BillingState =: abbrForQuery';
		}

		searchString += completeWhereClause + ' ORDER BY Name';

		List<Account> medSchools = Database.query(searchString);

		return medSchools;
	}

	@AuraEnabled
	public static List<States_and_Territories__mdt> returnStatePicklistValues(String medSchoolCountry) {
		List<States_and_Territories__mdt> stateValues;

		if (String.isNotBlank(medSchoolCountry)) {
			stateValues = [SELECT MasterLabel
							FROM States_and_Territories__mdt
							WHERE Country__c =: medSchoolCountry
							ORDER BY MasterLabel];
		}

		if(stateValues != null) {
	    	return stateValues;
	    }

    	return null;
	}
}