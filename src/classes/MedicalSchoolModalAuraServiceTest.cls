@isTest
public with sharing class MedicalSchoolModalAuraServiceTest {
	
	@isTest
	static void returnMatchingMedicalSchools_DoNotPassInValues_MedSchoolsReturned() {
		RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];

		Account a = new Account(Name = 'Cleveland Clinic',
								BillingCountry = 'United States',
								BillingState = 'OH',
								Examinee_Profile_Visibility__c = true,
								RecordType = businessAccountRecordType);

		insert a;

		Test.startTest();
			List<Account> result = MedicalSchoolModalAuraService.returnMatchingMedicalSchools('', '', '');
		Test.stopTest();

		System.assertEquals(result.size() > 0, true);
	}

	@isTest
	static void returnMatchingMedicalSchools_PassInSchoolName_MedSchoolsReturned() {
		RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];

		Account a = new Account(Name = 'Cleveland Clinic',
								BillingCountry = 'United States',
								BillingState = 'OH',
								Examinee_Profile_Visibility__c = true,
								RecordType = businessAccountRecordType);

		insert a;

		Test.startTest();
			List<Account> result = MedicalSchoolModalAuraService.returnMatchingMedicalSchools(a.Name, '', '');
		Test.stopTest();

		System.assertEquals(result.size() > 0, true);
	}

	@isTest
	static void returnMatchingMedicalSchools_PassInCountry_MedSchoolsReturned() {
		RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];

		Account a = new Account(Name = 'Cleveland Clinic',
								BillingCountry = 'United States',
								BillingState = 'OH',
								Examinee_Profile_Visibility__c = true,
								RecordType = businessAccountRecordType);

		insert a;

		Test.startTest();
			List<Account> result = MedicalSchoolModalAuraService.returnMatchingMedicalSchools('', a.BillingCountry, '');
		Test.stopTest();

		System.assertEquals(result.size() > 0, true);
	}

	@isTest
	static void returnMatchingMedicalSchools_PassInState_MedSchoolsReturned() {
		RecordType businessAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Business Account'];

		Account a = new Account(Name = 'Cleveland Clinic',
								BillingCountry = 'United States',
								BillingState = 'OH',
								Examinee_Profile_Visibility__c = true,
								RecordType = businessAccountRecordType);

		insert a;

		Test.startTest();
			List<Account> result = MedicalSchoolModalAuraService.returnMatchingMedicalSchools('', '', 'Ohio');
		Test.stopTest();

		System.assertEquals(result.size() > 0, true);
	}

	@isTest
	static void returnStatePicklistValues_DoNotPassInCountry_NullListReturned() {
		Test.startTest();
			List<States_and_Territories__mdt> result = MedicalSchoolModalAuraService.returnStatePicklistValues('');
		Test.stopTest();

		System.assertEquals(result, null);
	}

	@isTest
	static void returnStatePicklistValues_PassInCountryThatDoesNotHaveStates_NullListReturned() {
		Test.startTest();
			List<States_and_Territories__mdt> result = MedicalSchoolModalAuraService.returnStatePicklistValues('');
		Test.stopTest();

		System.assertEquals(result, null);
	}

	@isTest
	static void returnStatePicklistValues_PassInCountryThatHasStates_StateListReturned() {
		Test.startTest();
			List<States_and_Territories__mdt> result = MedicalSchoolModalAuraService.returnStatePicklistValues('United States');
		Test.stopTest();

		System.assertNotEquals(result, null);
	}
}