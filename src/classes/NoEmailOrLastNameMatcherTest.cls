@isTest
public with sharing class NoEmailOrLastNameMatcherTest {

	@isTest
	static void findMatches_PassInValuesForSingleSuccessfulMatch_OneMatchIdReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Testing',
												LastName = 'Test',
												Phone = '111-222-3333',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Test.startTest();
			NoEmailOrLastNameMatcher matchTest = new NoEmailOrLastNameMatcher();
			Set<Id> result = matchTest.findMatches(testPersonAccount.FirstName,null,testPersonAccount.LastName,null,null,null,null,testPersonAccount.PersonBirthdate,testPersonAccount.Phone,null,null);
		Test.stopTest();

		System.assertEquals(result.size(), 1);
	}

	@isTest
	static void findMatches_PassInValuesForMultipleSuccessfulMatches_MultipleMatchIdsReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount1 = new Account(FirstName = 'Testing',
												LastName = 'Test',
												Phone = '111-222-3333',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount1;

		Account testPersonAccount2 = new Account(FirstName = 'Testing',
												LastName = 'Tester',
												Phone = '111-222-3333',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount2;

		Test.startTest();
			NoEmailOrLastNameMatcher matchTest = new NoEmailOrLastNameMatcher();
			Set<Id> result = matchTest.findMatches(testPersonAccount1.FirstName,null,testPersonAccount2.LastName,null,null,null,null,testPersonAccount1.PersonBirthdate,testPersonAccount1.Phone,null,null);
		Test.stopTest();

		System.assertEquals(result.size() > 1, true);
	}

	@isTest
	static void findMatches_PassInValuesForNoSuccessfulMatches_NoMatchIdsReturned() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Testing',
												LastName = 'Test',
												Phone = '111-222-3333',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Test.startTest();
			NoEmailOrLastNameMatcher matchTest = new NoEmailOrLastNameMatcher();
			Set<Id> result = matchTest.findMatches('Test',null,null,null,null,null,null,Date.newInstance(1989, 1, 1),'5559997788',null,null);
		Test.stopTest();

		System.assertEquals(result.size(), 0);
	}

	@isTest
	static void handleMatch_PassInUser_MaskedUserEmailObjectReturned() {
		UserRole r = new UserRole(name = 'Test Role');
		insert r;

		User userWithRole = new User(LastName = 'Tester',
									Email = 'tester123@email.com',
									Birthdate__c = Date.newInstance(1989, 1, 1),
									Username = 'tester123@email.com',
									Alias = 'Tester',
									TimeZoneSidKey = 'America/New_York',
									LocaleSidKey = 'en_US',
									EmailEncodingKey = 'UTF-8',
									LanguageLocaleKey = 'en_US',
									CommunityNickname = 'Tester',
									ProfileId = UserInfo.getProfileId(),
									UserRoleId = r.Id);

		Account testPersonAccount;
		System.runAs(userWithRole) {
			User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
			insert utc;

			Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

			Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
			insert rrts;
			
			RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

			testPersonAccount = new Account(LastName = 'Testing',
													PersonEmail = 'testing@email.com',
													PersonBirthdate = Date.newInstance(1990, 1, 1),
													RecordType = personAccountRecordType);

			insert testPersonAccount;

			Account a = [SELECT PersonContactId FROM Account WHERE Id =: testPersonAccount.Id];
			Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

			User testUser = new User(LastName = 'Testing',
										Email = 'testing@email.com',
										Birthdate__c = Date.newInstance(1990, 1, 1),
										Username = 'testing@email.com',
										Alias = 'TestUser',
										TimeZoneSidKey = 'America/New_York',
										LocaleSidKey = 'en_US',
										EmailEncodingKey = 'UTF-8',
										LanguageLocaleKey = 'en_US',
										CommunityNickname = 'TestUser',
										ProfileId = p.Id,
										ContactId = a.PersonContactId);

			insert testUser;

			String maskedEmail;
			String[] splitEmail = testUser.Email.split('@');

			if(splitEmail.size() == 2){
				maskedEmail = splitEmail[0].left(2);
				String asterisk = '*';
				maskedEmail += asterisk.repeat(splitEmail[0].length() - 2);

				String[] splitEmail2 = splitEmail[1].split('\\.', 2);
				maskedEmail += '@' + splitEmail2[0].left(1);
				maskedEmail += asterisk.repeat(splitEmail2[0].length() - 1);

				maskedEmail += '.' + splitEmail2[1];
			}

			Test.startTest();
				NoEmailOrLastNameMatcher matchTest = new NoEmailOrLastNameMatcher();
				RegistrationResponse result = matchTest.handleMatch(testUser,null,null,null,null,null,null,null,null,null,null,null,null);
			Test.stopTest();

			System.assertEquals(result.isSuccess, true);
			System.assertEquals(result.message, 'The following email address was found for the user: ' + maskedEmail);
		}
	}

	@isTest
	static void handleMatch_NoUserPassedIn_FailedObjectReturned() {
		Test.startTest();
			NoEmailOrLastNameMatcher matchTest = new NoEmailOrLastNameMatcher();
			RegistrationResponse result = matchTest.handleMatch(null,null,null,null,null,null,null,null,null,null,null,null,null);
		Test.stopTest();

		System.assertEquals(result.isSuccess, false);
		System.assertEquals(result.message, 'Matching error - No user found.');
	}

	@isTest
	static void handleNoMatch_AccountIdPassedIn_NewPortalUserCreatedForPersonAccountAndPersonAccountEmailUpdated() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account testPersonAccount = new Account(FirstName = 'Test',
												LastName = 'Testing',
												PersonEmail = 'testing@email.com',
												PersonBirthdate = Date.newInstance(1990, 1, 1),
												RecordType = personAccountRecordType);

		insert testPersonAccount;

		Set<Id> acc = new Map<Id, Account> ([SELECT Id
											FROM Account
											WHERE Id =: testPersonAccount.Id]).keySet();
		
		Test.startTest();
			NoEmailOrLastNameMatcher matchTest = new NoEmailOrLastNameMatcher();
			RegistrationResponse result = matchTest.handleNoMatch(null,acc,testPersonAccount.FirstName,null,testPersonAccount.LastName,testPersonAccount.PersonEmail,null,null,null,null,null,null,null);
		Test.stopTest();

		System.assertEquals(result.isSuccess, true);
		System.assertEquals(result.message, 'A new user has been created and the email address on your account has been updated.');
	}
}