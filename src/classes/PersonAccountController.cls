public with sharing class PersonAccountController {
	
	@AuraEnabled
	public static Account getAccount() {
		String uId = UserInfo.getUserId();
		User u = [SELECT ContactId 
					FROM User 
					WHERE Id =: uId];

		Account personAccountAttr = [SELECT FirstName, 
								MiddleName, 
								LastName, 
								Suffix, 
								BillingStreet, 
								BillingCity, 
								BillingState, 
								BillingPostalCode, 
								BillingCountry, 
								PersonEmail, 
								Phone 
						FROM Account 
						WHERE PersonContactId = :u.ContactId LIMIT 1];
		return personAccountAttr;
	}

	@AuraEnabled
	public static Form_Agreement__mdt findFormByItemIdAttr(String registrationForm) {
		Form_Agreement__mdt formAgreementAttr = [SELECT Label,
								Form__c,
								Fee_Agreement_Text__c,
								Privacy_Agreement_Text__c
						FROM Form_Agreement__mdt
						WHERE Form__c =: registrationForm
						LIMIT 1];

		return formAgreementAttr;
	}

	@AuraEnabled
	public static String updateRegistrationRecord(Id personAcctId,
													String cartId,
													String itemId,
													String firstName,
													String middleName,
													String lastName,
													String suffix,
													String streetAddress,
													String city,
													String state,
													String zipcode,
													String country,
													String email,
													String phone) {
		System.debug('in update reg record');

		String result = '';

		try {
			List<Registration__c> reg = [SELECT First_Name__c,
												Middle_Name__c,
												Last_Name__c,
												Suffix__c,
												Street_Address__c,
												City__c,
												State_Province__c,
												Zip_Postal_Code__c,
												Country2__c,
												Email__c,
												Phone__c,
												Cart__c,
												Cart_Item_ID__c
										FROM Registration__c
										WHERE Cart__c =: cartId
										AND Cart_Item_ID__c =: itemId
										AND Examinee__c =: personAcctId];

			for(Registration__c r : reg) {
				r.First_Name__c = firstName;
				r.Middle_Name__c = middleName;
				r.Last_Name__c = lastName;
				r.Suffix__c = suffix;
				r.Street_Address__c = streetAddress;
				r.City__c = city;
				r.State_Province__c = state;
				r.Zip_Postal_Code__c = zipcode;
				r.Country2__c = country;
				r.Email__c = email;
				r.Phone__c = phone;
			}

			update reg;

			result = 'SUCCESS';
		} catch (Exception ex) {
			result = ex.getMessage();
		}
		System.debug('result: ' + result);

		return result;
	}
}