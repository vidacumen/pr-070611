/**
 * Created by aszampias on 8/17/18.
 */

@isTest
public with sharing class PersonAccountControllerTest {

	@testSetup static void setup(){
		/* Gary Codeee */
		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;
		/* Gary Codeee */

		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
				PersonEmail = 'testing@email.com',
				PersonBirthdate = Date.newInstance(1990, 1, 1),
				RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User u = new User(LastName = 'Tester',
				Email = 'tester123@email.com',
				Birthdate__c = Date.newInstance(1989, 1, 1),
				Username = 'tester123@email.com',
				Alias = 'Tester',
				TimeZoneSidKey = 'America/New_York',
				LocaleSidKey = 'en_US',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				CommunityNickname = 'Tester',
				ProfileId = p.Id,
				ContactId = userAccount.PersonContactId);
		insert u;

	}

	@isTest static void PersonAccountController_GetAccount_ReturnAccount(){
		User u = [SELECT Id FROM User WHERE Alias = 'Tester'];
		Test.startTest();
		System.runas(u) {
		Account a = PersonAccountController.getAccount();
		System.assertNotEquals(a.Id, null);
		}
		Test.stopTest();
	}

	@isTest static void PersonAccountControllter_findFormByItemIdAttr_ReturnFormAgreement(){
		Test.startTest();
		Form_Agreement__mdt mdt = PersonAccountController.findFormByItemIdAttr('NSAS');
		System.assertEquals('NSAS', mdt.Form__c);
		Test.stopTest();
	}

	@isTest static void PersonAccountController_updateRegistrationRecord_ReturnString(){
		User u = [SELECT Id, AccountId FROM User WHERE Alias = 'Tester'];
		Test.startTest();
		ccrz__E_Cart__c BigCart = new ccrz__E_Cart__c(ccrz__Account__c = u.AccountId,
													  ccrz__ActiveCart__c = true,
													  ccrz__EncryptedId__c = 'string');
		insert BigCart;
		ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c(ccrz__Price__c = 15.00,
															   ccrz__Cart__c = BigCart.id);
		insert cartItem;
		Registration__c reg = new Registration__c(Examinee__c=u.AccountId,
												  Cart__c = BigCart.id,
												  Cart_Item_ID__c = cartItem.id);
		insert reg;
		System.debug(reg.Examinee__c + ' : ' + ' : ' + String.valueOf(reg.Cart__c) + ' : ' + String.valueOf(reg.Cart_Item_ID__c));
		String result = PersonAccountController.updateRegistrationRecord(reg.Examinee__c,
														String.valueOf(reg.Cart__c),
														String.valueOf(reg.Cart_Item_ID__c),
														'Amanda',
														'Melissa',
														'Szampias',
														'Miss',
														'5099 Helen Drive',
														'Brunswick',
														'OH',
														'44212',
														'USA',
														'amanda@gmail.com',
														'330-410-3311');
		System.assertEquals('SUCCESS', result);
		Test.stopTest();
	}
}