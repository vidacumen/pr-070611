/**
 * Batch Class
 * Manage merging of person accounts
 * Manage merging of person accounts with priority option
 *
 * @version 1.0
 *
 * @release 
 *
 * @company NBME
 *
 * @reference Acumen Solutions/NBME B2C (MVP) Communities Implementation
 *
 * CHANGE HISTORY
 * @author Gary Alfrey <galfrey@acumensolutions.com> - class created 2018-04-03
 * 
 */
global class PersonAccountMergeBatch implements Database.Batchable<sObject> {

	public String query;
	public Set<Id> recIds;
	public Id jobLogId;

	global PersonAccountMergeBatch(Boolean priorityMerge, Set<Id> ids, Id logId) {
		//Start by building the query needed for the duplicate custom object
		recIds = ids;
		query = PersonAccountMergeHelper.duplicateRecordQueryBuilder(recIds);
		jobLogId = logId;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<DSE__DS_Duplicates__c> scope) {
		System.debug('SCOPE: ' + scope);
		Boolean isSuccess = true;
		for (DSE__DS_Duplicates__c ds : scope) {
			try {
				PersonAccountMergeHelper.startPersonAccountMerge(ds);
			} catch(Exception e) {
				isSuccess = false;
				DSE__DS_Error_Log__c errorLog = new DSE__DS_Error_Log__c();
				errorLog.DSE__DS_Job_Log__c = jobLogId;
				errorLog.DSE__DS_Error_Message__c = e.getMessage() + '\n' + e.getStackTraceString();
				errorLog.DSE__DS_Failed_Record_ID__c = ds.Id;
				insert errorLog;
			}
		}

		DSE__DS_Job_Log__c jobLog = new DSE__DS_Job_Log__c(Id=jobLogId, DSE__DS_End_Time__c=Datetime.now());
		if (isSuccess) {
			jobLog.DSE__DS_Job_Status__c = 'Completed';
		} else {
			jobLog.DSE__DS_Job_Status__c = 'Failed';
		}
		update joblog;
	}

	global void finish(Database.BatchableContext BC) {
		
	}

}