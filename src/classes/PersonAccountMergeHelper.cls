public class PersonAccountMergeHelper {

	/* These standard objects do not have name fields and must be dealt with carefully */
	public static final Map<String, String> IRREGULAR_OBJECTS = new Map<String, String>{
		'Case' => 'AccountId', 
		'ccrz__E_AddressBook__c' => 'ccrz__Account__c', 
		'ccrz__E_AccountAddressBook__c' => 'ccrz__Account__c',
		'ccrz__E_Order__c' => 'ccrz__Account__c',
		'Registration__c' => 'Examinee__c',
		'CustomDocument__c' => 'Examinee__c',
		'Task' => 'WhatId',
		'Event' => 'WhatId',
		'EmailMessage' => 'RelatedToId'
	};

	public static final Set<String> CHILD_OBJECTS_TO_REPARENT = new Set<String> {
		'Case',
		'ccrz__E_AddressBook__c',
		'ccrz__E_AccountAddressBook__c',
		'ccrz__E_Order__c',
		'Registration__c',
		'CustomDocument__c',
		'Task',
		'Event',
		'EmailMessage'
	};

	public static final String TARGET_ACCOUNT = 'Target';
	public static final String SOURCE_ACCOUNT = 'Source';

	public static String duplicateRecordQueryBuilder(Set<Id> recIds) {
		Set<String> dseDupeFieldSet = getFieldSet(SObjectType.DSE__DS_Duplicates__c.FieldSets.Person_Account_Merge.getFields());

		String qStr = 'SELECT OwnerId';
		for (String s : dseDupeFieldSet) {
			qStr += ', ' + s; 
		}
		//qStr += ', DSE__DS_Duplicate__c, DSE__DS_Duplicate__r.DSE__DS_Account__c, DSE__DS_Master__c, DSE__DS_Master__r.DSE__DS_Account__c';
		qStr += ' FROM DSE__DS_Duplicates__c WHERE Id IN :recIds';

		return qStr;
	}

	//public static Account fetchAccount(String recordId, Set<String> fieldSet) {
	//	String queryStr = 'SELECT Id, CreatedDate, IsCustomerPortal, PersonContactId, BillingAddress, ShippingAddress';
	//	for (String s : fieldSet) {
	//		if (s != 'Name') {
	//			queryStr += ', ' + s;
	//		}
	//	}
	//	queryStr += ' FROM Account WHERE Id = :recordId';

	//	return Database.query(queryStr);
	//}

	//public static DSE__DS_Master_Bean__c fetchMasterBean(String recordId) {
	//	return [
	//				SELECT Id, DSE__DS_Account__c, DSE__DS_Account__r.Name, DSE__DS_Parent__c, DSE__DS_Ultimate_Parent__c, 
	//					DSE__DS_Account_Deleted__c, DSE__X_Account_Id__c, DSE__DS_Check_Hierarchy__c, DSE__DS_Billing_City__c, 
	//					DSE__DS_Company__c, DSE__DS_Company_Norm__c, DSE__DS_Convert_to_Account__c, DSE__DS_Convert_to_Lead__c, 
	//					DSE__X_ConvertToRecordTypeId__c, DSE__DS_Custom_Field_17__c, DSE__DS_Custom_Field_18__c, DSE__DS_Custom_Field_19__c, 
	//					DSE__DS_Convert_without_Duplicate_Check__c, DSE__DS_Billing_Country__c, DSE__DS_Country_ISO_Code__c, 
	//					DSE__DS_Custom_Field_1__c, DSE__DS_Custom_Field_10__c, DSE__DS_Custom_Field_2__c, DSE__DS_Custom_Field_20__c, 
	//					DSE__DS_Custom_Field_11__c, DSE__DS_Custom_Field_12__c, DSE__DS_Custom_Field_13__c, DSE__DS_Custom_Field_14__c, 
	//					DSE__DS_Custom_Field_15__c, DSE__DS_Custom_Field_16__c, DSE__DS_Custom_Field_4__c, DSE__DS_Custom_Field_5__c, 
	//					DSE__DS_Custom_Field_7__c, DSE__DS_Custom_Field_8__c, DSE__DS_Custom_Field_9__c, DSE__DS_Custom_Field_3__c, 
	//					DSE__DS_Custom_Field_6__c, DSE__DS_Phone__c, DSE__DS_Billing_Postal_Code__c, DSE__DS_Prefix__c, 
	//					DSE__DS_Segment__c, DSE__DS_Billing_State__c, DSE__DS_Billing_Street__c, DSE__DS_Cleansed__c, 
	//					DSE__DS_Website__c, DSE__DS_Record_Type_Name__c, Owner.Name, Name, DSE__DS_Domain__c 
	//				FROM DSE__DS_Master_Bean__c 
	//				WHERE Id = :recordId
	//			];
	//}

	private static Set<String> getFieldSet(List<Schema.FieldSetMember> fieldSet) {
		Set<String> fields = new Set<String>();

		for (Schema.FieldSetMember field : fieldSet) {
			fields.add(field.getFieldPath());
		}

		return fields;
	}

	public static void startPersonAccountMerge(DSE__DS_Duplicates__c dupeRecord) {
		Set<String> accountFieldSet = getFieldSet(SObjectType.Account.FieldSets.Account_Merge_Fields.getFields());

		Map<String, Account> targetSourceMap = determineMasterAccount(dupeRecord, accountFieldSet);

		targetSourceMap.put(TARGET_ACCOUNT, mergeAccounts(targetSourceMap.get(TARGET_ACCOUNT),targetSourceMap.get(SOURCE_ACCOUNT), accountFieldSet));

		//Update the target
		update targetSourceMap.get(TARGET_ACCOUNT);

		//Reparent Child Records
		System.debug('Source Account Id: ' + targetSourceMap.get(SOURCE_ACCOUNT).Id);
		Map<String, List<SObject>> childMap = queryChildObjects(targetSourceMap.get(SOURCE_ACCOUNT).Id);
		System.debug('CHILD MAP: ' + JSON.serializePretty(childMap));
		reParentChildRecords(targetSourceMap.get(TARGET_ACCOUNT), childMap);
		reShareContentDocuments(targetSourceMap.get(TARGET_ACCOUNT), targetSourceMap.get(SOURCE_ACCOUNT));
		reCreateAccountContactRelations(targetSourceMap.get(TARGET_ACCOUNT), targetSourceMap.get(SOURCE_ACCOUNT));

		//Move Bean??????  Doing this deletes the master bean.
		List<DSE__DS_Bean__c> sourceBeans = processSourceBean(targetSourceMap, dupeRecord);
		

		//VERY LAST STEP: DELETE SOURCE ACCOUNT......
		delete targetSourceMap.get(SOURCE_ACCOUNT);


		insert sourceBeans;
	}

	public static List<DSE__DS_Bean__c> processSourceBean(Map<String, Account> targetSourceMap, DSE__DS_Duplicates__c dupeRecord) {
		List<DSE__DS_Bean__c> sourceBeans = fetchSourceBeanByAccountId(targetSourceMap.get(SOURCE_ACCOUNT).Id);
		List<DSE__DS_Bean__c> sourceBeans2 = new List<DSE__DS_Bean__c>();
		for (DSE__DS_Bean__c bean : sourceBeans) {
			DSE__DS_Bean__c bean2 = bean.clone(false, true, false, false);
			if (targetSourceMap.get(TARGET_ACCOUNT).Id == dupeRecord.DSE__DS_Master__r.DSE__DS_Account__c) {
				bean2.DSE__DS_Master_Bean__c = dupeRecord.DSE__DS_Master__c;
			} else {
				bean2.DSE__DS_Master_Bean__c = dupeRecord.DSE__DS_Duplicate__c;
			}

			bean2.DSE__DS_Account__c = null;
			bean2.DSE__DS_Contact__c = null;
			bean2.Duplicate_Account__c = null;
			bean2.DSE__DS_Lead__c = null;
			bean2.DSE__DS_Parent__c = null;
			bean2.Other_Duplicate__c = null;
			bean2.DSE__DS_Ultimate_Parent__c = null;
			bean2.DSE__DS_Account_Deleted__c = true;

			sourceBeans2.add(bean2);
		}

		System.debug('ORIGINAL SOURCE BEAN RECORD: ' + JSON.serializePretty(sourceBeans));
		System.debug('CLONED SOURCE BEAN RECORD: ' + JSON.serializePretty(sourceBeans2));
		delete sourceBeans;
		return sourceBeans2;
	}

	public static List<DSE__DS_Bean__c> fetchSourceBeanByAccountId(Id accountId) {
		return [
					SELECT Id, DSE__DS_Account__c, DSE__DS_Contact__c, DSE__DS_Master_Bean__c, Duplicate_Account__c, DSE__DS_Lead__c, 
						DSE__DS_Parent__c, Other_Duplicate__c, DSE__DS_Ultimate_Parent__c, DSE__DS_Account_Deleted__c, DSE__DS_Source_No__c, 
						DSE__DS_Birthdate__c, DSE__DS_Company__c, DSE__DS_Company_Norm__c, 
						DSE__DS_Country_ISO_Code__c, DSE__DS_Custom_Field_1__c, DSE__DS_Custom_Field_10__c, DSE__DS_Custom_Field_11__c, 
						DSE__DS_Custom_Field_8__c, DSE__DS_Custom_Field_3__c, DSE__DS_Custom_Field_17__c, DSE__DS_Custom_Field_12__c, 
						DSE__DS_Custom_Field_9__c, DSE__DS_Custom_Field_4__c, DSE__DS_Custom_Field_18__c, DSE__DS_Custom_Field_13__c, 
						DSE__DS_Custom_Field_5__c, DSE__DS_Custom_Field_19__c, DSE__DS_Custom_Field_14__c, DSE__Custom_Index__c, 
						DSE__DS_Custom_Field_6__c, DSE__DS_Custom_Field_20__c, DSE__DS_Custom_Field_15__c, DSE__DS_Description__c, 
						DSE__DS_Custom_Field_7__c, DSE__DS_Custom_Field_2__c, DSE__DS_Custom_Field_16__c, DSE__DS_Email__c, 
						DSE__DS_External_ID__c, DSE__DS_First_Name__c, DSE__DS_Last_Name__c, DSE__DS_Last_Updated__c, 
						DSE__DS_Mobile_Phone__c, DSE__DS_Phone__c, DSE__DS_Score__c, DSE__DS_Source__c, DSE__DS_Billing_State__c, 
						DSE__DS_Billing_Street__c, DSE__DS_Billing_City__c, DSE__DS_Billing_Country__c, DSE__DS_Billing_Postal_Code__c 
					FROM DSE__DS_Bean__c 
					WHERE DSE__DS_Account__c = :accountId
				];
	}

	public static Account mergeAccounts(Account target, Account source, Set<String> fieldSet) {
		for (String s : fieldSet) {
			if (!s.contains('__r') && !s.contains('.') && s != 'Name') {
				if (target.get(s) == null && source.get(s) != null) {
					target.put(s, source.get(s));
				}
			}
		}

		return target;
	}

	public static void reParentChildRecords(Account acct, Map<String, List<SObject>> childMap) {
		for (String s : CHILD_OBJECTS_TO_REPARENT) {
			System.debug('CURRENT OBJECT ' + s);
			if (childMap.containsKey(s)) {
				List<SObject> recordList = childMap.get(s);
				System.debug('CURRENT RECORD LIST: ' + JSON.serializePretty(recordList));
				for (SObject so : recordList) {
					if (s == 'Case') {
						so.put('ContactId', acct.PersonContactId);
					}
					if (s == 'Task' || s == 'Event') {
						so.put('WhoId', acct.PersonContactId);
					}
					so.put(IRREGULAR_OBJECTS.get(s), acct.Id);
				}
				System.debug('ALTERED RECORD LIST: ' + JSON.serializePretty(recordList));
				childMap.put(s, recordList);
				update recordList;
			}
		}

	}

	public static Map<String, List<SObject>> queryChildObjects(Id accountId) {
		System.debug('Source Account Id2: ' + accountId);
		Map<String, List<SObject>> childMap = new Map<String, List<SObject>>();
		for (String s : CHILD_OBJECTS_TO_REPARENT) {
			String query = 'SELECT Id';
			query += ', ' + IRREGULAR_OBJECTS.get(s);
			if (s == 'Case') {
				query += ', ContactId';
			}
			if (s == 'Task' || s == 'Event') {
				query += ', WhoId';
			}
			query += ' FROM ' + s;
			query += ' WHERE ' + IRREGULAR_OBJECTS.get(s) + ' = :accountId';
			System.debug('Query String: ' + query);
			List<SObject> recordList = Database.query(query);
			if (recordList.size() > 0) {
				childMap.put(s, recordList);
			}
		}
		return childMap;
	}

	public static void reShareContentDocuments(Account target, Account source) {
		List<ContentDocumentLink> linkList = [SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId = :source.Id];

		if (linkList != null && linkList.size() > 0) {
			List<ContentDocumentLink> newLinkList = new List<ContentDocumentLink>();
			for (ContentDocumentLink cdl : linkList) {
				ContentDocumentLink newCdl = cdl.Clone(false, true, false, false);
				newCdl.LinkedEntityId = target.Id;
				newLinkList.add(newCdl);
			}

			Database.insert(newLinkList, false);
		}
	}

	public static void reCreateAccountContactRelations(Account target, Account source) {
		List<AccountContactRelation> acrList = [SELECT Id, AccountId, ContactId, IsActive, Roles FROM AccountContactRelation WHERE ContactId = :source.PersonContactId];

		if (acrList != null && acrList.size() > 0) {
			List<AccountContactRelation> newAcrList = new List<AccountContactRelation>();
			for (AccountContactRelation acr : acrList) {
				AccountContactRelation newAcr = acr.Clone(false, true, false, false);
				newAcr.ContactId = target.PersonContactId;
				newAcrList.add(newAcr);
			}

			Database.insert(newAcrList, false);
		}
	}

	public static Map<String, Account> determineMasterAccount(DSE__DS_Duplicates__c dupeRecord, Set<String> accountFieldSet) {
		Set<Id> dupeAcctIds = new Set<Id>{dupeRecord.DSE__DS_Master__r.DSE__DS_Account__c, dupeRecord.DSE__DS_Duplicate__r.DSE__DS_Account__c};
		List<Account> dupeAccounts = queryAccountsById(dupeAcctIds, accountFieldSet);

		Boolean acctOneCommunity = checkAccountForCommunity(dupeAccounts[0]);
		Boolean acctTwoCommunity = checkAccountForCommunity(dupeAccounts[1]);

		Map<String, Account> accountMap = new Map<String, Account>();
		//ONLY TWO ACCOUNTS EVER - Make sure at least one isn't a customer portal
		if (!dupeAccounts[0].IsCustomerPortal || !dupeAccounts[1].IsCustomerPortal) {
			if (dupeAccounts[0].IsCustomerPortal) {
				accountMap.put(TARGET_ACCOUNT, dupeAccounts[0]);
				accountMap.put(SOURCE_ACCOUNT, dupeAccounts[1]);
			} else if (dupeAccounts[1].IsCustomerPortal) {
				accountMap.put(TARGET_ACCOUNT, dupeAccounts[1]);
				accountMap.put(SOURCE_ACCOUNT, dupeAccounts[0]);
			} else if (dupeAccounts[0].CreatedDate < dupeAccounts[1].CreatedDate) {
				accountMap.put(TARGET_ACCOUNT, dupeAccounts[0]);
				accountMap.put(SOURCE_ACCOUNT, dupeAccounts[1]);
			} else {
				accountMap.put(TARGET_ACCOUNT, dupeAccounts[1]);
				accountMap.put(SOURCE_ACCOUNT, dupeAccounts[0]);
			}
		}

		return accountMap;
	}

	public static List<Account> queryAccountsById(Set<Id> recIds, Set<String> accountFieldSet) {
		String queryStr = 'SELECT Id, CreatedDate, IsCustomerPortal, PersonContactId';
		for (String s : accountFieldSet) {
			if (s != 'Name') {
				queryStr += ', ' + s;
			}
		}
		queryStr += ' FROM Account WHERE Id IN :recIds';

		return Database.query(queryStr);
	}

	public static Boolean checkAccountForCommunity(Account a) {
		List<User> u = [SELECT Id, ContactId FROM User WHERE ContactId = :a.PersonContactId];
		if (u != null && u.size() > 0) {
			return true;
		}
		return false;
	}













}