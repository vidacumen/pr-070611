public class PersonAccountMergeLaunchCtrl {

	@AuraEnabled
	public static List<DSE__DS_Duplicates__c> queryDuplicatePersonAccounts() {
		List<DSE__DS_Duplicates__c> dupes = [
					SELECT Id, DSE__DS_Duplicate__c,DSE__DS_Duplicate__r.Id, DSE__DS_Duplicate__r.Name, DSE__DS_Master__c, DSE__DS_Master__r.Name, 
						DSE__DS_Master__r.DSE__DS_Account__r.Name, Duplicate_Is_Customer_Portal__c, Duplicate_Is_Person_Account__c, Priority_Merge__c, 
						Master_Is_Customer_Portal__c, Master_Is_Person_Account__c, DSE__DS_Duplicate__r.DSE__DS_Account__r.Name, DSE__DS_Ignore_Duplicate__c 
					FROM DSE__DS_Duplicates__c 
					WHERE Duplicate_Is_Person_Account__c = true 
					AND Master_Is_Person_Account__c = true 
					AND DSE__DS_Ignore_Duplicate__c = false
				];

		List<DSE__DS_Duplicates__c> displayList = new List<DSE__DS_Duplicates__c>();
		for (DSE__DS_Duplicates__c d : dupes) {
			if (d.Master_Is_Customer_Portal__c != d.Duplicate_Is_Customer_Portal__c || (!d.Master_Is_Customer_Portal__c && !d.Duplicate_Is_Customer_Portal__c)) {
				displayList.add(d);
			}
		}

		return displayList;
	}

	public static Set<Id> getDuplicateRecordIds(List<DSE__DS_Duplicates__c> dupeList) {
		Set<Id> dupeIds = new Set<Id>();
		for (DSE__DS_Duplicates__c dupe : dupeList) {
			dupeIds.add(dupe.Id);
		}

		return dupeIds;
	}

	public static Set<Id> getPriorityDuplicateRecordIds(List<DSE__DS_Duplicates__c> dupeList) {
		Set<Id> dupeIds = new Set<Id>();
		for (DSE__DS_Duplicates__c dupe : dupeList) {
			if (dupe.Priority_Merge__c) {
				dupeIds.add(dupe.Id);
			}
		}

		return dupeIds;
	}

	@AuraEnabled
	public static String launchBatch(List<DSE__DS_Duplicates__c> dupeList) {
		Set<Id> dupeIds = getDuplicateRecordIds(dupeList);
		if (dupeIds.size() > 0) {
			Id jobLogId = createLogJob('Person Account Merge', dupeList.size());
			PersonAccountMergeBatch mergeBatch = new PersonAccountMergeBatch(false, dupeIds, jobLogId);
			Id batchId = Database.executeBatch(mergeBatch, 1);
			return 'SUCCESS';
		} else {
			return 'There are no records to process.';
		}

	}

	@AuraEnabled
	public static String launchPriorityBatch(List<DSE__DS_Duplicates__c> dupeList) {
		Set<Id> dupeIds = getPriorityDuplicateRecordIds(dupeList);
		if (dupeIds.size() > 0) {
			Id jobLogId = createLogJob('Priority Account Merge', dupeList.size());
			PersonAccountMergeBatch mergeBatch = new PersonAccountMergeBatch(false, dupeIds, jobLogId);
			Database.executeBatch(mergeBatch, 1);
			return 'SUCCESS';
		} else {
			return 'There are no priority records to process.';
		}
	}

	public static Id createLogJob(String jobType, Integer numRecords) {
		DSE__DS_Job_Log__c jobLog = new DSE__DS_Job_Log__c();
		jobLog.DSE__DS_Job_Type__c = jobType;
		jobLog.DSE__DS_Start_Time__c = Datetime.now();
		jobLog.DSE__DS_Job_Description__c = jobType;
		jobLog.DSE__DS_Total_Job_Items__c = numRecords;
		jobLog.DSE__DS_Total_Items_Processed__c = numRecords;
		jobLog.DSE__DS_Job_Status__c = 'In Progress';
		insert jobLog;

		return jobLog.Id;
	}
}