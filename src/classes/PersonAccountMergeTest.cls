@isTest
private class PersonAccountMergeTest {

	@testSetup
	static void setup() {
		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Id examineeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
		Account newPersonAcct = new Account(FirstName = 'Test'
												, LastName = 'Testing'
												, PersonEmail = 'test123@noemail.com'
												, BillingCountry = 'United States'
												, BillingState = 'NY'
												, BillingStreet = '101 Main Street'
												, BillingCity = 'New York'
												, BillingPostalCode = '10027'
												, RecordTypeId = examineeRecordTypeId
											);
		Insert newPersonAcct;

		Account newPersonAcct2 = new Account(FirstName = 'Test2'
												, LastName = 'Testing2'
												, PersonEmail = 'test123@noemail2.com'
												, BillingCountry = 'United States'
												, BillingState = 'NY'
												, BillingStreet = '101 Main Street'
												, BillingCity = 'New York'
												, BillingPostalCode = '10027'
												, RecordTypeId = examineeRecordTypeId
											);
		Insert newPersonAcct2;

		Account acct = [SELECT Id, PersonContactId FROM Account WHERE Id = :newPersonAcct.Id];
		Account acct2 = [SELECT Id, PersonContactId FROM Account WHERE Id = :newPersonAcct2.Id];

		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		Account a = [SELECT PersonContactId FROM Account WHERE PersonEmail = 'test123@noemail2.com'];
		Case c = new Case();
		c.Subject = 'test';
		c.Description = 'test';
		c.Delivery_Type__c = 'CBT (Computer Based Testing)';
		c.Test_Delivery_Location__c = 'Not Applicable';
		c.Program__c = 'AAOMS';
		c.Product__c = 'Comprehensive Basic Science';
		c.Reason = 'Ongoing / Repeat Issue';
		c.ContactId = acct2.PersonContactId;
		c.AccountId = acct2.Id;
		c.Grant_Examinee_Access__c = true;

		insert c;

		DSE__DS_Master_Bean__c m1 = new DSE__DS_Master_Bean__c(DSE__DS_Account__c=newPersonAcct.Id);
		insert m1;
		DSE__DS_Master_Bean__c m2 = new DSE__DS_Master_Bean__c(DSE__DS_Account__c=newPersonAcct2.Id);
		insert m2;

		DSE__DS_Bean__c b1 = new DSE__DS_Bean__c(DSE__DS_Account__c=newPersonAcct.Id, DSE__DS_Master_Bean__c=m1.Id, DSE__DS_Source__c='Account');
		insert b1;

		String pairId = newPersonAcct.Id + '#' + newPersonAcct2.Id;
		DSE__DS_Duplicates__c dupe = new DSE__DS_Duplicates__c(
																DSE__DS_Pair_ID__c=pairId, 
																DSE__DS_Duplicate__c=m1.Id, 
																DSE__DS_Master__c=m2.Id, 
																DSE__DS_Score__c=270, 
																WinningAccountID__c=newPersonAcct.Id, 
																LosingAccountID__c=newPersonAcct2.Id
															);

		insert dupe;

		User user = new User(Alias = 'test123', Email='test123@noemail2.com',
				EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
				LocalesIdKey='en_US', ProfileId = p.Id, Country='United States',IsActive =true,
				ContactId = acct2.PersonContactId,
				TimezonesIdKey='America/Los_Angeles', Username='test123@noemail2.com');
		insert user;
	}

	@isTest static void test_method_one() {
		DSE__DS_Job_Log__c jobLog = new DSE__DS_Job_Log__c();
		jobLog.DSE__DS_Job_Type__c = 'Person Account Merge';
		jobLog.DSE__DS_Start_Time__c = Datetime.now();
		jobLog.DSE__DS_Job_Description__c = 'Person Account Merge';
		jobLog.DSE__DS_Total_Job_Items__c = 1;
		jobLog.DSE__DS_Total_Items_Processed__c = 1;
		jobLog.DSE__DS_Job_Status__c = 'In Progress';
		insert jobLog;
		DSE__DS_Duplicates__c dupe = [SELECT Id FROM DSE__DS_Duplicates__c LIMIT 1];
		PersonAccountMergeBatch b = new PersonAccountMergeBatch(false, new Set<Id>{dupe.Id}, jobLog.Id);
		database.executebatch(b);
	}

	@isTest static void test_method_two() {
		List<DSE__DS_Duplicates__c> dupes = PersonAccountMergeLaunchCtrl.queryDuplicatePersonAccounts();
		Set<Id> ids = PersonAccountMergeLaunchCtrl.getDuplicateRecordIds(dupes);
		Set<Id> ids2 = PersonAccountMergeLaunchCtrl.getPriorityDuplicateRecordIds(dupes);
		String x1 = PersonAccountMergeLaunchCtrl.launchBatch(dupes);
	}

}