public class PicklistOptions {
	@AuraEnabled
    public String label;
    @AuraEnabled
    public String value;
}