public with sharing class ProductSearchAuraService {
	@AuraEnabled
	public static String getCloudCrazeProductSearchURL(String inputValue) {
		CloudCrazeSettings ccSettings = new CloudCrazeSettings();
		String SFInstance = System.URL.getSalesforceBaseURL().toExternalForm();
		String communityURLPathName = ccSettings.getCommunityURLPathName();
		String cloudCrazeURL = SFInstance+'/'+communityURLPathName+'/ccrz__Products?operation=quickSearch&searchText='+inputValue;

		return cloudCrazeURL;
	}
}