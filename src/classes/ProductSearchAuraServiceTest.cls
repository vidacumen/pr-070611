@isTest
public with sharing class ProductSearchAuraServiceTest {
	
	@isTest
	static void getCloudCrazeProductSearchURL_PassInSearchText_CCSearchForProductsPageURLReturned() {
		String searchText = 'Test';
		String cloudCrazeURL = '/ccrz__Products?operation=quickSearch&searchText='+searchText;

		Test.startTest();
		String result = ProductSearchAuraService.getCloudCrazeProductSearchURL(searchText);
		Test.stopTest();

		System.assertEquals(result.contains(cloudCrazeURL), true);
	}
}