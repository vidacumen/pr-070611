public with sharing class ProductSearchByCategoryAuraService {
	private static CloudCrazeDAO CloudCrazeDA = new CloudCrazeDataAccessor();
	public static CloudCrazeDAO ccda {get; set {
		CloudCrazeDA = value;
	}}

	@AuraEnabled
	public static List<CCResponse> returnCCProductCategoriesForStorefront() {
		CloudCrazeSettings ccSettings = new CloudCrazeSettings();
		String communityStorefront = ccSettings.getCommunityStorefront();
		List<CCResponse> categories = CloudCrazeDA.getCCCategories(communityStorefront);

		return categories;
	}

	//@AuraEnabled
	//public static void returnCCProductSubCategoriesForStorefront() {

	//}

	//@AuraEnabled
	//public static void getCCSubCategories(String rootCategoryID) {

	//}

	@AuraEnabled
	public static String getCloudCrazeProductSearchURL(String categoryId) {
		CloudCrazeSettings ccSettings = new CloudCrazeSettings();
		String SFInstance = System.URL.getSalesforceBaseURL().toExternalForm();
		String communityURLPathName = ccSettings.getCommunityURLPathName();
		String cloudCrazeURL = SFInstance+'/'+communityURLPathName+'/ccrz__Products?categoryId='+categoryId;

		return cloudCrazeURL;
	}
}