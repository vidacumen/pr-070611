@isTest
public with sharing class ProductSearchByCategoryAuraServiceTest {
	private static final String CATEGORY_JSON = '{"name:test","parentCategory:test","sfid:test","categoryID:test","ownerId:test"}';
	
	@isTest
	static void getCloudCrazeProductSearchURL_PassInCategoryId_CCSearchForProductCategoryPageURLReturned() {
		String categoryId = '1234567';
		String cloudCrazeURL = '/ccrz__Products?categoryId='+categoryId;

		Test.startTest();
		String result = ProductSearchByCategoryAuraService.getCloudCrazeProductSearchURL(categoryId);
		Test.stopTest();

		System.assertEquals(result.contains(cloudCrazeURL), true);
	}

	@isTest
	static void getCCCategories_CallMethod_CCProductCategoriesReturned() {
		ProductSearchByCategoryAuraService.ccda = new CloudCrazeDataAccessorMock();

		Test.startTest();
			List<CCResponse> result = ProductSearchByCategoryAuraService.returnCCProductCategoriesForStorefront();
		Test.stopTest();

		System.assertNotEquals(null, result);
	}
}