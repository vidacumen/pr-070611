/**
 * Created by btenis on 1/31/2018
 */

public with sharing class RegistrationAuraService {
	@AuraEnabled
	public static List<Registration_Stage_Map__mdt> queryForRegistrationStagesByRegistrationId(Id registrationId) {
		Registration__c r = [SELECT RecordType.Name
							FROM Registration__c
							WHERE Id =: registrationId
		];

		return [SELECT Id,
					Order__c,
					Registration_Record_Type__c,
					External_Registration_Status_Name__c
				FROM Registration_Stage_Map__mdt
				WHERE Registration_Record_Type__c =: r.RecordType.Name
				ORDER BY Order__c
		];
	}

	@AuraEnabled
	public static String returnRegistrationViewerContentForTheRegistrationRecord(Id registrationId) {
		Registration__c r = [SELECT Registration_Stage_Viewer_Content__c
				FROM Registration__c
				WHERE Id =: registrationId
		];

		return r.Registration_Stage_Viewer_Content__c;
	}

	@AuraEnabled
	public static Decimal returnCurrentStageNum(Id registrationId) {
		Registration__c r = [SELECT RecordType.Name,
									External_Registration_Status__c
							FROM Registration__c
							WHERE Id =: registrationId
		];

		Registration_Stage_Map__mdt regMap = [SELECT Order__c
				FROM Registration_Stage_Map__mdt
				WHERE Registration_Record_Type__c =: r.RecordType.Name AND
				External_Registration_Status_Name__c =: r.External_Registration_Status__c
		];

		return regMap.Order__c;
	}

	@AuraEnabled
	public static String queryRegistrationModalityType(Id registrationId) {
		Registration__c r = [SELECT Delivery_Modality_Type__c
				FROM Registration__c
				WHERE Id =: registrationId
		];

		return r.Delivery_Modality_Type__c;
	}

	@AuraEnabled
	public static String queryDownloadLink(Id registrationId) {
		Registration__c r = [SELECT View_Score_Report__c
				FROM Registration__c
				WHERE Id =: registrationId
		];

		return r.View_Score_Report__c;
	}

	@AuraEnabled
	public static String setButtonLabelByRegistrationStatus(Id registrationId) {
		String buttonLabel = 'No Button';

		Registration__c r = [SELECT Registration_Status__c,
									Score_Report_Expiry_Date__c
							FROM Registration__c
							WHERE Id =: registrationId
		];

		if (r.Registration_Status__c == 'Assessment Available') {
			buttonLabel = 'Start Your Assessment';
		} else if (r.Registration_Status__c == 'Assessment In Progress') {
			buttonLabel = 'Return To Your Assessment';
		} else if (r.Registration_Status__c == 'Expired after Launch' || r.Registration_Status__c == 'Assessment Complete') {
			buttonLabel = 'View Your Performance Profile';
		} else if (r.Registration_Status__c == 'Score Report Available' && r.Score_Report_Expiry_Date__c > Date.today()) {
			buttonLabel = 'View Your Results Interactively';
		}

		return buttonLabel;
	}

	@AuraEnabled
	public static String setButtonDownloadLabelByRegistrationStatus(Id registrationId) {
		String buttonLabel = 'No Button';

		Registration__c r = [SELECT Registration_Status__c
							FROM Registration__c
							WHERE Id =: registrationId
		];

		if (r.Registration_Status__c == 'Score Report Available') {
			buttonLabel = 'Download Your Score Report';
		}

		return buttonLabel;
	}
}