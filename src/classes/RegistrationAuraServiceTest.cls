/**
 * Created by btenis on 1/31/2018
 */

@isTest
public with sharing class RegistrationAuraServiceTest {
	@testSetup
	static void setup() {
		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
								PersonEmail = 'testing@email.com',
								PersonBirthdate = Date.newInstance(1990, 1, 1),
								RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

		Lightning_Community_Setup__c lcs = new Lightning_Community_Setup__c(Name = 'Community Name', Value__c = '');
		insert lcs;

		User u = new User(LastName = 'Tester',
						Email = 'tester123@email.com',
						Birthdate__c = Date.newInstance(1989, 1, 1),
						Username = 'tester123@email.com',
						Alias = 'Tester',
						TimeZoneSidKey = 'America/New_York',
						LocaleSidKey = 'en_US',
						EmailEncodingKey = 'UTF-8',
						LanguageLocaleKey = 'en_US',
						CommunityNickname = 'Tester',
						ProfileId = p.Id,
						ContactId = userAccount.PersonContactId);

		insert u;
	}
	
	@isTest
	static void queryForRegistrationStagesByRegistrationId_PassInRegistrationId_ReturnRegistrationStages() {
		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		Registration__c reg = new Registration__c(RecordTypeId = testRecordType.Id);
		insert reg;

		Test.startTest();
			List<Registration_Stage_Map__mdt> result = RegistrationAuraService.queryForRegistrationStagesByRegistrationId(reg.Id);
		Test.stopTest();

		List<Registration_Stage_Map__mdt> regMap = [SELECT Id,
															Order__c,
															Registration_Record_Type__c,
															External_Registration_Status_Name__c
													FROM Registration_Stage_Map__mdt
													WHERE Registration_Record_Type__c = 'NSAS Registration'
													ORDER BY Order__c];

		System.assertEquals(result, regMap);
	}

	@isTest
	static void returnRegistrationViewerContentForTheRegistrationRecord_PassInRegistrationIdForRecordWithViewerContentPopulated_ReturnViewerContent() {
		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		Registration__c reg = new Registration__c(RecordTypeId = testRecordType.Id,
													Registration_Stage_Viewer_Content__c = 'This is text');
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.returnRegistrationViewerContentForTheRegistrationRecord(reg.Id);
		Test.stopTest();

		System.assertEquals(result, reg.Registration_Stage_Viewer_Content__c);
	}

	@isTest
	static void returnRegistrationViewerContentForTheRegistrationRecord_PassInRegistrationIdForRecordWithoutViewerContentPopulated_ReturnNull() {
		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		Registration__c reg = new Registration__c(RecordTypeId = testRecordType.Id);
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.returnRegistrationViewerContentForTheRegistrationRecord(reg.Id);
		Test.stopTest();

		System.assertEquals(result, null);
	}

	@isTest
	static void returnCurrentStageNum_PassInRegistrationIdForRecordPopulatedStageName_CurrentMappingStageNumberReturned() {
		RecordType testRecordType = [SELECT Id FROM RecordType WHERE Name = 'NSAS Registration'];

		Registration__c reg = new Registration__c(RecordTypeId = testRecordType.Id,
													Registration_Status__c = 'Assessment Available');
		insert reg;

		Test.startTest();
			Decimal result = RegistrationAuraService.returnCurrentStageNum(reg.Id);
		Test.stopTest();

		Registration__c reg2 = [SELECT External_Registration_Status__c
								FROM Registration__c
								WHERE Id =: reg.Id];

		Registration_Stage_Map__mdt regMap = [SELECT Order__c
				FROM Registration_Stage_Map__mdt
				WHERE Registration_Record_Type__c = 'NSAS Registration' AND
				External_Registration_Status_Name__c =: reg2.External_Registration_Status__c
		];

		System.assertEquals(result, regMap.Order__c);
	}

	@isTest
	static void queryRegistrationModalityType_PassInRegistration_returnDeliveryModalityType(){
		ccrz__E_Product__c p = new ccrz__E_Product__c(Name = 'Test Product',
														ccrz__SKU__c = '1234',
														Delivery_Modality_Type__c = 'WED');
		insert p;

		Registration__c reg = new Registration__c(Registration_Status__c = 'Assessment Available',
												  Assessment_Product__c = p.id);
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.queryRegistrationModalityType(reg.Id);
		Test.stopTest();

		System.assertEquals('WED', result);
	}

	@isTest
	static void queryDownloadLink_PassInRegistration_returnDownloadLinkString(){
		Registration__c reg = new Registration__c(Registration_Status__c = 'Assessment Available',
													Score_Report_File_ID__c = '1234567890');
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.queryDownloadLink(reg.Id);
		Test.stopTest();

		Registration__c reg2 = [SELECT View_Score_Report__c
								FROM Registration__c
								WHERE Id =: reg.Id];

		System.assertEquals(reg2.View_Score_Report__c, result);
	}

	@isTest
	static void setButtonLabelByRegistrationStatus_PassInRegistrationAssessmentAvailable_returnAssessmentAvailableButtonLabelString(){
		Registration__c reg = new Registration__c(Registration_Status__c = 'Assessment Available');
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.setButtonLabelByRegistrationStatus(reg.Id);
		Test.stopTest();

		System.assertEquals('Start Your Assessment', result);
	}

	@isTest
	static void setButtonLabelByRegistrationStatus_PassInRegistrationAssessmentInProgress_returnAssessmentInProgressButtonLabelString(){
		ccrz__E_Product__c p = new ccrz__E_Product__c(Name = 'Test Product',
														ccrz__SKU__c = '12345',
														Exam_Completion_Limit__c = 2);

		insert p;

		Registration__c reg = new Registration__c(Registration_Status__c = 'Assessment In Progress',
													Assessment_Product__c = p.Id,
													Exam_Start_Date__c = Date.today());
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.setButtonLabelByRegistrationStatus(reg.Id);
		Test.stopTest();

		System.assertEquals('Return To Your Assessment', result);
	}

	@isTest
	static void setButtonLabelByRegistrationStatus_PassInRegistrationAssessmentComplete_returnAssessmentCompleteButtonLabelString(){
		ccrz__E_Product__c p = new ccrz__E_Product__c(Name = 'Test Product',
														ccrz__SKU__c = '12345',
														Interactive_Score_Report_Expiry_Days__c = 2);

		insert p;

		Registration__c reg = new Registration__c(Registration_Status__c = 'Assessment Complete',
													Assessment_Product__c = p.Id,
													Exam_Completion_Date__c = Date.today());
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.setButtonLabelByRegistrationStatus(reg.Id);
		Test.stopTest();

		System.assertEquals('View Your Performance Profile', result);
	}

	@isTest
	static void setButtonLabelByRegistrationStatus_PassInRegistrationExpiredAfterLaunch_returnExpiredAfterLaunchButtonLabelString(){
		Registration__c reg = new Registration__c(Registration_Status__c = 'Expired after Launch');
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.setButtonLabelByRegistrationStatus(reg.Id);
		Test.stopTest();

		System.assertEquals('View Your Performance Profile', result);
	}

	@isTest
	static void setButtonDownloadLabelByRegistrationStatus_PassInRegistrationExpiredAfterLaunch_returnNoButtonLabelString(){
		Registration__c reg = new Registration__c(Registration_Status__c = 'Expired after Launch');
		insert reg;

		Test.startTest();
			String result = RegistrationAuraService.setButtonDownloadLabelByRegistrationStatus(reg.Id);
		Test.stopTest();

		System.assertEquals('No Button', result);
	}
}