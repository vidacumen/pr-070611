public class RegistrationResponse {
	@AuraEnabled
	public boolean isSuccess;
	@AuraEnabled
	public String message;
}