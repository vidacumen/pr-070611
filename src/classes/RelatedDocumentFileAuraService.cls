public with sharing class RelatedDocumentFileAuraService {
	@AuraEnabled
	public static String downloadFile(Id fileId) {
		ContentVersion f = [SELECT Id 
								FROM ContentVersion 
								WHERE ContentDocumentId =: fileId
								AND IsLatest = true];

	    String sfInstance = URL.getSalesforceBaseUrl().toExternalForm();
	    String communityName = Lightning_Community_Setup__c.getValues('Community Name').Value__c;
	    String urlForDownload = sfInstance + '/' + communityName + '/sfc/servlet.shepherd/version/download/' + f.Id;

	    return urlForDownload;
	}
}