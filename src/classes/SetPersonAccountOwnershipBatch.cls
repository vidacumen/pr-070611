global class SetPersonAccountOwnershipBatch implements Database.Batchable<sObject> {

	public static Id examineeAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
	public Set<String> uNames;

	global SetPersonAccountOwnershipBatch() {
		List<User_Trigger_Controller__c> controls = User_Trigger_Controller__c.getAll().values();
		uNames = new Set<String>();
		for (User_Trigger_Controller__c utc : controls) {
			uNames.add(utc.Name);
		}
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([
											SELECT Id, OwnerId, IsPersonAccount 
											FROM Account 
											WHERE IsPersonAccount = true 
											AND RecordTypeId = :examineeAcctRecTypeId 
											AND Owner.UserName IN :uNames
										]);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
	}

	global void finish(Database.BatchableContext BC) {
		
	}

}