public with sharing class ShoppingCartController {

	@AuraEnabled
	public static List<ccrz__E_Cart__c> getCart() {
		User personAccountUser = [SELECT AccountId
								FROM User
								WHERE Id =: UserInfo.getUserId()];

		List<ccrz__E_Cart__c> cart = new List<ccrz__E_Cart__c>();

		cart = [SELECT Total_Quantity_Minor_Items__c, ccrz__TotalAmount__c, ccrz__EncryptedId__c
				FROM ccrz__E_Cart__c
				WHERE ccrz__Account__c =: personAccountUser.AccountId
				AND ccrz__ActiveCart__c =: true
				LIMIT 1];
				
		System.debug('cart' + cart.size());
		return cart;
	}
	
	//Registration records have fields called Cart__c (this holds the Salesforce Id for the cart) and Cart_Item_ID__c 
	//(this holds the Salesforce Id for the cart item)
	@AuraEnabled
	public static List<ccrz__E_CartItem__c> getCartItems() {
		User personAccountUser = [SELECT AccountId
								FROM User
								WHERE Id =: UserInfo.getUserId()];

		List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c>();

		cartItems = [SELECT Id, ccrz__Cart__r.ccrz__CartId__c, ccrz__Cart__r.ccrz__TotalQuantity__c, 
				ccrz__Cart__r.ccrz__TotalAmount__c, ccrz__Quantity__c, ccrz__Product__r.Name, 
				ccrz__Product__r.Registration_Form__c, ccrz__Cart__r.ccrz__EncryptedId__c, 
				ccrz__Cart__c, ccrz__Product__r.ccrz__SKU__c, ccrz__PrimaryAttr__r.ccrz__DisplayName__c, 
				ccrz__SecondaryAttr__r.ccrz__DisplayName__c, ccrz__Price__c, ccrz__ItemTotal__c,
				ccrz__Product__r.Require_EULA_Agreement__c, ccrz__Product__r.Require_Fee_Agreement__c,
				ccrz__Product__r.Require_Privacy_Agreement__c, ccrz__Product__r.Product_Terms__c, ccrz__Coupon__c
				FROM ccrz__E_CartItem__c
				WHERE ccrz__Cart__r.ccrz__Account__c =: personAccountUser.AccountId
				AND ccrz__Cart__r.ccrz__ActiveCart__c =: true 
				AND ccrz__cartItemType__c != 'Major'];

		system.debug('cartitem: ' + cartItems.size());
		return cartItems;
	}


	//When deleting cart items, you will need to query for the related Registration record and also 
	//delete the Registration record for that cart item
	//Get Cart ID and Cart Item ID from CloudCraze API callout and filter Registration query by both of those ID's
	//There will always be one Registration record per cart item
	@AuraEnabled
	public static void deleteCartItemAndRegistration(Id cartItemId, Id cartId, String encryptedId) {
		List<ccrz.ccApiCart.LineData> lineItems = new List<ccrz.ccApiCart.LineData>();
		ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();
		lineItem.sfid = cartItemId;
		lineItems.add(lineItem);

		//remove Registration record for the cart item first
		List<Registration__c> registration = [SELECT Id
										FROM Registration__c
										WHERE Cart__c =: cartId AND Cart_Item_ID__c =: cartItemId];
		
		Savepoint sp = Database.setSavepoint();
		Boolean wasSuccessful = false;

		try{

			if (registration.size() > 0) {
				System.debug('registration: ' + registration);
				delete registration;
			}
			// remove cart item from the cart
			Map<String, Object> cartRemoveItem = new Map<String, Object>{
				ccrz.ccAPICart.CART_ENCID => encryptedId, // this variable needs to be the encrypted ID of the cart
				ccrz.ccAPI.API_VERSION => 1,
				ccrz.ccApiCart.LINE_DATA => lineItems // this variable needs to be the Salesforce ID for the cart item being removed, it will be List<ccrz.ccApiCart.LineData>
			};

			
			Map<String,Object> cartResult = ccrz.ccApiCart.removeFrom(cartRemoveItem); // call the removeFrom method to remove item from the cart
			System.debug('cartresult ' + cartResult);
			wasSuccessful = (Boolean) cartResult.get(ccrz.ccAPI.SUCCESS); // boolean for if item was successfully removed, rollback transaction if false
			System.debug('wassuccessful: ' + wasSuccessful);
			List<ccrz.cc_bean_Message> outputMessages = (List<ccrz.cc_bean_Message>) cartResult.get(ccrz.ccAPI.MESSAGES); // list of messages to describe the results of the operation
		
		} catch(Exception e){
			System.debug('exception: ' + e);
		}

		if(!wasSuccessful){
			Database.rollback(sp);
		}
	}

}