public with sharing class UnauthenticatedUserErrorController { 
    
        public PageReference redirectToPage(){
        
        PageReference newPage = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/s/loginerror');
 
        return newPage;
    }
    
}