/**
 * Created by aszampias on 8/20/18.
 */
@isTest
public with sharing class UpdatePersonAccountsTest {
	@isTest  static void UpdateUserMap_Success(){
		Account acct1 = new Account(LastName='Szampias');
		Account acct2 = new Account(LastName='Smith');
		List<Account> lstAccounts = new List<Account>();
		lstAccounts.add(acct1);
		lstAccounts.add(acct2);
		insert lstAccounts;
		Test.startTest();
			System.enqueueJob(new UpdatePersonAccounts(lstAccounts));
			Account acctOne = [SELECT Id FROM Account LIMIT 1];
			System.assertNotEquals(null, acctOne);
		Test.stopTest();
	}

}