public class UpdateUserMap implements Queueable {
	private Map<Id, User> uMap;

	public UpdateUserMap(Map<Id, User> recordsMap) {
		this.uMap = recordsMap;
	}

	public void execute(QueueableContext context) {
		update uMap.values();
	}
}