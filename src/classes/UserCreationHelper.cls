public class UserCreationHelper {

	public static void beforeUserCreationSetUserOwner(List<User> triggerNew) {
		//System.debug('-------------------------------------------------------UserTrigger Start process');
		String uName = UserInfo.getUserName();
		List<User_Trigger_Controller__c> controls = User_Trigger_Controller__c.getAll().values();
		Set<String> uNames = new Set<String>();
		for (User_Trigger_Controller__c utc : controls) {
			uNames.add(utc.Name);
		}

		if (!uNames.contains(uName)) {
			List<User> filteredUsers = filterUsersInsert(triggerNew);

			if (filteredUsers.size() > 0) {
				setUserOwner(filteredUsers);
			}
		}
	}

	public static List<User> filterUsersInsert(List<User> triggerNew) {
		List<User> filteredUserList = new List<User>();
		for (User u : triggerNew) {
			if (u.ContactId != null && u.Profile_Name__c == 'B2C Customer Login') {
				filteredUserList.add(u);
			}
		}

		return filteredUserList;
	}

	public static void setUserOwner(List<User> filteredUsers) {
		Set<Id> recordsFromTrigger = new Set<Id>();

		for (User u : filteredUsers) {
			recordsFromTrigger.add(u.Account_ID_Text__c);
		}

		List<Account> userPersonAccounts = queryPersonAccounts(recordsFromTrigger);
		Round_Robin_Trigger_Settings__c rrts = [SELECT Public_Group_Id__c, User_Id__c, Limit__c, Alert_Email__c, Alert_Email_Template__c FROM Round_Robin_Trigger_Settings__c LIMIT 1];

		List<User_Portal_Ownership_Tacker__c> userList = User_Portal_Ownership_Tacker__c.getall().values();

		Integer x = 0;

		for (User_Portal_Ownership_Tacker__c u : userList) {
			if (u.Records_Owned__c < rrts.Limit__c) {
				while (u.Records_Owned__c < rrts.Limit__c && x < userPersonAccounts.size()) {
					userPersonAccounts[x].OwnerId = u.User_Id__c;
					u.Records_Owned__c++;
					x++;
				}
			}

			if (x >= userPersonAccounts.size()) {
				break;
			}
		}

		update userList;

		update userPersonAccounts;

	}

	public static List<Account> queryPersonAccounts(Set<Id> aIds) {
		if (Test.isRunningTest()) {
			return null; //UserTriggerFacadeTest.mockedAccountList;
		} else {
			return [SELECT Id, OwnerId, IsCustomerPortal, PersonContactId FROM Account WHERE Id IN: aIds];
		}
	}

	public static void updateCustomSetting(List<User> accountOwnerList, User userToUse, Round_Robin_Trigger_Settings__c rrts) {
		Integer userIndex = accountOwnerList.indexOf(userToUse);

		if (userIndex < accountOwnerList.size() - 1) {
			rrts.User_Id__c = accountOwnerList.get(userIndex + 1).Id;
		} else {
			rrts.User_Id__c = accountOwnerList.get(0).Id;
		}

		update rrts;
	}
}