public without sharing class UserManagement {
	public Boolean isSuccess;

	public Boolean updateUser(User user) {
		try {
			user.isactive = true;
			update user;
			isSuccess = true;
		} catch (Exception ex) {
			isSuccess = false;
		}

		return isSuccess;
	}

	public Boolean createUser(User u, Account a) {
		try {
			Id userId = Site.createExternalUser(u, a.Id, null, true);
			isSuccess = true;
		} catch (Exception ex) {
			isSuccess = false;
		}

		return isSuccess;
	}

	public Boolean resetUserPassword(User user) {
		try {
			System.resetPassword(user.Id,true);
			isSuccess = true;
		} catch (Exception ex) {
			isSuccess = false;
		}

		return isSuccess;
	}
}