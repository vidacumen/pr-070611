/**
 * Created by aszampias on 8/17/18.
 */

@isTest
public with sharing class UserManagementTest {
	@testSetup static void setup(){
		UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;

		User u = new User(
				ProfileId = '00e460000018ygEAAQ',
				LastName = 'last',
				Email = 'puser000@amamama.com',
				Username = 'puser000@amamama.com' + System.currentTimeMillis(),
				CompanyName = 'TEST',
				Title = 'title',
				Alias = 'alias',
				TimeZoneSidKey = 'America/Los_Angeles',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				UserRoleId = r.Id
		);

		insert u;

	}

	@isTest static void UserManagement_updateUser_True(){
		UserManagement uManagement = new UserManagement();
		User idUser = [SELECT Id FROM User WHERE LastName = 'last' LIMIT 1];
		boolean bSuccess = uManagement.updateUser(idUser);
		Test.startTest();
			System.assertEquals(true, bSuccess);
		Test.stopTest();
	}

	@isTest static void UserManagement_updateUser_False(){
		UserManagement uManagement = new UserManagement();
		UserRole r = new UserRole(DeveloperName = 'MyCustomRole1', Name = 'My Role1');
		insert r;

		User ud = new User(
				ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
				LastName = 'lastt',
				Email = 'puserr000@amamama.com',
				Username = 'puser0003@amamama.com' + System.currentTimeMillis(),
				CompanyName = 'TESTT',
				Title = 'title1',
				Alias = 'alias1',
				TimeZoneSidKey = 'America/Los_Angeles',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				UserRoleId = r.Id
		);

		boolean bSuccess = uManagement.updateUser(ud);
		Test.startTest();
			System.assertEquals(false, bSuccess);
		Test.stopTest();
	}

	@isTest static void UserManagement_createUser_True(){
		UserManagement uManagement = new UserManagement();
		User uUser = [SELECT Id FROM User LIMIT 1];
		Account a = new Account(LastName='Szampias');
		insert a;
		Boolean bSuccess = uManagement.createUser(uUser, a);
		Test.startTest();
			System.assertEquals(true, bSuccess);
		Test.stopTest();
	}

	@isTest static void UserManagement_password_False(){
		UserManagement uA = new UserManagement();
		User u = [SELECT Id FROM User LIMIT 1];
		Boolean bSuccess = uA.resetUserPassword(u);
		Test.startTest();
			System.assertEquals(false, bSuccess);
		Test.stopTest();
	}
}