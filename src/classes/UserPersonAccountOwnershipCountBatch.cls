global class UserPersonAccountOwnershipCountBatch implements Database.Batchable<sObject>, Schedulable {

	public static Id examineeAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();

	global UserPersonAccountOwnershipCountBatch() {}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, Person_Accounts_Owned__c FROM User]);
	}

	global void execute(Database.BatchableContext BC, List<User> scope) {
		try {
			System.debug('scope: ' + scope);
			processUserRollup(scope);
			System.debug('done with method');
		} catch (Exception e) {
			System.debug(e.getMessage() + ' ' + e.getLineNumber());
		}
	}

	global void finish(Database.BatchableContext BC) {}

	public static void processUserRollup(List<User> scope) {
		Set<Id> uIds = getUserIds(scope);
		List<Account> pAccounts = queryPersonAccounts(uIds);
		Map<Id, Integer> userToIntMap = mapUserToNumberOfAccountsOwned(pAccounts);

		for (User u : scope) {
			u.Person_Accounts_Owned__c = 0;

			if (userToIntMap.containsKey(u.Id)) {
				u.Person_Accounts_Owned__c = userToIntMap.get(u.Id);
			}
		}

		Database.update(scope, false);
	}

	public static Map<Id, Integer> mapUserToNumberOfAccountsOwned(List<Account> pAccounts) {
		Map<Id, Integer> userToInt = new Map<Id, Integer>();
		for (Account a : pAccounts) {
			if (userToInt.containsKey(a.OwnerId)) {
				Integer x = userToInt.get(a.OwnerId) + 1;
				userToInt.put(a.OwnerId, x);
			} else {
				userToInt.put(a.OwnerId, 1);
			}
		}

		return userToInt;
	}

	public static Set<Id> getUserIds(List<User> scope) {
		Set<Id> uIds = new Set<Id>();
		for (User u : scope) {
			uIds.add(u.Id);
		}

		return uIds;
	}

	public static List<Account> queryPersonAccounts(Set<Id> uIds) {
		return [SELECT Id, OwnerId, RecordTypeId 
				FROM Account 
				WHERE OwnerId IN :uIds 
				AND RecordTypeId = :examineeAcctRecTypeId 
				AND IsPersonAccount = true];
	}

	global void execute(SchedulableContext sc) {
		UserPersonAccountOwnershipCountBatch b = new UserPersonAccountOwnershipCountBatch();
		database.executebatch(b);
	}

}