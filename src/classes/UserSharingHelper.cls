public class UserSharingHelper {

	public static void afterUserActivationUpdateExamineeSharing(List<User> triggerNew, Map<Id, User> triggerOldMap) {
		//Step 1: filter users
		List<User> filteredUsers = filterUsers(triggerNew, triggerOldMap);

		UserSharingHelper.shareAccountWithCommunityUser(filteredUsers);
		UserSharingHelper.updateExamineeSharing(filteredUsers);
	}

	public static void afterUserCreationUpdateExamineeSharing(List<User> triggerNew) {
		//Step 1: filter users
		List<User> filteredUsers = filterUsersInsert(triggerNew);
		UserSharingHelper.shareAccountWithCommunityUser(filteredUsers);
		UserSharingHelper.updateExamineeSharing(filteredUsers);
	}

	public static void updateExamineeSharing(List<User> filteredUsers) {
		//Step 2: Extract Contact Ids
		Set<Id> contactIds = extractContactIds(filteredUsers);

		//Step 3: Query Person Accounts
		List<Account> personAccounts = queryPersonAccounts(contactIds);

		//Step 4: Extract Person Account Ids
		Set<Id> accountIds = extractAccountIds(personAccounts);

		//Step 5: Query Cases
		List<Case> casestoShare = queryCasesByAccountId(accountIds);

		//Step 6: Query Documents
		List<CustomDocument__c> documentsToShare = queryDocumentsByAccountId(accountIds);

		//Step 7: Send Cases to CaseSharingHelper
		CaseSharingHelper.afterInsertUpdateExamineeSharing(casestoShare);

		//Step 8: Send Documents to CustomDocumentSharingHelper
		CustomDocumentSharingHelper.afterInsertUpdateExamineeSharing(documentsToShare);
	}

	public static void shareAccountWithCommunityUser(List<User> filteredUsers) {
		List<AccountShare> asList = new List<AccountShare>();
		for (User u : filteredUsers) {
			if (u.Account_ID_Text__c != null) {
				AccountShare aShare = calculateAccountSharing(u.Account_ID_Text__c, u.Id);
				asList.add(aShare);
			}
		}

		System.debug('---------------------------Account Shares to Insert: ' + JSON.serializePretty(asList));
		List<Database.SaveResult> srList = Database.Insert(asList, false);
		System.debug(JSON.serializePretty(srList));
	}

	public static AccountShare calculateAccountSharing(Id acctId, Id userId) {
		AccountShare share = new AccountShare();
		share.AccountId = acctId;
		share.UserOrGroupId = userId;
		share.AccountAccessLevel = 'edit';
		share.OpportunityAccessLevel = 'read';
		share.CaseAccessLevel = 'read';
		share.RowCause = 'manual';

		System.debug('NEW SHARE REORD: ' + share);
		return share;
	}

	public static List<User> filterUsers(List<User> triggerNew, Map<Id, User> triggerOldMap) {
		List<User> filteredUserList = new List<User>();
		for (User u : triggerNew) {
			User oldUser = triggerOldMap.get(u.Id);
			if (u.IsActive && !oldUser.IsActive && u.ContactId != null) {
				filteredUserList.add(u);
			}
		}

		return filteredUserList;
	}

	public static List<User> filterUsersInsert(List<User> triggerNew) {
		List<User> filteredUserList = new List<User>();
		for (User u : triggerNew) {
			if (u.ContactId != null) {
				filteredUserList.add(u);
			}
		}

		return filteredUserList;
	}

	public static Set<Id> extractContactIds(List<User> userList) {
		Set<Id> contactIds = new Set<Id>();
		for (User u : userList) {
			contactIds.add(u.ContactId);
		}

		return contactIds;
	}

	public static Set<Id> extractAccountIds(List<Account> personAccounts) {
		Set<Id> accountIds = new Set<Id>();
		for (Account a : personAccounts) {
			accountIds.add(a.Id);
		}

		return accountIds;
	}

	public static List<Account> queryPersonAccounts(Set<Id> contactIds) {
		return [
					SELECT Id, PersonContactId 
					FROM Account 
					WHERE PersonContactId IN :contactIds
				];
	}

	public static List<Case> queryCasesByAccountId(Set<Id> accountIds) {
		return [
					SELECT Id, CaseNumber, AccountId, Account.PersonContactId, Account.RecordTypeId, Grant_Examinee_Access__c, Examinee_Access_Status__c 
					FROM Case 
					WHERE AccountId IN :accountIds 
					AND Grant_Examinee_Access__c = true
				];
	}

	public static List<CustomDocument__c> queryDocumentsByAccountId(Set<Id> accountIds) {
		return [
					SELECT Id, Examinee__c, Examinee__r.PersonContactId, Examinee__r.RecordTypeId, Grant_Examinee_Access__c, Examinee_Access_Status__c 
					FROM CustomDocument__c 
					WHERE Examinee__c IN :accountIds 
					AND Grant_Examinee_Access__c = true
				];
	}






}