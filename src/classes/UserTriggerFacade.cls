public class UserTriggerFacade {

	public static Boolean skipTrigger = false;

	public static void onAfterUpdate(List<User> triggerNew, Map<Id, User> triggerOldMap) {
		UserSharingHelper.afterUserActivationUpdateExamineeSharing(triggerNew, triggerOldMap);
	}

	public static void onAfterInsert(List<User> triggerNew) {
		System.debug('TRIGGER NEW AFTER INSERT: ' + JSON.serializePretty(triggerNew));
		UserSharingHelper.afterUserCreationUpdateExamineeSharing(triggerNew);
	}

	public static void onBeforeInsert(List<User> triggerNew) {
		System.debug('TRIGGER NEW BEFORE INSERT: ' + JSON.serializePretty(triggerNew));
		UserCreationHelper.beforeUserCreationSetUserOwner(triggerNew);
	}

}