public class WebFredApiHelper {

	public static HttpResponse sendRequest(HttpRequest request) {
		Http http = new Http();
		return http.send(request);
	}

	public static String getEndpoint(String value) {
		return Web_Service_Setup__c.getValues(value).Value__c;
	}

	public static Registration__c queryRegistration(Id registrationId) {
		if (Test.isRunningTest()) {
			return WebFredApiTest.buildMockRegistration();
		}
		return [
					SELECT Id, Assessment_Product__c, Examinee__c, Examinee__r.Person_ID__c, Examinee__r.PersonEmail, Email__c, First_Name__c, Last_Name__c, Form__c, 
						Timing_Factor__c, Form_Name__c, Exam__c, Name, Examinee__r.Id, VIP_Indicator__c, Driver_Assessment_Id__c, 
						Form__r.Driver_Form_ID__c, Pacing__r.Timing_Factor__c, Registration_Status__c 
					FROM Registration__c 
					WHERE Id = :registrationId
				];
	}

	public static Map<String, String> buildExamRequestHeader(String accessToken) {
		Map<String, String> headerMap = new Map<String, String>();

		headerMap.put('Authorization', 'Bearer ' + accessToken);
		headerMap.put('Content-Type', 'application/json;charset=UTF-8');

		return headerMap;
	}

}