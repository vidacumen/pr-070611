@isTest
public class WebFredApiResponseMock implements HttpCalloutMock {

	public HTTPResponse respond(HTTPRequest req) {

		HttpResponse res = new HttpResponse();
		res.setBody(buildResponseBody());
		res.setStatusCode(200);
		res.setStatus('Success');
		return res;
	}

	public static String buildResponseBody() {
		String body = '{'+
							'\"access_token\":\"01234567890\",'+
							'\"url\":\"www.testingSalesforce.com\"'+
						'}';

		return body;
	}

}