@isTest
public class WebFredApiTest {

	@testSetup
	static void setup() {
		Id examineeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Examinee').getRecordTypeId();
		Account newPersonAcct = new Account(FirstName = 'Test'
												, LastName = 'Testing'
												, PersonEmail = 'test123@noemail.com'
												, BillingCountry = 'United States'
												, BillingState = 'NY'
												, BillingStreet = '101 Main Street'
												, BillingCity = 'New York'
												, BillingPostalCode = '10027'
												, RecordTypeId = examineeRecordTypeId
											);
		Insert newPersonAcct;

		Account acct = [SELECT Id, PersonContactId FROM Account WHERE Id = :newPersonAcct.Id];

		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		User user = new User(Alias = 'test123', Email='test123@noemail.com',
				EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
				LocalesIdKey='en_US', ProfileId = p.Id, Country='United States',IsActive =true,
				ContactId = acct.PersonContactId,
				TimezonesIdKey='America/Los_Angeles', Username='tester@noemail.com');
		insert user;

		Web_Service_Setup__c wsSetup1 = new Web_Service_Setup__c(Name = 'LaunchTokenClientId', Value__c = 'client_id=83bdf9ad-da33-471b-9072-519966909658');
		Web_Service_Setup__c wsSetup2 = new Web_Service_Setup__c(Name = 'LaunchTokenClientSecret', Value__c = '	S6ukcYbsmM985zmR2msOvv1+3490P6/JxQiLL2JMMuU=');
		Web_Service_Setup__c wsSetup3 = new Web_Service_Setup__c(Name = 'LaunchTokenEndpoint', Value__c = 'https://login.microsoftonline.com/azurenbme.onmicrosoft.com/oauth2/token');
		Web_Service_Setup__c wsSetup4 = new Web_Service_Setup__c(Name = 'LaunchTokenGrantType', Value__c = 'grant_type=client_credentials');
		Web_Service_Setup__c wsSetup5 = new Web_Service_Setup__c(Name = 'LaunchTokenNamespace', Value__c = 'DEV');
		Web_Service_Setup__c wsSetup6 = new Web_Service_Setup__c(Name = 'LaunchTokenResource', Value__c = 'resource=https://azurenbme.onmicrosoft.com/b445409d-5e5b-442a-9739-4a4278a5c09c');
		Web_Service_Setup__c wsSetup7 = new Web_Service_Setup__c(Name = 'SupportTokenClientId', Value__c = 'client_id=a7ae1f9b-14bb-428d-aeea-94ca04abfb44');
		Web_Service_Setup__c wsSetup8 = new Web_Service_Setup__c(Name = 'SupportTokenClientSecret', Value__c = 'Dz9FbM3i/qUgf4+pvtYR8WSEuLev9TOnaRTXwyWBTmc=');
		Web_Service_Setup__c wsSetup9 = new Web_Service_Setup__c(Name = 'SupportTokenEndpoint', Value__c = 'https://login.microsoftonline.com/azurenbme.onmicrosoft.com/oauth2/token');
		Web_Service_Setup__c wsSetup10 = new Web_Service_Setup__c(Name = 'SupportTokenGrantType', Value__c = 'grant_type=client_credentials');
		Web_Service_Setup__c wsSetup11 = new Web_Service_Setup__c(Name = 'SupportTokenResource', Value__c = 'resource=a7ae1f9b-14bb-428d-aeea-94ca04abfb44');
		Web_Service_Setup__c wsSetup12 = new Web_Service_Setup__c(Name = 'SupportTokenScope', Value__c = 'scope=read write');
		Web_Service_Setup__c wsSetup13 = new Web_Service_Setup__c(Name = 'WebFredExamEndpoint', Value__c = 'https://examdriver-internal-dev.azurewebsites.net/launch/api/launch');
		Web_Service_Setup__c wsSetup14 = new Web_Service_Setup__c(Name = 'WebFredSupportToolEndpoint', Value__c = 'https://wedmin-dvlp.azurewebsites.net/wedminService/login/sf');
		Web_Service_Setup__c wsSetup15 = new Web_Service_Setup__c(Name = 'WebFredTerminateEndpoint', Value__c = 'https://examdriver-internal-dev.azurewebsites.net/launch/api/terminate');

		List<Web_Service_Setup__c> wsSetupList = new List<Web_Service_Setup__c>{wsSetup1,
																				wsSetup2,
																				wsSetup3,
																				wsSetup4,
																				wsSetup5,
																				wsSetup6,
																				wsSetup7,
																				wsSetup8,
																				wsSetup9,
																				wsSetup10,
																				wsSetup11,
																				wsSetup12,
																				wsSetup13,
																				wsSetup14,
																				wsSetup15};

		insert wsSetupList;
	}

	//@isTest static void test_method_one() {
	//	test.startTest();
	//	Test.setMock(HttpCalloutMock.class, new WebFredApiResponseMock());
	//	Account acct = [SELECT Id, PersonContactId, Person_ID__c FROM Account WHERE PersonEmail = 'test123@noemail.com' LIMIT 1];
	//	WebFredExamLauncherExt.processWebFredExamRequest('a0hK000000Bvcsg');
	//	test.stopTest();
	//}

	//@isTest static void test_method_two() {
	//	test.startTest();
	//	Test.setMock(HttpCalloutMock.class, new WebFredApiResponseMock());
	//	Account acct = [SELECT Id, PersonContactId, Person_ID__c FROM Account WHERE PersonEmail = 'test123@noemail.com' LIMIT 1];
	//	WebFredSupportToolExt.processWebFredSupportToolRequest('a0hK000000Bvcsg');
	//	test.stopTest();
	//}

	public static Registration__c buildMockRegistration() {
		Account acct = [SELECT Id, PersonContactId, Person_ID__c FROM Account WHERE PersonEmail = 'test123@noemail.com' LIMIT 1];
		Registration__c mockRegistration = new Registration__c(First_Name__c = 'test'
																	, Last_Name__c = 'testing'
																	, Examinee__c = acct.Id
																	//, Examinee__r.Person_ID__c = acct.Person_ID__c
																	, Email__c = 'test123@noemail.com'
																	//, Driver_Assessment_Id__c = 'Test'
																	, Form__c = 'a0sK000000FaIYO'
																	//, Form__r.Driver_Form_ID__c = 'test'
																	//, Name = 'test'
																	//, Form__r.Timing_Factor__c = 1
																);

		return mockRegistration;
	}

}