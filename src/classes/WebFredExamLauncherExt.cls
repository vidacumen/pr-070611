public class WebFredExamLauncherExt {

	//private static String nameSpace = 'namespace:';

	public static String processWebFredExamRequest(Id registrationId) {
		//This will return the redirect Url
		String redirectUrl = '';

		//Step 1: Get Token
		String jsonResponse = getToken();
		System.debug('TOKEN RESPONSE' + jsonResponse);

		Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonResponse);

		String accessToken = (String)responseMap.get('access_token');

		Registration__c reg = WebFredApiHelper.queryRegistration(registrationId);

		String endPoint = '';
		String requestBody = '';

		if (reg.Registration_Status__c == 'Expired After Launch') {
			endPoint = WebFredApiHelper.getEndpoint('WebFredTerminateEndpoint');
			requestBody = buildExamTerminateBody(reg);
		} else {
			endPoint = WebFredApiHelper.getEndpoint('WebFredExamEndpoint');
			requestBody = buildExamRequestBody(reg);
		}

		Map<String, String> headerMap = WebFredApiHelper.buildExamRequestHeader(accessToken);
		HttpRequest request = HttpCalloutHelper.buildHttpRequestWithHeader(requestBody, headerMap, endPoint, 'POST');
		HttpResponse response = WebFredApiHelper.sendRequest(request);
		System.debug('URL RESPONSE' + response);
		String errorCode;
		String errorMessage;
		String jsonInput;

		// ** mcm - there needs to be a check for the http status... if a 500 error occurs and html is returned, the response cannot be parsed for json and an exception occurs (Class.System.JSON.deserializeUntyped)
		if (response.getStatusCode() == 200) {
			jsonInput = response.getBody();
			System.debug('URL RESPONSE BODY' + jsonInput);
			Map<String, Object> UrlResponseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonInput);
			redirectUrl = (String)UrlResponseMap.get('url');
			
			//Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endPoint, 
			//	Integration_Type__c = 'WebFredExamLauncherExt', 
			//	Integration_Process__c = 'processWebFredExamRequest',
			//	Message_Details__c = '(' + ((reg.Registration_Status__c == 'Expired After Launch') ? 'Terminate' : 'Launch') + ') registrationId=' + registrationId, 
			//	Message_Status__c = 'Success',
			//	Message_Request_Body__c = requestBody,
			//	Message_Response_Body__c = jsonInput,
			//	Message_HTTP_Status_Code__c = String.valueOf(response.getStatusCode())
			//);

			//IntegrationLogHelper.insertLog(log);
		} else {
			errorCode = String.valueOf(response.getStatusCode());
			errorMessage = response.getStatus();
			redirectUrl = 'ERROR';

			//Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endPoint, 
			//	Integration_Type__c = 'WebFredExamLauncherExt', 
			//	Integration_Process__c = 'processWebFredExamRequest',
			//	Message_Details__c = '(' + ((reg.Registration_Status__c == 'Expired After Launch') ? 'Terminate' : 'Launch') + ') registrationId=' + registrationId, 
			//	Message_Status__c = 'Error',
			//	Message_Request_Body__c = requestBody,
			//	Message_Response_Error_Code__c = errorCode,
			//	Message_Response_Error_Description__c = errorMessage
			//);

			//IntegrationLogHelper.insertLog(log);
		}

		return redirectUrl;
	}

	public static String buildExamTerminateBody(Registration__c reg) {
		String nameSpace = WebFredApiHelper.getEndpoint('LaunchTokenNamespace');
		String requestBody = '{"registrationId":"' + nameSpace + ':' + reg.Name + '"}';
		System.debug('URL BODY: ' + requestBody);
		return requestBody;
	}

	public static String buildExamRequestBody(Registration__c reg) {
		String nameSpace = WebFredApiHelper.getEndpoint('LaunchTokenNamespace');
		String requestBody = '{"firstName":"' + reg.First_Name__c + '", "lastName":"' + reg.Last_Name__c + '", "personId":"' 
							+ reg.Examinee__r.Person_ID__c + '", "email":"' + reg.Email__c + '", "examName":"' 
							+ reg.Driver_Assessment_Id__c + '", "formName":"' 
							+ reg.Form__r.Driver_Form_ID__c + '", "registrationId":"' + nameSpace + ':' + reg.Name + '", "timingFactor":' + reg.Pacing__r.Timing_Factor__c + '}';
		System.debug('URL BODY: ' + requestBody);
		return requestBody;
	}

	public static String getToken() {
		String token = '';

		String endPoint = WebFredApiHelper.getEndpoint('LaunchTokenEndpoint');
		String requestBody = buildTokenRequestBody();

		HttpRequest request = HttpCalloutHelper.buildHttpRequestWithoutHeader(requestBody, endPoint, 'POST');
		HttpResponse response = WebFredApiHelper.sendRequest(request);

		System.debug(response.getBody());
		String jsonInput = response.getBody();

		return jsonInput;
	}

	public static String buildTokenRequestBody() {
		String body = '';

		String grantType = Web_Service_Setup__c.getValues('LaunchTokenGrantType').Value__c;
		String clientId = Web_Service_Setup__c.getValues('LaunchTokenClientId').Value__c;
		String clientSecret = 'client_secret=' + EncodingUtil.urlEncode(Web_Service_Setup__c.getValues('LaunchTokenClientSecret').Value__c,'UTF-8');
		String resource = Web_Service_Setup__c.getValues('LaunchTokenResource').Value__c;

		body = grantType + '&' + clientId + '&' + clientSecret + '&' + resource;

		System.debug('REQUEST BODY: ' + body);

		return body;
	}

}