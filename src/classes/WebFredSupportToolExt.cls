public class WebFredSupportToolExt {

	private static String nameSpace = WebFredApiHelper.getEndpoint('LaunchTokenNamespace');
	//private static String nameSpace = 'namespace:';

	public static String processWebFredSupportToolRequest(Id registrationId) {
		Registration__c reg = WebFredApiHelper.queryRegistration(registrationId);
		//This will return the redirect Url
		String redirectUrl = '';

		//Step 1: Get Token
		String jsonResponse = getToken();

		Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonResponse);
		System.debug(responseMap);

		String accessToken = (String)responseMap.get('access_token');
		System.debug('--------------------- accessToken: ' + accessToken);

		String endPoint = WebFredApiHelper.getEndpoint('WebFredSupportToolEndpoint');
		String requestBody = buildSupportToolRequestBody(reg);

		Map<String, String> headerMap = WebFredApiHelper.buildExamRequestHeader(accessToken);
		HttpRequest request = HttpCalloutHelper.buildHttpRequestWithHeader(requestBody, headerMap, endPoint, 'POST');
		HttpResponse response = WebFredApiHelper.sendRequest(request);
		System.debug('URL RESPONSE' + response);

		// ** mcm - there needs to be a check for the http status... if a 500 error occurs and html is returned, the response cannot be parsed for json and an exception occurs (Class.System.JSON.deserializeUntyped)
		if (response.getStatusCode() == 200) {
			String jsonInput = response.getBody();
			System.debug('URL RESPONSE BODY' + jsonInput);
			Map<String, Object> UrlResponseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonInput);
			redirectUrl = (String)UrlResponseMap.get('url');

			//Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endPoint, 
			//	Integration_Type__c = 'WebFredSupportToolExt',
			//	Integration_Process__c = 'processWebFredSupportToolRequest', 
			//	Message_Details__c = 'registration: ' + registrationId,
			//	Message_Status__c = 'Success',
			//	Message_Request_Body__c = requestBody,
			//	Message_Response_Body__c = jsonInput,
			//	Message_HTTP_Status_Code__c = String.valueOf(response.getStatusCode())
			//);

			//IntegrationLogHelper.insertLog(log);
		} else {
			redirectUrl = 'ERROR';

			//Integration_Log__c log = new Integration_Log__c(
			//	Integration_Target__c = endPoint, 
			//	Integration_Type__c = 'WebFredSupportToolExt', 
			//	Integration_Process__c = 'processWebFredSupportToolRequest',
			//	Message_Details__c = 'registration: ' + registrationId,
			//	Message_Status__c = 'Error',
			//	Message_Request_Body__c = requestBody,
			//	Message_Response_Error_Code__c = String.valueOf(response.getStatusCode()),
			//	Message_Response_Error_Description__c = response.getStatus()
			//);

			//IntegrationLogHelper.insertLog(log);
		}

		return redirectUrl;
	}

	public static String getToken() {
		String token = '';

		String endPoint = WebFredApiHelper.getEndpoint('SupportTokenEndpoint');
		String requestBody = buildTokenRequestBody();

		HttpRequest request = HttpCalloutHelper.buildHttpRequestWithoutHeader(requestBody, endPoint, 'POST');
		HttpResponse response = WebFredApiHelper.sendRequest(request);

		System.debug(response.getBody());
		String jsonInput = response.getBody();

		return jsonInput;
	}

	public static String buildTokenRequestBody() {
		String body = '';

		String grantType = Web_Service_Setup__c.getValues('SupportTokenGrantType').Value__c;
		String scope = Web_Service_Setup__c.getValues('SupportTokenScope').Value__c;
		String clientId = Web_Service_Setup__c.getValues('SupportTokenClientId').Value__c;
		String clientSecret = 'client_secret=' + EncodingUtil.urlEncode(Web_Service_Setup__c.getValues('SupportTokenClientSecret').Value__c,'UTF-8');
		String resource = Web_Service_Setup__c.getValues('SupportTokenResource').Value__c;

		body = grantType+'&'+scope+'&'+clientId+'&'+ clientSecret+'&'+resource;

		System.debug('REQUEST BODY: ' + body);

		return body;
	}

	public static String buildSupportToolRequestBody(Registration__c reg) {
		//String requestBody = '{"examRegisterId":"zhe1", "lastName": "McGehee","firstName": "Mike","email":"comuser@sf.org"}'; 
		String requestBody = '{"examRegisterId":"' + namespace + ':' + reg.Name + '", "lastName":"' + UserInfo.getLastName() + '", "firstName":"' 
							+ UserInfo.getFirstName() + '", "email":"' + UserInfo.getUserEmail() + '"' + '}';
		System.debug('URL BODY: ' + requestBody);
		return requestBody;
	}

}