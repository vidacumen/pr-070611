@isTest
private class ccCustomLogicCartTest {

	@testSetup
	static void setup() {
		User_Trigger_Controller__c utc = new User_Trigger_Controller__c(Name=UserInfo.getUserName(), UserId__c=UserInfo.getUserId());
		insert utc;

		Group publicGroup = [SELECT Id FROM Group WHERE Name = 'Person Account Owner Round Robin' LIMIT 1];

		Round_Robin_Trigger_Settings__c rrts = new Round_Robin_Trigger_Settings__c(Name = 'Value', Public_Group_Id__c = publicGroup.Id, Limit__c = 9000);
		insert rrts;

	}

	@isTest static void test_method_one() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci2 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='External', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId2
														);
		ccrz__E_CartItem__c ci3 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId3
														);
		ccrz__E_CartItem__c ci4 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId2, 
															Id=ciId4
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId2, 
															Id=ciId5
														);

		ccCustomLogicCartPrice.groupCartItemList(new List<ccrz__E_CartItem__c>{ci1,ci2,ci3,ci4,ci5});
	}

	@isTest static void test_method_two() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId1, 
															Id=ciId5
														);

		String ABSOLUTE_DISCOUNTTYPE = 'Absolute';
		String PERCENTAGE_DISCOUNTTYPE = 'Percentage';
		String PRICE_DISCOUNTTYPE = 'Price';

		Map<String,Object> couponMap = new Map<String, Object>();
		couponMap.put('discountType', PERCENTAGE_DISCOUNTTYPE);
		couponMap.put('discountAmount', 5);
		ccCustomLogicCartPrice.evaluateModifiedPriceAndSubtotal(ci5,couponMap);
	}

	@isTest static void test_method_3() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId1, 
															Id=ciId5
														);

		String ABSOLUTE_DISCOUNTTYPE = 'Absolute';
		String PERCENTAGE_DISCOUNTTYPE = 'Percentage';
		String PRICE_DISCOUNTTYPE = 'Price';

		Map<String,Object> couponMap = new Map<String, Object>();
		couponMap.put('discountType', ABSOLUTE_DISCOUNTTYPE);
		couponMap.put('discountAmount', 5);
		ccCustomLogicCartPrice.evaluateModifiedPriceAndSubtotal(ci5,couponMap);
	}

	@isTest static void test_method_4() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId1, 
															Id=ciId5
														);

		String ABSOLUTE_DISCOUNTTYPE = 'Absolute';
		String PERCENTAGE_DISCOUNTTYPE = 'Percentage';
		String PRICE_DISCOUNTTYPE = 'Price';

		Map<String,Object> couponMap = new Map<String, Object>();
		couponMap.put('discountType', PRICE_DISCOUNTTYPE);
		couponMap.put('discountAmount', 100);
		ccCustomLogicCartPrice.evaluateModifiedPriceAndSubtotal(ci5,couponMap);
	}

	@isTest static void test_method_5() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId1, 
															Id=ciId5
														);

		String ABSOLUTE_DISCOUNTTYPE = 'Absolute';
		String PERCENTAGE_DISCOUNTTYPE = 'Percentage';
		String PRICE_DISCOUNTTYPE = 'Price';

		Map<String,Object> couponMap = new Map<String, Object>();
		couponMap.put('discountType', PRICE_DISCOUNTTYPE);
		couponMap.put('discountAmount', 100);
		try {
			ccCustomLogicCartPrice.updateCartItemWithCouponDetails(ci1,ci5,couponMap);
		} catch (Exception e) {
			//nothing
		}

	}

	@isTest static void test_method_6() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId1, 
															Id=ciId5
														);

		String ABSOLUTE_DISCOUNTTYPE = 'Absolute';
		String PERCENTAGE_DISCOUNTTYPE = 'Percentage';
		String PRICE_DISCOUNTTYPE = 'Price';

		Map<String,Object> couponMap = new Map<String, Object>();
		couponMap.put('discountType', PRICE_DISCOUNTTYPE);
		couponMap.put('discountAmount', 100);
		try {
			ccCustomLogicCartPrice.isCouponOnAttributeDrivenCartIem(new List<ccrz__E_CartItem__c>{ci1});
		} catch (Exception e) {
			//nothing
		}

	}

	@isTest static void test_method_7() {
		Id ciId1 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 1);
		Id ciId2 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 2);
		Id ciId3 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 3);
		Id ciId4 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 4);
		Id ciId5 = CreateTestData.createFakeId('ccrz__E_CartItem__c', 5);

		ccrz__E_CartItem__c ci1 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Major', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															Id=ciId1
														);
		ccrz__E_CartItem__c ci5 = new ccrz__E_CartItem__c(
															ccrz__PricingType__c='auto', 
															ccrz__cartItemType__c='Minor', 
															ccrz__Quantity__c=1, 
															ccrz__Price__c=100, 
															ccrz__ParentCartItem__c=ciId1, 
															Id=ciId5
														);

		String ABSOLUTE_DISCOUNTTYPE = 'Absolute';
		String PERCENTAGE_DISCOUNTTYPE = 'Percentage';
		String PRICE_DISCOUNTTYPE = 'Price';

		Map<String,Object> couponMap = new Map<String, Object>();
		couponMap.put('discountType', PRICE_DISCOUNTTYPE);
		couponMap.put('discountAmount', 100);
		try {
			ccCustomLogicCartPrice.evaluateTargetedMinorItem(new List<ccrz__E_CartItem__c>{ci5}, couponMap);
		} catch (Exception e) {
			//nothing
		}

	}

	@isTest static void test_method_8() {
		ccCustomLogicCartPrice.testing();

	}

}