global class ccCustomServiceCoupon extends ccrz.ccServiceCoupon  { 
	global virtual override Map<String, Object> getFieldsMap(Map<String,Object> inputData) { 
		Map<String, Object> outputData = super.getFieldsMap(inputData); 

		String objectFields = (String)outputData.get(ccrz.ccService.OBJECTFIELDS); 

		//put our custom field here
		objectFields += ',Target_Primary_Attribute__c, Target_Primary_Attribute__r.Id'; 

		return new Map<String,Object>{ccrz.ccService.OBJECTFIELDS => objectFields}; 
	}
}