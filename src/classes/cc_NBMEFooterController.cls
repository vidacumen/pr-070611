global with sharing class cc_NBMEFooterController {
	public String copyrightText {get; set;}
	public String nbmeLink {get; set;}
	public String nbmeNewsLink {get; set;}
	public String contactUsLink {get; set;}
	public String privacyPolicyLink {get; set;}
	public String termsOfUseLink {get; set;}
	public String nbmeLabel {get; set;}
	public String nbmeNewsLabel {get; set;}
	public String contactUsLabel {get; set;}
	public String privacyPolicyLabel {get; set;}
	public String termsOfUseLabel {get; set;}
	public Account personAccountInfo {get; set;}

	global cc_NBMEFooterController() {
		copyrightText = CloudCraze_Footer__c.getValues('CopyrightText').Value__c;
		nbmeLink = CloudCraze_Footer__c.getValues('NBME®').Value__c;
		nbmeNewsLink = CloudCraze_Footer__c.getValues('NBME News & Events').Value__c;
		contactUsLink = CloudCraze_Footer__c.getValues('Contact Us').Value__c;
		privacyPolicyLink = CloudCraze_Footer__c.getValues('Privacy Policy').Value__c;
		termsOfUseLink = CloudCraze_Footer__c.getValues('Terms of Use').Value__c;

		nbmeLabel = CloudCraze_Footer__c.getValues('NBME®').Name;
		nbmeNewsLabel = CloudCraze_Footer__c.getValues('NBME News & Events').Name;
		contactUsLabel = CloudCraze_Footer__c.getValues('Contact Us').Name;
		privacyPolicyLabel = CloudCraze_Footer__c.getValues('Privacy Policy').Name;
		termsOfUseLabel = CloudCraze_Footer__c.getValues('Terms of Use').Name;

		String uId = UserInfo.getUserId();
		User u = [SELECT ContactId 
					FROM User 
					WHERE Id =: uId];

		personAccountInfo = [SELECT Id 
							FROM Account 
							WHERE PersonContactId = :u.ContactId LIMIT 1];
	}
}