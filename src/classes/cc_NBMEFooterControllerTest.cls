/**
 * Created by aszampias on 8/20/18.
 * Modified by btenis on 8/21/18.
 */
@isTest
public with sharing class cc_NBMEFooterControllerTest {
	@isTest 
	static void cc_NBMEFooterController_callConstructor_footerLabelsAndLinksSet() {
		List<CloudCraze_Footer__c> ccFooterVals = new List<CloudCraze_Footer__c>();
		ccFooterVals.add(new CloudCraze_Footer__c(Name = 'Contact Us', Value__c = '/examinees/s/contactsupport'));
		ccFooterVals.add(new CloudCraze_Footer__c(Name = 'CopyrightText', Value__c = 'Copyright© 2018 National Board of Medical Examiners®, All Rights Reserved | NBME is a registered trademark of National Board of Medical Examiners.'));
		ccFooterVals.add(new CloudCraze_Footer__c(Name = 'NBME News & Events', Value__c = 'http://www.nbme.org/newsroom/index.html'));
		ccFooterVals.add(new CloudCraze_Footer__c(Name = 'NBME®', Value__c = 'https://www.nbme.org/'));
		ccFooterVals.add(new CloudCraze_Footer__c(Name = 'Privacy Policy', Value__c = 'http://www.nbme.org/about/privacy.html'));
		ccFooterVals.add(new CloudCraze_Footer__c(Name = 'Terms of Use', Value__c = 'http://www.google.com'));

		insert ccFooterVals;

		RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Examinee'];

		Account a = new Account(LastName = 'Testing',
								PersonEmail = 'testing@email.com',
								PersonBirthdate = Date.newInstance(1990, 1, 1),
								RecordType = personAccountRecordType);
		insert a;

		Account userAccount = [SELECT PersonContactId FROM Account WHERE Id =: a.Id];
		Profile p = [SELECT Id FROM Profile WHERE Name = 'B2C Customer Login' LIMIT 1];

		//User u = new User(LastName = 'Tester',
		//				Email = 'tester123@email.com',
		//				Birthdate__c = Date.newInstance(1989, 1, 1),
		//				Username = 'tester123@email.com',
		//				Alias = 'Tester',
		//				TimeZoneSidKey = 'America/New_York',
		//				LocaleSidKey = 'en_US',
		//				EmailEncodingKey = 'UTF-8',
		//				LanguageLocaleKey = 'en_US',
		//				CommunityNickname = 'Tester',
		//				ProfileId = p.Id,
		//				ContactId = userAccount.PersonContactId);
        //
		//insert u;
        //
		//System.runAs(u) {
		//	Test.startTest();
		//		cc_NBMEFooterController fc = new cc_NBMEFooterController();
		//	Test.stopTest();
        //
		//	System.assertEquals(fc.contactUsLabel, ccFooterVals[0].Name);
		//	System.assertEquals(fc.nbmeNewsLabel, ccFooterVals[2].Name);
		//	System.assertEquals(fc.nbmeLabel, ccFooterVals[3].Name);
		//	System.assertEquals(fc.privacyPolicyLabel, ccFooterVals[4].Name);
		//	System.assertEquals(fc.termsOfUseLabel, ccFooterVals[5].Name);
        //
		//	System.assertEquals(fc.contactUsLink, ccFooterVals[0].Value__c);
		//	System.assertEquals(fc.copyrightText, ccFooterVals[1].Value__c);
		//	System.assertEquals(fc.nbmeNewsLink, ccFooterVals[2].Value__c);
		//	System.assertEquals(fc.nbmeLink, ccFooterVals[3].Value__c);
		//	System.assertEquals(fc.privacyPolicyLink, ccFooterVals[4].Value__c);
		//	System.assertEquals(fc.termsOfUseLink, ccFooterVals[5].Value__c);
		//}
	}
}