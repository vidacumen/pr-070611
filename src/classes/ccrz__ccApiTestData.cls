/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ccApiTestData {
    global static String APPLICATION_SETTINGS;
    global static String BASE_PRICELISTS;
    global static String CONFIG_SETTINGS;
    global static String CONFIGURATIONS;
    global static String HOOK_SETTINGS;
    global static String LOGIC_KEYS;
    global static String LOGIC_SETTINGS;
    global static String SERVICE_KEYS;
    global static String SERVICE_SETTINGS;
    global static String STOREFRONT_SETTINGS;
    global ccApiTestData() {

    }
    global static Map<String,Object> setupData(Map<String,Map<String,Object>> inpData) {
        return null;
    }
    global static Map<String,Object> setupData(String inpJSON) {
        return null;
    }
}
