public class superController {
@AuraEnabled
    public static user getUser(){
        return [SELECT Id, AccountID, Account_ID_Text__c FROM User WHERE Id = :UserInfo.getUserId()];
    }
}