<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NSAS Form</label>
    <protected>false</protected>
    <values>
        <field>Copy_Registrations_in_Cart__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Fee_Agreement_Text__c</field>
        <value xsi:type="xsd:string">Fees for Self-Assessments are nonrefundable. Credit card payments, if approved, will be processed when you place your order. You must start the assessment within 90 days (access period) of your order. If you do not complete the assessment within the access period, or complete it within the access period and wish to take the assessment in the future, you must submit a new order and pay a separate fee.</value>
    </values>
    <values>
        <field>Form__c</field>
        <value xsi:type="xsd:string">NSAS</value>
    </values>
    <values>
        <field>Privacy_Agreement_Text__c</field>
        <value xsi:type="xsd:string">I have read, understand and agree to NBME&apos;s Privacy Policy.</value>
    </values>
    <values>
        <field>Privacy_Policy_Link__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Require_EULA_Agreement__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Require_Fee_Agreement__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Require_Privacy_Agreement__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
