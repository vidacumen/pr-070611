<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Privacy Policy Link</label>
    <protected>false</protected>
    <values>
        <field>Copy_Registrations_in_Cart__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Fee_Agreement_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Form__c</field>
        <value xsi:type="xsd:string">All Privacy Policy</value>
    </values>
    <values>
        <field>Privacy_Agreement_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Privacy_Policy_Link__c</field>
        <value xsi:type="xsd:string">http://www.nbme.org/about/privacy.html</value>
    </values>
    <values>
        <field>Require_EULA_Agreement__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Require_Fee_Agreement__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Require_Privacy_Agreement__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
