<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NSAS3</label>
    <protected>false</protected>
    <values>
        <field>External_Registration_Status_Name__c</field>
        <value xsi:type="xsd:string">Assessment Complete</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Registration_Record_Type__c</field>
        <value xsi:type="xsd:string">NSAS Registration</value>
    </values>
</CustomMetadata>
