<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- When checked, Cloud Customer 360 will generate a Duplicate Warning if an Account matches with an existing Lead --></help>
        <label><!-- Account Duplicate Warning if Lead Exists --></label>
        <name>DSE__DS_Account_Duplicate_if_Lead_exists__c</name>
    </fields>
    <fields>
        <help><!-- When Cloud Customer 360 is running in automatic mode, it will launch a job when changes are made to the database that require it to be reprocessed. This setting controls the number of minutes that Cloud Customer 360 waits before launching the jobs. --></help>
        <label><!-- Automatic Mode Job Delay --></label>
        <name>DSE__DS_Automatic_Mode_Job_Delay__c</name>
    </fields>
    <fields>
        <label><!-- Concurrent Jobs Limit --></label>
        <name>DSE__DS_Concurrent_Jobs_Limit__c</name>
    </fields>
    <fields>
        <help><!-- If enabled, Cloud Customer 360 will run self-tests when all other Apex tests are run. --></help>
        <label><!-- Enable Cloud Customer 360 Self-Tests --></label>
        <name>DSE__DS_Enable_Data_Scout_Tests__c</name>
    </fields>
    <fields>
        <help><!-- When checked, Cloud Customer 360 automatically tries to extract the Domain (e.g. &apos;data-scout.com&apos;) from the Website or Email field --></help>
        <label><!-- Extract Domain --></label>
        <name>DSE__DS_Extract_Domain__c</name>
    </fields>
    <fields>
        <label><!-- Extract Legal Form --></label>
        <name>DSE__DS_Extract_Legal_Form__c</name>
    </fields>
    <fields>
        <help><!-- Asynchronous Hierarchy Batch chunk size limit --></help>
        <label><!-- Hierarchy Batch Size Limit --></label>
        <name>DSE__DS_Hierarchy_Batch_Size_Limit__c</name>
    </fields>
    <fields>
        <help><!-- To be used if Master Bean deletion returns a Hierarchy Scout error. Create a new external ID field on the Master Bean object, and populate its API name here. --></help>
        <label><!-- Hierarchy Scout Index Name --></label>
        <name>DSE__DS_Hierarchy_Scout_Index_Name__c</name>
    </fields>
    <fields>
        <label><!-- Hierarchy Scout Node Page Size --></label>
        <name>DSE__DS_Hierarchy_Scout_Node_Page_Size__c</name>
    </fields>
    <fields>
        <help><!-- If enabled, Cloud Customer 360 will not use a record&apos;s Country ISO Code when generating its Segment Key --></help>
        <label><!-- Ignore ISO Code --></label>
        <name>DSE__DS_Ignore_ISO_Code__c</name>
    </fields>
    <fields>
        <help><!-- When checked, Cloud Customer 360 will generate a Duplicate Warning if a Lead matches with an existing Account --></help>
        <label><!-- Lead Duplicate Warning if Account Exists --></label>
        <name>DSE__DS_Lead_Duplicate_if_Account_exists__c</name>
    </fields>
    <fields>
        <label><!-- Override Account --></label>
        <name>DSE__DS_MB_Override__c</name>
    </fields>
    <fields>
        <label><!-- Maintenance Window Active --></label>
        <name>DSE__DS_Maintenance_Window_Active__c</name>
    </fields>
    <fields>
        <help><!-- Maintenance window allows Cloud Customer 360 to update. Once the window ends, all previous job schedules will be restored, and previously running jobs will resume. Cloud Customer 360 jobs will be available for scheduling and execution. --></help>
        <label><!-- Maintenance Window End Time --></label>
        <name>DSE__DS_Maintenance_Window_End_Time__c</name>
    </fields>
    <fields>
        <help><!-- Start time for a maintenance window allowing Cloud Customer 360 to update. When the maintenance window starts, all active Cloud Customer 360 jobs will  be aborted and descheduled, and you will be unable to run or schedule any Cloud Customer 360 jobs. --></help>
        <label><!-- Maintenance Window Start Time --></label>
        <name>DSE__DS_Maintenance_Window_Start_Time__c</name>
    </fields>
    <fields>
        <label><!-- Match Using Record Type Rules --></label>
        <name>DSE__DS_Match_Using_Record_Type_Rules__c</name>
    </fields>
    <fields>
        <help><!-- Maximum nodes in hierarchy supported by Cloud Customer 360 --></help>
        <label><!-- Maximum Hierarchy Limit --></label>
        <name>DSE__DS_Max_Hierarchy_Limit__c</name>
    </fields>
    <fields>
        <help><!-- Maximum hierarchy nodes supported at Cloud Customer 360 Triggers level --></help>
        <label><!-- Maximum Trigger Hierarchy Limit --></label>
        <name>DSE__DS_Max_Hierarchy_TriggerLimit__c</name>
    </fields>
    <fields>
        <help><!-- The number of Job Logs that Cloud Customer 360 can store for each job type. --></help>
        <label><!-- Maximum Job Logs --></label>
        <name>DSE__DS_Maximum_Job_Logs__c</name>
    </fields>
    <fields>
        <label><!-- Running Mode --></label>
        <name>DSE__DS_Running_Mode__c</name>
    </fields>
    <fields>
        <help><!-- When checked, detailed statistics on each Batch Job run by Cloud Customer 360 will be saved against their Job Log. Slightly increases the storage space used by Cloud Customer 360. --></help>
        <label><!-- Save Job Statistics --></label>
        <name>DSE__DS_Save_Job_Statistics__c</name>
    </fields>
    <fields>
        <help><!-- Max. number of records displayed as search results --></help>
        <label><!-- Search Results Limit --></label>
        <name>DSE__DS_Search_Results_Limit__c</name>
    </fields>
    <fields>
        <help><!-- The length of the optional indexed segment key generated in LDV Org. May need to be longer for very large data volumes. --></help>
        <label><!-- Segment Key Index Length --></label>
        <name>DSE__DS_Segment_Key_Index_Length__c</name>
    </fields>
    <fields>
        <help><!-- To be used if non-selective query errors are encountered. Create a new external ID field on the Master Bean and Bean objects, and populate its API name here. --></help>
        <label><!-- Segment Key Index Name --></label>
        <name>DSE__DS_Segment_Key_Index_Name__c</name>
    </fields>
    <fields>
        <label><!-- Setup Completed --></label>
        <name>DSE__DS_Setup_Completed__c</name>
    </fields>
    <fields>
        <help><!-- Enable this to reduce consumption of system resources when it is not necessary to know the size of batch jobs. --></help>
        <label><!-- Skip Batch Job Size Check --></label>
        <name>DSE__DS_Skip_Batch_Job_Size_Check__c</name>
    </fields>
    <fields>
        <label><!-- Standardize Country --></label>
        <name>DSE__DS_Standardize_Billing_Country__c</name>
    </fields>
    <fields>
        <help><!-- Tick this checkbox to use Cloud Customer 360&apos;s default configuration and ignore the current settings. --></help>
        <label><!-- Use Default Settings --></label>
        <name>DSE__DS_Use_Default_Settings__c</name>
    </fields>
    <fields>
        <label><!-- Validate Country --></label>
        <name>DSE__DS_Validate_Billing_Country__c</name>
    </fields>
</CustomObjectTranslation>
