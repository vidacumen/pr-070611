<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Cloud Customer 360: Optimization parameters for CC360 Jobs</description>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>DSE__DS_Batch_Acceleration_Level__c</fullName>
        <defaultValue>3</defaultValue>
        <deprecated>false</deprecated>
        <description>This value determines the acceleration level of the specific Batch job. The higher the value, the faster the Fuzzy logic. Higher acceleration levels might reduce the quality of the Fuzzy logic. As a rule of thumb, use an Acceleration Level that equals the digits of the number of Accounts in your system (e.g. 1000 Accounts -&gt; Level 4, 100.000 Accounts -&gt; Level 6)</description>
        <externalId>false</externalId>
        <inlineHelpText>This value determines the acceleration level of the specific Batch job. As a rule of thumb, use an Acceleration Level that equals the digits of the number of Accounts in your system (e.g. 1000 Accounts -&gt; Level 4, 100.000 Accounts -&gt; Level 6)</inlineHelpText>
        <label>Batch Acceleration Level</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Batch_Size__c</fullName>
        <defaultValue>10</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Batch Size</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Concurrent_Jobs__c</fullName>
        <deprecated>false</deprecated>
        <description>Determines how many batch jobs Cloud Customer 360 will attempt to run in parallel when this job is run.</description>
        <externalId>false</externalId>
        <inlineHelpText>1 - 5. Speed up this job by setting this value above 1, allowing Cloud Customer 360 to process multiple sets of records in parallel.</inlineHelpText>
        <label>Concurrent Jobs</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Fuzzy_Limit__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fuzzy Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Job_Priority__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Determines the priority that a job is given when Cloud Customer 360 is running in automatic mode. 1 is highest, while setting this to 0 or null will always skip the job.</inlineHelpText>
        <label>Job Priority</label>
        <precision>1</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Limits_Threshold__c</fullName>
        <defaultValue>90</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Limits Threshold</label>
        <precision>2</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>DSE__DS_Loop__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>When checked, the correspnding Batch Job will be repeated until all Data has been processed or until an Error occurs</description>
        <externalId>false</externalId>
        <inlineHelpText>When checked, the correspnding Batch Job will be repeated until all Data has been processed or until an Error occurs</inlineHelpText>
        <label>Loop</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>DSE__DS_Query_Limit__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Query Limit</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Run_Automatic__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>When in automatic mode, a job will be scheduled to run whenever a record is added or modified in such a way that it requires processing by Cloud Customer 360</inlineHelpText>
        <label>Run Automatic</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>DSE__DS_Search_Acceleration_Level__c</fullName>
        <defaultValue>3</defaultValue>
        <deprecated>false</deprecated>
        <description>This value determines the acceleration level of a Fuzzy Search. The higher the value, the faster the Fuzzy logic. Higher acceleration levels might reduce the number of results. Increase the acceleration level if you want to achieve faster search results.</description>
        <externalId>false</externalId>
        <inlineHelpText>This value determines the acceleration level of a Fuzzy Search. The higher the value, the faster the Fuzzy logic. Higher acceleration levels might reduce the number of results. Increase the acceleration level if you want to achieve faster search results.</inlineHelpText>
        <label>Search Acceleration Level</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE__DS_Segment_Limit__c</fullName>
        <defaultValue>20</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Segment Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Performance Settings</label>
    <visibility>Public</visibility>
</CustomObject>
