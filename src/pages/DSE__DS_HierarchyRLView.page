<apex:page standardController="Account" readOnly="true" sidebar="false" extensions="DSE.DS_HierarchyRLViewComponentController" action="{!checkLicense}">
    <!-- Load dataTables.css before AccountHierarchyView.css to override Data Table Styles and maintain Salesforce styles-->
	<apex:stylesheet value="{!URLFOR($Resource.DSE__jQueryDataTablesZip, 'jQueryDataTables/css/jquery.dataTables.css')}"/>
	<apex:stylesheet value="{!URLFOR($Resource.DSE__HierarchyIconsZip, 'CSS/AccountHierarchy.css')}" />
	<apex:stylesheet value="{!URLFOR($Resource.DSE__jquery_min, 'jquery_min/jquery_1.11.1_min/jquery-ui.css')}" />
	<apex:includeScript value="{!URLFOR($Resource.DSE__DS_Hierarchy_Resources, 'jquery.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.DSE__jquery_min, 'jquery_min/jquery_1.11.1_min/jquery-ui.min.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.DSE__DS_Hierarchy_Resources, 'jquery.jstree.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.DSE__jQueryDataTablesZip, 'jQueryDataTables/js/jquery.dataTables.js')}"/>
	<!-- Required to block div during loading  -->
	<apex:includeScript value="{!URLFOR($Resource.DSE__DS_Hierarchy_Resources, 'jquery.blockUI.js')}" />
    
    
    <script type="text/javascript">
    	var dseJQ = jQuery.noConflict();
        function setFocusOnLoad() {}
        
        function blockDiv(divId) {
            console.log("blockDiv - " + divId);
            dseJQ(divId).block({ 
                message: '<br/><apex:image value="/img/loading.gif"/> {!$Label.dse__ds_message_loadingresults}', 
                css: { height: '50px', width: '20%' },
                overlayCSS: { opacity: .2 }
            });
        }
        
        function unblockDiv(divId) {
            console.log("unblockDiv - " + divId);
            dseJQ(divId).unblock();
        }
    </script>
    
    <style>
		/* Expand-Collapse Sidebar-Content CSS */ 
		#sidebar {
		    border-color: #BCD;
		    display: none;
		}
		#content {
		    border-color: #BCD;
		    width: 99%;
		}
		 
		.use-sidebar #content {
			width: 74%;
		}
		
		.use-sidebar #sidebar {
		    display: block;
		    width: 25%;
		}
		 
		.sidebar-at-left #content {
			float: right;
		}
		
		.use-sidebar.sidebar-at-left #sidebar {
			float: left;
		}
		 
		#separator {
		    display: block;
		    outline: none;
		    width: 0.5%;
		    float: right;
		}
		
		#separator:hover {
		    background: #DEF;
		}
		
		/* Search Overlay CSS */
		.searchOverlayBox {
            position: absolute;
            display: none;
            width: 800px;
            background-color: white;
            border-radius: 4px;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			border: 3px solid #015ba7;
			padding-right: 8px;
        }
        
        /* Search Box CSS */
        .searchBox {
        	position: relative;
			padding: 4px 5px;
			border: 1px solid rgb(169, 169, 169);
			height: 17px;
			font-weight: bold;
        }
		
    </style>
    
    
    
    
    
    <div class="use-sidebar sidebar-at-left" id="main">
    	<div id="hierarchyOneView">
    	<apex:sectionHeader title="Customer 360" />
    	</div>
        <div id="sidebar">
            <div style="padding-left: 10px; height: 40px;">
            	<div class="searchBox" style="overflow:auto;">
            		<apex:image value="{!$Resource.DSE__Search}" height="16px" width="16px" style="vertical-align: middle; padding-left: 8px;"/>
            		<apex:outputLink style="text-decoration: none; vertical-align: middle; padding-left: 10px; font-size: 105%;" 
            							onclick="doOverlayOpen('searchOverlayBox'); return false;">Click here to search within hierarchy</apex:outputLink>
           		</div>
           	</div>
            <div style="overflow:auto;">
            	<c:DS_AccountHierarchyView accountId="{!$CurrentPage.parameters.id}" hierarchyTypeV1="{!$CurrentPage.parameters.hierarchyType}" relatedListMode="true" id="jsTree" rendered="{!isLicenseActive}"/> 
           	</div>
        </div>
        <div id="content" style="overflow:auto;">
            <c:DS_AcctHierarchyRelatedListView id="rlDetails" rendered="{!isLicenseActive}"/>
        </div>
        <a href="#" id="separator"></a>
        <div class="clearer">&nbsp;</div>
    </div>

	
	<!-- overlay box -->
    <div class="searchOverlayBox" style="overflow:auto;">
    	<c:DS_HierarchySearch relatedListMode="true" rendered="{!isLicenseActive}"/>
    </div>
    
    <script>
    	dseJQ(document).ready(function(){
		    // Variables
		    var objMain = dseJQ('#main');
		 	
		 	objMain.css('height', dseJQ(window).height() + 'px');
		 	
		    // Show sidebar
		    function showSidebar(){
		        objMain.addClass('use-sidebar');
		    }
		 
		    // Hide sidebar
		    function hideSidebar(){
		        objMain.removeClass('use-sidebar');
		    }
		 
		    // Sidebar separator
		    var objSeparator = dseJQ('#separator');
		 
		    objSeparator.click(function(e){
		    	var sidebarWidth = 	 dseJQ('#sidebar').width();	    	
		    	var contentWidth = 	 dseJQ('#content').width();
		    	
		        e.preventDefault();
		        if ( objMain.hasClass('use-sidebar') ){
		            hideSidebar();
		            dseJQ('#content').css('width', contentWidth + sidebarWidth + 'px');
		        }
		        else {
		            showSidebar();
		            dseJQ('#content').css('width', contentWidth - dseJQ('#sidebar').width() + 'px');		            
		        }
		    }).css('height', objSeparator.parent().outerHeight() + 'px');
		    
		    
		    dseJQ('#sidebar').resizable({
		     	minWidth: dseJQ('#main').width()/7,
    			maxWidth: dseJQ('#main').width()/7 * 4 ,
		    	handles : "e",
		    	resize: function (e, ui) {
	            var x = e.pageX;	            
		            var minW = dseJQ('#main').width()/7;
		            var maxW =  dseJQ('#main').width()/7 * 4;
		            if(x >= minW && x <= maxW) //check for min and max width range
		            {      
	            dseJQ(this).width(x);
	            	dseJQ('#content').width(dseJQ('#main').width() - x - 11);//some space for separator
		        	}
		        	else if(x < minW) //if slided more reset to minwidth
		        	{
		        		dseJQ(this).width(minW);
		            	dseJQ('#content').width(dseJQ('#main').width() - (minW) - 11);//some space for separator		            	
		        	}
		        	else // if slided more set to maxwidth 
		        	{
		        		dseJQ(this).width(maxW);
		            	dseJQ('#content').width(dseJQ('#main').width() - (maxW) - 11);//some space for separator		            	
		        	}
	        	}
	        	
		    });
		    
		    dseJQ(window).resize(function() {
		    		    	
		    	 if ( objMain.hasClass('use-sidebar') ) {
		    	  
		    		dseJQ('#sidebar').css('width', '25%');
		    		dseJQ('#content').css('width', '74%');
		    		
		    		dseJQ('#sidebar').resizable({
		    	
		     			minWidth: dseJQ('#main').width()/7,
    					maxWidth: dseJQ('#main').width()/7 * 4 ,
		    			handles : "e"
		    		});
		    	}
		    	else
		    	{
		    		dseJQ('#content').css('width', '99%');
	        	}
		    });
		});
		
		
		function doOverlayOpen(overlayName) {
            // set the properties of the overlay box, the left and top positions
            dseJQ('.' + overlayName).css({
                left: (dseJQ(window).width() - dseJQ('.' + overlayName).width()) / 2,
                top: (dseJQ(window).height()) / 10
            });
			
			dseJQ('.' + overlayName).show();
			
			if(overlayName == 'filterOverlayBox') {
				dseJQ('[id$=rlDivMain]').css('opacity', '0.1');
				dseJQ('[id$=sidebar]').css('opacity', '0.1');
				dseJQ('[id$=separator]').css('opacity', '0.1');
				dseJQ('[id$=hierarchyOneView]').css('opacity', '0.1');
			} else {
				dseJQ('[id$=main]').css('opacity', '0.1');
			}
			
            return false;
        }

        function doOverlayClose(overlayName) {
        	dseJQ('.' + overlayName).hide();
        	
        	if(overlayName == 'filterOverlayBox') {
				dseJQ('[id$=rlDivMain]').css('opacity', '');
				dseJQ('[id$=sidebar]').css('opacity', '');
				dseJQ('[id$=separator]').css('opacity', '');
				dseJQ('[id$=hierarchyOneView]').css('opacity', '');
			} else {
        		dseJQ('[id$=main]').css('opacity', '');
            }
            
            return false;
        }
		
    </script>
    
    <!-- Remote Objects definition to set accessible sObjects and fields -->
    <apex:remoteObjects >
        <apex:remoteObjectModel name="DSE__DS_Bean__c" jsShorthand="DS_Bean" fields="Name,Id">
            <apex:remoteObjectField name="DSE__DS_Account__c" jsShorthand="DS_Account"/>
        </apex:remoteObjectModel>
    </apex:remoteObjects>
	
	<!-- JavaScript to make Remote Objects calls -->
    <script>
    	function getAccountIdFromBean(passedBeanId){
        	// Create a new Remote Object
            var beanObj = new SObjectModel.DS_Bean();
            
            var beanId;
            var accountId;
            var clickedObjId;
            
            // Use the Remote Object to query for 1 bean record based on the passed beanId
            beanObj.retrieve({ where: {Id: {eq: passedBeanId } } }, function(err, records, event){
                if(err) {
                    console.log('Remote Object Error : ' + err.message);
                } else {
                    records.forEach(function(record) {
                        if(null == accountId) {
                        	beanId = record.get("Id").substring(0, 15);
                        	accountId = record.get("DS_Account").substring(0, 15);
                        }
                    });
                    
                    if(null == accountId)
                    	goToSelectedNode_RelatedListMode(beanId, beanId);
                   	else
                    	goToSelectedNode_RelatedListMode(accountId, beanId);
                }
            });
		}	
    </script>
    
</apex:page>