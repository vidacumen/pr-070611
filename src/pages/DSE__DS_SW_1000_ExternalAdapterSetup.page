<apex:page action="{!loadSettings}" controller="DSE.DS_SetupWizardController" extensions="DSE.DS_ExternalAdapterSetupEx" standardStylesheets="true" sidebar="true" showheader="true" tabStyle="Account">

	<apex:includeScript value="{!URLFOR($Resource.DSE__DS_Hierarchy_Resources, 'jquery.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.DSE__DS_Hierarchy_Resources, 'jquery.blockUI.js')}" />
	
	<apex:actionStatus id="showSystemUpdationStatus" onstart="blockDiv('#setupWizardDiv');" onstop="unblockDiv('#setupWizardDiv');" />
	
	<!--  ********** CSS Stuff ************** -->
	<style>
		.ProgressBarDone {display:inline-block;padding-left:0px;padding-right:0px;margin-top:5px;margin-bottom:10px;text-align:center;width:66px;height:45px;background-image:url('{!$Resource.bgDone}');background-repeat:repeat-x;background-position: 0px 15px ;} 
		.ProgressBarNotDone {display:inline-block;padding-left:0px;padding-right:0px;margin-top:5px;margin-bottom:10px;text-align:center;width:66px;height:45px;background-image:url('{!$Resource.bgNotDone}');background-repeat:repeat-x;background-position: 0px 15px ;} 
		
		.menuDone{display:block;margin-top:5px;margin-left:20px;width:30px;height:30px;border-radius:15px;font-size:20px;color:rgb(255,255,255);line-height:30px;text-align:center;text-decoration:none;background:rgb(173,200,238)}
		.menuCurrent{display:block;margin-top:5px;margin-left:20px;width:30px;height:30px;border-radius:15px;font-size:20px;color:rgb(255,255,255);line-height:30px;text-align:center;text-decoration:none;background:rgb(133,160,198);}
		.menuNotDone{display:block;margin-left:20px;margin-top:5px;width:30px;height:30px;border-radius:15px;font-size:20px;color:rgb(90,90,90);line-height:30px;text-align:center;text-decoration:none;background:rgb(210,210,210)}
		
		.menuDone:hover{color:#ccc;text-align:center;text-decoration:none;background:rgb(105,155,224)}
		.menuNotDone:hover{color:#ccc;text-decoration:none;background:rgb(105,155,224)}
	</style>
	
	<script type="text/javascript">
    	var dseJQ = jQuery.noConflict();
        
        function inputCheck(e,allow) {
           	var AllowableCharacters = '';

            if (allow == 'Letters'){AllowableCharacters=' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';}
            if (allow == 'Numbers'){AllowableCharacters='1234567890';}
            if (allow == 'NameCharacters'){AllowableCharacters=' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';}
            if (allow == 'NameCharactersAndNumbers'){AllowableCharacters='1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'';}
            if (allow == 'Currency'){AllowableCharacters='1234567890.';}

            var k = document.all?parseInt(e.keyCode): parseInt(e.which);
            //codes for backspace, delete, enter
            if (k!=0 && k!=8 && k!=13){
                if ((e.ctrlKey==false) && (e.altKey==false)) {
                return (AllowableCharacters.indexOf(String.fromCharCode(k))!=-1);
                } else {
                return true;
                }
            } else {
                return true;
            }
        }
        
        function js_AddParameter(sectionHeader) {
        	var optionValue = dseJQ("select[class*='" + sectionHeader + "']").val();
        	add_Parameter(sectionHeader, optionValue);
        }
        
        function blockDiv(divId) {
            dseJQ(divId).block({
                message: '<br/><apex:image value="/img/loading.gif"/> {!$Label.dse__ds_message_loadingresults}',
                css: {
                    height: '50px',
                    width: '20%'
                },
                overlayCSS: {
                    opacity: .2
                }
            });
        }

        function unblockDiv(divId) {
            dseJQ(divId).unblock();
        }
    </script>
	
	
	
	<apex:form >
	<div id="setupWizardDiv">
		<!--**************************************** -->
		<!--************* Progress Bar ************* -->
		<!--**************************************** --> 
		<div style='font-size:0px;'> 
			<apex:variable value="{!1}" var="VFCounter"/>
			
			<apex:repeat value="{!ConnectorPageList}" var="PN">
				<span class="{!if(VFCounter<=	CurrentPosn,'ProgressBarDone','ProgressBarNotDone')}" >
					<apex:outputLink styleclass="{!if(VFCounter<	CurrentPosn,'menuDone','menuNotDone')}" title="{!PN.LabelName}" rendered="{!PN.CurrentPage==false}" value="/apex/{!PN.URL}">
						{!PN.Sequence}
					</apex:outputLink>
					<apex:outputtext styleclass="menuCurrent" value="{!PN.Sequence}" title="{!PN.LabelName}" rendered="{!PN.CurrentPage==true}"/>
				</span>
			    <apex:variable var="VFCounter" value="{!VFCounter + 1}"/>
			</apex:repeat>
		</div>
		
		
		<!--******************************************* -->
		<!--  ************* Title Of Page ************* -->
		<!--******************************************* -->
		<div class="bWizardBlock tertiaryPalette">
		    <div class="pbWizardTitle tertiaryPalette brandTertiaryBgr" style='background-color:#8E9DBE;'>
		        <!-- <div class="ptRightTitle">{!adapterSetting.DS_Adapter_Number__c}</div> -->
		        <h2>Service : {!adapter.Name}</h2>
		    </div>
		</div>

		<!--  **************************************************************** -->
		<!--  *********************** Buttons Here    ************************ -->
		<!--  **************************************************************** -->
		<apex:pageblock mode="maindetail"  >  
			<apex:outputpanel style="float:right;" id="wizbun" >
				
				
				<apex:commandButton value="{!$Label.dse__ds_wizard_previous}" immediate="true" rendered="{!NOT(ISNULL($CurrentPage.parameters.Ret))}" disabled="{!NOT(ShowPrev)}" action="{!prevConnector}"/>
				<apex:commandButton value="{!$Label.dse__ds_wizard_save}" action="{!saveConnectorParams}" rerender="Topper" status="showSystemUpdationStatus"/>
				<apex:commandButton value="{!$Label.dse__ds_wizard_next}" immediate="true" rendered="{!NOT(ISNULL($CurrentPage.parameters.Ret))}" disabled="{!NOT(ShowNext)}" action="{!nextConnector}"/>
				<apex:commandButton value="{!$Label.dse__ds_wizard_cancelwizard}" action="{!s_ReturnToWizardHome}" immediate="true"/>  
		
			</apex:outputpanel>	
		</apex:pageblock>
		
		<!--**************************************** -->
		<!--************* Main Wizard  ************* -->
		<!--**************************************** --> 
	
		<apex:pageblock id="Topper">
			<apex:pageMessages id="pageMessageBit" />
		
			<apex:pageblocksection columns="2" title="Service Information">
				<apex:outputfield value="{!adapter.Name}" />
				<apex:inputfield value="{!adapter.DSE__DS_Enabled__c}"/>
				<apex:outputfield value="{!adapter.DSE__DS_Class_Name__c}" />
				<apex:pageblocksectionitem >
					<apex:outputpanel layout="none"/>
					<apex:outputPanel layout="none">
						<apex:commandbutton id="testConnectionBtn" value="Test Connection" action="{!testConnection}" rerender="pageMessageBit" status="showSystemUpdationStatus" disabled="{!NOT(TestConnectionEnabled)}"/>
						&nbsp;
						<apex:image value="/img/alohaSkin/help_orange.png" title="Tests the connection between Salesforce and {!adapter.Name}" style="vertical-align: sub;"/>
					</apex:outputPanel>
				</apex:pageblocksectionitem>
			</apex:pageblocksection>
		
			<apex:pageblocksection title="{!adapter.Name} Configuration" columns="2">
				<!-- <apex:outputPanel> -->
					<apex:repeat value="{!sectionHeaders}" var="secHead">
						<apex:pageblocksection title="{!secHead}" columns="1">
							<apex:variable var="i" value="{!0}"/>
							<apex:pageblockTable value="{!adapterParams[secHead]}" var="adptParam">
								<apex:column headerValue="Parameter Name" value="{!adptParam.paramDefn.label}" width="47%"/>
								<apex:column headerValue="Parameter Value" width="50%">
									<apex:inputText value="{!adptParam.value}" rendered="{!adptParam.paramDefn.dataType == 'STRING'}" maxlength="240">
										<apex:actionSupport event="onchange" action="{!disableTestConnection}" rerender="testConnectionBtn" rendered="{!secHead == 'Authentication'}"/>
									</apex:inputText>
									<apex:inputSecret value="{!adptParam.value}" rendered="{!adptParam.paramDefn.dataType == 'ENCRYPTEDSTRING'}" redisplay="true" maxlength="240">
										<apex:actionSupport event="onchange" action="{!disableTestConnection}" rerender="testConnectionBtn" rendered="{!secHead == 'Authentication'}"/>
									</apex:inputSecret>
									<apex:inputText value="{!adptParam.value}" rendered="{!adptParam.paramDefn.dataType == 'INTEGER'}" maxlength="240" html-placeholder="Enter only numeric data" onkeypress="return inputCheck(event,'Numbers');"/>
									<apex:inputCheckbox value="{!adptParam.value}" rendered="{!adptParam.paramDefn.dataType == 'BOOLEAN'}"/>
									<apex:selectList value="{!adptParam.value}" rendered="{!adptParam.paramDefn.dataType == 'PICKLIST'}" size="1" multiselect="false">
										<apex:selectOptions value="{!adptParam.paramDefn.allowedOptions}"/>
									</apex:selectList>
									<apex:selectList value="{!adptParam.value}" rendered="{!adptParam.paramDefn.dataType == 'MULTIPICKLIST'}" multiselect="true">
										<apex:selectOptions value="{!adptParam.paramDefn.allowedOptions}"/>
									</apex:selectList>
									&nbsp;&nbsp;
									<apex:image value="/img/alohaSkin/help_orange.png" height="16px" width="16px" title="{!adptParam.paramDefn.helpText}" style="vertical-align: top;" rendered="NOT(ISNULL(adptParam.paramDefn.helpText))"/>
								</apex:column>
								<apex:column >
									<apex:commandlink action="{!removeParameter}" rerender="Topper" status="showSystemUpdationStatus" rendered="{!NOT(adptParam.paramDefn.isRequired)}">
										<apex:image value="{!URLFOR($Resource.DSE__dataquality,'/dataquality/images/cancel_16.png')}" height="16px" width="16px" title="Remove Parameter" style="vertical-align: sub;"/>
										<apex:param name="param_header" value="{!secHead}" assignTo="{!selectedHeaderValue}"/>
										<apex:param name="param_api" value="{!secHead}#{!adptParam.paramDefn.apiName}" assignTo="{!selectedOptionalParam}"/>
										<apex:param name="param_index" value="{!i}" assignTo="{!paramIndex}"/>
									</apex:commandlink>
									<apex:variable var="i" value="{!i+1}"/>
								</apex:column>
							</apex:pageblockTable>
							
							<!-- Commenting copy login link - CC-146 -->							
							<!-- <apex:commandlink action="{!copyLoginInfoFromEmail}" rerender="Topper" status="showSystemUpdationStatus" rendered="{!AND(secHead == sectionHeaderAuthentication, showCopyLoginLink)}">Copy Login Information from Email Verification</apex:commandlink> -->
							
							<apex:variable value="{!optionalParamOptions[secHead]}"  var="ndpOptions"/><!-- not displayed options -->
							
							<apex:pageblocksectionitem rendered="{!ndpOptions.size > 0}">
							
								<apex:outputlabel value="Optional Parameters"/>
								<apex:outputpanel >
									<apex:selectList value="{!optionalSelectedParam[secHead]}" multiselect="false" size="1" styleClass="{!secHead}"><!-- selectedHeaderValue -->
										<apex:selectOptions value="{!optionalParamOptions[secHead]}"/>
									</apex:selectList>
									&nbsp;&nbsp;&nbsp;
									<apex:commandlink onclick="js_AddParameter('{!secHead}'); return false;">
										<apex:image value="{!URLFOR($Resource.DSE__dataquality,'/dataquality/images/add_16.png')}" height="16px" width="16px" title="Add Parameter" style="vertical-align: sub;" />
									</apex:commandlink>
								</apex:outputpanel>
							</apex:pageblocksectionitem>
						</apex:pageblocksection>
					</apex:repeat>
				<!-- </apex:outputPanel> -->
			</apex:pageblocksection>
			
		</apex:pageblock>
		
		
		<apex:actionFunction name="add_Parameter" action="{!addParameter}" rerender="Topper" status="showSystemUpdationStatus">
			<apex:param name="param_header" value="" assignTo="{!selectedHeaderValue}"/>
			<apex:param name="param_api" value="" assignTo="{!selectedOptionalParam}"/>
		</apex:actionFunction>
	
	
	
	</div>	
	</apex:form>
    
</apex:page>