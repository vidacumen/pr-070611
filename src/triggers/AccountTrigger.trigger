/**
 * Created by btenis on 1/23/2018
 */

trigger AccountTrigger on Account (before insert) {
    AccountTriggerHandler acctTriggerHandler = new AccountTriggerHandler();

    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            acctTriggerHandler.handleBeforeInsert(Trigger.new);
        }
    }
}