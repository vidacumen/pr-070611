trigger CustomDocumentTrigger on CustomDocument__c (after insert, after update) {

	if (!CustomDocumentTriggerFacade.skipTrigger) {
		if(Trigger.isInsert && Trigger.isAfter){
			CustomDocumentTriggerFacade.onAfterInsert(Trigger.new);
		} else if (Trigger.isUpdate && Trigger.isAfter) {
			CustomDocumentTriggerFacade.onAfterUpdate(Trigger.new, Trigger.oldMap);
		}
	}

}