trigger FilterEmailToCase on Case (before insert) {
    FilterEmailToCaseTriggerHandler handler = new FilterEmailToCaseTriggerHandler();
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            handler.handleOOOReplies(Trigger.new);
        }
    }
}