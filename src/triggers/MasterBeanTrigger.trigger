trigger MasterBeanTrigger on DSE__DS_Master_Bean__c (before insert, before update) {

	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			MasterBeanTriggerFacade.onBeforeInsert(Trigger.new);
		} else if (Trigger.isUpdate) {
			MasterBeanTriggerFacade.onBeforeUpdate(Trigger.new);
		}
	}

}