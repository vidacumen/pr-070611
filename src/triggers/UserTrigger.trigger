trigger UserTrigger on User (after update, after insert, before insert) {
    if (!UserTriggerFacade.skipTrigger) {
        if (Trigger.isAfter) {
            if (Trigger.isUpdate) {
                UserTriggerFacade.onAfterUpdate(Trigger.new, Trigger.oldMap);
            } else if (Trigger.isInsert) {
                UserTriggerFacade.onAfterInsert(Trigger.new);
            }
        }

        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                UserTriggerFacade.onBeforeInsert(Trigger.new);
            }
        }
    }
}