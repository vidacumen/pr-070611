<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Opened_Auto_Response_Email</fullName>
        <description>Case Opened Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerfirstnoreply@nbme.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automatic_Email_Templates/Case_Opened_Auto_Response_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Resolution_Auto_Response_Email</fullName>
        <description>Case Resolution Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerfirstnoreply@nbme.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automatic_Email_Templates/Case_Resolution_Auto_Response_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_SLA_Exceeded_Email_Alert</fullName>
        <description>Case SLA Exceeded Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Automatic_Email_Templates/SLA_Email_Template_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Case_Owner_of_Call_Logged</fullName>
        <description>Notify Case Owner of Call Logged</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automatic_Email_Templates/Call_Logged_on_Your_Case</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Owner_Community_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Community_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner = Community Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Grant_Examine_Access_to_Case</fullName>
        <description>Checks the &apos;Grant Examinee Access&apos; flag on Cases created by Examinees.</description>
        <field>Grant_Examinee_Access__c</field>
        <literalValue>1</literalValue>
        <name>Grant Examine Access to Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_to_SLA_Exceeded</fullName>
        <field>SLA_Exceeded__c</field>
        <literalValue>1</literalValue>
        <name>Set Case to SLA Exceeded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case Opened Auto Response Email</fullName>
        <actions>
            <name>Case_Opened_Auto_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General Inquiry Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>The system will automatically email the Case Contact a case opened email, when the Case is &quot;New&quot; and the Record type = General Inquiry.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Origin %3D Community</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Owner_Community_Queue</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Workflow_Delay_Timer_5min__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Resolution Auto Response Email</fullName>
        <actions>
            <name>Case_Resolution_Auto_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Automatically_Send_Resolution_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>The system will automatically email the Case Contact a case resolution email, when the Case is &quot;Closed&quot; and &quot;Automatically Send Resolution Email&quot; is True.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Set to Escalated - Level 1</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>equals</operation>
            <value>Administration Prep,Create/Update Users,General Inquiry,Post Administration,Technical Issues,Test Content,Other</value>
        </criteriaItems>
        <description>The system will automatically flag a Case as &quot;Exceeded SLA&quot; when the SLA has been exceeded.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Case_to_SLA_Exceeded</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Set to Escalated - Level 2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>equals</operation>
            <value>Financial Transactions</value>
        </criteriaItems>
        <description>The system will automatically flag a Case as &quot;Exceeded SLA&quot; when the SLA has been exceeded.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Case_to_SLA_Exceeded</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Set to Escalated - Level 3</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>equals</operation>
            <value>Accommodations,Exam Security Issue</value>
        </criteriaItems>
        <description>The system will automatically flag a Case as &quot;Exceeded SLA&quot; when the SLA has been exceeded.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Case_to_SLA_Exceeded</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>92</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Grant Examine Access to Case</fullName>
        <actions>
            <name>Grant_Examine_Access_to_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Profile_Name__c</field>
            <operation>equals</operation>
            <value>B2C Customer Login</value>
        </criteriaItems>
        <description>Checks the &apos;Grant Examinee Access&apos; flag on Cases created by Examinees.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
